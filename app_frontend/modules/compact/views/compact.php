<main>

  <section>
    <div class="container">
      <div class="blog-title">
        <h2 class="text"><?=$this->lang->line('terms_of_use');?></h2>
      </div>
      <p><?php echo !empty($siteTitle) ? $siteTitle : "";?></p>
      <hr>
      <div id="accordion"> 
      <?php 
        if(!empty($compact)):  
          foreach($compact as  $key => $item):
      ?>
      <div class="row">
        <div class="col-md-12 col-12">
          <div class="card">
            <div class="card-header">
              <a class="collapsed card-link" data-toggle="collapse" href="#collapse-<?=$key?>">
                <i class="fas fa-question"></i> <?php echo !empty($item->title) ? $item->title : null?>
                <i class="fas fa-chevron-<?php echo $key==0 ? "up" : "down";?>" style="float: right;"></i>
              </a>
            </div>
            <div id="collapse-<?=$key?>" class="collapse <?php echo $key==0 ? "show" : NULL;?>" data-parent="#accordion">
              <div class="card-body">
                <p><?php echo !empty($item->excerpt) ? html_entity_decode($item->excerpt) : null?></p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php
          endforeach;
        endif;
      ?>
      </div> 
    </div>
  </section>

</main>