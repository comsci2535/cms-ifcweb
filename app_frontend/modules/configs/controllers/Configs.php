<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configs extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('config_m');
	} 

	public function get_setting($type)
	{  
        $info = $this->config_m->get_config($type);
        $temp = array();
        foreach ($info->result_array() as $rs):
            $temp[$rs['variable']] = $rs['value'];
        endforeach; 
        return $temp;
    }

}