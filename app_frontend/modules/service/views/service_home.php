<section>
  <div class="container">
    <div class="blog-title">
      <h2 class="text"><?=$this->lang->line('service');?></h2>
    </div>
    <div style="position: relative;">
      <ul id="slider_service">
        <?php if(!empty($services)): 
          foreach ($services as $key => $item):
          ?>
        <li>
          <div class="p-2">
            <div class="blog-default">
              <a href="<?=site_url('service/detail/'.(!empty($item->slug) ? $item->slug: null));?>">
                <img src="<?php echo !empty($item->file) ? base_url($item->file) : '';?>" class="wfull"
                  alt="<?php echo !empty($item->title) ? $item->title: null;?>"
                  onerror="this.src='<?=base_url('template/frontend/images/no-image.png');?>'">
                <div class="detail">
                  <div class="title">
                    <h5><?php echo !empty($item->title) ? $item->title: null;?></h5>
                  </div>
                  <div class="expert">
                    <p><?php echo !empty($item->excerpt) ? $item->excerpt: null;?> </p>
                  </div>
                </div>
              </a>
            </div>
          </div>
        </li>
        <?php 
          endforeach;
        endif; 
        ?>
      </ul>
      <?php 
        function svg_file($path) {
          $svg_file = file_get_contents( $path );
          $find_string   = '<svg';
          $position = strpos($svg_file, $find_string);
          $svg_file_new = substr($svg_file, $position);
          return $svg_file_new;
        } 
        ?>
      <div class="C-lsAction">
        <a href="javascript:;" id="goToPrevSlide">
          <div class='nb'><?=svg_file(base_url('images/logo/back.svg'));?></div>
        </a>
        <a href="javascript:;" id="goToNextSlide">
          <div class='nb'><?=svg_file(base_url('images/logo/next.svg'));?></div>
        </a>
      </div>
    </div>
  </div>
</section>