<script src="<?=base_url('scripts/frontend/lightslider-master/dist/js/lightslider.min.js')?>"></script>
<script>
  $(document).ready(function () {
    $('#slider_service').lightSlider({
      item: 3,
      loop: true,
      slideMargin: 15,
      responsive: [
        {
          breakpoint: 992,
          settings: {
            item: 3,
          }
        },
        {
          breakpoint: 768,
          settings: {
            item: 2,
          }
        },
      ]
    });
  });
</script>