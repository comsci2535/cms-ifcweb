<main>

  <section>
    <div class="container">

      <div class="row">
        <div class="col-md-12 mb-3 text-center">
          <h5><?php echo !empty($services->title) ? $services->title : null;?></h5>
        </div>
        <div class="offset-md-2 col-md-8 mb-3">
          <img src="<?php echo !empty($services->file) ? base_url($services->file) : '';?>" 
            class="wfull" 
            alt="<?php echo !empty($services->title) ? $services->title: null;?>" 
            onerror="this.src='<?=base_url('template/frontend/images/no-image.png');?>'" >
        </div>
        <div class="col-md-12 mb-3">
          <div class="expert">
            <p><?php echo !empty($services->excerpt) ? $services->excerpt: null;?> </p> 
          </div>
          <div class="expert"> 
            <p><?php echo !empty($services->detail) ? html_entity_decode($services->detail): null;?> </p>
          </div>
        </div>
        <div class="col-md-12 col-12">
          <hr>
        </div> 
        <!-- load service other -->
      </div>
    </div>
  </section>
  <?php echo Modules::run('service/get_service')?>  

</main>