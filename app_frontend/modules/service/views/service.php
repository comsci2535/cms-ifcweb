<main>

  <section>
    <div class="container">
      <div class="blog-title">
        <h2 class="text"><?=$this->lang->line('service');?></h2>
      </div>
      <?php 
      if(!empty($services)):
        foreach($services as $item):
      ?>
      <div class="row">
        <div class="col-md-4 col-12">
          <div class="blog-default">
            <img src="<?php echo !empty($item->file) ? base_url($item->file) : '';?>" 
            class="wfull" 
            alt="<?php echo !empty($item->title) ? $item->title: null;?>" 
            onerror="this.src='<?=base_url('template/frontend/images/no-image.png');?>'" >
          </div>
        </div>
        <div class="col-md-8 col-12">
          <div class="">
            <div class="detail">
              <div class="title">
                <h5><?php echo !empty($item->title) ? $item->title: null;?></h5>
              </div>
              <div class="expert mb-3">
                <p><?php echo !empty($item->excerpt) ? $item->excerpt: null;?> </p>
              </div>
              <a href="<?=site_url('service/detail/'.(!empty($item->slug) ? $item->slug: null));?>">
                <button type="button" class="btn btn-primary mb-3"><?=$this->lang->line('read_more');?></button>
              </a>
            </div>
          </div>
        </div>
      </div>
        <?php 
          endforeach;
        endif; 
      ?>

    </div>
  </section>

  <ul class="pagination justify-content-center py-4">
    <?php echo !empty($pagination) ? $pagination : ''; ?> 
  </ul>
</main>