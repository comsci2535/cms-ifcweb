<script type="text/javascript">
  $(document).ready(function () {
    $.validator.addMethod("lettersonly", function(value, element) {
      return this.optional(element) || /^[a-z0-9]+$/i.test(value);
    }, "Letters only please"); 

    $("#form_contact").validate({
      rules: {
        fname: {
          required: true,
          lettersonly: true,
        },
        lname: {
          required: true,
          lettersonly: true,
        },
        email: {
          required: true,
          email: true,
        },
        phone: {
          required: true,
          minlength: 7,
          maxlength: 10,
          number: true
        },
        title: {
          required: true,
        },
        detail: {
          required: true,
        },
      },
      messages: {
        fname: {
          required: "Please enter first name",
          lettersonly: "Please enter A-Z or 0-9",
        },
        lname: {
          required: "Please enter last name",
          lettersonly: "Please enter A-Z or 0-9",
        },
        email: {
          required: "Please enter email",
        },
        phone: {
          required: "Please enter phone",
          minlength: "Your username min 7 characters",
          maxlength: "Your username max 10 characters",
        },
        title: {
          required: "Please enter title",
        },
        detail: {
          required: "Please enter detail",
        },
      },
      submitHandler: function(form) {  
        if ($(form).valid()==true) {
          $.ajax({
            url: "<?=site_url('contact/save_contact');?>",
            method: "POST",
            dataType: "json",
            data: $("#form_contact").serialize(),
            success: function (data) {
              console.log(data);
              Swal.fire({
                title: data.status, text: data.message, type: data.status
              }).then(function() {
                if(data.status=='success') {
                  var url_back = '<?=(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>';
                  window.location = url_back;
                }
              })
            }
          });
        }
      },
      errorElement: "em",
    });
  });
</script>
