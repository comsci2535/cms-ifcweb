<main>

  <section class="contact">
    <div class="container">
      <div class="blog-title">
        <h2 class="text"><?=$this->lang->line('contact_us');?></h2>
      </div>

      <div class="row">
        <div class="col-md-12 col-12 mb-4">
          <?php echo !empty($contactus['google_map']) ? str_replace('width="600"', 'width="100%"', html_entity_decode($contactus['google_map'])) : null?>
        </div>
        <div class="col-md-4 col-12 address">
          <h4><?=$this->lang->line('Contact_address');?></h4>
          <div class="">
            <br>
            <h5 class="c_blue"><?php echo !empty($contactus['company_name']) ? $contactus['company_name'] : "-"?></h5>
            <?php
            if(CURRENT_LANG=='th'):
            ?>
            <p><?php echo !empty($contactus['detail']) ? html_entity_decode($contactus['detail']) : "-"?></p>
            <?php
             endif;
            ?>
            <?php
            if(CURRENT_LANG=='en'):
            ?>
            <p><?php echo !empty($contactus['detail_en']) ? html_entity_decode($contactus['detail_en']) : html_entity_decode($contactus['detail'])?></p>
            <?php
            endif;
            ?>
            <br>
            <p><span class="c_pink"><?=$this->lang->line('phone_number');?></span> : <?php echo !empty($contactus['phone']) ? $contactus['phone'] : "-"?></p>
            <p><span class="c_pink"><?=$this->lang->line('email');?></span> : <?php echo !empty($contactus['email']) ? $contactus['email'] : "-"?></p>
            <p>
              <?php
              if(!empty($contactus['facebook'])):
              ?>
              <a href="<?php echo $contactus['link_facebook']?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
              <?php
              endif;
              ?>
              <?php
              if(!empty($contactus['link_line'])):
              ?>
              <a href="<?php echo $contactus['link_line']?>" target="_blank"><i class="fab fa-line"></i></a>
              <?php
              endif;
              ?>
              <?php
              if(!empty($contactus['link_instagram'])):
              ?>
              <a href="<?php echo $contactus['link_instagram']?>" target="_blank"><i class="fab fa-instagram"></i></a>
              <?php
              endif;
              ?>
            </p>
          </div>
          <!-- <div class="mt-3">
            <a href="<?=site_url();?>">
              <img src="<?=base_url('images/logo/social (1).jpg')?>" alt="IFC Express">
            </a>
            <a href="<?=site_url();?>">
              <img src="<?=base_url('images/logo/social (2).jpg')?>" alt="IFC Express">
            </a>
            <a href="<?=site_url();?>">
              <img src="<?=base_url('images/logo/social (3).jpg')?>" alt="IFC Express">
            </a>
          </div> -->
        </div>
        <div class="col-md-8 col-12">
          <h4><?=$this->lang->line('contact_form');?></h4>
          <form id="form_contact" method="post">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for=""><?=$this->lang->line('fname');?></label>
                  <input type="text" class="form-control" placeholder="<?=$this->lang->line('fname');?>..." name="fname">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for=""><?=$this->lang->line('lname');?></label>
                  <input type="text" class="form-control" placeholder="<?=$this->lang->line('lname');?>..." name="lname">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for=""><?=$this->lang->line('email');?></label>
                  <input type="email" class="form-control" placeholder="<?=$this->lang->line('email');?>..." name="email">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for=""><?=$this->lang->line('phone_number');?></label>
                  <input type="text" class="form-control" placeholder="<?=$this->lang->line('phone_number');?>..." name="phone">
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label for=""><?=$this->lang->line('title');?></label>
                  <input type="text" class="form-control" placeholder="<?=$this->lang->line('title');?>..." name="title">
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <label for=""><?=$this->lang->line('detail');?></label>
                  <textarea class="form-control" name="detail"></textarea>
                </div>
              </div>
              <div class="col-12 text-center">
                <button type="submit" class="btn btn-dufault mt-3"><?=$this->lang->line('save');?></button>
                <button type="reset" class="btn btn-primary mt-3"><?=$this->lang->line('cancel');?></button>
                </p>
              </div>
            </div>
          </form>
        </div>
      </div>

    </div>
  </section>

</main>