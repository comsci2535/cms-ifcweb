<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('config_m');
        $this->load->model('contact_m');

	}

    private function seo($seo = null)
	{
		$title          = !empty($seo->meta_title) ? $seo->meta_title : "IFC | About";
		$robots         = !empty($seo->meta_title) ? $seo->meta_title : "";
		$description    = !empty($seo->meta_description) ? $seo->meta_description : "";
        $keywords       = !empty($seo->meta_keyword) ? $seo->meta_keyword : "";
        $url            = !empty($seo->file) ? base_url($seo->file) : base_url('images/logo/logo.png');
         
		$meta  			= '<TITLE>'.$title.'</TITLE>';
		$meta 		   .= '<meta name="robots" content="'.$robots.'"/>';
		$meta		   .= '<meta name="description" content="'.$description.'"/>';
        $meta 		   .= '<meta name="keywords" content="'.$keywords.'"/>';
        $meta 		   .= '<meta property="og:url" content="'.site_url().'" />';
        $meta 		   .= '<meta property="og:type" content="website" />';
        $meta 		   .= '<meta property="og:title" content="'.$title.'" />';
        $meta 		   .= '<meta property="og:description" content="'.$description.'" />';
        $meta 		   .= '<meta property="og:image" content="'.$url.'" />';
		return $meta;
	}

	public function index()
	{

        $obj_seo = Modules::run('configs/get_setting', 'general');
        $sco = new stdClass();
        $sco->meta_title = !empty($obj_seo['siteTitle']) ? $obj_seo['siteTitle'] : '';
        $sco->meta_description = !empty($obj_seo['metaDescription']) ? $obj_seo['metaDescription'] : '';
        $sco->meta_keyword = !empty($obj_seo['metaKeyword']) ? $obj_seo['metaKeyword'] : '';


        $type = 'contactus';
        $info = $this->config_m->get_config($type);
        $temp = array();
        foreach ($info->result_array() as $rs) {
            $temp[$rs['variable']] = $rs['value'];
        } 

        $data = array(
            'seo'     => $this->seo($sco),
            'menu'    => 'contact',
            'header'  => 'header',
            'content' => 'contact',
            'footer'  => 'footer',
            'function'=>  array('contact'),
        );

        $data['contactus'] = $temp;

        $this->load->view('template/body', $data);
    }

    private function sentMail($data)
    { 

        $temp  = Modules::run('configs/get_setting', 'mail');  

        if(!empty($temp)):
            // load mail config  
            $Host 		= !empty($temp['SMTPserver']) ? $temp['SMTPserver'] : '';
            $Username 	= !empty($temp['SMTPusername']) ? $temp['SMTPusername'] : '';
            $Password 	= !empty($temp['SMTPpassword']) ? $temp['SMTPpassword'] : '';
            $SMTPSecure = 'ssl';
            $Port 		= !empty($temp['SMTPport']) ? $temp['SMTPport'] : '';  
            $viewMail = $this->load->view('email/contact', $data, TRUE);

            // load mail send config 
            
            require 'app_frontend/third_party/phpmailer/PHPMailerAutoload.php';
            $mail = new PHPMailer;
            $mail->SMTPDebug = 0;                               	// Enable verbose debug output
            
            $mail->isSMTP();                                      	// Set mailer to use SMTP
            $mail->Host 		= $Host;              				// Specify main and backup SMTP servers
            $mail->SMTPAuth 	= true;                             // Enable SMTP authentication
            $mail->Username 	= $Username;                		// SMTP username
            $mail->Password 	= $Password;                        // SMTP password
            $mail->SMTPSecure 	= $SMTPSecure;                      // Enable TLS encryption, `ssl` also accepted
            $mail->Port 		= $Port;                            // TCP port to connect to
            $mail->CharSet 		= 'UTF-8';
            
            $mail->From 		= $Username;
            $mail->FromName 	= $Username;

            $email_to 			= $data['email'];

            $mail->addAddress($email_to);               			// Name is optional
            $mail->isHTML(false);                                  	// Set email format to HTML

            $mail->Subject = $email_to;
            $mail->Body    = $viewMail;
            $mail->AltBody = $viewMail; 
            $mail->Send();
            // $mail->ErrorInfo; 
        endif;
    }

    public function contact_footer()
	{
        $this->db->where('type', 'contactus');
        $info = $this->db->get('config');
        $temp = array();
        foreach ($info->result_array() as $rs) {
            $temp[$rs['variable']] = $rs['value'];
        } 

        $result = $temp;
        return $result;
    }

    public function save_contact()
	{
        $input = $this->input->post(null, true);
        
        $data = array(
            'fname'         => $input['fname'],
            'lname'         => $input['lname'],
            'email'         => $input['email'],
            'phone'         => $input['phone'],
            'title'         => $input['title'],
            'detail'        => $input['detail'],
        );
        $data['created_at']    = db_datetime_now();
        $data['created_by']    = 0;

        $info = $this->contact_m->save_contact($data);
        if ($info==true) {
            $data_arr['name']              = $data['fname'].' '.$data['lname']; 
            $data_arr['email']             = $data['email']; 
            $data_arr['title']             = $data['title']; 
            $data_arr['detail']             = $data['detail']; 
            $this->sentMail($data_arr);
            // endif;
            $res['status'] = "success";
            $res['message'] = "บันทึกสำเร็จ";       
        }else{
            $res['status'] = "warning";
            $res['message'] = "ไม่สามารถบันทึกข้อมูลได้";    
        }
        echo json_encode($res);
    }


}