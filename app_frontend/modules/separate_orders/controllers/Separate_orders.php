<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Separate_orders extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('separate_orders_m');
        $this->load->model('orders/orders_m');
        $this->load->model('deliverys/deliverys_m');
	} 

	public function get_order_import($id)
    {
        $input              = $this->input->post();
        $input['recycle']   = 0;
        $input['order_id']  = $id;

        $order              = $this->orders_m->get_order_key($input)->row();
        
        $deliverys_title    = '';
        if(!empty($order)):
            $deliverys          = $this->deliverys_m->get_delivery_setting($order->transport_id)->row();
            $deliverys_name     = !empty($deliverys->title) ? $deliverys->title : "ไม่ระบุ";
            $deliverys_title    = ' (ส่งโดย : '.$deliverys_name.')';
        endif;

        $info               = $this->separate_orders_m->get_rows($input); 
        $column             = array(); 
        
        foreach ($info->result() as $key => $rs):



            $detail_send = '<p>';
            $detail_send.= '<span> Tracking : '.(!empty($rs->tracking_in_code) ? $rs->tracking_in_code : 'ไม่ระบุ').'</span>';
            $detail_send.= '<br><span> เลขพัสดุไทย : '.(!empty($rs->tracking_code) ? $rs->tracking_code.$deliverys_title : 'ไม่ระบุ').'</span>';
            $detail_send.= '<br><span> เลขพัสดุที่ได้รับ : '.(!empty($rs->tracking_receive) ? $rs->tracking_receive : 'ไม่ระบุ').'</span>';
            $detail_send.= '<br><span> ขึ้นตู้ : '.(!empty($rs->date_up) ? date_languagefull($rs->date_up, false, ""): null).'</span>';
            $detail_send.= '<br><span> ถึง : '.(!empty($rs->date_down) ? date_languagefull($rs->date_down, false, ""): null).'</span>'; 

            $detail = '<p>';
            $detail.= '<span> ข้อมูล : '.$rs->detail.'</span>';
            $detail.= '<br><span> ประเภท : '.$rs->type_note.'</span>';
            $detail.= '<br><span> น้ำหนัก : '.$rs->weight.'</span>'; 
            $detail.= '<br><span> ขนาด : '.$rs->size.'</span>'; 
            
            $tracking_china = '<p>เลขพัสดุจีน : <br>'.(!empty($rs->tracking_china) ? $rs->tracking_china : 'ไม่ระบุ').'</p>'; 
            $service_charge = '<p>ค่าบริการ : <br>'.$rs->service_charge.' บาท</p>'; 
            $import_cost    = '<p>ค่านำเข้า : <br>'.$rs->import_cost.' บาท</p>'; 
            $import_cost   .= '<p>ค่าบริการรีแพ็คลัง / กระสอบ : <br>'.$rs->repack_sack.' บาท</p>'; 

            
 
            $column[$key]['weight']             = $rs->weight;
            $column[$key]['size']               = $rs->size;
            $column[$key]['service']            = $rs->service_charge; 
            $column[$key]['cost']               = $rs->import_cost;
            $column[$key]['repack_sack']        = $rs->repack_sack;
            $column[$key]['detail_send']        = $detail_send;
            $column[$key]['tracking_china']     = $tracking_china;
            $column[$key]['service_charge']     = $service_charge;
            $column[$key]['detail']             = $detail;
            $column[$key]['import_cost']        = $import_cost;

        endforeach; 
        
        return $column;
    }

}