<main>

  <section>
    <div class="container">
      <div class="blog-title">
        <h2 class="text"><?=$this->lang->line('about_us');?></h2>
      </div>
      <div class="my-4 text-center">
        <h5> <?php echo !empty($about[0]->title) ? $about[0]->title: null;?></h5>
        <hr>
      </div> 

      <?php  
      if(!empty($about)):
        $i = 0;
        foreach($about as $key => $item):
          if($key > 0):
            if ($i % 2 == 0):
      ?>
      <div class="row">
        <div class="col-md-4 col-12">
          <div class="blog-default">
            <img src="<?php echo !empty($item->file) ? base_url($item->file) : '';?>" 
              class="wfull" 
              alt="<?php echo !empty($item->title) ? $item->title: null;?>" 
              onerror="this.src='<?=base_url('template/frontend/images/no-image.png');?>'" >
          </div>
        </div>
        <div class="col-md-8 col-12">
          <div style="position: relative;left: 50%;top: 50%;transform: translate(-50%, -50%);">
            <div class="detail">
              <div class="title">
                <h5><?php echo !empty($item->title) ? $item->title: null;?></h5>
              </div>
              <div class="expert mb-3">
                <p><?php echo !empty($item->excerpt) ? html_entity_decode($item->excerpt): null;?> </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php else: ?>
      <div class="row">
        <div class="col-md-8 col-12">
          <div style="position: relative;left: 50%;top: 50%;transform: translate(-50%, -50%);">
            <div class="detail">
              <div class="title">
                <h5><?php echo !empty($item->title) ? $item->title: null;?></h5>
              </div>
              <div class="expert mb-3">
                <p><?php echo !empty($item->excerpt) ? html_entity_decode($item->excerpt): null;?> </p>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4 col-12">
          <div class="blog-default">
            <img src="<?=base_url('images/product.jpg')?>" class="wfull" alt="IFC Express">
          </div>
        </div>
      </div>
      <?php 
          endif;
          $i++;
        endif; 
        
        endforeach;
      endif;
      ?> 
    </div>
  </section> 
</main>