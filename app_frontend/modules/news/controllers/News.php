<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends MX_Controller {

    public function __construct()
	{
		parent::__construct();
        $this->load->model('news_m');
	}

    private function seo($seo=null)
	{
		$title          = !empty($seo->meta_title) ? $seo->meta_title : "IFC | News - Aticle";
		$robots         = !empty($seo->meta_title) ? $seo->meta_title : "";
		$description    = !empty($seo->meta_description) ? $seo->meta_description : "";
        $keywords       = !empty($seo->meta_keyword) ? $seo->meta_keyword : "";
        $url            = !empty($seo->file) ? base_url($seo->file) : base_url('images/logo/logo.png');
		$meta  			= '<TITLE>'.$title.'</TITLE>';
		$meta 		   .= '<meta name="robots" content="'.$robots.'"/>';
		$meta		   .= '<meta name="description" content="'.$description.'"/>';
        $meta 		   .= '<meta name="keywords" content="'.$keywords.'"/>';
        $meta 		   .= '<meta property="og:url" content="'.site_url().'" />';
        $meta 		   .= '<meta property="og:type" content="website" />';
        $meta 		   .= '<meta property="og:title" content="'.$title.'" />';
        $meta 		   .= '<meta property="og:description" content="'.$description.'" />';
        $meta 		   .= '<meta property="og:image" content="'.$url.'" />';
		return $meta;
    } 

	public function index()
	{
        $obj_seo = Modules::run('configs/get_setting', 'general');
        $sco = new stdClass();
        $sco->meta_title = !empty($obj_seo['siteTitle']) ? $obj_seo['siteTitle'] : '';
        $sco->meta_description = !empty($obj_seo['metaDescription']) ? $obj_seo['metaDescription'] : '';
        $sco->meta_keyword = !empty($obj_seo['metaKeyword']) ? $obj_seo['metaKeyword'] : '';

        $data = array(
            'seo'     => $this->seo($sco),
            'menu'    => 'news',
            'header'  => 'header',
            'content' => 'news',
            'footer'  => 'footer',
            'function'=>  array('news'),
        );

        $input['active']    = 1;
        $input['recycle']   = 0;

        $per_page = 12;
		$page = !empty($this->input->get('per_page')) ? ($this->input->get('per_page')-1) : 0; // เลขหน้าที่จะถูกส่งมาเช่น home/member/3
 
		$uri                = 'news/index'; // ชี้หน้าเพจหลักที่จะใช้งานมาที่ home/member
        $segment            = $page;
        $total              = $this->news_m->get_count($input); // จำนวนข้อมูลทั้งหมด
		$config['per_page'] = $per_page; // จำนวนข้อมูลต่อหน้า 
        $data['pagination'] = $this->pagin($uri, $total, $segment, $per_page); // เลขหน้า 
        $input['length']    = $per_page;
        $input['start']     = ($page*$per_page); 
        $news               = $this->news_m->get_rows($input)->result();  
        if(!empty($news)): 
            foreach($news as $item):
                if(CURRENT_LANG=='en'):
                    $item->title     = !empty($item->title_en) ? $item->title_en : $item->title;
                    $item->excerpt   = !empty($item->excerpt_en) ? $item->excerpt_en : $item->excerpt;
                    $item->detail    = !empty($item->detail_en) ? $item->detail_en : $item->detail;  
                endif; 
            endforeach;
        endif; 
        $data['news']       = $news;

        $this->load->view('template/body', $data);
    }

    public function detail($slug)
	{

        $input['active']    = 1;
        $input['recycle']   = 0;
        $input['slug']      = $slug;
        $news               = $this->news_m->get_rows($input)->row();  
        if(!empty($news)): 
            if(CURRENT_LANG=='en'):
                $news->title     = !empty($news->title_en) ? $news->title_en : $news->title;
                $news->excerpt   = !empty($news->excerpt_en) ? $news->excerpt_en : $news->excerpt;
                $news->detail    = !empty($news->detail_en) ? $news->detail_en : $news->detail;  
            endif; 
        endif; 
        $data = array(
            'seo'     => $this->seo($news),
            'menu'    => 'news',
            'header'  => 'header',
            'content' => 'detail',
            'footer'  => 'footer',
            'function'=>  array('news'),
        );

        $data['news']   = $news; 

        $this->load->view('template/body', $data);
    }

    public function get_home()
    { 
        $input['active']    = 1;
        $input['recycle']   = 0;
        $input['length']    = 4;
        $input['start']     = 0; 
        $news               = $this->news_m->get_rows($input)->result();  
        if(!empty($news)): 
            foreach($news as $item):
                if(CURRENT_LANG=='en'):
                    $item->title     = !empty($item->title_en) ? $item->title_en : $item->title;
                    $item->excerpt   = !empty($item->excerpt_en) ? $item->excerpt_en : $item->excerpt;
                    $item->detail    = !empty($item->detail_en) ? $item->detail_en : $item->detail; 
                endif; 
            endforeach;
        endif; 
        $data['news']       = $news;
        $this->load->view('news_home', $data);
    }

    public function get_news()
    { 
        $input['active']    = 1;
        $input['recycle']   = 0;
        $input['length']    = 4;
        $input['start']     = 0; 
        $news               = $this->news_m->get_rows($input)->result();  
        if(!empty($news)): 
            foreach($news as $item):
                if(CURRENT_LANG=='en'):
                    $item->title     = !empty($item->title_en) ? $item->title_en : $item->title;
                    $item->excerpt   = !empty($item->excerpt_en) ? $item->excerpt_en : $item->excerpt;
                    $item->detail    = !empty($item->detail_en) ? $item->detail_en : $item->detail; 
                endif; 
            endforeach;
        endif; 
        $data['news']       = $news;
        $this->load->view('news_other', $data);
    }
    
}