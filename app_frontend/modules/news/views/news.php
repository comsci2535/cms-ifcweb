<main>

  <section>
    <div class="container">
      <div class="blog-title">
        <h2 class="text"><?=$this->lang->line('news_articles');?></h2>
      </div>
      <div class="row">
        <?php   
        if(!empty($news)):
          foreach($news as $item): 
         ?>
        <div class="col-md-3 col-6">
          <div class="blog-default">
            <a href="<?=site_url('news/detail/'.(!empty($item->slug) ? $item->slug: null));?>">
            <img src="<?php echo !empty($item->file) ? base_url($item->file) : '';?>" 
            class="wfull" 
            alt="<?php echo !empty($item->title) ? $item->title: null;?>" 
            onerror="this.src='<?=base_url('template/frontend/images/no-image.png');?>'" >
              <div class="detail">
                <div class="title">
                  <h5><?php echo !empty($item->title) ? $item->title: null;?></h5>
                </div>
                <div class="expert">
                  <p><?php echo !empty($item->excerpt) ? $item->excerpt: null;?> </p> 
                </div>
                <div class="enter">
                  <div class="en"> <i class="fas fa-angle-right"></i> </div>
                </div>
              </div>
            </a>
            <div class="buttom">
              <div class="box">
                <span><i class="far fa-calendar-alt"></i> <?php echo !empty($item->updated_at) ? date_languagefull($item->updated_at, false, CURRENT_LANG): null;?></span>
              </div>
              <div class="box text-right">
                <a href="<?=site_url('news/detail/'.(!empty($item->slug) ? $item->slug: null));?>">
                  <button type="button" class="btn btn-primary w-a"><?=$this->lang->line('read_more');?></button>
                </a>
              </div>
            </div>
          </div>
        </div>
        <?php 
          endforeach;
        endif; 
      ?>
      </div>

    </div>
  </section>

  <ul class="pagination justify-content-center py-4">
    <?php echo !empty($pagination) ? $pagination : ''; ?> 
  </ul>
  
</main>