<section>
  <div class="container">
    <div class="blog-title">
      <h2 class="text"><?=$this->lang->line('news_articles');?></h2>
    </div>
    <div style="position: relative;">
      <ul id="slider_news">
        <?php if(!empty($news)): 
        foreach ($news as $key => $item):
        ?>
        <li>
          <div class="blog-default m-0">
            <a href="<?=site_url('news/detail/'.(!empty($item->slug) ? $item->slug: null));?>">
              <img src="<?php echo !empty($item->file) ? base_url($item->file) : '';?>" class="wfull"
                alt="<?php echo !empty($item->title) ? $item->title: null;?>"
                onerror="this.src='<?=base_url('template/frontend/images/no-image.png');?>'">
              <div class="detail">
                <div class="title">
                  <h5><?php echo !empty($item->title) ? $item->title: null;?></h5>
                </div>
                <div class="expert">
                  <p><?php echo !empty($item->excerpt) ? $item->excerpt: null;?> </p>
                </div>
                <div class="enter">
                  <div class="en"> <i class="fas fa-angle-right"></i> </div>
                </div>
              </div>
            </a>
            <div class="buttom">
              <div class="box">
                <span><i class="far fa-calendar-alt"></i>
                  <?php echo !empty($item->updated_at) ? date_languagefull($item->updated_at, false, CURRENT_LANG): null;?></span>
              </div>
              <div class="box text-right">
                <a href="<?=site_url('news/detail/'.(!empty($item->slug) ? $item->slug: null));?>">
                  <button type="button" class="btn btn-primary w-a"><?=$this->lang->line('read_more');?></button>
                </a>
              </div>
            </div>
          </div>
        </li>
        <?php 
        endforeach;
      endif; 
      ?>
      </ul>
      <div class="C-lsAction">
        <a href="javascript:;" id="goToPrevSlide" class="slider_news1">
          <div class='nb'><?=svg_file(base_url('images/logo/back.svg'));?></div>
        </a>
        <a href="javascript:;" id="goToNextSlide" class="slider_news2">
          <div class='nb'><?=svg_file(base_url('images/logo/next.svg'));?></div>
        </a>
      </div>
    </div>
  </div>
</section>