<main>

  <section>
    <div class="container">

      <div class="row">
        <div class="col-md-12 mb-3 text-center">
          <h5><?php echo !empty($news->title) ? $news->title : null;?></h5>
        </div>
        <div class="offset-md-2 col-md-8 mb-3">
          <img src="<?php echo !empty($news->file) ? base_url($news->file) : '';?>" 
            class="wfull" 
            alt="<?php echo !empty($news->title) ? $news->title: null;?>" 
            onerror="this.src='<?=base_url('template/frontend/images/no-image.png');?>'" >
        </div>
        <div class="col-md-12 mb-3">
          <div class="expert">
            <p><?php echo !empty($news->excerpt) ? $news->excerpt: null;?> </p> 
          </div>
          <div class="expert"> 
            <p><?php echo !empty($news->detail) ? html_entity_decode($news->detail): null;?> </p>
          </div>
        </div>
        <div class="col-md-12 col-12">
          <hr>
        </div> 
        <!-- load news other -->
        <?php echo Modules::run('news/get_news')?>  
      </div>
      
    </div>
  </section>

</main>