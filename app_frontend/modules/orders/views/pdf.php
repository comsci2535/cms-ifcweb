<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>IFC-PDF:<?php echo !empty($order_code) ? $order_code : "";?></title>
</head>

<style>
    table{
        width: 100%;
    }

    .table-print{
        border: 0px solid;
    }
    .table-print tr td{
        border: 0px solid;
    }

    .td-border-no{
        border: none !important;
    }

    .table-print td{
        border: 1px solid;
    }
    .table-print th{
        border: 0px solid;
    }

</style>
<body>

<table style="width: 100%;">
    <tr>
        <td style="width: 100px;">
            <img alt="" src="<?=base_url('images/logo/logo.png');?>" style="height: 75px;">
        </td>
        <td style="width: 450px;font-size: 12px;">
            <strong><?php echo !empty($company->title)? $company->title: '';?></strong>
            <br><?php echo !empty($company->title_en)? $company->title_en: '';?>
            <br><?php echo !empty($company->excerpt)? $company->excerpt: '';?>
            <br>เลขประจำตัวผู้เสียภาษีอากร : <?php echo !empty($company->tax_id)? $company->tax_id: '';?>
            
        </td>
        <td style="width: 200px;font-size: 11px;text-align: right;">
            สาขาที่ออกใบเสร็จรับเงิน : สำนักงานใหญ่
            <br>Import worldwide products from China
            <br>Tel : <?php echo !empty($company->tel)? $company->tel: '';?>
            <br>ifcexpcessshipping@gmail.com
            <br>Line : @ifcexpcessshipping
        </td>
    </tr>
</table>
<br>
<table class="table-print" style="width: 100%;font-size: 12px;">
    <tr style="background-color: #d2d2d2;">
        <td colspan="4" style="text-align: center;"><strong>ใบคำกับภาษี/ใบเสร็จรับเงิน</strong></td>
    </tr>
    <tr>
        <td class="td-border-no" style="width: 175px;">รหัสลูกค้า</td>
        <td class="td-border-no" style="width: 175px;"><?php echo !empty($info->members->member_id) ? $info->members->member_id :"";?></td>
        <td  class="td-border-no" style="text-align: right;width: 150px;">เลขที่บิล :</td>
        <td  class="td-border-no" style="width: 250px;"><?php echo !empty($info->order_code) ? $info->order_code:"";?></td>
    </tr>
    <tr>
        <td class="td-border-no">ผู้ชำระเงิน</td>
        <td class="td-border-no"><?php echo !empty($info->name) ? $info->name : "";?> <?php echo !empty($info->lasname) ? $info->lasname : "";?></td>
        <td class="td-border-no" style="text-align: right;">วันที่ออกบิล :</td>
        <td class="td-border-no"><?php echo !empty($info->print_date) ? $info->print_date:"";?></td>
    </tr>
    <tr>
        <td class="td-border-no">เลขประจำตัวผู้เสียภาษีอากร</td>
        <td class="td-border-no"><?php echo !empty($info->members->invoice_no) ? $info->members->invoice_no :"";?></td>
        <td class="td-border-no" style="text-align: right;">สถานะ :</td>
        <td class="td-border-no"><?php echo !empty($info->status_name) ? $info->status_name : "";?></td>
    </tr>
    <tr>
        <td class="td-border-no">โทรศัพท์</td>
        <td class="td-border-no"><?php echo !empty($info->tel) ? $info->tel : "";?></td>
        <td class="td-border-no"></td>
        <td class="td-border-no"></td>
    </tr>
</table>
<br>
<table class="table-print" style="width: 100%;font-size: 12px;">
    <tr style="background-color: #d2d2d2;">
        <td colspan="9" style="text-align: center;"><strong>ค่าขนส่งระหว่างประเทศ</strong></td>
    </tr>
    <tr style="background-color: #d2d2d2;">
        <td rowspan="2" style="text-align: center;width:150px;"><strong>วิธีขนส่ง</strong></td>
        <td colspan="4" style="text-align: center;"><strong>น้ำหนัก(KG)</strong></td>
        <td colspan="4" style="text-align: center;"><strong>คิว(CBM)</strong></td>
    </tr>
    <tr style="background-color: #d2d2d2;">
        <td style="text-align: center;"><strong>จำนวน(ชิ้น)</strong></td>
        <td style="text-align: center;"><strong>น้ำหนัก(KG)</strong></td>
        <td style="text-align: center;"><strong>ค่าขนส่ง(บาท/KG)</strong></td>
        <td style="text-align: center;width: 75px;"><strong>รวม</strong></td>
        <td style="text-align: center;"><strong>จำนวน(ชิ้น)</strong></td>
        <td style="text-align: center;"><strong>คิว(CBM)</strong></td>
        <td style="text-align: center;"><strong>ค่าขนส่ง(บาท/CBM)</strong></td>
        <td style="text-align: center;width: 75px;"><strong>รวม</strong></td>
    </tr>
    <?php
    $total                      = 0;
    $total_import               = 0;
    $total_service              = 0;
    $total_repack               = 0;
    $deliverys_price            = !empty($info->deliverys_price) ? $info->deliverys_price : 0;
    $deliverys_price_extra      = !empty($info->deliverys_price_extra) ? $info->deliverys_price_extra : 0;
    $total_deliverys            = $deliverys_price + $deliverys_price_extra;
    if(!empty($order_import)):
        foreach($order_import as $import):
        
            $total_import   += $import->import_cost;
            $total_service  += $import->service_charge;
            $total_repack   += $import->repack_sack;
            
    ?>
    <tr>
        <td>
            <?php
            if(!empty($import->attribute)):
                foreach($import->attribute as $attr):
            ?>
                <span><?php echo !empty($attr->title) ? $attr->title : "";?></span>
            <?php
                endforeach;
            endif;
            ?>
        </td>
        <td style="text-align: right;"><?php echo !empty($import->qty_china) ? $import->qty_china : 0;?></td>
        <td style="text-align: right;"><?php echo !empty($import->weight) ? $import->weight : 0.00;?></td>
        <td style="text-align: right;"><?php echo !empty($import->import_cost) ? number_format($import->import_cost, 2) : 0.00;?></td>
        <td style="text-align: right;"><?php echo !empty($import->import_cost) ? number_format($import->import_cost, 2) : 0.00;?></td>
        <td style="text-align: right;">0</td>
        <td style="text-align: right;">0</td>
        <td style="text-align: right;">0.00</td>
        <td style="text-align: right;">0.00</td>
    </tr>
    <?php
        endforeach;
    endif;
    $total = $total_import + $total_service + $total_repack + $total_deliverys;
    ?>
</table>
<br>
<table class="table-print" style="width: 100%;font-size: 12px;">
    <tr style="background-color: #d2d2d2;">
        <td colspan="2" style="text-align: center;"><strong>ข้อมูลการขนส่งในประเทศ</strong></td>
    </tr>
    <tr>
        <td class="td-border-no" style="width: 150px;">ชื่อผู้รับ</td>
        <td class="td-border-no"><?php echo !empty($info->name) ? $info->name : "";?> <?php echo !empty($info->lasname) ? $info->lasname : "";?></td>
    </tr>
    <tr>
        <td class="td-border-no" >ที่อยู่</td>
        <td class="td-border-no" >
            <?php echo !empty($info->address) ? $info->address : "";?> 
            <?php echo !empty($info->districts) ? $info->districts : "";?>
            <?php echo !empty($info->amphures) ? $info->amphures : "";?>
            <?php echo !empty($info->provinces) ? $info->provinces : "";?>
            <?php echo !empty($info->zip_code) ? $info->zip_code : "";?>
            
        </td>
    </tr>
    <tr>
        <td class="td-border-no" >ข้อมูลการขนส่งในประเทศ</td>
        <td class="td-border-no" ><?php echo !empty($info->tracking_title) ? $info->tracking_title : "ไม่ระบุ";?></td>
    </tr>
</table>
<br>
<table style="width: 100%;font-size: 12px;">
    <tr>
        <td style="width: 150px;">อัตราแลกเปลี่ยน : </td>
        <td><?php echo !empty($money['money_th']) ? number_format($money['money_th'],2) : 0.00;?></td>
        <td style="text-align: right;">ค่าขนส่งระหว่างประเทศ(บาท) : </td>
        <td style="text-align: right;border: 1px solid;width: 80px;"><?=number_format($total_service,2);?></td>
    </tr>
    <tr>
        <td style="width: 150px;">จำนวนแทร็ค : </td>
        <td>
            <?php
            $tracking_text = '';
            if(!empty($trackings)):
                foreach($trackings as $key => $tracking):
                    if($key == 0):
                        $tracking_text = $tracking;
                    else:
                        $tracking_text.= ' ,'.$tracking;
                    endif;
                endforeach;
            endif;
            ?>
            <?php echo !empty($tracking_text) ? $tracking_text : "ไม่ระบุ";?>
        </td>
        <td style="text-align: right;">ค่าขนส่งในประเทศจีน(บาท) : </td>
        <td style="text-align: right;border: 1px solid;"><?=number_format($total_import,2);?></td>
    </tr>
    <tr>
        <td style="width: 150px;">หมายเหตุ(ลูกค้า) : </td>
        <td><?php echo !empty($info->note) ? $info->note : "ไม่ระบุ";?></td>
        <td style="text-align: right;">ค่าขนส่งในประเทศไทย(บาท) : </td>
        <td style="text-align: right;border: 1px solid;"><?=number_format($total_deliverys,2);?></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td style="text-align: right;">ดำเนินการและค่าแพ็คสินค้า(บาท) : </td>
        <td style="text-align: right;border: 1px solid;"><?=number_format($total_repack,2);?></td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td style="text-align: right;">ยอดที่ต้องชำระ(บาท) : </td>
        <td style="text-align: right;border: 1px solid;"><?=number_format($total,2);?></td>
    </tr>
</table>
<br>
<table class="table-print" style="width: 100%;font-size: 12px;">
    <tr style="background-color: #d2d2d2;">
        <td style="text-align: center;"><strong>บัญชีธนาคารสำหรับชำระเงิน</strong></td>
    </tr>
    <?php
    if(!empty($banks)):
    ?>
    <tr>
        <td style="text-align: center;">
            <?php echo !empty($banks->title) ? $banks->title :"";?>
            <?php echo !empty($banks->account_branch) ? $banks->account_branch :"";?>
            <strong>ชื่อบัญชี</strong> : <?php echo !empty($banks->account_name) ? $banks->account_name :"";?>
            <strong>เลขที่บัญชี</strong> : <?php echo !empty($banks->account_number) ? $banks->account_number :"";?>
        </td>
    </tr>
    <?php
    else:
    ?>
    <tr>
        <td style="text-align: center;">ไม่ระบุ</td>
    </tr>
    <?php
    endif;
    ?>
</table>
<br>
<table class="table-print" style="width: 100%;font-size: 12px;">
    <tr style="background-color: #d2d2d2;">
        <td style="width: 35px;text-align: center;"><strong>No.</strong></td>
        <td style="width: 150px;text-align: center;"><strong>Products</strong></td>
        <td style="width: 35px;text-align: center;"><strong>Pes</strong></td>
        <td style="width: 35px;text-align: center;"><strong>Length</strong></td>
        <td style="width: 35px;text-align: center;"><strong>Width</strong></td>
        <td style="width: 35px;text-align: center;"><strong>Height</strong></td>
        <td style="width: 35px;text-align: center;"><strong>CBM</strong></td>
        <td style="width: 35px;text-align: center;"><strong>Weight(KG)</strong></td>
        <td style="width: 35px;text-align: center;"><strong>วันที่ส่งออก</strong></td>
    </tr>
    <?php
    if(!empty($order_import)):
        foreach($order_import as $key => $imports):
            $longs = !empty($imports->longs) ? $imports->longs : 0;
            $wide = !empty($imports->wide) ? $imports->wide : 0;
            $high = !empty($imports->high) ? $imports->high : 0;
            $total_sum = ($longs * $wide * $high) / 1000000;
    ?>
    <tr>
        <td style="text-align: center;"><?=($key+1);?></td>
        <td><?php echo !empty($imports->type_note) ? $imports->type_note : "ไม่ระบุ";?></td>
        <td style="text-align: center;"><?php echo !empty($imports->qty_china) ? $imports->qty_china : 0;?></td>
        <td style="text-align: center;"><?php echo !empty($imports->longs) ? $imports->longs : 0;?></td>
        <td style="text-align: center;"><?php echo !empty($imports->wide) ? $imports->wide : 0;?></td>
        <td style="text-align: center;"><?php echo !empty($imports->high) ? $imports->high : 0;?></td>
        <td style="text-align: center;"><?=number_format($total_sum, 2);?></td>
        <td style="text-align: center;"><?php echo !empty($imports->weight) ? $imports->weight : 0;?></td>
        <td style="text-align: center;"><?php echo !empty($imports->date_up) ? date_format(date_create($imports->date_up),"d.m.Y") : "ไม่ระบุ";?></td>
    </tr>
    <?php
        endforeach;
    endif;
    ?>

</table>
<br>
<p style="font-size: 12px;color:red">หมายเหตุ : ค่านำเข้าสินค้าหากวิธีการคำนวณใดได้ค่ามากกว่าจะคิดราคาตามนั้น คุณลูกค้าจำเป็นต้องชำระเงินให้เรียบร้อยก่อนการมารับสินค้าที่โกดังไทยและก่อนการจัดส่งสินค้าให้ทุกครั้ง</p>
                
<p style="font-size: 12px;color:red">******* ระบบการคำนวณค่าสินค้าจากเว็บไซต์จีน ไม่มีการรวมค่าขนส่งจากจีนไปโกดังจีน******<br>
รอรวมสินค้าไม่เกิน 5 วัน หลังจาก 5 วัน ทางบริษัทจะปิดตู้และตัดขึ้นบิลใหม่ส่งให้ลูกค้า และในสินค้าตัวต่อไปที่จะมาถึงหลังจากเลยกำหนดการ ของบริษัท ทางบริษัทจะคำนวณราคาค่านำเข้าสินค้า ตามราคามาตรฐานของบริษัทและมีค่านำเข้าขั้นต่ำ 1000 บาท</p>  
</body>
</html>       




