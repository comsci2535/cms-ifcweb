<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends MX_Controller {

    public function __construct()
	{
		parent::__construct();
        $this->load->model('orders_m');
        $this->load->model("status/status_m"); 
        $this->load->model("info/info_m"); 
        $this->load->model("banks/banks_m"); 
        $this->load->model("deliverys/deliverys_m");
        $this->load->model("member/member_m");
        $this->load->model('separate_orders/separate_orders_m');
        $this->load->model('attribute_sends/attribute_sends_m');
    }
    
    public function pdf($id="")
    {
        set_time_limit(0);
        ini_set('memory_limit', '128M');
        
        $id = decode_id($id);
        $input['order_id'] = $id;
        $info = $this->orders_m->get_rows($input);
        if ( $info->num_rows() == 0) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
            redirect_back();
        }
        $info                   = $info->row();  
        $info->status_name      = $this->status_m->get_status_by_id($info->status)->row()->title;
        $info->print_date       = date('d.m.Y');

        $input_['id']           = $info->user_id;
        $info->members          = $this->member_m->get_rows($input_)->row();

        if(!empty($info->transport_id)):
            $input__['delivery_company_id'] = $info->transport_id;
            $info->tracking_title           = $this->deliverys_m->get_rows($input__)->row()->title;
        endif;
        
        $data['info']           = $info;

        $input___['recycle']   = 0;
        $input___['order_id']  = $info->order_id;
        $order_import          = $this->separate_orders_m->get_rows($input___)->result();
        $arr_tracking          = [];
        if(!empty($order_import)):
            foreach($order_import as $import):
                if(!empty($import->tracking_code)):
                    array_push($arr_tracking, $import->tracking_code);
                endif;
                
                $attribute['recycle']       = 0;
                $attribute['attribute_send_in']  = explode(',', $import->attribute_send_id);
                $import->attribute    = $this->attribute_sends_m->get_rows($attribute)->result();
                
            endforeach;
        endif;

        $data['trackings']          = $arr_tracking;
        $data['order_import']       = $order_import;
        
        $input____['type']          = 1;
        $input____['order_id']      = $info->order_id;
        $orders_payments            = $this->orders_m->get_orders_payments_by_param($input____)->row();
        if(!empty($orders_payments)):
            $input_____['bank_id']  = $orders_payments->bank_id;
            $banks                  = $this->banks_m->get_rows($input_____)->row();
            $data['banks']          = $banks; 
        endif;
        $data['orders_payments']    = $orders_payments;
        
        $code                = $info->order_code;
        $data['order_code']  = $code;
        
        //ดึงข้อมูลบริษัท
        $info_com           = $this->info_m->get_rows('');
        $info_com           = $info_com->row();
        
        $data['company']    =  $info_com;

        $money  = Modules::run('configs/get_setting', 'money_setting');
        $data['money'] = $money;
        
        $mpdfConfig = [
            'default_font_size' => 9,
            'default_font'      => 'garuda'
        ];

        require APPPATH.'../vendor/autoload.php';

        $mpdf = new \Mpdf\Mpdf($mpdfConfig);
        $content = $this->load->view("{$this->router->class}/pdf", $data, TRUE);
        $mpdf->WriteHTML($content);
        
        
        $download = FALSE;
        $saveFile = FALSE;
        $viewFile = TRUE;
        $fileName = "orders.pdf";
        $pathFile = "uploads/pdf/orders/";
        if ( $saveFile ) {
            create_dir($pathFile);
            $fileName = $pathFile.$fileName;
            $mpdf->Output($fileName, 'F');
        }
        if ( $download ) 
            $mpdf->Output($fileName, 'D');
        if ( $viewFile ) 
            $mpdf->Output();
    }
    
}