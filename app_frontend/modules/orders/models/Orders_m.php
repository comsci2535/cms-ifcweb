<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Orders_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }

    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*')
                        ->from('orders a')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->from('orders a')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {   
        // START form filter 
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.order_code', $param['search']['value'])
                    ->or_like('a.name', $param['keyword'])
                    ->or_like('a.lasname', $param['keyword'])
                    ->or_like('a.tel', $param['keyword'])
                    ->or_like('a.email', $param['keyword'])
                    ->group_end();
        }
        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.created_at,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
        }
        if ( isset($param['updateDateRange']) && $param['updateDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.updated_at,'%Y-%m-%d') BETWEEN '{$param['updateStartDate']}' AND '{$param['updateEndDate']}'");
        }     
        if ( isset($param['active']) && $param['active'] != "" ) {
            $this->db->where('a.active', $param['active']);
        }         
        // END form filter
        
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.order_code', $param['search']['value'])
                    ->or_like('a.name', $param['search']['value'])
                    ->or_like('a.lasname', $param['search']['value'])
                    ->or_like('a.tel', $param['search']['value'])
                    ->or_like('a.email', $param['search']['value'])
                    ->group_end();
        }

        if (!empty($param['order_year'])):
            $this->db->where('YEAR(a.created_at)', $param['order_year']);
        endif;

        if (!empty($param['order_month'])):
            $this->db->where('MONTH(a.created_at)', $param['order_month']);
        endif;

        if (!empty($param['order_day'])):
            $this->db->where('DAY(a.created_at)', $param['order_day']);
        endif;

        if (!empty($param['created_at'])):
            $this->db->where('DATE(a.created_at)', $param['created_at']);
        endif;

        if (!empty($param['updated_at'])):
            $this->db->where('DATE(a.updated_at)', $param['updated_at']);
        endif;

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.created_at";
            if ($param['order'][0]['column'] == 2) $columnOrder = "a.order_code";
            if ($param['order'][0]['column'] == 3) $columnOrder = "a.name";            
            if ( $this->router->method =="data_index" ) {
                //if ($param['order'][0]['column'] == 3) $columnOrder = "a.created_at";
                if ($param['order'][0]['column'] == 4) $columnOrder = "a.updated_at";
            } else if ( $this->router->method =="data_trash" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "a.recycle_at";
            } 

            $this->db
                    ->order_by($columnOrder, $param['order'][0]['dir']);
        } 
        
        if ( isset($param['order_id']) ) 
            $this->db->where('a.order_id', $param['order_id']);

        
        if(!empty($param['status'])){
            $this->db->where_in('a.status', $param['status']);
        }

        if(!empty($param['statuss'])){
            $this->db->where('a.status', $param['statuss']);
        }

        if(!empty($param['order_by'])){
            $this->db->order_by('a.created_at', $param['order_by']);
        }
        
        if (!empty($param['transports']) ):
            $this->db->where_in('a.transport_id', $param['transports']);
        endif;

        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);

    }

    public function get_orders($param) 
    {
          
        if(!empty($param['user_id'])):
            if(!empty($param['name'])):
                $this->db->where('a.name', $param['name']);  
            endif;
            if(!empty($param['lasname'])):
                $this->db->where('a.lasname', $param['lasname']); 
            endif;
            $this->db->where('a.user_id', $param['user_id']);
        else:
            $this->db->where('a.name', $param['name']);  
            $this->db->where('a.lasname', $param['lasname']);  
        endif;

        if(!empty($param['order_id'])):
            $this->db->where('a.order_id', $param['order_id']);
        endif;
        
        $this->db->where('a.recycle', 0);
        $this->db->order_by('a.created_at', 'DESC');
        $query = $this->db
                        ->select('a.*')
                        ->from('orders a')
                        ->get();
        return $query;
    }

    public function get_order_key($param) 
    {

        if(!empty($param['order_id'])):
            $this->db->where('a.order_id', $param['order_id']);
        endif;
        
        $this->db->where('a.recycle', 0);
        $this->db->order_by('a.created_at', 'DESC');
        $query = $this->db
                        ->select('a.*')
                        ->from('orders a')
                        ->get();
        return $query;
    }

    public function get_orders_detail_by_param($param)
    {
        if(!empty($param['order_detail_id'])):
            $this->db->where('order_detail_id', $param['order_detail_id']);
        endif;
        if(!empty($param['order_id'])):
            $this->db->where('order_id', $param['order_id']);
        endif;
        if(!empty($param['product_id'])):
            $this->db->where('product_id', $param['product_id']);
        endif;
       
        $query = $this->db
                ->select('a.*')
                ->from('orders_detail a')
                ->get();
        return $query; 
    }

    public function get_orders_param($param) 
    {
         
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);

        if(!empty($param['order_id'])):
            $this->db->where('a.order_id', $param['order_id']);
        endif;
        if(!empty($param['user_id'])):
            $this->db->where('a.user_id', $param['user_id']);
        endif; 
        
        $this->db->order_by('a.created_at', 'DESC'); 
        
        $this->db->where('a.recycle', 0);
        $query = $this->db
                        ->select('a.*')
                        ->from('orders a')
                        ->get();
        return $query;
    }

    public function get_orders_count_param($param) 
    { 
            
        if(!empty($param['order_id'])):
            $this->db->or_where('a.order_id', $param['order_id']);
        endif;
        if(!empty($param['user_id'])):
            $this->db->or_where('a.user_id', $param['user_id']);
        endif;
        
        $this->db->where('a.recycle', 0);
        $query = $this->db
                        ->select('a.*')
                        ->from('orders a')
                        ->get();
        return $query->num_rows();
    }

    public function get_orders_detail_import_param($param) 
    {
         
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);

        if(!empty($param['order_id'])):
            $this->db->where('a.order_id', $param['order_id']);
        endif;
        if(!empty($param['user_id'])):
            $this->db->where('a.user_id', $param['user_id']);
        endif;

        if(isset($param['order_type'])):
            $this->db->where('b.order_type', $param['order_type']);
        endif;

        $this->db->order_by('a.created_at', 'DESC'); 
        
        $this->db->where('a.active', 1);
        $this->db->where('a.recycle', 0);
        $query = $this->db
                        ->select('a.*,b.order_code,b.status')
                        ->from('orders_detail_import a')
                        ->join('orders b','a.order_id = b.order_id')
                        ->get();
        return $query;
    }

    public function get_orders_detail_import_count_param($param) 
    {
         
        if(!empty($param['order_id'])):
            $this->db->where('a.order_id', $param['order_id']);
        endif;
        if(!empty($param['user_id'])):
            $this->db->where('a.user_id', $param['user_id']);
        endif;

        if(isset($param['order_type'])):
            $this->db->where('b.order_type', $param['order_type']);
        endif;
        
        $this->db->where('a.active', 1);
        $this->db->where('a.recycle', 0);
        $query = $this->db
                        ->select('a.*,b.order_code,b.status')
                        ->from('orders_detail_import a')
                        ->join('orders b','a.order_id = b.order_id')
                        ->get();
        return $query->num_rows();
    }

    public function get_orders_detail_by_order_id($order_id)
    {
        $this->db->where('a.order_id', $order_id);
        $query = $this->db
                ->select('a.*,b.title, b.excerpt')
                ->from('orders_detail a')
                ->join('products b','a.product_id = b.product_id')
                ->get();
        return $query; 
    }

    public function get_orders_status_action_by_order_id($order_id, $status)
    {
        $this->db->where('a.order_id', $order_id);
        $this->db->where_in('a.status', $status);
        $this->db->where_in('a.is_process', 0);
        
        $query = $this->db
                ->select('a.*,b.title as status_title')
                ->from('orders_status_action a')
                ->join('status b','a.status = b.status_id', 'left')
                ->order_by('order_status_action_id', 'asc')
                ->get();
        return $query; 
    }

    public function get_orders_product_by_id($product_id)
    {
        $query = $this->db
                ->select_sum('quantity')
                ->from('orders_detail')
                ->where('product_id', $product_id)
                ->get();
        return $query; 
    }

    public function get_orders_detail_attribute_id($uniqid)
    {
        $this->db->where('a.order_detail_uniqid', $uniqid);
        $query = $this->db
                ->select('a.*')
                ->from('orders_detail_attribute a') 
                ->get();
        return $query; 
    }
    
    public function insert($value) 
    {
        $this->db->insert('orders', $value);
        return $this->db->insert_id();
    }

    public function insert_orders_user_address($value) 
    {
        $this->db->insert('orders_user_address_send', $value);
        return $this->db->insert_id();
    }
    
    public function insert_orders_status_action($value) 
    {
        $this->db->insert('orders_status_action', $value);
        return $this->db->insert_id();
    }

    public function insert_orders_payments($value) 
    {
        $this->db->insert('orders_payments', $value);
        return $this->db->insert_id();
    }
    
    public function insert_orders_detail($value) 
    {
        return $this->db->insert_batch('orders_detail', $value);
    }

    public function insert_orders_detail_url($value) 
    {
        return $this->db->insert_batch('orders_detail_url', $value);
    }

    public function insert_orders_detail_attribute($value) 
    {
        return $this->db->insert_batch('orders_detail_attribute', $value);
    }

    public function insert_orders_detail_import($value) 
    {
        return $this->db->insert('orders_detail_import', $value);
    }

    public function insert_invoice($value) 
    {
        $this->db->insert('invoice', $value);
        return $this->db->insert_id();
    }

    public function get_invoice()
    {
        $query = $this->db->select('*')
                ->from('invoice')
                ->order_by('invoice_number', 'DESC')
                ->get();
        return $query;
    }

    public function get_transport_by_id($id)
    {
        $query = $this->db->select('*')
                ->from('transports')
                ->where('transport_id', $id)
                ->get();
        return $query;
    }

    public function get_transport_all()
    {
        $query = $this->db->select('*')
                ->from('transports')
                ->where('active', 1)
                ->where('recycle', 0)
                ->get();
        return $query;
    }

    public function get_orders_user_address_send_by_id($id)
    {
        $query = $this->db->select('*')
                ->from('orders_user_address_send')
                ->where('user_id', $id)
                ->get();
        return $query;
    }

    public function get_orders_payments_by_param($param)
    {
        if(!empty($param['orders_payments_id'])):
            $this->db->where('a.orders_payments_id', $param['orders_payments_id']);
        endif;

        if(!empty($param['order_id'])):
            $this->db->where('a.order_id', $param['order_id']);
        endif;

        if(!empty($param['bank_id'])):
            $this->db->where('a.bank_id', $param['bank_id']);
        endif;

        if(!empty($param['user_id'])):
            $this->db->where('a.user_id', $param['user_id']);
        endif;

        if(isset($param['type'])):
            $this->db->where('a.type', $param['type']);
        endif;

        if(!empty($param['payment_type'])):
            $this->db->where('a.payment_type', $param['payment_type']);
        endif;
       
        $query = $this->db
                ->select('a.*')
                ->from('orders_payments a')
                ->get();
        return $query; 
    }

    public function set_order_id()
    {
        $date = date('Y-m-d H:i:s');
        $query = "INSERT
        INTO `auto_order_id`
        (`id`, cdate)
        VALUES (
            # ใช้ฟังก์ชั่นนี้เชื่อมต่อสตริงเข้าด้วยกัน
            CONCAT(          
                # ปีและเดือนของเวลาปัจจุบันตามด้วยขีด เช่น 2013-03-
                DATE_FORMAT(NOW(), '%Y%m'),
                # ใช้ LPAD() เพื่อเติมตัวอักษรตามที่ต้องการเข้าข้างหน้าตัวเลข (ในที่นี้คือ 0)
                LPAD(
                    IFNULL(
                        # Sub Query ที่จะเลือก 'ตัวเลขสุดท้าย' ของ id ในปีปัจจุบัน
                        (SELECT
                            # จำนวนสูงจุดของ ส่วนหลังเครื่องหมาย - ของ id
                            MAX(SUBSTR(`id`, 9))
                            FROM `auto_order_id` AS `alias`
                            # โดยหาเฉพาะปีและเดือนปัจจุบัน
                            WHERE SUBSTR(`id`, 1, 6) = DATE_FORMAT(NOW(), '%Y%m')
                            # เรียงตามลำดับ id จากมากไปหาน้อย เพื่อเอาค่าล่าสุดออกมา
                            ORDER BY `id` DESC
                            LIMIT 1
                        )
                        # และ + ด้วย 1 เสมอ ซึ่งจะทำให้ได้เลขที่เรียงกันไป
                        + 1,
                        # หาก Sub Query ข้างบนคืนแถวกลับมาเป็น NULL ก็ให้ใช้ค่า 1 (เริ่มแถวแรกของปี)
                        1
                    ),
                    6,  # โดยให้เป็นตัวเลข 5 หลัก
                    '0' # ตัวอักษรที่จะเติมข้างหน้าตัวเลข
                )
            ), '{$date}'
        )";
        $order_code = null;
        $query = $this->db->query($query);
        if($query){
            $this->db->select_max('id','order_code');
            $this->db->order_by('id', 'DESC');
            $this->db->limit(1);
            $order_code = $this->db->get('auto_order_id')->row();
        }
        return $order_code;
    }

    public function set_order_in_id()
    {
        $date = date('Y-m-d H:i:s');
        $query = "INSERT INTO `auto_order_in_id` (`id`, cdate) VALUES 
        ( CONCAT( DATE_FORMAT(NOW(), '%Y%m'), LPAD( IFNULL( (SELECT MAX(SUBSTR(`id`, 9)) 
        FROM `auto_order_in_id` AS `alias` WHERE SUBSTR(`id`, 1, 6) = DATE_FORMAT(NOW(), '%Y%m') 
        ORDER BY `id` DESC LIMIT 1 ) + 1, 1 ), 6, '0' ) ), '{$date}' )";
        $order_code = null;
        $query = $this->db->query($query);
        if($query){
            $this->db->select_max('id','order_code');
            $this->db->order_by('id', 'DESC');
            $this->db->limit(1);
            $order_code = $this->db->get('auto_order_in_id')->row();
        }
        return $order_code;
    }

    public function set_order_ym_id()
    {
        $date = date('Y-m-d H:i:s');
        $query = "INSERT INTO `auto_order_in_id` (`id`, cdate) 
        VALUES (CONCAT(DATE_FORMAT(NOW(), '68-%y%m'),
        LPAD(IFNULL((SELECT MAX(SUBSTR(`id`, 8)) FROM `auto_order_in_id` AS `alias` 
        WHERE SUBSTR(`id`, 1, 7) = DATE_FORMAT(NOW(), '68-%y%m') 
        ORDER BY `id` DESC LIMIT 1) + 1, 1 ), 5, '0')), '{$date}')";
        $order_code = null;
        $query = $this->db->query($query);
        if($query){
            $this->db->select_max('id','order_code');
            $this->db->order_by('id', 'DESC');
            $this->db->limit(1);
            $order_code = $this->db->get('auto_order_in_id')->row();
        }
        return $order_code;
    }

    public function set_invoice_id()
    {

        $query = "INSERT
        INTO `auto_invoice_id`
        (`id`)
        VALUES (
            # ใช้ฟังก์ชั่นนี้เชื่อมต่อสตริงเข้าด้วยกัน
            CONCAT(          
                # ปีและเดือนของเวลาปัจจุบันตามด้วยขีด เช่น 2013-03-
                DATE_FORMAT(NOW(), 'Rcpt%y%m'),
                # ใช้ LPAD() เพื่อเติมตัวอักษรตามที่ต้องการเข้าข้างหน้าตัวเลข (ในที่นี้คือ 0)
                LPAD(
                    IFNULL(
                        # Sub Query ที่จะเลือก 'ตัวเลขสุดท้าย' ของ id ในปีปัจจุบัน
                        (SELECT
                            # จำนวนสูงจุดของ ส่วนหลังเครื่องหมาย - ของ id
                            MAX(SUBSTR(`id`, 11))
                            FROM `auto_invoice_id` AS `alias`
                            # โดยหาเฉพาะปีและเดือนปัจจุบัน
                            WHERE SUBSTR(`id`, 1, 8) = DATE_FORMAT(NOW(), 'Rcpt%y%m')
                            # เรียงตามลำดับ id จากมากไปหาน้อย เพื่อเอาค่าล่าสุดออกมา
                            ORDER BY `id` DESC
                            LIMIT 1
                        )
                        # และ + ด้วย 1 เสมอ ซึ่งจะทำให้ได้เลขที่เรียงกันไป
                        + 1,
                        # หาก Sub Query ข้างบนคืนแถวกลับมาเป็น NULL ก็ให้ใช้ค่า 1 (เริ่มแถวแรกของปี)
                        1
                    ),
                    6,  # โดยให้เป็นตัวเลข 5 หลัก
                    '0' # ตัวอักษรที่จะเติมข้างหน้าตัวเลข
                )
            )
        )";
        $invoice_code = null;
        $query = $this->db->query($query);
        if($query){
            $this->db->select_max('id','invoice_code');
            $this->db->order_by('id', 'DESC');
            $this->db->limit(1);
            $invoice_code = $this->db->get('auto_invoice_id')->row();
        }
        return $invoice_code;
    }
   
}
