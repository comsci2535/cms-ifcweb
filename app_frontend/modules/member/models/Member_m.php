<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Member_m Extends CI_Model {
    
    
    public function __construct()
    {
        parent::__construct();
    }
    
    public function check_match($value=null)
    {
        if (!empty($value['email'])){
            $this->db->where('email', $value['email']);
        }
        if (!empty($value['username'])){
            $this->db->where('username', $value['username']);
        }
        if (!empty($value['id'])){
            $this->db->where('id !=', $value['id']);
        }

        $this->db->where('recycle', 0);

        $query = $this->db
                        ->from('member')
                        ->get();
        return $query->num_rows();
    }

    public function getRows($value=null)
    {
        if (!empty($value['username'])){
            $this->db->where('username', $value['username']);
        }

        if (!empty($value['id'])){
            $this->db->where('id', $value['id']);
        }

        if(isset($value['active'])):
            $this->db->where('active', $value['active']);
        endif;

        $this->db->where('recycle', 0);
        
        $query = $this->db
                        ->from('member')
                        ->get()
                        ->row();
        return $query;
    }

    public function register($value=null)
    {
        $rs = $this->db->insert('member', $value);
        return $rs;
    }

    public function update($value=null, $data=null)
    {
        if (!empty($value)){
            $this->db->where('id', $value);
        }
        $rs = $this->db->update('member', $data);
        return $rs;
    }

    public function forget_pass($value=null, $data=null)
    {
        if (!empty($value)){
            $this->db->where('email', $value);
        }
        
        $rs = $this->db->update('member', $data);
        return $rs;
    }

    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*')
                        ->from('member a')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->from('member a')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {   
        // START form filter 
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['keyword'])
                    ->or_like('a.excerpt', $param['keyword'])
                    ->or_like('a.detail', $param['keyword'])
                    ->group_end();
        }
        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.created_at,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
        }
        if ( isset($param['updateDateRange']) && $param['updateDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.updated_at,'%Y-%m-%d') BETWEEN '{$param['updateStartDate']}' AND '{$param['updateEndDate']}'");
        }     
        if ( isset($param['active']) && $param['active'] != "" ) {
            $this->db->where('a.active', $param['active']);
        }         
        // END form filter
        
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['search']['value'])
                    ->or_like('a.excerpt', $param['search']['value'])
                    ->or_like('a.detail', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.full_name";
            if ($param['order'][0]['column'] == 2) $columnOrder = "a.member_id"; 
            if ($param['order'][0]['column'] == 3) $columnOrder = "a.phone";            
            if ( $this->router->method =="data_index" ) {
                if ($param['order'][0]['column'] == 4) $columnOrder = "a.created_at";
                if ($param['order'][0]['column'] == 5) $columnOrder = "a.updated_at";
            } else if ( $this->router->method =="data_trash" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "a.recycle_at";
            }
            $this->db
                    ->order_by($columnOrder, $param['order'][0]['dir']);
        } 
        
        if ( isset($param['id']) ) 
            $this->db->where('a.id', $param['id']);

        if ( isset($param['member_id']) ) 
            $this->db->where('a.member_id', $param['member_id']);

        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);

    }
    
}

