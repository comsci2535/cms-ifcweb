<main>

    <section class="">
        <div class="container">

            <div class="row">
                <div class="offset-md-2 col-md-8">
                    <form action="" method="post">
                        <div class="input-group form-control-search">
                            <input type="text" class="form-control" id="aaa1" placeholder="คำค้นหา"
                                aria-describedby="aaa" required>
                            <div class="input-group-prepend">
                                <span class="input-group-text btn" id="aa">
                                    <span class="min-sm">ค้นหาสินค้า</span>
                                    <span class="max-sm"><i class="fas fa-search"></i></span>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-12 mt-4"> 
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <tbody>
                                <?php 
                                if(!empty($orders)):
                                    foreach($orders as $key => $item):
                                ?>
                                <tr>
                                    <td>
                                         <?php echo !empty($item['order_code']) ? $item['order_code'] :'';?>
                                    </td>
                                    <td>
                                        <?php echo !empty($item['qty']) ? $item['qty'] :'';?>
                                    </td>
                                    <td>
                                        <?php echo !empty($item['discount']) ? $item['discount'] :'';?>
                                    </td>
                                    <td class="text-right" style="min-width: 108px;">
                                        <a href="<?=site_url('member/history_detail/'.$item['id']);?>">
                                            <p class="c_blue">ดูรายละเอียด <i class="fa fa-chevron-right" aria-hidden="true"></i></p>
                                        </a>
                                        <!-- <a href="<?=site_url('orders/pdf/'.$item['id']);?>" target="_blank">
                                            <p class="c_blue" style="padding:2px;"><i class="fa fa-print"></i> ปริ้นบิลสั่งซื้อ</p>
                                        </a> -->
                                    </td>
                                </tr>
                                <?php
                                    endforeach;
                                endif; 
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </section>

    <ul class="pagination justify-content-center py-4">
        <?php echo !empty($pagination) ? $pagination : ''; ?> 
    </ul>

</main>