<main>

    <form id="form-cart-success" method="post" action="<?php echo site_url('member/history_success')?>" enctype="multipart/form-data" autocomplete="off">
        <section class="cart_payment">
            <div class="container">

                <div class="row">

                    <div class="col-md-6"> 
                        <!-- load banks -->
                        <?php echo Modules::run('banks/get_banks');?> 
                    </div>
                    <div class="col-md-4">
                        <h5><?=$this->lang->line('proof_of_transfer');?></h5>
                        <div class="form-group"> 
                            <div style="position:relative;">
                                <span class='label label-info' id="upload-file-info"></span><br>
                                <a class='btn btn-primary mt-2' href='javascript:;'>
                                    <i class="fas fa-upload"></i> อัพโหลดหลักฐาน
                                    <input type="file"
                                        style='position:absolute;height: 100%;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;'
                                        id="file_source"
                                        name="file_source" size="40"
                                        onchange='$("#upload-file-info").html($(this).val());'>
                                </a>
                            </div>
                        </div> 
                    </div>

                    <div class="col-md-12 mt-3">
                        <div class="table-redius">
                        <table class="table table-bordered table-hover" style="background: #fff;">
                            <thead>
                                <tr>
                                    <th>เลขที่สั่งซื้อ</th>
                                    <th>รายละเอียดข้อมูล</th>
                                    <th>ค่าบริการ</th>
                                    <th>ค่านำส่ง</th>
                                    <th>ยอดรวม</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php  
                                $total      = 0;
                                $sumTotal   = 0;
                                $total_size         = 0;
                                $total_weight       = 0;
                                $total_service      = 0;
                                $total_cost         = 0;
                                $total_repack_sack  = 0;
                                if(!empty($import_order)):  
                                    $tr_count = count($import_order);
                                    foreach($import_order as $key => $item):
                                        $service    = !empty($item['service']) ? $item['service'] : 0;
                                        $cost       = !empty($item['cost']) ? $item['cost'] : 0;
                                        $repack_sack = !empty($item['repack_sack']) ? $item['repack_sack'] : 0;
                                        $total      = $service + $cost + $repack_sack;
                                        $sumTotal   += $total;

                                        $total_size += !empty($item['size']) ? $item['size'] : 0;
                                        $total_weight += !empty($item['weight']) ? $item['weight'] : 0;
                                        $total_service += !empty($item['service']) ? $item['service'] : 0;
                                        $total_cost += !empty($item['cost']) ? $item['cost'] : 0; 
                                        $total_repack_sack += !empty($item['repack_sack']) ? $item['repack_sack'] : 0;
                                ?>
                                <tr>
                                    <?php
                                    if($key == 0):
                                    ?>
                                    <td rowspan="<?=$tr_count ?>">
                                        <?php echo !empty($order_code) ? $order_code : null;?>
                                    </td>
                                    <?php
                                    endif;
                                    ?>
                                    <td><?php echo !empty($item['detail']) ? $item['detail'] : '';?></td>
                                    <td><?php echo !empty($item['service_charge']) ? $item['service_charge'] : '';?></td>
                                    <td><?php echo !empty($item['import_cost']) ? $item['import_cost'] : '';?></td>
                                    <td><?php echo !empty($total) ? number_format($total, 1) : 0;?></td>
                                </tr>
                                <?php 
                                    endforeach;
                                endif;
                                ?>
                            </tbody>
                        </table>
                        </div>
                    </div>

                </div>
                <div class="text-right">
                    <br>
                    <?php
                    $total_deliverys = 0;
                    $deliverys       = !empty($import_payment['total_deliverys']) ? $import_payment['total_deliverys'] : 0;
                    if($deliverys > 0):
                        $total_deliverys = ($deliverys*20)/100;
                    endif; 

                    $sumTotal = $sumTotal + $deliverys + $total_deliverys;

                    ?> 
                    <p>จำนวน : <?php echo !empty($info->order_qty) ? $info->order_qty : 0;?></p>
                    <p>น้ำหนัก : <?php echo !empty($total_weight) ? $total_weight : 0;?></p>
                    <p>ขนาด : <?php echo !empty($total_size) ? $total_size : 0;?></p>
                    <p>ค่าบริการ : <?php echo !empty($total_service) ? number_format($total_service) : 0;?> บาท</p>
                    <p>ค่านำเข้า/ขนส่ง : <?php echo !empty($total_cost) ? number_format($total_cost) : 0;?> บาท</p>
                    <p>ค่าขนส่งในประเทศ : <?php echo !empty($deliverys) ? number_format($deliverys) : 0;?> บาท</p>
                    <p>ค่าบริการรีแพ็คลัง/กระสอบ : <?php echo !empty($total_repack_sack) ? number_format($total_repack_sack) : 0;?> </p>
                    <p>ค่าขนส่งในประเทศคำนวณเพิ่มอีก 20% : <?php echo !empty($total_deliverys) ? number_format($total_deliverys) : 0;?> บาท</p>
                    <h5>รวม <span class="c_pink"><?php echo number_format($sumTotal, 1);?></span> บาท</h5> 
                    <button type="button" id="btn--cart-success" class="btn btn-dufault mt-1">ยืนยันการสั่งซื้อ</button> 
                    <hr>
                    <input type="hidden" name="ship_address" value="<?php echo !empty($import_payment['ship_address']) ? $import_payment['ship_address'] : null;?>" style="display: :none;">
                    <input type="hidden" name="full_name" value="<?php echo !empty($import_payment['full_name']) ? $import_payment['full_name'] : null;?>" style="display: :none;">
                    <input type="hidden" name="house_number" value="<?php echo !empty($import_payment['house_number']) ? $import_payment['house_number'] : null;?>" style="display: :none;">
                    <input type="hidden" name="building" value="<?php echo !empty($import_payment['building']) ? $import_payment['building'] : null;?>" style="display: :none;">
                    <input type="hidden" name="road" value="<?php echo !empty($import_payment['road']) ? $import_payment['road'] : null;?>" style="display: :none;">
                    <input type="hidden" name="district" value="<?php echo !empty($import_payment['district']) ? $import_payment['district'] : null;?>" style="display: :none;">
                    <input type="hidden" name="amphoe" value="<?php echo !empty($import_payment['amphoe']) ? $import_payment['amphoe'] : null;?>" style="display: :none;">
                    <input type="hidden" name="province" value="<?php echo !empty($import_payment['province']) ? $import_payment['province'] : null;?>" style="display: :none;">
                    <input type="hidden" name="zipcode" value="<?php echo !empty($import_payment['zipcode']) ? $import_payment['zipcode'] : null;?>">
                    <input type="hidden" name="delivery_id" value="<?php echo !empty($import_payment['delivery_id']) ? $import_payment['delivery_id'] : null;?>" style="display: :none;">
                    <input type="hidden" name="input_kg" value="<?php echo !empty($import_payment['input_kg']) ? $import_payment['input_kg'] : null;?>">
                    <input type="hidden" name="input_queue" value="<?php echo !empty($import_payment['input_queue']) ? $import_payment['input_queue'] : null;?>" style="display: :none;">
                    <input type="hidden" name="deliverys" value="<?php echo !empty($import_payment['total_deliverys']) ? $import_payment['total_deliverys'] : 0;?>" style="display: :none;">
                    <input type="hidden" name="total_deliverys" value="<?php echo !empty($total_deliverys) ? $total_deliverys : 0;?>" style="display: :none;">
                    <input type="hidden" name="total_repack_sack" value="<?php echo !empty($total_repack_sack) ? $total_repack_sack : 0;?>" style="display: :none;">
                    <input type="hidden" name="code" value="<?php echo !empty($import_payment['code']) ? $import_payment['code'] : null;?>" style="display: :none;">
                    <input type="hidden" name="discount" value="<?=$sumTotal?>">
                </div>deliverys_repack_sack
            </div>
        </section>
    </form>

</main>