<main>

    <section class="member">
        <div class="container">
            <div class="blog-title">
                <h2 class="text"><?=$this->lang->line('text_lang_2');?></h2>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <form id="form_upload_profile" method="post">
                        <div class="profile-left">
                            <div class="top">
                                <p><?=$this->lang->line('text_lang_3');?> <?=isset($rs_mem->member_id)?$rs_mem->member_id:$rs_mem->full_name;?></p>
                                <?php 
                                $ses_mem = modules::run('member/_get_member'); 
                                if($ses_mem->picture){ ?>
                                <img id="img-profie" src="<?=base_url((isset($rs_mem->picture)) ? $rs_mem->picture : '');?>" class="wfull" alt=""
                                    onerror="this.src='<?=base_url('images/users/8.png');?>'">
                                <?php }else{ ?>
                                <img id="img-profie" src="<?php echo !empty($ses_mem->oauth_picture) ? $ses_mem->oauth_picture : '';?>" class="wfull" alt=""
                                    onerror="this.src='<?=base_url('images/users/8.png');?>'">
                                <?php } ?>
                            </div>
                            <div class="bottom pt-3">
                                <div style="position:relative;">
                                    <a class="btn btn-primary mt-2" href="javascript:;">
                                        <i class="fas fa-upload"></i> <?=$this->lang->line('upload_profile');?>
                                        <input type="file"
                                        style="position:absolute;height: 100%;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:&quot;progid:DXImageTransform.Microsoft.Alpha(Opacity=0)&quot;;opacity:0;background-color:transparent;color:transparent;"
                                        name="file_source" size="40" onchange="$(&quot;#upload-file-info&quot;).html($(this).val());">
                                        <input type="hidden" id="outfile" name="outfile" value="<?=(isset($rs_mem->picture)) ? $rs_mem->picture : ''; ?>">
                                    </a>
                                </div>
                                <a href="">
                                    <p class="py-3"><?=$this->lang->line('text_lang_4');?></p>
                                </a>
                                <input type="hidden" name="id" value="<?=isset($rs_mem->id)?$rs_mem->id:null;?>">
                                <input type="submit" hidden>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="col-md-8">
                    <form id="form_update_pass" method="post">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('user_name');?></label>
                                    <input type="text" class="form-control" name="username"
                                    value="<?=isset($rs_mem->username)?$rs_mem->username:null;?>" maxlength="50" placeholder="<?=$this->lang->line('user_name');?>...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('password');?></label>
                                    <input type="password" class="form-control" name="password" id="password" maxlength="50" placeholder="<?=$this->lang->line('password');?>...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('confirm_password');?></label>
                                    <input type="password" class="form-control" name="confirm_password" maxlength="50" placeholder="<?=$this->lang->line('confirm_password');?>...">
                                </div>
                            </div>
                            <div class="col-12 text-center">
                                <input type="hidden" name="id" value="<?=isset($rs_mem->id)?$rs_mem->id:null;?>">
                                <button type="submit" class="btn btn-dufault mr-3"><?=$this->lang->line('edit');?></button>
                            </div>
                        </div>
                    </form>
                    <form id="form_register" method="post">
                        <div class="row">
                            <div class="col-12 text-center">
                                <hr style="margin: 10px 0;">
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('fullname');?></label>
                                    <input type="text" class="form-control" name="full_name"
                                    value="<?=isset($rs_mem->full_name)?$rs_mem->full_name:null;?>" placeholder="<?=$this->lang->line('fullname');?>...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('address');?></label>
                                    <input type="text" class="form-control" name="house_number"
                                    value="<?=isset($rs_mem->house_number)?$rs_mem->house_number:null;?>" placeholder="<?=$this->lang->line('address');?>...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('building_village');?></label>
                                    <input type="text" class="form-control" name="building"
                                    value="<?=isset($rs_mem->building)?$rs_mem->building:null;?>" placeholder="<?=$this->lang->line('building_village');?>...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('street');?></label>
                                    <input type="text" class="form-control" name="road"
                                    value="<?=isset($rs_mem->road)?$rs_mem->road:null;?>" placeholder="<?=$this->lang->line('street');?>...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('Sub_district');?></label>
                                    <input type="text" class="form-control" name="district"
                                    value="<?=isset($rs_mem->district)?$rs_mem->district:null;?>" placeholder="<?=$this->lang->line('Sub_district');?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('district');?></label>
                                    <input type="text" class="form-control" name="amphoe"
                                    value="<?=isset($rs_mem->amphoe)?$rs_mem->amphoe:null;?>" placeholder="<?=$this->lang->line('district');?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('province');?></label>
                                    <input type="text" class="form-control" name="province"
                                    value="<?=isset($rs_mem->province)?$rs_mem->province:null;?>" placeholder="<?=$this->lang->line('province');?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('postal_code');?></label>
                                    <input type="text" class="form-control" name="zipcode"
                                    value="<?=isset($rs_mem->zipcode)?$rs_mem->zipcode:null;?>" maxlength="5" placeholder="<?=$this->lang->line('postal_code');?>">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('email');?></label>
                                    <input type="email" class="form-control" name="email"
                                    value="<?=isset($rs_mem->email)?$rs_mem->email:null;?>" placeholder="<?=$this->lang->line('email');?>...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('phone_number');?></label>
                                    <input type="text" class="form-control" name="phone"
                                    value="<?=isset($rs_mem->phone)?$rs_mem->phone:null;?>" maxlength="10" placeholder="<?=$this->lang->line('phone_number');?>...">
                                </div>
                            </div>
                            <div class="col-12 text-center">
                                <hr style="margin: 10px 0;">
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('tax_invoice_no');?></label>
                                    <input type="text" class="form-control" name="invoice_no"
                                    value="<?=isset($rs_mem->invoice_no)?$rs_mem->invoice_no:null;?>" maxlength="13" placeholder="<?=$this->lang->line('tax_invoice_no');?>...">
                                </div>
                            </div>
                            <div class="col-12 text-center">
                                <input type="hidden" name="id" value="<?=isset($rs_mem->id)?$rs_mem->id:null;?>">
                                <button type="submit" class="btn btn-dufault mr-3"><?=$this->lang->line('save');?></button>
                                <!-- <a href="<?=site_url('help');?>">
                                    <button type="button" class="btn btn-primary">เข้าสู่ระบบ</button>
                                </a> -->
                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </section>

</main>