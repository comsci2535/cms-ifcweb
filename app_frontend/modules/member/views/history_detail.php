<main> 
    <section class="">
        <div class="container"> 
            <div class="row">
                <div class="col-md-12 mt-4" style="font-weight: bold;">
                    <p>เลขที่บิล : <?php echo !empty($order_code) ? $order_code : '';?></p>
                    <p>รหัสสมาชิก: <?php echo !empty($member_id) ? $member_id : '';?></p>
                    <p>ชื่อ - นามสกุล : <span><?php echo !empty($full_name) ? $full_name : '';?></span></p>
                    <p>วันที่นำเข้า : <span><?php echo !empty($created_at) ? $created_at : '';?></span></p>
                </div> 
                <div class="col-md-12 mt-4"> 
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <tbody>
                                <?php 
                                $total_size             = 0;
                                $total_weight           = 0;
                                $total_service          = 0;
                                $total_cost             = 0;
                                $total_repack_sack      = 0;
                                $deliverys_price        = !empty($info->deliverys_price) ? $info->deliverys_price : 0;
                                $deliverys_price_extra  = !empty($info->deliverys_price_extra) ? $info->deliverys_price_extra : 0;
                                
                                if(!empty($import_order)):
                                    foreach($import_order as $key => $item):
                                        $total_size += !empty($item['size']) ? $item['size'] : 0;
                                        $total_weight += !empty($item['weight']) ? $item['weight'] : 0;
                                        $total_service += !empty($item['service']) ? $item['service'] : 0;
                                        $total_cost += !empty($item['cost']) ? $item['cost'] : 0;
                                        $total_repack_sack += !empty($item['repack_sack']) ? $item['repack_sack'] : 0;
                                ?>
                                <tr>
                                    <td>
                                        <?php echo !empty($item['detail_send']) ? $item['detail_send'] :'';?>
                                    </td>
                                    <td>
                                        <?php echo !empty($item['tracking_china']) ? $item['tracking_china'] :'';?>
                                    </td>
                                    <td>
                                        <?php echo !empty($item['detail']) ? $item['detail'] :'';?>
                                    </td>
                                    <td class="text-right" style="min-width: 108px;">
                                        <?php echo !empty($item['service_charge']) ? $item['service_charge'] :'';?>
                                        <?php echo !empty($item['import_cost']) ? $item['import_cost'] :'';?> 
                                    </td>
                                </tr>
                                <?php  
                                    endforeach;
                                endif;
                                ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="text-right">
                        <hr>   
                        <p>จำนวน : <?php echo !empty($info->order_qty) ? $info->order_qty : 0;?></p>
                        <p>น้ำหนัก : <?php echo !empty($total_weight) ? $total_weight : 0;?></p>
                        <p>ขนาด : <?php echo !empty($total_size) ? $total_size : 0;?></p>
                        <p>ค่าบริการ : <?php echo !empty($total_service) ? number_format($total_service) : 0;?></p>
                        <p>ค่านำเข้า/ขนส่ง : <?php echo !empty($total_cost) ? number_format($total_cost) : 0;?></p>
                        <p>ค่าขนส่งในประเทศ : <?php echo !empty($deliverys_price) ? number_format($deliverys_price) : 0;?> </p>
                        <p>ค่าบริการรีแพ็คลัง/กระสอบ : <?php echo !empty($total_repack_sack) ? number_format($total_repack_sack) : 0;?> </p>
                        <p>ค่าขนส่งในประเทศคำนวณเพิ่มอีก 20% : <?php echo !empty($deliverys_price_extra) ? number_format($deliverys_price_extra) : 0;?> </p>
                        <h5>รวม <span class="c_pink"><?php echo number_format($total_service + $total_cost + $deliverys_price + $deliverys_price_extra + $total_repack_sack);?></span> บาท</h5>
                        <?php
                         if(!empty($import_order)):
                            if(empty($is_payments)):
                        ?>
                        <a href="<?=site_url('member/history_shipping/'.$code);?>">
                            <button type="button" class="btn btn-dufault mt-1">ยืนยันการสั่งซื้อ</button>
                        </a>
                        <?php
                            endif;
                        endif;
                        ?>
                        <hr>
                    </div>
                </div>
            </div>

        </div>
    </section>

</main>