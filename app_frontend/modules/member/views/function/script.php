<script>
$('input[name="ship_address"]').change(function() {
    $('#ship_address_box').slideUp(600);
    if (this.value == 'new') {
        $('#ship_address_box').slideDown(600);
    }
});

$('.click-delivery').change(function() {
    var $this = $(this); 
    $('.display-deliverys-detail').hide()
    $('#display-deliverys-detail-'+$this.val()).show();
});

$(document).on('change', 'input[type="file"]', function(ee){  
    if(ee.target.files[0].type =="image/png" 
        || ee.target.files[0].type =="image/jpg" 
        || ee.target.files[0].type =="image/jpeg"
        || ee.target.files[0].type =="image/gif"
    ){ 
        console.log(ee.target.files[0].size);
        var $this = $(this); 
        var reader  = new FileReader();
        reader.onload = function (e) { 
            var image = new Image();  
            image.src = e.target.result;  
            image.onload = function () { 
                var height = this.height;
                var width = this.width;
                if (ee.target.files[0].size > 307200) { 
                    alert_box('กรุณาตรวจสอบขนาดรูปภาพ'); 
                    $('#upload-file-info').text('');
                    $(this).val('');
                    return false;
                }else{
                     
                    $("#form_upload_profile").submit(); 
                    $('#img-profie').attr('src', e.target.result); 
                    return true;
                } 
            }; 
        };
        reader.readAsDataURL(ee.target.files[0]);
    }else{
        $('#upload-file-info').text('');
        $(this).val('');
        alert_box('กรุณาตรวจสอบประเภทรูปภาพ'); 
    }
}); 
 
$("#form_upload_profile").submit(function(evt){	 
    evt.preventDefault();
    var formData = new FormData($(this)[0]); 
    $.ajax({
        url: 'update_img',
        type: 'POST',
        dataType: "json",
        data: formData,
        async: false,
        cache: false,
        contentType: false,
        enctype: 'multipart/form-data',
        processData: false,
        success: function (response) { 
            console.log(response);
            Swal.fire({
                title: response.status,
                text: response.message,
                type: response.status
            }).then(function() {
                if(response.status=='success') {
                    $('#outfile').val(response.picture);
                    $('#upload-file-info').text('');
                } 
            })
        }
    }); 
}); 
 
$('#form-submit-import').validate({
  rules: {
      address_china: true,
      tracking_china: true,
      qty_china: true, 
      note_china: true  
  },
  messages: { 
      address_china: {
        required: "ระบุที่อยู๋โกดัง"
      },
      tracking_china: {
        required: "ระบุเลขพัสดุจีน", 
      },
      qty_china: {
        required: "ระบุจำนวน" 
      }, 
      note_china: {
        required: "ระบุหมายเหตุ" 
      }
    },
    submitHandler: function(form) {  
      if ($(form).valid()==true) {
        $.ajax({
          url: "<?=site_url('product/import_save');?>",
          method: "POST",
          dataType: "json",
          data: $('#form-submit-import').serialize(),
          success: function (data) { 
            Swal.fire({
              title: data.status,
              text: data.message,
              type: data.status
            }).then(function() {
              if(data.status=='success') {
                var url_back = '<?=(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>';
                window.location = url_back;
              }
            })
          }
        });
      }
    },
    errorElement: "em",
})


// $('.click-delivery').click();

$(document).on('change','.click-delivery', function(){
  var delivery = $(this).val()
  var kg    = $('#text-input-kg').val()
  var queue = $('#text-input-queue').val()
  if(kg == '' && queue == ''){
    alert('กรุณาระบุน้ำหนัก');
    return false
  }
  kg    = parseInt(kg);
  queue = parseInt(queue);
  var total = 0;
  $('#text-total-calculate').html(total);
  $('#total-deliverys').val(total)
  $("#display-deliverys-detail-"+delivery+" #table-calculate-shipping tbody tr .text-input-price").each(function(){
    var $this = $(this);
    var weight_min = parseInt($this.attr('data-weight-min'))
    var weight_max = parseInt($this.attr('data-weight-max'))
    var value      = parseInt($this.val());
    console.log("weight_min = "+weight_min)
    console.log("weight_max = "+weight_max)
    console.log("value = "+value)
    console.log("=========================")
    if(weight_min <= kg && kg <= weight_max){
      total = value
      $('#text-total-calculate').html(total);
      $('#total-deliverys').val(total)
      return false
    } 

    if(weight_min <= queue && queue <= weight_max){
      total = value
      $('#text-total-calculate').html(total);
      $('#total-deliverys').val(total)
      return false
    }
  }) 

  $('#text-total-calculate').html(total);

});

$(document).on('click', '#btn--cart-success', function(){
    var file = $('#file_source').val();
    if(file != ''){
      Swal.fire({
          title: 'ยืนยันการสั่งซื้อสินค้า',
          text: 'คุณต้องการดำเนินการต่อ หรือไม่ ?',
          type: 'warning',
          showCancelButton: true,
          confirmButtonColor: '#3085d6',
          cancelButtonColor: '#d33',
          cancelButtonText: 'ยกเลิก',
          confirmButtonText: 'ยื่นยันสั่งซื้อ'
      }).then(function(result){
          if (result.value) {
              $( "#form-cart-success" ).submit();
          }
      });  
    }else{
        alert_box('กรุณาเลือกรูปภาพ');
    } 
});

$('[name="ship_address"]').change(function () {
  var province = this.value; 
    if(province == "original"){ 
      $.ajax({
        type: "POST",
        url: '<?=base_url('deliverys/ajax_deliverys_detail')?>', 
        data:{
          province    : '' 
        },
        success: function (request) { 
            console.log(request); 
            $( ".click-delivery" ).prop( "checked", false );
            $('#text-total-calculate').html('0.0');
            $('#total-deliverys').val(0)
            $('#show-load-data-new').html(request.deliverys_detail);
        },
        error: function (request, status, error) {
            console.log("ajax call went wrong:" + request.responseText);
        }
    }); 
  }else{ 
    $('[name="house_number"]').val('');
    $('[name="building"]').val('');
    $('[name="district"]').val('');
    $('[name="amphoe"]').val('');
    $('[name="province"]').val('');
    $('[name="zipcode"]').val(''); 
    $( ".click-delivery" ).prop( "checked", false );
    $('#text-total-calculate').html('0.0');
    $('#total-deliverys').val(0)
    $('.display-deliverys-detail').hide()
  }
});

$('[name="province"]').change(function () {
  var province = this.value;
    console.log(province); 
    $.ajax({
      type: "POST",
      url: '<?=base_url('deliverys/ajax_deliverys_detail')?>', 
      data:{
        province    : province 
      },
      success: function (request) { 
          console.log(request); 
          $( ".click-delivery" ).prop( "checked", false );
          $('#text-total-calculate').html('0.0');
          $('#total-deliverys').val(0)
          $('#show-load-data-new').html(request.deliverys_detail);
      },
      error: function (request, status, error) {
          console.log("ajax call went wrong:" + request.responseText);
      }
  }); 

});

</script>