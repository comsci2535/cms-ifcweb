<main>

    <section class="cart_payment">
        <div class="container"> 
            <div class="row"> 
                <form action="<?=base_url('member/history_payment/'.(!empty($code) ? $code : ''))?>" method="POST" autocomplete="off">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <h5>ที่อยู่จัดส่ง</h5>
                            </div>
                            <div class="col-md-12 col-6">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="radio" checked class="form-check-input" name="ship_address" value="original">
                                        <span>เลือกเป็นที่อยู๋จัดส่งหลัก</span>
                                    </label>
                                </div>
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input type="radio" class="form-check-input" name="ship_address" value="new">
                                        <span>เลือกเป็นที่อยู๋จัดส่งใหม่</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3" id="ship_address_box" style="display: none;">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">ชื่อ - นามสกุล</label>
                                    <input type="text" class="form-control" name="full_name" placeholder="ชื่อ - นามสกุล...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">บ้านเลขที่</label>
                                    <input type="text" class="form-control" name="house_number" placeholder="บ้านเลขที่...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">อาคาร / หมุ่บ้าน</label>
                                    <input type="text" class="form-control" name="building"
                                        placeholder="อาคาร / หมุ่บ้าน...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">ถนน</label>
                                    <input type="text" class="form-control" name="road" placeholder="ถนน...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">แขวง / ตำบล</label>
                                    <input type="text" class="form-control btn-select-send" name="district" placeholder="แขวง / ตำบล...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">เขต / อำเภอ</label>
                                    <input type="text" class="form-control btn-select-send" name="amphoe" placeholder="เขต / อำเภอ...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">จังหวัด</label>
                                    <input type="text" class="form-control btn-select-send" name="province" placeholder="จังหวัด...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="">รหัสไปรษณีย์</label>
                                    <input type="text" class="form-control btn-select-send" name="zipcode" placeholder="รหัสไปรษณีย์...">
                                </div>
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="col-md-12">
                                <h5>ช่องทางการจัดส่ง</h5>
                            </div>
                            <?php echo Modules::run('deliverys/get_deliverys');?> 
                        </div>
                    </div>
                </div> 
                <div id="show-load-data-new" class="row">
                    <?php echo Modules::run('deliverys/get_deliverys_detail');?>  
                </div> 
                <div class="row mt-3">
                    <div class="col-md-12">
                        <h5>คำนวนค่าส่ง</h5>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">น้ำหนักสินค้ารวม : กิโลกรัม</label>
                            <input type="text" class="form-control" id="text-input-kg" value="<?php echo !empty($weight) ? $weight : 0;?>" placeholder="กิโลกรัม..." name="input_kg" readonly>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="">น้ำหนักสินค้ารวม : คิว</label>
                            <input type="text" class="form-control" id="text-input-queue" placeholder="คิว..." name="input_queue" readonly>
                        </div>
                    </div>
                    <div class="col-12 text-center">
                        <h4 class="mt-3">ราคา <span id="text-total-calculate">0.0</span> บาท</h4>
                        <!-- <button type="button" id="btn-calculate-show" class="btn btn-primary mt-3"><i class="fas fa-calculator"></i> คำนวน</button> -->
                        <button type="submit" class="btn btn-dufault mt-3">ดำเนินการต่อ</button>
                        <p class="mt-4 c-red">** หมายเหตุ ค่าจัดส่งจะคำนวณเพิ่มอีก 20% เมื่อหักค่าจัดส่งของแล้วค่าส่งคงเหลือ
                        เจ้าหน้าที่ที่จะทำการโอนคืนเงินลูกค้าเต็มจำนวน
                        </p>
                        <input type="hidden" name="total_deliverys" id="total-deliverys">
                        <input type="hidden" name="code" value="<?php echo !empty($code) ? $code : '';?>">
                    </div>
                </form>
            </div>
        </div>
    </section>

</main>