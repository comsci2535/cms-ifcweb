<main> 
    <section class="cart_payment">
        <div class="container"> 
            <div class="row"> 
                <div class="col-md-12"> 
                    <form id="form-submit-import" action="#" method="POST" autocomplete="off" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('warehouse_address');?></label>
                                    <p><?php echo !empty($china_address) ? html_entity_decode($china_address) : '';?></p> 
                                    <textarea name="china_address" id="china_address" cols="30" rows="10" hidden><?php echo !empty($china_address) ? $china_address : '';?></textarea> 
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('chinese_parcel_number');?></label>
                                    <input type="text" class="form-control" id="tracking_china" name="tracking_china" placeholder="ระบุเลขพัสดุจีน..." required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('quatity');?></label>
                                    <input type="text" class="form-control" id="qty_china" name="qty_china" placeholder="ตัวอย่าง : 1 โหล" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('product_note');?></label>
                                    <input type="text" class="form-control" id="note_china" name="note_china" placeholder="ตัวอย่าง : รองเท้าแฟชั่น" required>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <h5><?=$this->lang->line('text_lang_5');?></h5>
                            </div> 
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-2">
                                    <?php echo Modules::run('attribute_sends/get_attribute_sends', 0);?> 
                                    </div>
                                    <div class="col-md-2"> 
                                    <?php echo Modules::run('type_transportations_setting/get_type_transportations_setting');?>
                                    </div>
                                    <div class="col-md-2"> 
                                    <?php echo Modules::run('attribute_sends/get_attribute_sends', 1);?>
                                    </div>
                                </div>  
                                <br>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('note');?></label>
                                    <textarea cols="30" rows="3" class="form-control" id="note_import" name="note_import" placeholder=""></textarea>
                                </div>
                            </div>
                            <div class="col-12">
                                <button type="submit" class="btn btn-dufault mt-3"><?=$this->lang->line('save');?></button>
                            </div>
                        </div>
                    </form>
                    <hr>
                </div> 
                <div class="col-md-12 mt-3">
                    <div class="table-redius table-responsive" style="overflow: auto;">
                        <table class="table table-bordered table-hover" style="background: #fff;">
                            <thead>
                                <tr>
                                    <th class="text-center" style="w"><?=$this->lang->line('no');?></th>
                                    <th class="text-center"><?=$this->lang->line('status');?></th>
                                    <th class="text-center"><?=$this->lang->line('chinese_parcel_number');?></th>
                                    <th class="text-center"><?=$this->lang->line('cabinet_date');?></th>
                                    <th class="text-center"><?=$this->lang->line('recording_date');?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                if(!empty($order_import)):
                                    $i = 1;
                                    foreach($order_import as $item):
                                        $tracking_china         = !empty($item->tracking_china) ? $item->tracking_china : '';
                                        $tracking_china_text    = str_replace(","," ,", $tracking_china);
                                ?>
                                <tr>
                                    <td class="text-center"><?=$i;?></td>
                                    <td class="text-center"><?php echo !empty($item->status) ? $item->status : '';?></td>
                                    <td><p><?php echo $tracking_china_text;?></p></td>
                                    <td class="text-center"><?php echo !empty($item->date_up) ? $item->date_up : '-';?></td>
                                    <td class="text-center"><?php echo !empty($item->created_at) ? $item->created_at : '';?></td>
                                </tr>
                                <?php 
                                    $i++;
                                    endforeach;
                                endif;
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <ul class="pagination justify-content-center py-4">
        <?php echo !empty($pagination) ? $pagination : ''; ?> 
    </ul> 
</main>