<main>

    <section class="cart_payment">
        <div class="container">

            <div class="row">

                <div class="col-md-12 text-center">
                    <img src="<?=base_url('images/success.jpg');?>" style="width: 90px;margin-bottom: 25px;" alt="">

                    <h3>
                        เราได้รับคำสั่งซื้อของคุณแล้ว <br>
                        ขอบคุณที่ไว้วางใจในบริการของเรา
                    </h3>
                    <br>
                    <p>เลขคำสั่งซื้อของคุณคือ : <span class="c_pink"><?php echo !empty($order_code) ? $order_code : null;?></span></p>
                    <p>คุณจะได้รับอีเมลการสั่งซื้อ พร้อมรายละเอียดคำสั่งซื้อ และรายละเอียดการชำระเงิน</p>
                    <a class='btn btn-dufault mt-2' href="<?=base_url('member/history')?>">ประวัติการสั่งซื้อ</a>
                </div>

            </div>

        </div>
    </section>

</main>