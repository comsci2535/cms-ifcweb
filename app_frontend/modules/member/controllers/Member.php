<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends MX_Controller {

	public function __construct()
	{
        parent::__construct();
        $this->load->helper('string');
        $this->load->model('member_m');
        $this->load->model('orders/orders_m');
        $this->load->model('status/status_m'); 
        $this->load->library('uploadfile_library');

        // $this->load->library('facebook');
        // $this->load->model('facebook_model');


        $this->order_type = array('ฝากซื้อ', 'นำเข้าสินค้า');
	}

    private function seo()
	{
		$title          = "IFC | Member";
		$robots         = "noindex,nofollow";
		$description    = "titlewebtitleweb";
		$keywords       = "titleweb,titleweb";
		$meta  			= '<TITLE>'.$title.'</TITLE>';
		$meta 		   .= '<meta name="robots" content="'.$robots.'"/>';
		$meta		   .= '<meta name="description" content="'.$description.'"/>';
        $meta 		   .= '<meta name="keywords" content="'.$keywords.'"/>';
        $meta 		   .= '<meta property="og:url" content="'.site_url().'" />';
        $meta 		   .= '<meta property="og:type" content="website" />';
        $meta 		   .= '<meta property="og:title" content="'.$title.'" />';
        $meta 		   .= '<meta property="og:description" content="'.$description.'" />';
        $meta 		   .= '<meta property="og:image" content="'.base_url('images/logo/logo.png').'" />';
		return $meta;
	}

	public function profile()
	{
        if(!empty($this->session->userdata['ses_mem']->id)):
            $data = array(
                'seo'     => $this->seo(),
                'menu'    => 'mem_pro',
                'header'  => 'header',
                'content' => 'profile',
                'footer'  => 'footer',
                'function'=>  array('member'),
            );
            $data['rs_mem'] = $this->_get_member();
            $this->load->view('template/body', $data);
        else:
            redirect(site_url(), 'refresh');
        endif;
    }

    public function imported()
	{
        if(!empty($this->session->userdata['ses_mem']->id)):
            $data = array(
                'seo'     => $this->seo(),
                'menu'    => 'mem_imp',
                'header'  => 'header',
                'content' => 'imported',
                'footer'  => 'footer',
                'function'=>  array('member'),
            );

            // load config money
            $china_address  = Modules::run('configs/get_setting', 'setting_china_addres');  
            $data['china_address']   = !empty($china_address['china_address']) ? $china_address['china_address'] : '';

            $input['order_type'] = 1;
            $input['user_id']   = $this->session->userdata['ses_mem']->id;
            
            $per_page = 4;
            $page = !empty($this->input->get('per_page')) ? ($this->input->get('per_page')-1) : 0; // เลขหน้าที่จะถูกส่งมาเช่น home/member/3
    
            $uri                = 'member/imported'; // ชี้หน้าเพจหลักที่จะใช้งานมาที่ home/member
            $segment            = $page;
            $total              = $this->orders_m->get_orders_detail_import_count_param($input); // จำนวนข้อมูลทั้งหมด
            $config['per_page'] = $per_page; // จำนวนข้อมูลต่อหน้า 
            $data['pagination'] = $this->pagin($uri, $total, $segment, $per_page); // เลขหน้า 
            $input['length']    = $per_page;
            $input['start']     = ($page*$per_page); 
            
            $order_import = $this->orders_m->get_orders_detail_import_param($input)->result();
            if(!empty($order_import)):
                foreach($order_import as $item):
                    $input_['status_id'] = $item->status;
                    $status = $this->status_m->get_rows($input_)->row();
                    $item->status = !empty($status->title) ? $status->title : '';
                endforeach;
            endif;
            $data['order_import'] = $order_import; 

            $this->load->view('template/body', $data);
        else:
            redirect(site_url(), 'refresh');
        endif;
    }

    public function history()
	{
        if(!empty($this->session->userdata['ses_mem']->id)):
            $data = array(
                'seo'     => $this->seo(),
                'menu'    => 'mem_his',
                'header'  => 'header',
                'content' => 'history',
                'footer'  => 'footer',
                'function'=>  array('member'),
            );

            $column             = array();
            $input['recycle']   = 0;
            $input['user_id']   = $this->session->userdata['ses_mem']->id;

            $per_page           = 5;
            $page = !empty($this->input->get('per_page')) ? ($this->input->get('per_page')-1) : 0; // เลขหน้าที่จะถูกส่งมาเช่น home/member/3
    
            $uri                = 'member/history'; // ชี้หน้าเพจหลักที่จะใช้งานมาที่ home/member
            $segment            = $page;
            $total              = $this->orders_m->get_orders_count_param($input); // จำนวนข้อมูลทั้งหมด
            $config['per_page'] = $per_page; // จำนวนข้อมูลต่อหน้า 
            $data['pagination'] = $this->pagin($uri, $total, $segment, $per_page); // เลขหน้า 
            $input['length']    = $per_page;
            $input['start']     = ($page*$per_page); 
            
            $info = $this->orders_m->get_orders_param($input)->result(); 

            foreach ($info as $key => $rs):

                $id  = encode_id($rs->order_id); 

                $input_['status_id'] = $rs->status;
                $infoStatus = $this->status_m->get_rows($input_)->row(); 

                $text_order = '<p>';
                $text_order.= 'เลขที่บิล : <span class="text-order-code">'.$rs->order_code.'</span>';
                $text_order.= '<br>ขึ้นตู้ : '.(!empty($rs->updated_at) ? date_languagefull($rs->updated_at, false, ""): null);
                $text_order.= '<br>สถานะ : <span class="c_pink">'.(!empty($infoStatus->title) ? $infoStatus->title : '').'</span>';
                $text_order.= '<br>ประเภท : <span class="c_green">'.$this->order_type[$rs->order_type].'</span>';
                $text_order.= '</p>'; 

                $text_qty = '<p>';
                $text_qty.= 'จำนวนชิ้น  : <span class="c_pink">'.(!empty($rs->order_qty) ? $rs->order_qty  : 0).'</span>'; 
                $text_qty.= '</p>'; 
                
                $total          = 0;
                $import_order   = Modules::run('separate_orders/get_order_import', $rs->order_id);
                if(!empty($import_order)):
                    foreach($import_order as $item):
                        $total += $item['service'] + $item['cost']; 
                    endforeach;
                endif;
                $sumTotal           = 0;
                $input__['order_id'] = $rs->order_id;
                $orders_detail = $this->orders_m->get_orders_detail_by_param($input__)->result();
                if(!empty($orders_detail)):
                    foreach($orders_detail as $detail):
                        $sumTotal  += $detail->product_price * $detail->quantity; 
                    endforeach;
                endif;

                $deliverys_price        = !empty($rs->deliverys_price) ? $rs->deliverys_price : 0;
                $deliverys_price_extra  = !empty($rs->deliverys_price_extra) ? $rs->deliverys_price_extra : 0;
                $deliverys_repack_sack  = !empty($rs->deliverys_repack_sack) ? $rs->deliverys_repack_sack : 0;

                $total  = $total + $sumTotal + $deliverys_price + $deliverys_price_extra + $deliverys_repack_sack;
                
                $text_discount = '<p>';
                $text_discount.= 'ยอดรวมค่า'.$this->order_type[$rs->order_type].'  : <span class="c_pink">'.number_format($total).'</span> บาท'; 
                $text_discount.= '</p>';

                $column[$key]['id']             = $id;
                $column[$key]['name']           = $rs->name.' '.$rs->lasname;
                $column[$key]['order_code']     = $text_order;
                $column[$key]['qty']            = $text_qty;
                $column[$key]['email']          = $rs->email;
                $column[$key]['tracking_code']  = $rs->tracking_code;
                $column[$key]['discount']       = $text_discount; 
                $column[$key]['created_at']     = datetime_table($rs->created_at);
                $column[$key]['updated_at']     = datetime_table($rs->updated_at); 
            endforeach; 

            $data['orders'] = $column;

            $this->load->view('template/body', $data);
        else:
            redirect(site_url(), 'refresh');
        endif;
    }

    public function history_detail($code)
	{
        if(!empty($this->session->userdata['ses_mem']->id)):
            $data = array(
                'seo'     => $this->seo(),
                'menu'    => 'mem_his',
                'header'  => 'header',
                'content' => 'history_detail',
                'footer'  => 'footer',
                'function'=>  array('member'),
            );
            $input['user_id']               = $this->session->userdata['ses_mem']->id;
            $input['recycle']               = 0;
            $input['order_id']              = decode_id($code);
            
            $info                           = $this->orders_m->get_orders($input)->row();  
            $title                          = !empty($info->order_code) ? $info->order_code : ""; 
            $data['code']                   = $code;
            $data['order_code']             = $title;
            $data['created_at']             = $info->created_at;
            $data['info']                   = $info;
            $input['recycle']               = 0;
            $input['id']                    = $info->user_id;
            $members                        = $this->member_m->get_rows($input)->row();  
            $data['full_name']              = $members->full_name;
            $data['member_id']              = $members->member_id; 

            $input_['type']                 = 1;
            $input_['order_id']             = $info->order_id;
            $orders_payments                = $this->orders_m->get_orders_payments_by_param($input_)->row();
            if(!empty($orders_payments)):
                $data['is_payments']        = 1;
            endif;

            $data['import_order']   = Modules::run('separate_orders/get_order_import', $info->order_id); 

            $this->load->view('template/body', $data);
        else:
            redirect(site_url(), 'refresh');
        endif;
    }

    public function history_shipping($code)
	{
        if(!empty($this->session->userdata['ses_mem']->id)):
            $data = array(
                'seo'     => $this->seo(),
                'menu'    => 'mem_his',
                'header'  => 'header',
                'content' => 'history_shipping',
                'footer'  => 'footer',
                'function'=>  array('member'),
            );

            $input['user_id']               = $this->session->userdata['ses_mem']->id;
            $input['recycle']               = 0;
            $input['order_id']              = decode_id($code);
            $info                           = $this->orders_m->get_orders($input)->row(); 
            $import_order                   = Modules::run('separate_orders/get_order_import', $info->order_id);
            
            $weight         = 0;
            if(!empty($import_order)):
                foreach($import_order as $item):
                    $weight+= !empty($item['weight']) ? $item['weight'] : 0;
                endforeach;
            endif;


            $member_arr = array();
            $member     = $this->_get_member(); 
            if(!empty($member)):
                $member_arr['district'] = $member->district;
                $member_arr['amphoe']   = $member->amphoe;
                $member_arr['province'] = $member->province;
                $member_arr['zipcode']  = $member->zipcode;
            endif;

            $data['member'] = $member_arr;
            $data['code']   = $code;
            $data['weight']   = $weight;

            $this->load->view('template/body', $data);
        else:
            redirect(site_url(), 'refresh');
        endif;
    }

    public function history_payment($code)
	{
        if(!empty($this->session->userdata['ses_mem']->id)):
            $data = array(
                'seo'     => $this->seo(),
                'menu'    => 'mem_his',
                'header'  => 'header',
                'content' => 'history_payment',
                'footer'  => 'footer',
                'function'=>  array('member'),
            );

            $input = $this->input->post();

            $input['user_id']               = $this->session->userdata['ses_mem']->id;
            $input['recycle']               = 0;
            $input['order_id']              = decode_id($code);
            $info                           = $this->orders_m->get_orders($input)->row(); 
            $import_order                   = Modules::run('separate_orders/get_order_import', $info->order_id);
    
            $data['info']           = $info;
            $data['order_code']     = $info->order_code;
            $data['import_payment'] = $input;
            $data['import_order']   = $import_order;
            $data['code']           = $code; 

            $this->load->view('template/body', $data);
        else:
            redirect(site_url(), 'refresh');
        endif;
    }

    public function history_success()
	{ 

        $input                  = $this->input->post(); 
        $input['payment_type']  = 0;
        $input['type']          = 1; 
        $code                   = decode_id($input['code']);  
        $input_['user_id']      = $this->session->userdata['ses_mem']->id;
        $input_['recycle']      = 0;
        $input_['order_id']     = decode_id($input['code']);
        $info                    = $this->orders_m->get_orders($input_)->row();  
        if(!empty($info)):
            $member = Modules::run('member/_get_member');  
            if(!empty($input['ship_address'])): 
                if($input['ship_address'] == 'original'): 
                    $house_number   = !empty($member->address) ? $member->address:'';
                    $building       = !empty($member->building) ? $member->building:'';
                    $road           = !empty($member->road) ? $member->road:'';
                    $address        = $house_number.' '.$building.' '.$road;
                    
                    $value_code['address']           = !empty($address) ? $address:'';
                    $value_code['provinces']         = !empty($member->province) ? $member->province:'';
                    $value_code['amphures']          = !empty($member->amphoe) ? $member->amphoe:'';
                    $value_code['districts']         = !empty($member->district) ? $member->district:'';
                    $value_code['zip_code']          = !empty($member->zipcode) ? $member->zipcode:'';
                    $value_code['tel']               = !empty($member->phone) ? $member->phone:'';
                    $value_code['email']             = !empty($member->email) ? $member->email:''; 
                endif;
                if($input['ship_address'] == 'new'): 

                    if(!empty($input['full_name'])): 
                        list($name, $lasname) = explode(' ', $input['full_name']);
                    endif;  
                    $house_number   = !empty($input['house_number']) ? $input['house_number']:'';
                    $building       = !empty($input['building']) ? $input['building']:'';
                    $road           = !empty($input['road']) ? $input['road']:'';
                    $address        = $house_number.' '.$building.' '.$road;
                    $value_code['name']              = !empty($name) ? $name : '';
                    $value_code['lasname']           = !empty($lasname) ? $lasname : '';
                    $value_code['address']           = !empty($address) ? $address:'';
                    $value_code['provinces']         = !empty($input['province']) ? $input['province']:'';
                    $value_code['amphures']          = !empty($input['amphoe']) ? $input['amphoe']:'';
                    $value_code['districts']         = !empty($input['district']) ? $input['district']:'';
                    $value_code['zip_code']          = !empty($input['zipcode']) ? $input['zipcode']:'';
                    $value_code['tel']               = !empty($member->phone) ? $member->phone:'';
                    $value_code['email']             = !empty($member->email) ? $member->email:''; 
                endif;
            endif;
            $value_code['transport_id']             = !empty($input['delivery_id']) ? $input['delivery_id'] : 0;
            $value_code['deliverys_price']          = !empty($input['deliverys']) ? $input['deliverys'] : 0;
            $value_code['deliverys_price_extra']    = !empty($input['total_deliverys']) ? $input['total_deliverys'] : 0;
            $value_code['deliverys_repack_sack']    = !empty($input['total_repack_sack']) ? $input['total_repack_sack'] : 0; 
            $value_code['status']                   = 3;  
            if($this->db->update('orders',$value_code, array('order_id' => $code))): 
                $orders_payments = $this->_build_data_orders_payments($code, $input);
                $this->orders_m->insert_orders_payments($orders_payments);
                $data_['order_id']   = $code;
                $data_['status']     = 3;
                $data_['is_process'] = 0;
                $data_['title']      = null;
                $data_['process']    = null;
                $this->set_orders_status_action($data_); 

                redirect(site_url("{$this->router->class}/payment_success/".$input['code']));
            else:
                redirect(site_url("{$this->router->class}/history"));
            endif;
        else:
            redirect(site_url("{$this->router->class}/history"));
        endif;  
    }

    public function payment_success($code)
	{
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'mem_his',
            'header'  => 'header',
            'content' => 'history_success',
            'footer'  => 'footer',
            'function'=>  array('member'),
        );
  
        $input_['user_id']      = $this->session->userdata['ses_mem']->id;
        $input_['recycle']      = 0;
        $input_['order_id']     = decode_id($code);
        $info                    = $this->orders_m->get_orders($input_)->row(); 

        $data['order_code']         = !empty($info->order_code) ? $info->order_code : ''; 

        $this->load->view('template/body', $data);
    }

    private function set_orders_status_action($data)
    {
        $value['order_id']      = $data['order_id'];
        $value['status']        = $data['status'];
        $value['is_process']    = $data['is_process'];
        $value['title']         = $data['title'];
        $value['process']       = $data['process'];
        $value['created_at']    = db_datetime_now();
        $value['created_by']    = 0;

        $this->orders_m->insert_orders_status_action($value);
    }

    private function _build_data_orders_payments($order_id, $input)
    { 
 
        $value['order_id']          = $order_id; 
        $value['bank_id']           = !empty($input['bank_id']) ? $input['bank_id'] : ''; 
        $value['payment_type']      = !empty($input['payment_type']) ? $input['payment_type'] : 0; 
        $value['discount']          = !empty($input['discount']) ? $input['discount'] : 0; 
        $value['type']              = !empty($input['type']) ? $input['type'] : 0; 
        $value['active']            = 1;  
        // Add file form product
        $path                = 'payment';
        $upload              = $this->uploadfile_library->do_upload('file_source',TRUE,$path);
        $file                = '';
        if(isset($upload['index'])){
            $file = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name']; 
            $value['file']      = $file;
        }

        if(!empty($this->session->userdata['ses_mem']->id)):
            $value['user_id'] = !empty($this->session->userdata['ses_mem']->id) ? $this->session->userdata['ses_mem']->id : 0;
        endif;
        $value['created_at'] = db_datetime_now();
        $value['created_by'] = 0;
        $value['updated_at'] = db_datetime_now();
        $value['updated_by'] = 0;
       
        return $value;
    }

    public function _get_member()
	{
        $getRows['id'] = $this->session->userdata['ses_mem']->id;
        $rs_member = $this->member_m->getRows($getRows);
        return $rs_member;
    }

    public function member_save()
	{
        $input = $this->input->post(null, true);

        if (!empty($input['id'])) {
            if (!empty($input['username'])) {
                $res = $this->member_update_pass();
            } else {
                $res = $this->member_update();
            }
            echo json_encode($res);
            exit(0);
        }

        $system_date = date("Y-m-d H:i:s");
        
        $data = array(
            'username'      => $input['username'],
            'password'      => password_hash($input['password'],PASSWORD_DEFAULT),
            'full_name'     => $input['full_name'],
            'house_number'  => $input['house_number'],
            'building'      => $input['building'],
            'road'          => $input['road'],
            'district'      => $input['district'],
            'amphoe'        => $input['amphoe'],
            'province'      => $input['province'],
            'zipcode'       => $input['zipcode'],
            'email'         => $input['email'],
            'phone'         => $input['phone'],
            'created'       => $system_date,
            'updated_at'    => $system_date,
            'created_at'    => $system_date,
            'active'        => 1
        );
        $info = $this->member_m->register($data);
        if ($info==true) {
            $check_login = $this->check_login($input['username'], $input['password']); 
            if ($check_login['status']=='success') {
                $res['status'] = "success";
                $res['message'] = $this->lang->line('text_lang_11');       
            }else{
                $res['status'] = "success";
                $res['message'] = $this->lang->line('text_lang_13');       
            }
        }else{
            $res['status'] = "warning";
            $res['message'] = $this->lang->line('text_lang_12');    
        }
        echo json_encode($res);
    }

    private function member_update()
	{
        $input          = $this->input->post(null, true);
        $system_date    = date("Y-m-d H:i:s");

        $data = array(
            'full_name'     => $input['full_name'],
            'house_number'  => $input['house_number'],
            'building'      => $input['building'],
            'road'          => $input['road'],
            'district'      => $input['district'],
            'amphoe'        => $input['amphoe'],
            'province'      => $input['province'],
            'zipcode'       => $input['zipcode'],
            'email'         => $input['email'],
            'phone'         => $input['phone'],
            'invoice_no'    => $input['invoice_no'],
            'modified'      => $system_date,
            'updated_at'    => $system_date,
        );
        $info = $this->member_m->update($input['id'], $data);
        if ($info==true) {
            $res['status'] = "success";
            $res['message'] = $this->lang->line('text_lang_14');      
        }else{
            $res['status'] = "warning";
            $res['message'] = $this->lang->line('text_lang_15');    
        }
        return $res;
    }

    public function update_img()
    {

        $input = $this->input->post(null, true);
        $path = 'users';
        $upload = $this->uploadfile_library->do_upload('file_source',TRUE,$path); 
        $file = '';
		if(isset($upload['index'])){
            $file = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
            $outfile = $input['outfile'];
			if(isset($outfile)){
				$this->load->helper("file");
				@unlink($outfile);
            }
            $data['picture'] = $file; 
        } 

        $info = $this->member_m->update($input['id'], $data);

        if ($info==true):
            $res['status']      = "success";
            $res['picture']     = $file;
            $res['message']     = $this->lang->line('text_lang_16');   
        else:
            $res['status']      = "warning";
            $res['message']     = $this->lang->line('text_lang_17');    
        endif; 
        echo json_encode($res);
    }

    private function member_update_pass()
	{
        $input = $this->input->post(null, true);

        $data = array(
            'username' => $input['username'],
            'password' => password_hash($input['password'],PASSWORD_DEFAULT),
            'modified' => date("Y-m-d H:i:s"),
        );
        $info = $this->member_m->update($input['id'], $data);
        if ($info==true) {
            $res['status'] = "success";
            $res['message'] = $this->lang->line('text_lang_14');      
        }else{
            $res['status'] = "warning";
            $res['message'] = $this->lang->line('text_lang_15');    
        }
        return $res;
    }

    private function sentMail($data)
    { 

        $temp  = Modules::run('configs/get_setting', 'mail');  

        if(!empty($temp)):
            // load mail config  
            $Host 		= !empty($temp['SMTPserver']) ? $temp['SMTPserver'] : '';
            $Username 	= !empty($temp['SMTPusername']) ? $temp['SMTPusername'] : '';
            $Password 	= !empty($temp['SMTPpassword']) ? $temp['SMTPpassword'] : '';
            $SMTPSecure = 'ssl';
            $Port 		= !empty($temp['SMTPport']) ? $temp['SMTPport'] : '';  
            $viewMail = $this->load->view('email/forget_password', $data, TRUE);

            // load mail send config 
            
            require 'app_frontend/third_party/phpmailer/PHPMailerAutoload.php';
            $mail = new PHPMailer;
            $mail->SMTPDebug = 0;                               	// Enable verbose debug output
            
            $mail->isSMTP();                                      	// Set mailer to use SMTP
            $mail->Host 		= $Host;              				// Specify main and backup SMTP servers
            $mail->SMTPAuth 	= true;                             // Enable SMTP authentication
            $mail->Username 	= $Username;                		// SMTP username
            $mail->Password 	= $Password;                        // SMTP password
            $mail->SMTPSecure 	= $SMTPSecure;                      // Enable TLS encryption, `ssl` also accepted
            $mail->Port 		= $Port;                            // TCP port to connect to
            $mail->CharSet 		= 'UTF-8';
            
            $mail->From 		= $Username;
            $mail->FromName 	= $Username;

            $email_to 			= $data['email'];

            $mail->addAddress($email_to);               			// Name is optional
            $mail->isHTML(false);                                  	// Set email format to HTML

            $mail->Subject = $email_to;
            $mail->Body    = $viewMail;
            $mail->AltBody = $viewMail; 
            // $mail->Send();
            $message = $mail->Send();
            // $mail->ErrorInfo; 
            return $message;
        endif;
    }

    public function member_forget_pass()
	{
        $input = $this->input->post(null, true);
        $new_passwors = random_string('alnum', 5);

        $data_arr['email']              = $input['email']; 
        $data_arr['new_passwors']       = $new_passwors; 
        $sentMail = $this->sentMail($data_arr);
        // echo json_encode($sentMail);
        // exit(0);

        if( $sentMail == true ){
            // forget_pass
            $data = array(
                'password' => password_hash($new_passwors,PASSWORD_DEFAULT),
                'modified' => date("Y-m-d H:i:s"),
            );    
            $info = $this->member_m->forget_pass($input['email'], $data);
            if ($info==true) {
                $res['status'] = "success";
                $res['message'] = "รหัสผ่านใหม่ถูกส่งไปยังอีเมลของคุณ. (".$input['email'].")";           
            }else{
                $res['status'] = "warning";
                $res['message'] = $this->lang->line('text_lang_15');    
            }
        }else{
            $res['status'] = "warning";
            $res['message'] = $this->lang->line('text_lang_18');   
            // show_error($this->email->print_debugger());
        }
        echo json_encode($res);
    }

    public function check_match()
	{
        $input = $this->input->post(null, true);

        $info = $this->member_m->check_match($input);
        if ($info == 0) {
            echo 'true';
        }else{
            echo 'false';
        }
    }

    private function check_login($username=null, $password=null)
    {
        $getRows['username'] = $username;
        $getRows['active']   = 1; 
        $rs_member = $this->member_m->getRows($getRows);
        if($rs_member){
            if (password_verify($password, $rs_member->password)){ 
                $this->session->set_userdata('ses_mem', $rs_member);
                // Modules::run('carts/add_data_cart'); //get data cart on database
                $res['status'] = "success";
                $res['message'] = $this->lang->line('text_lang_19');    
            }else{
                $res['status'] = "warning";
                $res['message'] = $this->lang->line('text_lang_20'); 
            }
        }else{
            $res['status'] = "warning";
            $res['message'] = $this->lang->line('text_lang_21'); 
        }
        return $res;
    }

    public function login()
	{
        $input = $this->input->post(null, true);
        $res = $this->check_login($input['username'], $input['password']);
        echo json_encode($res);
    }

    public function logout()
	{
        $this->session->sess_destroy();
        redirect( site_url('home'), 'refresh' );
    }
    
}