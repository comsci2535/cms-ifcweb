<?php 
if(!empty($type_transportations_setting)):
  foreach($type_transportations_setting as $key => $transportations_setting):
    $checked = '';
    if($key == 0):
      $checked = 'checked';
    endif; 
?>
  
    <div class="col-md-12 col-12">
      <div class="form-check">
          <label class="form-check-label">
              <input type="radio" class="form-check-input" 
              name="type_transportations_setting_id" <?=$checked?> 
              value="<?php echo !empty($transportations_setting->type_transportations_setting_id) ? $transportations_setting->type_transportations_setting_id : '';?>">
              <span><?php echo !empty($transportations_setting->title) ? $transportations_setting->title : '';?></span>
          </label>
      </div>
    </div> 
<?php
  
  endforeach;
endif;
?>