<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Type_transportations_setting extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('type_transportations_setting_m');
	} 

	public function get_type_transportations_setting()
	{ 
        $input['active']            = 1;
        $input['recycle']           = 0; 
        $type_transportations_setting            = $this->type_transportations_setting_m->get_rows($input)->result();    
        $data['type_transportations_setting']    = $type_transportations_setting; 
        $this->load->view('type_transportations_setting', $data);
    }
    
}