<main>

  <section>
    <?php if(!empty($questions && $this->uri->segment(3)=='คำถามที่พบบ่อย')): ?>
    <div class="container">
      <div class="blog-title">
        <h2 class="text"><?=$this->lang->line('question');?></h2>
      </div>
      <div id="accordion">
      <?php foreach($questions as $key => $item): ?>
        <div class="row">
          <div class="col-md-12 col-12">
            <div class="card">
              <div class="card-header">
                <a class="collapsed card-link" data-toggle="collapse" href="#collapse<?=$key;?>">
                  <i class="fas fa-question"></i> <?php echo !empty($item->title) ? $item->title : null;?>
                  <i class="fas fa-chevron-<?php echo $key==0 ? "up" : "down";?>" style="float: right;"></i>
                </a>
              </div>
              <div id="collapse<?=$key;?>" class="collapse <?php if ($key==0){echo 'show';} ?>" data-parent="#accordion">
                <div class="card-body">
                <p><?php echo !empty($item->excerpt) ? html_entity_decode($item->excerpt) : null;?></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php endforeach;?>
      </div>
    </div>
    <?php endif;?>

    <?php if(!empty($uses && $this->uri->segment(3)=='วิธีการใช้งาน')): ?>
    <div class="container">
      <div class="blog-title">
        <h2 class="text"><?=$this->lang->line('how_to_website');?></h2>
      </div>
      <div id="accordion-uses">
      <?php foreach($uses as $keys => $items): ?>
      <div class="row">
        <div class="col-md-12 col-12">
          <div class="card">
            <div class="card-header">
              <a class="collapsed card-link" data-toggle="collapse" href="#collapse-uses<?=$keys;?>">
              <i class="fas fa-question"></i> <?php echo !empty($items->title) ? $items->title : null;?>
              <i class="fas fa-chevron-<?php echo $keys==0 ? "up" : "down";?>" style="float: right;"></i>
              </a>
            </div>
            <div id="collapse-uses<?=$keys;?>" class="collapse <?php if ($keys==0){echo 'show';} ?>" data-parent="#accordion-uses">
              <div class="card-body">
                <p><?php echo !empty($items->excerpt) ? html_entity_decode($items->excerpt) : null;?></p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php endforeach;?>
      </div>
    </div>
    <?php endif;?>

  </section>

</main>