<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Help extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('questions_m');
        $this->load->model('uses_m');
	}

    private function seo($seo = null)
	{
		$title          = !empty($seo->meta_title) ? $seo->meta_title : "IFC | Service";
		$robots         = !empty($seo->meta_title) ? $seo->meta_title : "";
		$description    = !empty($seo->meta_description) ? $seo->meta_description : "";
        $keywords       = !empty($seo->meta_keyword) ? $seo->meta_keyword : "";
        $url            = !empty($seo->file) ? base_url($seo->file) : base_url('images/logo/logo.png');
		$meta  			= '<TITLE>'.$title.'</TITLE>';
		$meta 		   .= '<meta name="robots" content="'.$robots.'"/>';
		$meta		   .= '<meta name="description" content="'.$description.'"/>';
        $meta 		   .= '<meta name="keywords" content="'.$keywords.'"/>';
        $meta 		   .= '<meta property="og:url" content="'.site_url().'" />';
        $meta 		   .= '<meta property="og:type" content="website" />';
        $meta 		   .= '<meta property="og:title" content="'.$title.'" />';
        $meta 		   .= '<meta property="og:description" content="'.$description.'" />';
        $meta 		   .= '<meta property="og:image" content="'.$url.'" />';
		return $meta;
	}

	public function index()
	{

        $obj_seo = Modules::run('configs/get_setting', 'general');
        $sco = new stdClass();
        $sco->meta_title = !empty($obj_seo['siteTitle']) ? $obj_seo['siteTitle'] : '';
        $sco->meta_description = !empty($obj_seo['metaDescription']) ? $obj_seo['metaDescription'] : '';
        $sco->meta_keyword = !empty($obj_seo['metaKeyword']) ? $obj_seo['metaKeyword'] : '';

        $input['order'][0]['column']    = 0;
        $input['order'][0]['dir']       = 'asc';
        $input['active']                = 1;
        $input['recycle']               = 0;
        $questions                      = $this->questions_m->get_rows($input)->result();
        if(!empty($questions)):
            foreach($questions as $item):
                if(CURRENT_LANG=='en'):
                    $item->title     = !empty($item->title_en) ? $item->title_en : $item->title;
                    $item->excerpt   = !empty($item->excerpt_en) ? $item->excerpt_en : $item->excerpt;
                    $item->detail    = !empty($item->detail_en) ? $item->detail_en : $item->detail;  
                endif;
            endforeach;
        endif; 
        $uses                           = $this->uses_m->get_rows($input)->result();  
        if(!empty($uses)):
            foreach($uses as $item):
                if(CURRENT_LANG=='en'):
                    $item->title     = !empty($item->title_en) ? $item->title_en : $item->title;
                    $item->excerpt   = !empty($item->excerpt_en) ? $item->excerpt_en : $item->excerpt;
                    $item->detail    = !empty($item->detail_en) ? $item->detail_en : $item->detail; 
                endif;
            endforeach;
        endif; 

        $data = array(
            'seo'     => $this->seo($sco),
            'menu'    => 'help',
            'header'  => 'header',
            'content' => 'help',
            'footer'  => 'footer',
            'function'=>  array('help'),
        );

        $data['questions']      = $questions;
        $data['uses']           = $uses;
        $this->load->view('template/body', $data);
    }
    
}