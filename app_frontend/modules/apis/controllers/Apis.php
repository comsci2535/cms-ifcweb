<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Apis extends MX_Controller {

    private $token;
	public function __construct()
	{
        parent::__construct();
        header('Content-Type: application/json');
        $this->token = "Token 2e31a243e1a66bd8738e6a2dff6fa2cc70d08c1e";
	} 

	public function get()
	{
        $id  = $this->input->post('id');
        $urlCall  = $this->input->post('urlCall');
        $url = $urlCall.$id;

        $curl = curl_init(); 
        curl_setopt_array($curl, array(
            CURLOPT_URL             => $url, 
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_ENCODING        => "",
            CURLOPT_MAXREDIRS       => 10,
            CURLOPT_TIMEOUT         => 0,
            CURLOPT_FOLLOWLOCATION  => true, 
            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1, 
            CURLOPT_CUSTOMREQUEST   => "GET", 
            CURLOPT_HTTPHEADER      => array("Authorization: {$this->token}")
            )
        );
            
        $response   = curl_exec($curl);
        curl_close($curl); 
        // $response = file_get_contents(APPPATH.'modules/apis/controllers/1688.json');
        // $response = file_get_contents(APPPATH.'modules/apis/controllers/taobao.json');

        echo $response;
    }
    
}