<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ip extends MX_Controller {

	public function __construct()
	{
        parent::__construct();
        
	} 

	public function index()
	{
	   arr($this->input->ip_address());
	   arr($_SERVER);
	}


	public function get_ip()
	{
		$curl = curl_init(); 
        curl_setopt_array($curl, array(
			CURLOPT_URL             => "https://ifcexpressshipping.com/apis/ip", 
			// CURLOPT_URL             => "https://ifc.getdev.top/apis/ip", 
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_ENCODING        => "",
            CURLOPT_MAXREDIRS       => 10,
            CURLOPT_TIMEOUT         => 0,
            CURLOPT_FOLLOWLOCATION  => true, 
            CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1, 
            CURLOPT_CUSTOMREQUEST   => "GET"
            )
        );
            
        $response   = curl_exec($curl);
        curl_close($curl); 
        // $response = file_get_contents(APPPATH.'modules/apis/controllers/1688.json');
        // $response = file_get_contents(APPPATH.'modules/apis/controllers/taobao.json');

        echo $response;
    }

    public function getNumberThai()
    {
        $input  = 55342343023423;
        $number = $this->setNumberThai($input);
        $data['numberThai'] = array("number" => number_format($input,2) , "th" => $number);
        return $this->output
            ->set_content_type('application/json')
            ->set_output(json_encode($data));
    }

    private function setNumberThai($num = 0){   
        $num            = str_replace(",","",$num);
        $num_decimal    = explode(".",$num);
        $num            =   $num_decimal[0];
        $returnNumWord  = '';   
        $lenNumber      = strlen($num);   
        $lenNumber2     = $lenNumber-1;   
        $kaGroup        = array("","สิบ","ร้อย","พัน","หมื่น","แสน","ล้าน","สิบ","ร้อย","พัน","หมื่น","แสน","ล้าน");   
        $kaDigit        = array("","หนึ่ง","สอง","สาม","สี่","ห้า","หก","เจ็ต","แปด","เก้า");   
        $kaDigitDecimal = array("ศูนย์","หนึ่ง","สอง","สาม","สี่","ห้า","หก","เจ็ต","แปด","เก้า");   
        $ii=0;   
        for($i=$lenNumber2;$i>=0;$i--):
            $kaNumWord[$i]=substr($num,$ii,1);   
            $ii++;   
        endfor; 
        $ii = 0;   
        for($i=$lenNumber2;$i>=0;$i--):
            if(($kaNumWord[$i]==2 && $i==1) || ($kaNumWord[$i]==2 && $i==7)):  
                $kaDigit[$kaNumWord[$i]]="ยี่";   
            else:   
                if($kaNumWord[$i]==2): 
                    $kaDigit[$kaNumWord[$i]]="สอง";        
                endif;   
                if(($kaNumWord[$i]==1 && $i<=2 && $i==0) || ($kaNumWord[$i]==1 && $lenNumber>6 && $i==6)): 
                    if($kaNumWord[$i+1]==0):  
                        $kaDigit[$kaNumWord[$i]]="หนึ่ง";      
                    else: 
                        $kaDigit[$kaNumWord[$i]]="เอ็ด";       
                    endif;
                elseif(($kaNumWord[$i]==1 && $i<=2 && $i==1) || ($kaNumWord[$i]==1 && $lenNumber>6 && $i==7)):  
                    $kaDigit[$kaNumWord[$i]]="";   
                else:
                    if($kaNumWord[$i]==1): 
                        $kaDigit[$kaNumWord[$i]]="หนึ่ง";   
                    endif;
                endif;
            endif;  
            if($kaNumWord[$i]==0):   
                if($i!=6):
                    $kaGroup[$i] = "";   
                endif;
            endif;

            $kaNumWord[$i] = substr($num,$ii,1);   
            $ii++;   
            $returnNumWord.= $kaDigit[$kaNumWord[$i]].$kaGroup[$i];   
        endfor;

        if(isset($num_decimal[1])):
            $returnNumWord.= "จุด";
            for($i=0;$i<strlen($num_decimal[1]);$i++):
                $returnNumWord.= $kaDigitDecimal[substr($num_decimal[1],$i,1)];  
            endfor;
        endif;

        return $returnNumWord;   
    }

}