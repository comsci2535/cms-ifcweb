<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Deliverys extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('deliverys_m');
        $this->setProvince = array('กรุงเทพมหานคร','นครปฐม','นนทบุรี','ปทุมธานี','สมุทรปราการ','สมุทรสาคร');
	} 

	public function get_deliverys()
	{
 
        $input['active']        = 1;
        $input['recycle']       = 0;
        $input['order']         = 'asc';
        $deliverys              = $this->deliverys_m->get_rows($input)->result();  
        $deliverys_arr = array();
        if(!empty($deliverys)):
            foreach($deliverys as $item):
                $cont = $this->deliverys_m->cont_delivery_setting($item->delivery_company_id);
                if($cont > 0 || $item->is_send == 2): 
                    array_push($deliverys_arr, array(
                        'delivery_company_id'   => $item->delivery_company_id,
                        'title'                 => $item->title,
                        'is_send'               => $item->is_send,
                        'file'                  => $item->file
                    ));
                endif;
            endforeach;
        endif; 
        $data['deliverys']      = $deliverys_arr; 
        $this->load->view('deliverys', $data);
    }

    public function get_deliverys_detail()
	{
 
        $input['active']        = 1;
        $input['recycle']       = 0; 
        $input['order']         = 'asc';
        $deliverys              = $this->deliverys_m->get_rows($input)->result();  
        $deliverys_arr = array();
        if(!empty($deliverys)):
            foreach($deliverys as $item):
                $delivery_setting = $this->deliverys_m->get_delivery_setting($item->delivery_company_id)->result();
                if(!empty($delivery_setting) || $item->is_send == 2): 
                    array_push($deliverys_arr, array(
                        'delivery_company_id'   => $item->delivery_company_id,
                        'title'                 => $item->title,
                        'is_send'               => $item->is_send,
                        'file'                  => $item->file,
                        'delivery_setting'      => $delivery_setting
                    ));
                endif;
            endforeach;
        endif;  
        $data['deliverys_detail']   = $deliverys_arr;
        $data['setProvince']        = $this->setProvince; 
        
        $this->load->view('deliverys_detail', $data);
    }

    public function ajax_deliverys_detail()
	{
        $input                  = $this->input->post();
        $input['active']        = 1;
        $input['recycle']       = 0; 
        $input['order']         = 'asc';
        $deliverys              = $this->deliverys_m->get_rows($input)->result();  
        $deliverys_arr = array();
        if(!empty($deliverys)):
            foreach($deliverys as $item):
                $delivery_setting = $this->deliverys_m->get_delivery_setting($item->delivery_company_id)->result();
                if(!empty($delivery_setting) || $item->is_send == 2): 
                    array_push($deliverys_arr, array(
                        'delivery_company_id'   => $item->delivery_company_id,
                        'title'                 => $item->title,
                        'is_send'               => $item->is_send,
                        'file'                  => $item->file,
                        'delivery_setting'      => $delivery_setting
                    ));
                endif;
            endforeach;
        endif;  

        $member = Modules::run('member/_get_member');
        if(!empty($input['province'])):
            $province = $input['province']; 
        else:
            $province = $member->province;
        endif;

        $data_arr['deliverys_detail']   = $deliverys_arr;
        $data_arr['setProvince']        = $this->setProvince; 
        $data_arr['member']['province'] = $province;
        $data['deliverys_detail']       = $this->load->view('deliverys_detail', $data_arr, TRUE); 

        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($data));
    }
    
} 