<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Deliverys_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param) 
    {
        
        if (!empty($param['delivery_company_id']) ) 
            $this->db->where('a.delivery_company_id', $param['delivery_company_id']);
        
        if (!empty($param['is_send']) ) 
            $this->db->where('a.is_send', $param['is_send']);

        if (isset($param['active']) ) 
            $this->db->where('a.active', $param['active']);

        if (isset($param['recycle']) ) 
            $this->db->where('a.recycle', $param['recycle']);
        
        if(!empty($param['order'])):  
            $this->db->order_by('a.order', $param['order']);
        endif;

        $query = $this->db
                        ->select('a.*')
                        ->from('delivery_companys a')
                        ->get();
        return $query;
    } 

    public function cont_delivery_setting($id)
    {
        $this->db->where('active', 1);
        $this->db->where('delivery_company_id', $id);
        $this->db->from('delivery_settings');
        return $this->db->count_all_results();
    }

    public function get_delivery_setting($id)
    {
        $this->db->where('active', 1);
        $this->db->where('delivery_company_id', $id);
        $this->db->from('delivery_settings');
        return $this->db->get();
    }
}
