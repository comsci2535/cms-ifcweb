<?php 

if(!empty($deliverys)):
    foreach($deliverys as $key => $delivery):
        $check = '';
        if($key == 0):
            // $check = 'checked';
        endif;
?>
 <div class="col-md-2 col-4 mb-2">
    <div class="form-check">
        <label class="form-check-label">
            <input type="radio" class="form-check-input click-delivery" name="delivery_id" 
            value="<?php echo !empty($delivery['delivery_company_id']) ? $delivery['delivery_company_id'] : '';?>" 
            <?php echo $check;?>
            style="top: 12px;">
            <span><?php echo !empty($delivery['title']) ? $delivery['title'] : '';?></span>
            <?php
            if($delivery['is_send'] != 2):
            ?>
            <img src="<?php echo !empty($delivery['file']) ? base_url($delivery['file']) : '';?>" 
                  onerror="this.src='<?=base_url('template/frontend/images/no-image.png');?>'"
                  style="width: 50px;" 
                  alt="<?php echo !empty($delivery['title']) ? $delivery['title'] : '';?>"/> 
            <?php
            endif;
            ?>
        </label>
    </div>
</div>
<?php
    endforeach;
endif;
?>