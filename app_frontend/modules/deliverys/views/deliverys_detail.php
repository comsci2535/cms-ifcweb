<?php   
if(!empty($deliverys_detail)):
    $i = 0;
    foreach($deliverys_detail as $key_detail => $delivery_detail): 
        $display = 'none';
        if($delivery_detail['is_send'] != 2 && !empty($delivery_detail['delivery_setting'])):  
            if($i == 0):
                // $display = 'show';
            endif; 
    ?>
            <div id="display-deliverys-detail-<?=$delivery_detail['delivery_company_id']?>" 
                class="col-md-12 mt-3 display-deliverys-detail" style="display: <?=$display?>;">
                <div class="table-redius">
                    <table id="table-calculate-shipping" class="table table-bordered table-hover" style="background: #fff;">
                        <thead>
                            <tr>
                                <th>น้ำหนัก</th>
                                <th>อัตรา (บาท)</th>
                            </tr> 
                        </thead>
                        <tbody>
                            <?php 
                            if(!empty($delivery_detail['delivery_setting'])):
                                foreach($delivery_detail['delivery_setting'] as $detail):
                            ?>
                            <tr>
                                <td>
                                    เกิน
                                    <?php echo isset($detail->weight_min) ? $detail->weight_min : '';?>
                                    -
                                    <?php echo isset($detail->weight_max) ? $detail->weight_max : '';?>
                                    กรัม
                                </td>
                                <td>
                                    <?php
                                    if(in_array($member['province'],$setProvince)):
                                    ?>
                                    <?php echo isset($detail->price_in) ? $detail->price_in : '';?>
                                    <input type="hidden" class="text-input-price" 
                                        value="<?php echo isset($detail->price_in) ? $detail->price_in : '';?>"
                                        data-weight-min="<?php echo isset($detail->weight_min) ? $detail->weight_min : 0;?>"
                                        data-weight-max="<?php echo isset($detail->weight_max) ? $detail->weight_max : 0;?>"
                                    >
                                    <?php
                                    else:
                                    ?>
                                    <?php echo isset($detail->price_out) ? $detail->price_out : '';?>
                                    <input type="hidden" class="text-input-price" 
                                        value="<?php echo isset($detail->price_out) ? $detail->price_out : '';?>"
                                        data-weight-min="<?php echo isset($detail->weight_min) ? $detail->weight_min : 0;?>"
                                        data-weight-max="<?php echo isset($detail->weight_max) ? $detail->weight_max : 0;?>"
                                        >
                                    <?php
                                    endif;
                                    ?>
                                </td> 
                            </tr>
                            <?php 
                                endforeach;
                            endif;
                            ?>
                        </tbody>
                    </table> 
                </div>
            </div>
<?php
        endif; 
        $i++;
    endforeach;
endif;
?>