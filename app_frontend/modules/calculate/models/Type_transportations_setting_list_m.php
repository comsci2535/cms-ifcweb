<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Type_transportations_setting_list_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*')
                        ->from('type_transportations_setting_list a')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->from('type_transportations_setting_list a')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {   
        // START form filter 
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.kg_min', $param['search']['value'])
                    ->or_like('a.kg_max', $param['search']['value'])
                    ->or_like('a.queue_min', $param['search']['value'])
                    ->or_like('a.queue_max', $param['search']['value'])
                    ->or_like('a.price_send', $param['search']['value'])
                    ->group_end();
        }
        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.created_at,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
        }
        if ( isset($param['updateDateRange']) && $param['updateDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.updated_at,'%Y-%m-%d') BETWEEN '{$param['updateStartDate']}' AND '{$param['updateEndDate']}'");
        }     
        if ( isset($param['active']) && $param['active'] != "" ) {
            $this->db->where('a.active', $param['active']);
        }         
        // END form filter
        
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.kg_min', $param['search']['value'])
                    ->or_like('a.kg_max', $param['search']['value'])
                    ->or_like('a.queue_min', $param['search']['value'])
                    ->or_like('a.queue_max', $param['search']['value'])
                    ->or_like('a.price_send', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.kg_min";
            if ($param['order'][0]['column'] == 2) $columnOrder = "a.queue_min";
            if ($param['order'][0]['column'] == 3) $columnOrder = "a.queue_min";            
            if ( $this->router->method =="data_index" ) {
                if ($param['order'][0]['column'] == 4) $columnOrder = "a.created_at";
                if ($param['order'][0]['column'] == 5) $columnOrder = "a.updated_at";
            } else if ( $this->router->method =="data_trash" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "a.recycle_at";
            }
            $this->db
                    ->order_by($columnOrder, $param['order'][0]['dir']);
        } 
        
        if ( isset($param['type_transportations_setting_list_id']) ) 
            $this->db->where('a.type_transportations_setting_list_id', $param['type_transportations_setting_list_id']);
            
        if ( isset($param['type_transportations_setting_id']) ) 
            $this->db->where('a.type_transportations_setting_id', $param['type_transportations_setting_id']);

        if ( isset($param['type_transportation_cust_id']) ) 
            $this->db->where('a.type_transportation_cust_id', $param['type_transportation_cust_id']);
        
        if ( isset($param['type_transportation_id']) ) 
            $this->db->where('a.type_transportation_id', $param['type_transportation_id']);
            
        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);

    }

    public function get_param($param)
    {

        if ( isset($param['type_transportations_setting_list_id']) ) 
            $this->db->where('a.type_transportations_setting_list_id', $param['type_transportations_setting_list_id']);
            
        if ( isset($param['type_transportations_setting_id']) ) 
            $this->db->where('a.type_transportations_setting_id', $param['type_transportations_setting_id']);
        
        if ( isset($param['type_transportation_cust_id']) ) 
            $this->db->where('a.type_transportation_cust_id', $param['type_transportation_cust_id']);
            
        if ( isset($param['type_transportation_id']) ) 
            $this->db->where('a.type_transportation_id', $param['type_transportation_id']);
        
        $this->db->where('a.recycle', $param['recycle']); 

        $this->db->where('a.active', $param['active']); 

        $query = $this->db
            ->select('a.*')
            ->from('type_transportations_setting_list a')
            ->order_by('type', 'asc')
            ->get();
        return $query;
    }

}
