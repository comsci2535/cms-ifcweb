<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Type_transportations_cust_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*')
                        ->from('type_transportations_cust a')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->from('type_transportations_cust a')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {   
        // START form filter 
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['keyword'])
                    ->or_like('a.excerpt', $param['keyword'])
                    ->or_like('a.detail', $param['keyword'])
                    ->group_end();
        }

        if ($param['order'][0]['column'] == 0) $columnOrder = "a.order"; 
            $this->db->order_by($columnOrder, $param['order'][0]['dir']);
           
        if ( isset($param['active']) && $param['active'] != "" ) {
            $this->db->where('a.active', $param['active']);
        }         
        // END form filter 
        
        if ( isset($param['type_transportation_cust_id']) ) 
            $this->db->where('a.type_transportation_cust_id', $param['type_transportation_cust_id']);

        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);
        
        if (!empty($param['slug']) )
            $this->db->where('a.slug', $param['slug']);
            
    }

    public function get_by_key($param)
    {
        if ( isset($param['active']) && $param['active'] != "" ) {
            $this->db->where('a.active', $param['active']);
        }         
        // END form filter 
        
        if ( isset($param['type_transportation_cust_id']) ) 
            $this->db->where('a.type_transportation_cust_id', $param['type_transportation_cust_id']);

        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);
        return $this->db
        ->select('a.title, a.title_en, a.type_transportation_cust_id')
        ->from('type_transportations_cust a')
        ->get();

    }

}
