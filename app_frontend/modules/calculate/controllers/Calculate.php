<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calculate extends MX_Controller {

	public function __construct()
	{
		parent::__construct(); 
		$this->load->model('type_transportations_m');
		$this->load->model('type_transportations_setting_m');
		$this->load->model('type_transportations_setting_list_m');
		$this->load->model('type_transportations_cust_m');
    } 
    
	public function get_calculate()
	{ 
        $input['active']    			= 1;
        $input['recycle']   			= 0;
		$type_transportations_setting           = $this->type_transportations_setting_m->get_rows($input)->result();
		if(!empty($type_transportations_setting)):
			foreach($type_transportations_setting as $item):
				if(CURRENT_LANG=='en'):
					$item->title     = !empty($item->title_en) ? $item->title_en : $item->title;
                    $item->excerpt   = !empty($item->excerpt_en) ? $item->excerpt_en : $item->excerpt;
					$item->detail    = !empty($item->detail_en) ? $item->detail_en : $item->detail;  
				endif;
				
			endforeach;
		endif;
		
		$data['type_transportations_setting']   = $type_transportations_setting;

        $this->load->view('calculate', $data);
	}
	
	public function get_data_calculate()
    {
		$input               = $this->input->post();  
		$input['order'][0]['column'] 	= 1;
        $input['order'][0]['dir'] 		= 'asc';
		$input['active']    			= 1;
		$input['recycle']   			= 0; 
		$input['type_transportations_setting_id']  = $input['code']; 
		$text_kg 		= !empty($input['text_kg']) ? $input['text_kg'] : 0;
		$text_queue 	= !empty($input['text_queue']) ? $input['text_queue'] : 0; 
		$info  			= $this->type_transportations_setting_list_m->get_rows($input)->result();
		$price_kg		= 0;
		$price_queue	= 0;
		if(!empty($info)):
			foreach($info as $item): 
				$kg_min 	= !empty($item->kg_min) ? $item->kg_min : 0;
				$kg_max 	= !empty($item->kg_max) ? $item->kg_max : 0;
				$queue_min 	= !empty($item->queue_min) ? $item->queue_min : 0;
				$queue_max 	= !empty($item->queue_max) ? $item->queue_max : 0; 
				if(!empty($text_kg)):
					if($kg_min <= $text_kg && $text_kg <= $kg_max):
						$price_kg = !empty($item->price_send) ? $item->price_send : 0;  
					endif;
				endif;
				if(!empty($text_queue)):
					if($queue_min <= $text_queue && $text_queue <= $queue_max):
						$price_queue = !empty($item->price_send) ? $item->price_send : 0;  
					endif; 
				endif;
			endforeach;
		endif;

		$data['price_kg']		= number_format($price_kg,1);
		$data['price_queue'] 	= number_format($price_queue,1); 

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
	}

	public function get_data()
	{
		header('Content-Type: application/json');

		$input = $this->input->post(null, true);

		$input['recycle'] = 0;
		$input['active'] = 1;

		$wide   	= $input['wide'];
		$long   	= $input['long'];
		$high   	= $input['high'];
		$weight   	= $input['weight'];

		$input_else_arr = array();
		$total     = 0;

		$info = $this->type_transportations_setting_list_m->get_param($input)->result();

		$total = ($wide * $long * $high) / 1000000;
		$total = number_format($total, 3);
		$info_arr = array();
		if(!empty($info)):
			foreach($info as $item):
				//set data on type
				$info_arr[$item->type][] = $item;
			endforeach;

			if(!empty($info_arr)):
				foreach($info_arr as $key => $loops):
					if(!empty($loops)):
						//check type
						if($key == 0):
							foreach($loops as $loop):
								$queue_min = $loop->queue_min;
								$queue_max = $loop->queue_max;
								if($queue_min <= $total && $total <= $queue_max):
									$total_sum = $total * $loop->price_send;
									$input_else_arr[$key] = array(
										'queue_min' 	=> $loop->queue_min,
										'queue_max' 	=> $loop->queue_max,
										'price' 		=> $loop->price_send,
										'price_send' 	=> number_format($total_sum,1),
										'price_check' 	=> $total_sum,
										'type'			=> $key
									);
								endif;
							endforeach;
						endif;
						//check type
						if($key == 1):
							if(!empty($weight)):
								foreach($loops as $loop_1):
									$queue_min = $loop_1->kg_min;
									$queue_max = $loop_1->kg_max;
									if($queue_min <= $weight && $weight <= $queue_max):
										$total_sum = $weight * $loop_1->price_send;
										$input_else_arr[$key] = array(
											'queue_min' 	=> $loop_1->kg_min,
											'queue_max' 	=> $loop_1->kg_max,
											'price' 		=> $loop_1->price_send,
											'price_send' 	=> number_format($total_sum,1),
											'price_check' 	=> $total_sum,
											'type'			=> $key
										);
									endif;
								endforeach;
							endif;
						endif;
					endif;
				endforeach;
			endif;

			if(!empty($input_else_arr)):
				$price_0 = !empty($input_else_arr[0]['price_check']) ? $input_else_arr[0]['price_check'] : 0;
				$price_1 = !empty($input_else_arr[1]['price_check']) ? $input_else_arr[1]['price_check'] : 0;
				//check price max
				
				if($price_1 > $price_0):
					$input_else_arr = $input_else_arr[1];
				else:
					$input_else_arr = $input_else_arr[0];
				endif;
			endif;

		endif;

		if(empty($input_else_arr) ):
			$input_else_arr = array(
				'queue_min' 	=> 0,
				'queue_max' 	=> 0,
				'price_send' 	=> number_format(0,1)
			);
		endif;

		$data['calculate'] = $total;
		$data['info'] = $input_else_arr;
		// $data['data'] = $info;
		// $data['info_arr'] = $info_arr;

		$this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
	}

	public function get_transportations_cust()
	{
		$input = $this->input->post(null, true);
		if($input['type'] == "cust"):
			$input['type_transportations_setting_id'] = $input['code'];
		endif;

		if($input['type'] == "trans"):
			$input['type_transportation_cust_id'] = $input['code'];
		endif;
		
		$input['active']    			= 1;
		$input['recycle']   			= 0;
		$data_arr = array();
		$info = $this->type_transportations_setting_list_m->get_rows($input)->result();
		if(!empty($info)):
			foreach($info as $item):
				if($input['type'] == "cust"):
					$input_['type_transportation_cust_id'] = $item->type_transportation_cust_id;
					$input_['active']    			= 1;
					$input_['recycle']   			= 0;
					$type_transportations_cust          = $this->type_transportations_cust_m->get_by_key($input_)->row();
					if(CURRENT_LANG=='en'):
						$title     = !empty($type_transportations_cust->title_en) ? $type_transportations_cust->title_en : $type_transportations_cust->title;
					else:
						$title     = !empty($type_transportations_cust->title) ? $type_transportations_cust->title : null; 
					endif;
					
					$data_arr[$item->type_transportation_cust_id] = array(
						'title' => $title,
						'type_transportation_cust_id' => $item->type_transportation_cust_id
					);
				endif;

				if($input['type'] == "trans"):
					$input_['type_transportation_id'] = $item->type_transportation_id;
					$input_['active']    			= 1;
					$input_['recycle']   			= 0;
					$type_transportations          = $this->type_transportations_m->get_rows($input_)->row();
					if(CURRENT_LANG=='en'):
						$title     = !empty($type_transportations->title_en) ? $type_transportations->title_en : $type_transportations->title;
					else:
						$title     = !empty($type_transportations->title) ? $type_transportations->title : $type_transportations->title; 
					endif;
					
					$data_arr[$item->type_transportation_id] = array(
						'title' => $title,
						'type_transportation_id' => $item->type_transportation_id
					);
				endif;
			endforeach;
			$data['info'] 	= $data_arr;
			$data['status'] = 200;
		else:
			$data['status'] = 500;
		endif;
		
		$this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
	}
}
        