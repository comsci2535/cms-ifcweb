<section>
    <div class="container">
       
        <div class="col-md-12 text-center">
            <img src="<?=base_url('images/home_4.jpg');?>" alt="">
        </div>
        <div class="col-md-12 text-center">
            <img src="<?=base_url('images/home_5.jpg');?>" alt="">
        </div>
        
        <div class="col-md-12 text-center">
            <img src="<?=base_url('images/home_1.jpg');?>" alt="">
        </div>
         <div class="col-md-12 text-center">
            <img src="<?=base_url('images/home_3.jpg');?>" alt="">
        </div>
        <div class="col-md-12 text-center">
            <img src="<?=base_url('images/home_2.jpg');?>" alt="">
        </div>
    </div>
    <div class="container" style="margin-top: 23px">
      <div class="blog-title">
        <h2 class="text"><?=$this->lang->line('calculate_shipping_cost');?></h2>
      </div>
      <!-- Nav tabs -->
      <!-- <div class="text-center">
        <div class="btn-group ">
          <?php 
          $transportations = array();
          if(!empty($type_transportations)):
            foreach($type_transportations as $key => $item):
            $transportations[] = $item->type_transportation_id;
          ?>
            <button type="button" class="btn btn-calculate-select <?php echo $key==0 ? "active" : NULL;?>" 
            data-transportations="<?php echo !empty($item->type_transportation_id) ? $item->type_transportation_id : null;?>"
            >
            <?php echo !empty($item->title) ? $item->title : null;?>
            </button>
          <?php
            endforeach;
          endif;
          ?> 
        </div>
      </div> -->
      <!-- Tab panes -->
      <?php for ($i=0; $i < count($transportations); $i++) {  ?>
      <!-- <div transportations="<?=$transportations[$i];?>" style="display: <?=($i==0) ? 'block' : 'none';?>;">
      <div class="row mt-3">
        <div class="col-md-12">
          <div class="blog-general py-3">
            <div id="general1"> 
                <div class="row my-4 text-center">
                  <?php 
                  if(!empty($type_transportations[$i]->transportations_list)):
                    foreach($type_transportations[$i]->transportations_list as $keys => $list):
                  ?>
                  <div class="col-md-2 col-6 <?php echo $keys==0 ? "offset-md-2" : NULL;?>">
                    <div class="form-check">
                      <label class="form-check-label">
                        <input type="radio" class="form-check-input type_transportations_setting_id" 
                        name="type_transportations_setting_id" 
                        value="<?php echo !empty($list->type_transportations_setting_id) ? $list->type_transportations_setting_id : 0;?>">
                        <?php echo !empty($list->title) ? $list->title : null;?>
                      </label>
                    </div>
                  </div>
                  <?php
                    endforeach;
                  endif;
                  ?>  
                </div>
                <div class="row">
                  <div class="offset-md-1 col-md-5">
                    <div class="form-group">
                      <label for=""><?=$this->lang->line('total_product_weight_kg');?></label>
                      <input type="text" class="form-control text_kg" placeholder="<?=$this->lang->line('total_product_weight_kg');?>..." name="text_kg" autocomplete="off">
                      <h5 class="my-2">= <?=$this->lang->line('price');?> : <span class="c_pink text-kg-price">0.0</span> <?=$this->lang->line('bath');?></h5>
                    </div>
                  </div>
                  <div class="col-md-5">
                    <div class="form-group">
                      <label for=""><?=$this->lang->line('total_product_weight_Q');?></label>
                      <input type="text" class="form-control text_queue" placeholder="<?=$this->lang->line('total_product_weight_Q');?>..." name="text_queue" autocomplete="off">
                      <h5 class="my-2">= <?=$this->lang->line('price');?> : <span class="c_pink text-queue-price">0.0</span> <?=$this->lang->line('bath');?></h5>
                    </div>
                  </div>
                  <div class="col-12 text-center">
                    <button type="button" class="btn btn-dufault mt-3 btn-calculate-process"><i class="fas fa-calculator"></i>
                    <?=$this->lang->line('calculate');?></button>
                    <p class="mt-4 c-red"><?=$this->lang->line('calculation_china_to_thailand');?></p>
                  </div>
                </div> 
            </div>
          </div>
        </div>
      </div>
      </div> -->
      <?php  } ?>

      <style>
      .nice-select {
        width: 100%;
        padding: 0 10px;
        font-size: 17px;
        float: none;
      }
      .form-control.v1 {
        height: 42px;
      }
      </style>

      <div class="row">
        <div class="col-md-2 text-center">
          <img src="<?=base_url('images/send-1.jpg');?>" style="width: auto;"/>
        </div>
        <div class="col-md-3">
          <div class="form-group">
              <input type="text" class="form-control v1" placeholder="กว้าง" id="wide" name="wide">
          </div>
          <div class="form-group">
              <input type="text" class="form-control v1" placeholder="ยาว" id="long" name="long">
          </div>
          <div class="form-group">
              <input type="text" class="form-control v1" placeholder="สูง" id="high" name="high">
          </div>
          <div class="form-group">
              <input type="text" class="form-control v1" placeholder="น้ำหนัก" id="weight" name="weight">
          </div>
        </div>
        <div class="col-md-2 text-center">
          <img src="<?=base_url('images/send-2.jpg');?>" style="width: auto;"/>
        </div>
        <div class="col-md-3">
          <div class="form-group">
            <select class="form-control" id="type_transportations_setting_id" name="type_transportations_setting_id">
              <option value="">เลือกประเภทสินค้า</option>
              <?php
              if(!empty($type_transportations_setting)):
                foreach($type_transportations_setting as $custs):
              ?>
                <option value="<?php echo !empty($custs->type_transportations_setting_id) ? $custs->type_transportations_setting_id : null;?>"><?php echo !empty($custs->title) ? $custs->title : null;?></option>
              <?php
                endforeach;
              endif;
              ?>
            </select>
          </div>
          <div class="form-group">
            <select class="form-control" id="type_transportation_cust_id" name="type_transportation_cust_id">
              <option value="">เลือกประเภทการซื้อ</option>
            </select>
          </div>
          <div class="form-group">
            <select class="form-control" id="type_transportation_id" name="type_transportation_id">
              <option value="">เลือกประเภทขนส่ง</option>
            </select>
          </div>
          <div class="form-group">
            <button type="button" id="btn-calculate-process" class="btn btn-dufault btn-calculate-process wfull"><?=$this->lang->line('calculate');?></button>
          </div>
        </div>
        <div class="col-md-2">
          <div class="card text-center">
            <div class="card-header" style="background: linear-gradient(to left, #b22d6c, #2c54a3);">
              <b style="color: #fff;">ราคาที่ต้องชำระ</b>
            </div>
            <div class="card-body">
              <h2 style="margin: 41px 0;"><b id="calculate-tatal">0.0฿</b></h2>
            </div>
          </div>
        </div>
      </div> 

    </div>
    <div class="container" style="margin-top: 23px">
        <div class="col-md-12 text-center">
            <img src="<?=base_url('images/home-01.jpg');?>" alt="">
        </div>
        <div class="col-md-12 text-center">
            <img src="<?=base_url('images/home-02.jpg');?>" alt="">
        </div>
    </div>
  </section>