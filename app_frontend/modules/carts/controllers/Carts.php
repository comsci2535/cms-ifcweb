<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 
 
class Carts extends MX_Controller 
{  
    public function __construct()
    { 
        parent::__construct(); 
        $this->load->library('cart'); 
        $this->load->model("product/product_m");
    } 

    public function data_cart()
    { 

        if(!empty($this->session->userdata['ses_mem']->id)):
            $input               = $this->input->post();
            $input['recycle']    = 0;
            $input['active']     = 1;
            $input['product_id'] = $input['code'];
            $qty                 = !empty($input['qty']) ? $input['qty'] : 1;
            $size                = !empty($input['size']) ? $input['size'] : null;
            $color               = !empty($input['color']) ? $input['color'] : null;
            $info                = $this->product_m->get_rows($input)->row();  
            $options             = array();
            if(!empty($input['size'])){
                array_push($options, array('size'=> $size));
            } 
            if(!empty($input['color'])){
                array_push($options, array('color'=> $color));
            }

            if(!empty($info)):
                $product_id         = !empty($info->product_id) ? $info->product_id : 0;
                $title              = !empty($info->title) ? $info->title : 'ไม่ระบุ';
                $price              = !empty($info->price) ? $info->price : 0;
                $qty                = $qty;
                $cart_arr = array(
                    'id'      => $product_id,
                    'qty'     => $qty,
                    'price'   => $price,
                    'name'    => $title,
                    'options' => $options
                ); 
                $this->cart->insert($cart_arr); 
                $data['data']               = 200; 
            else:
                $data['data']               = 400; 
            endif;  

            $data['obj_cart']               = $this->cart->contents(); 
            $data['count']                  = count($data['obj_cart']);
        else:
            $data['data']                   = 500; 
        endif;
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function data_api_cart()
    { 

        if(!empty($this->session->userdata['ses_mem']->id)):

            $input                  = $this->input->post();
            $code                   = !empty($input['code']) ? $input['code'] : 0;
            $price                  = !empty($input['price']) ? $input['price'] : 0;
            $attribute              = !empty($input['attribute']) ? $input['attribute'] : '';
            $name                   = !empty($input['name']) ? $input['name'] : '';
            $qty                    = !empty($input['qty']) ? $input['qty'] : 1;
            $image                  = !empty($input['image']) ? $input['image'] : '';
            $type                   = !empty($input['type']) ? $input['type'] : 0;
            $from_web               = !empty($input['from_web'])? $input['from_web'] : '';
            $from_web_url           = !empty($input['from_web_url']) ? $input['from_web_url'] : '';
            $options             = array();
            if(!empty($attribute)):
                foreach($attribute as $attr):
                    $attribute_title    = !empty($attr['attribute_title']) ? $attr['attribute_title']:'';
                    $attribute          = !empty($attr['attribute']) ? $attr['attribute']:'';
                    array_push($options, array('attribute_title'=> $attribute_title, 'attribute'=> $attribute));
                endforeach;
            endif;

            $cart_arr = array(
                'id'            => $code,
                'qty'           => $qty,
                'price'         => $price,
                'name'          => $name,
                'image'         => $image,
                'from_web'      => $from_web,
                'type'          => $type,
                'options'       => $options,
                'from_web_url'  => $from_web_url
            ); 
            if($this->cart->insert($cart_arr)):
                $data['data']               = 200;
                $data['obj_cart']           = $this->cart->contents(); 
                $data['count']              = count($data['obj_cart']);
            else:
                $data['obj_cart']           = null;
                $data['data']               = 400; 
            endif;
        else:
            $data['data']                   = 500;
        endif;

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function get_data_cart()
    {
        $data['obj_cart']               = $this->cart->contents(); 
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function get_data_cart_list()
    { 
        return $this->cart->contents();
    }

    public function delete_row()
    {

        $input             = $this->input->post();
        $input['rowid']     = $input['rowid'];

        $deleteItem = array(
            'rowid' => $input['rowid'],
            'qty'   => 0
        );
        
        if($this->cart->update($deleteItem)):
            $data['data']               = 200; 
        else:
            $data['data']               = 400; 
        endif;
        
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));

    }

    public function update_row()
    {

        $input             = $this->input->post();
        $input['rowid']    = $input['rowid'];
        $input['qty']      = $input['qty'];

        $updateItem = array(
            'rowid' => $input['rowid'],
            'qty'   => $input['qty']
        );
        
        if($this->cart->update($updateItem)):
            $data['data']               = 200; 
        else:
            $data['data']               = 400; 
        endif;
        
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));

    }

    public function save_cart_history()
    {
        $user_id    = $this->session->userdata['ses_mem']->id;
        if(!empty($user_id)):
            $cart       = $this->get_data_cart_list();
            $this->db->where('id', $user_id);
            if($this->db->count_all_results('member_cart_history') > 0):
                $cart_history = array(
                    'cart_json'     => json_encode($cart), 
                    'created_at'    => date('Y-m-d H:i:s')
                );
                $this->db->update('member_cart_history', $cart_history, array('id', $user_id));
            else:
                $cart_history = array(
                    'id'            => $user_id,
                    'cart_json'     => json_encode($cart), 
                    'created_at'    => date('Y-m-d H:i:s')
                );
        
                $this->db->insert('member_cart_history', $cart_history);
            endif;
        endif;
    }

    public function add_data_cart()
    {
        $user_id    = $this->session->userdata['ses_mem']->id;
        if(!empty($user_id)):
            $this->db->where('id', $user_id);
            $info = $this->db->get('member_cart_history')->row();
            $json = !empty($info->cart_json) ? json_decode($info->cart_json) : null;
            $cart_arr = array();
            if(!empty($json)):
                $i = 0;
                foreach($json as $item):
                    $cart_arr[$i]['id']               = $item->id;
                    $cart_arr[$i]['qty']              = $item->qty;
                    $cart_arr[$i]['price']            = $item->price;
                    $cart_arr[$i]['name']             = $item->name;
                    if(!empty($item->image)):
                        $cart_arr[$i]['image']        = $item->image;
                    endif;
                    if(!empty($item->image)):
                        $cart_arr[$i]['from_web']     = $item->from_web;
                    endif;
                    if(!empty($item->options)):
                        $cart_arr[$i]['options']      = $item->options;
                    endif;
                    if(!empty($item->image)):
                        $cart_arr[$i]['from_web_url'] = $item->from_web_url;
                    endif;
                    $i++;
                endforeach;
            endif;
            // arr($cart_arr);
            $this->cart->insert($cart_arr); 
            
            $cart       = $this->get_data_cart_list();
            arr($cart);
            exit();
        endif;
    }


    public function delete_cart_row()
    {

        $input             = $this->input->get();
        $input['rowid']    = $input['rowid'];

        $deleteItem = array(
            'rowid' => $input['rowid'],
            'qty'   => 0
        );
        
        $this->cart->update($deleteItem);    
        redirect(site_url("product/cart"));
    }

    public function delete_cart_by_row($parm)
    {

        if(!empty($parm['car_rowid'])):
            foreach($parm['car_rowid'] as $item):
                $deleteItem = array(
                    'rowid' => $item,
                    'qty'   => 0
                );
                $this->cart->update($deleteItem);    
            endforeach;
        endif;
    }

    public function delete_all()
    { 
        if(empty($this->cart->destroy())):
            $data['data']               = 200; 
        else:
            $data['data']               = 400; 
        endif;
        
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));

    }

    public function delete_cart_all()
    { 
        $this->cart->destroy();
    }
    
}
