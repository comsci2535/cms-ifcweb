<h5><?=$this->lang->line('payment');?></h5>
<?php  
if(!empty($banks)):
  foreach($banks as $bank):
?>
<div class="blog-bank">
    <div>
        <div class="form-check">
            <label class="form-check-label">
                <input type="radio" class="form-check-input bank_id" name="bank_id"
                    value="<?php echo !empty($bank->bank_id) ? $bank->bank_id : '';?>">
                <div class="box" style="vertical-align: top;">
                  <img src="<?php echo !empty($bank->file_logo) ? base_url($bank->file_logo) : '';?>" 
                  onerror="this.src='<?=base_url('template/frontend/images/no-image.png');?>'"
                  style="width: 50px;" /> 
                </div>
                <div class="box">
                    <p>บันชี <?php echo !empty($bank->title) ? $bank->title : '';?> : <?php echo !empty($bank->account_number) ? $bank->account_number : '';?></p>
                    <div style="font-size: 12px;">
                        <p>ชื่อบัญชี : <?php echo !empty($bank->account_name) ? $bank->account_name : '';?> สาขา : <?php echo !empty($bank->account_branch) ? $bank->account_branch : '';?></p>
                    </div>
                    <?php
                    if(!empty($bank->file)):
                    ?>
                    <img src="<?php echo !empty($bank->file) ? base_url($bank->file) : '';?>" 
                        onerror="this.src='<?=base_url('template/frontend/images/no-image.png');?>'"
                        style="width: 150px;" /> 
                    <?php
                    endif;
                    ?>
                </div>
                
            </label>
        </div>
    </div>
</div>
<?php 
  endforeach;
endif; 
?>