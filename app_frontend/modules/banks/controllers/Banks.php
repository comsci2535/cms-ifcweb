<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banks extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('banks_m');
	} 

	public function get_banks()
	{
 
        $input['active']    = 1;
        $input['recycle']   = 0;
        $banks            = $this->banks_m->get_rows($input)->result();
        if(!empty($banks)):
            foreach($banks as $item):
                if(CURRENT_LANG=='en'):
                    $item->title            = !empty($item->title_en) ? $item->title_en : $item->title;
                    $item->account_name     = !empty($item->account_name_en) ? $item->account_name_en : $item->account_name;
                    $item->account_branch   = !empty($item->account_branch_en) ? $item->account_branch_en : $item->account_branch; 
                endif;
            endforeach;
        endif; 
        $data['banks']    = $banks; 
        $this->load->view('banks', $data);
    }
    
}