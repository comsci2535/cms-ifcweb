<?php 
if(!empty($attribute_sends)):
  foreach($attribute_sends as $key => $attribute_send):
    $checked = '';
    if($key == 0):
      $checked = 'checked';
    endif; 
?>
    <?php
    if($attribute_send->type == 0):
    ?>
    <div class="col-md-12 col-12">
      <div class="form-check">
          <label class="form-check-label">
              <input type="radio" class="form-check-input" name="attribute_send_id" <?=$checked?> value="<?php echo !empty($attribute_send->attribute_send_id) ? $attribute_send->attribute_send_id : '';?>">
              <span><?php echo !empty($attribute_send->title) ? $attribute_send->title : '';?></span>
          </label>
      </div>
    </div> 
    <?php
    endif;
    ?> 
    <?php
    if($attribute_send->type == 1):
    ?>
    <div class="col-md-12 col-12">
      <div class="form-check">
          <label class="form-check-label">
              <input type="checkbox" class="form-check-input" name="attribute_send_type_id[]" <?=$checked?> value="<?php echo !empty($attribute_send->attribute_send_id) ? $attribute_send->attribute_send_id : '';?>">
              <span><?php echo !empty($attribute_send->title) ? $attribute_send->title : '';?></span>
          </label>
      </div>
    </div> 
    <?php
    endif;
    ?>
<?php
  
  endforeach;
endif;
?>