<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Attribute_sends extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('attribute_sends_m');
	} 

	public function get_attribute_sends($type = "")
	{ 
        $input['type']              = $type;
        $input['active']            = 1;
        $input['recycle']           = 0; 
        $attribute_sends            = $this->attribute_sends_m->get_rows($input)->result();    
        $data['attribute_sends']    = $attribute_sends; 
        $this->load->view('attribute_sends', $data);
    }
    
}