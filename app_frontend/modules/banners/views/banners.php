<div class="slide_banner">
  <div style="position: relative;">
    <ul id="slider_banner">
      <?php
      // arr($banners);
        if(!empty($banners)):
          foreach($banners as $banner):
      ?>
      <li>
        <div style="position: relative; display: flex;">
          <div class="col">
            <!-- <img src="<?=base_url('images/slide1.jpg')?>" class="wfull" alt="IFC Express"> -->
            <img src="<?php echo !empty($banner->file_th) ? base_url($banner->file_th) : '';?>" 
            class="wfull" alt="<?php echo !empty($banner->title) ? $banner->title : 'IFC Express';?> "
            onerror="this.src='<?=base_url('images/slide1.jpg');?>'">
            <div class="text">
              <!-- <?php echo !empty($banner->excerpt) ? html_entity_decode($banner->excerpt) : '';?> -->
              <p class="mt-2" style="font-size: 13px;"><?=$this->lang->line('Today_rate');?> 
                <?php echo !empty($money_setting['money_china']) ? '(¥'.$money_setting['money_china'].')' : '(¥0)';?></p>
              <button type="button" class="btn btn-dufault">
                <?php echo !empty($money_setting['money_th']) ? '฿'.$money_setting['money_th'] : '฿0';?>
              </button>
            </div>
          </div>
          <div class="col">
            <img src="<?php echo !empty($banner->file) ? base_url($banner->file) : '';?>" 
            class="wfull" alt="<?php echo !empty($banner->title) ? $banner->title : 'IFC Express';?> "
            onerror="this.src='<?=base_url('images/slide2.jpg');?>'">
          </div>
          <div class="enter">
            <div class="en">ENTER <br><i class="fas fa-chevron-down"></i></div>
          </div>
        </div>
      </li>
      <?php 
        endforeach; 
      endif;
      ?>
    </ul>
  </div>
</div>
<main>