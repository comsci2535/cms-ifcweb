<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banners extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('banners_m');
	} 

	public function get_banners()
	{  
        $input['active']    = 1;
        $input['recycle']   = 0; 
        $banners            = $this->banners_m->get_rows($input)->result(); 
        if(!empty($banners)):
            foreach($banners as $item):
                if(CURRENT_LANG=='en'):
                    $item->title       = !empty($item->title_en) ? $item->title_en : $item->title;
                    $item->excerpt     = !empty($item->excerpt_en) ? $item->excerpt_en : $item->excerpt; 
                    $item->file_th     = !empty($item->file_en) ? $item->file_en : $item->file_th; 
                endif;
            endforeach;
        endif;    
         // load config money
        $money_setting = Modules::run('configs/get_setting', 'money_setting');   
        $data['money_setting']  = $money_setting;
        $data['banners']        = $banners; 
        $this->load->view('banners', $data);
    }
    
}