<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Status_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }

    public function get_rows($param) 
    {
          
        if(!empty($param['status_id'])): 
            $this->db->where('a.status_id', $param['status_id']); 
        endif;

        if(isset($param['active'])): 
            $this->db->where('a.active', $param['active']); 
        endif;
        
        $this->db->where('a.recycle', 0); 

        $query = $this->db
                        ->select('a.*')
                        ->from('status a')
                        ->get();
        return $query;
    } 

    public function get_status_by_id($status_id) 
    {
        $query = $this->db
                        ->select('a.*')
                        ->from('status a')
                        ->where('status_id', $status_id)
                        ->get();
        return $query;
    }
}
