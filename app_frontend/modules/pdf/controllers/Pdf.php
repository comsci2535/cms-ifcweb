<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pdf extends MX_Controller {

	public function index()
	{
		$im = new imagick('file.pdf[0]');
		$im->setImageFormat('jpg');
		header('Content-Type: image/jpeg');
		echo $im;
	}
}
