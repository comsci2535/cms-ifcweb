<script>
  $('#search_url [submit]').click(function (e) {
    $('#search_url').submit();
  });
  $('#search_url_mini [submit]').click(function (e) {
    $('#search_url_mini').submit();
  });
  
</script>

<script>
  function modal_login(params) {
    $("#modal_login").modal("hide");
  }

  function modal_register(params) {
    $("#modal_register").modal("show");
  }

  function modal_forgot(params) {
    $("#modal_forgot").modal("show");
  }
  $('[modal_register]').click(function (e) {
    modal_login();
    modal_register();
  });

  $('[modal_forgot]').click(function (e) {
    modal_login();
    modal_forgot();
  });

  $('#max-sm-search-click').click(function (e) {
    // alert( $(this).attr('data-c') );
    if ($(this).attr('data-c') == '0') {
      $("#max-sm-search-click").attr('data-c', '1');
      $("#max-sm-search").show();
    } else {
      $("#max-sm-search-click").attr('data-c', '0');
      $("#max-sm-search").hide();
    }
  });
  $(document).mouseup(function (e) {
    var container = $("#max-sm-search");
    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) {
      container.hide();
    }
  });
</script>

<!-- jquery.Thailand-->
<script type="text/javascript" src="<?=base_url('scripts/frontend/jquery.Thailand/dependencies/zip.js/zip.js')?>">
</script>
<script type="text/javascript" src="<?=base_url('scripts/frontend/jquery.Thailand/dependencies/JQL.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('scripts/frontend/jquery.Thailand/dependencies/typeahead.bundle.js')?>">
</script>
<script type="text/javascript" src="<?=base_url('scripts/frontend/jquery.Thailand/jquery.Thailand.min.js')?>"></script>
<script type="text/javascript">
  $.Thailand({
    database: '<?=base_url('scripts/frontend/jquery.Thailand/db.json');?>',

    $district: $('[name="district"]'),
    $amphoe: $('[name="amphoe"]'),
    $province: $('[name="province"]'),
    $zipcode: $('[name="zipcode"]'),

    onDataFill: function (data) {
      //console.info('Data Filled', data);
    },

    onLoad: function () {
      console.info('Autocomplete is ready!');
      $('#loader, .demo').toggle();
    }
  });
  // watch on change

  // $('[name="district"]').change(function () {
  //   console.log('ตำบล', this.value);
  // });
  // $('[name="amphoe"]').change(function () {
  //   console.log('อำเภอ', this.value);
  // });
  // $('[name="province"]').change(function () {
  //   console.log('จังหวัด', this.value);
  // });
  // $('[name="zipcode"]').change(function () {
  //   console.log('รหัสไปรษณีย์', this.value);
  // });
</script>

<script>
  $(window).on('scroll', function () {
    var scroll = $(window).scrollTop();
    if (scroll < 400) {
      $(".social-icon").removeClass('scoll');
    } else {
      $(".social-icon").addClass('scoll');
    }
  });
</script>

<!-- loading -->
<script type="text/javascript" src="<?=base_url('scripts/frontend/loading/jquery.loading.js');?>"></script>
<!-- sweetalert2 -->
<script type="text/javascript" src="<?=base_url('scripts/frontend/sweetalert2/sweetalert2.min.js');?>"></script>
<!-- validate -->
<script type="text/javascript" src="<?=base_url('scripts/frontend/validate/jquery.validate.min.js')?>"></script>

<script type="text/javascript">
  $(document).ready(function () {
    $.validator.addMethod("lettersonly", function(value, element) {
      return this.optional(element) || /^[a-z0-9]+$/i.test(value);
    }, "Letters only please"); 

    $("#form_register").validate({
      rules: {
        full_name: "required",
        username: {
          required: true,
          minlength: 5,
          lettersonly: true,
          remote: {
            url: "<?=site_url('member/check_match');?>",
            type: "post",
            data: {
              id: function () {
                return $('input[name="id"]').val();
              }
            }
          }
        },
        password: {
          required: true,
          minlength: 5
        },
        confirm_password: {
          required: true,
          minlength: 5,
          equalTo: "#password"
        },
        email: {
          required: true,
          email: true,
          remote: {
            url: "<?=site_url('member/check_match');?>",
            type: "post",
            data: {
              id: function () {
                return $('input[name="id"]').val();
              }
            }
          }
        },
        phone: {
          required: true,
          minlength: 7,
          maxlength: 10,
          number: true
        },
      },
      messages: {
        full_name: "Please enter your name",
        username: {
          required: "Please enter username",
          minlength: "Your username min 5 characters",
          lettersonly: "Please enter A-Z or 0-9",
          remote: jQuery.validator.format("({0}) is already in use")
        },
        password: {
          required: "Please enter password",
          minlength: "Your username min 5 characters"
        },
        confirm_password: {
          required: "Please enter confirm password",
          minlength: "Your username min 5 characters",
          equalTo: "Password don't math"
        },
        email: {
          required: "Please enter email",
          remote: jQuery.validator.format("({0}) is already in use")
        },
        phone: {
          required: "Please enter phone",
          minlength: "Your username min 7 characters",
          maxlength: "Your username max 10 characters",
        },
      },
      submitHandler: function(form) {  
        if ($(form).valid()==true) {
          $("body").loading({theme: 'dark'});  // ****** loading

          $.ajax({
            url: "<?=site_url('member/member_save');?>",
            method: "POST",
            dataType: "json",
            data: $("#form_register").serialize(),
            success: function (data) {
              $("body").loading('stop');  // ****** loading
              console.log(data);
              Swal.fire({
                title: data.status,
                text: data.message,
                type: data.status
              }).then(function() {
                if(data.status=='success') {
                  var url_back = '<?=(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>';
                  window.location = url_back;
                }
              })
            }
          });
        }
      },
      errorElement: "em",
    });
  });
</script>

<script>
  $(document).ready(function () {
    $("#form_update_pass").validate({
      rules: {
        username: {
          required: true,
          minlength: 5,
          lettersonly: true,
          remote: {
            url: "<?=site_url('member/check_match');?>",
            type: "post",
            data: {
              id: function () {
                return $('input[name="id"]').val();
              }
            }
          }
        },
        password: {
          required: true,
          minlength: 5
        },
        confirm_password: {
          required: true,
          minlength: 5,
          equalTo: "#password"
        },
      },
      messages: {
        username: {
          required: "Please enter username",
          minlength: "Your username min 5 characters",
          lettersonly: "Please enter A-Z or 0-9",
          remote: jQuery.validator.format("({0}) is already in use")
        },
        password: {
          required: "Please enter password",
          minlength: "Your username min 5 characters"
        },
        confirm_password: {
          required: "Please enter confirm password",
          minlength: "Your username min 5 characters",
          equalTo: "Password don't math"
        },
      },
      submitHandler: function(form) {  
        if ($(form).valid()==true) {
          $("body").loading({theme: 'dark'});  // ****** loading

          $.ajax({
            url: "<?=site_url('member/member_save');?>",
            method: "POST",
            dataType: "json",
            data: $("#form_update_pass").serialize(),
            success: function (data) {
              $("body").loading('stop');  // ****** loading
              console.log(data);
              Swal.fire({
                title: data.status,
                text: data.message,
                type: data.status
              }).then(function() {
                if(data.status=='success') {
                  var url_back = '<?=(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>';
                  window.location = url_back;
                }
              })
            }
          });
        }
      },
      errorElement: "em",
    });
  });
</script>

<script>
  $(document).ready(function () {
    $("#form_forget_pass").validate({
      rules: {
        email: {
          required: true,
          email: true,
        },
      },
      messages: {
        email: {
          required: "Please enter email",
        },
      },
      submitHandler: function(form) {  
        if ($(form).valid()==true) {
          $("body").loading({theme: 'dark'}); // ****** loading

          $.ajax({
            url: "<?=site_url('member/member_forget_pass');?>",
            method: "POST",
            dataType: "json",
            data: $("#form_forget_pass").serialize(),
            success: function (data) {
              $("body").loading('stop');  // ****** loading
              console.log(data);
              Swal.fire({
                title: data.status, text: data.message, type: data.status
              }).then(function() {
                if(data.status=='success') {
                  var url_back = '<?=(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>';
                  window.location = url_back;
                }
              })
            }
          });
        }
      },
      errorElement: "em",
    });
  });
</script>

<script>
  $(document).ready(function () {
    $("#form_login").validate({
      rules: {
        username: "required",
        password: "required",
      },
      messages: {
        username: "Please enter username",
        password: "Please enter password",
      },
      submitHandler: function(form) {  
        if ($(form).valid()==true) {
          $("body").loading({theme: 'dark'});  // ****** loading

          $.ajax({
            url: "<?=site_url('member/login');?>",
            method: "POST",
            dataType: "json",
            data: $("#form_login").serialize(),
            success: function (data) {
              $("body").loading('stop');  // ****** loading
              console.log(data);
              Swal.fire({
                title: data.status,
                text: data.message,
                type: data.status
              }).then(function() {
                if(data.status=='success') {
                  var url_back = '<?=(isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>';
                  window.location = url_back;
                }
              })
            }
          });
        }
      },
      errorElement: "em",
    });
  }); 

</script>
