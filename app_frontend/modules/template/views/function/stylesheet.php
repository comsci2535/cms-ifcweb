<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
<!-- jquery.Thailand-->
<link rel="stylesheet" href="<?=base_url('scripts/frontend/jquery.Thailand/jquery.Thailand.min.css')?>">
<!-- sweetalert2 -->
<link rel="stylesheet" href="<?=base_url('scripts/frontend/sweetalert2/sweetalert2.min.css'); ?>">
<!-- loading -->
<link rel="stylesheet" href="<?=base_url('scripts/frontend/loading/jquery.loading.css'); ?>">
