
<!DOCTYPE html>

<html lang="en">
<!-- begin::Head -->

<head>
	<meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
	<?=$seo;?>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="facebook-domain-verification" content="4rend79zrzadq926pwlsb6ykcbda06" />
    <!-- Place favicon.ico in the root directory -->
	<link rel="shortcut icon" href="<?=base_url('images/logo/favicon.ico');?>" type="image/x-icon">
    <link rel="icon" href="<?=base_url('images/logo/favicon.ico');?>" type="image/x-icon">
    <!-- CSS here -->
	<link rel="stylesheet" href="<?=base_url('scripts/frontend/css/bootstrap.min.css')?>">
	<link rel="stylesheet" href="<?=base_url('scripts/frontend/css/owl.carousel.min.css')?>">
	<link rel="stylesheet" href="<?=base_url('scripts/frontend/css/slicknav.css')?>">
	<link rel="stylesheet" href="<?=base_url('scripts/frontend/css/animate.min.css')?>">
	<link rel="stylesheet" href="<?=base_url('scripts/frontend/css/magnific-popup.css')?>">
	<link rel="stylesheet" href="<?=base_url('scripts/frontend/css/themify-icons.css')?>">
	<link rel="stylesheet" href="<?=base_url('scripts/frontend/css/slick.css')?>">
	<link rel="stylesheet" href="<?=base_url('scripts/frontend/css/nice-select.css')?>">
	<link rel="stylesheet" href="<?=base_url('scripts/frontend/css/style.css')?>">
	<!-- fontawesome-free-5.13.1-web -->
	<link rel="stylesheet" href="<?=base_url('scripts/frontend/fontawesome-free-5.13.1-web/css/all.css')?>">
	
	<?php $this->load->view('template/function/stylesheet');?>
	<?php
	if(!empty($function)){
		$length1 = count($function);
		for ($x = 0; $x < $length1; $x++) {
			$this->load->view($function[$x].'/function/stylesheet');
		}
	}
	?>
	<!-- app -->
	<link rel="stylesheet" href="<?=base_url('scripts/frontend/class/css/app.css')?>">

</head>

<body>

	<?php $this->load->view('template/'.$header);?>
	<?php $this->load->view($content);?>
	<?php $this->load->view('template/'.$footer);?>

    	<!-- JS here -->

	<script src="<?=base_url('scripts/frontend/js/vendor/modernizr-3.5.0.min.js')?>"></script>
	<!-- Jquery, Popper, Bootstrap -->
	<script src="<?=base_url('scripts/frontend/js/vendor/jquery-1.12.4.min.js')?>"></script>
	<script src="<?=base_url('scripts/frontend/js/popper.min.js')?>"></script>
	<script src="<?=base_url('scripts/frontend/js/bootstrap.min.js')?>"></script>
	<!-- Jquery Mobile Menu -->
	<script src="<?=base_url('scripts/frontend/js/jquery.slicknav.min.js')?>"></script>

	<!-- Jquery Slick , Owl-Carousel Plugins -->
	<script src="<?=base_url('scripts/frontend/js/owl.carousel.min.js')?>"></script>
	<script src="<?=base_url('scripts/frontend/js/slick.min.js')?>"></script>
	<!-- One Page, Animated-HeadLin -->
	<script src="<?=base_url('scripts/frontend/js/wow.min.js')?>"></script>
	<script src="<?=base_url('scripts/frontend/js/animated.headline.js')?>"></script>
	<script src="<?=base_url('scripts/frontend/js/jquery.magnific-popup.js')?>"></script>
	
	<!-- Nice-select, sticky -->
	<script src="<?=base_url('scripts/frontend/js/jquery.nice-select.min.js')?>"></script>
	<script src="<?=base_url('scripts/frontend/js/jquery.sticky.js')?>"></script>
	
	<!-- Jquery Plugins, main Jquery -->	
	<script src="<?=base_url('scripts/frontend/js/plugins.js')?>"></script>
	<script src="<?=base_url('scripts/frontend/js/main.js')?>"></script>

	
	<?php $this->load->view('template/function/script');?>
	<?php
	if(!empty($function)){
		$length2 = count($function);
		for ($x = 0; $x < $length2; $x++) {
			$this->load->view($function[$x].'/function/script');
		}
	}
	?> 
	<script>
		function alert_box(text){ 
			Swal.fire({
				title: text, 
				type: 'warning',
				confirmButtonText: 'ตกลง'
			})
		}
	</script>

</body>
</html>