<?php 
    $contactus_f = Modules::run('contact/contact_footer');
?>
<div class="social-icon" style="z-index: 9999;">
    <a href="<?php echo !empty($contactus_f["link_facebook"]) ? $contactus_f["link_facebook"] : 'javascript:void(0);'?>" target="_blank">
        <div class="item">
            <img src="<?=base_url('images/logo/social (1).jpg')?>" alt="<?php echo !empty($contactus_f["facebook"]) ? $contactus_f["facebook"] : 'IFC Express'?>">
        </div>
    </a>
    <a href="<?php echo !empty($contactus_f["link_line"]) ? $contactus_f["link_line"] : 'javascript:void(0);'?>" target="_blank">
        <div class="item">
            <img src="<?=base_url('images/logo/social (2).jpg')?>" alt="<?php echo !empty($contactus_f["line"]) ? $contactus_f["line"] : 'IFC Express'?>">
        </div>
    </a>
    <a href="<?php echo !empty($contactus_f["phone"]) ? 'tel:'.$contactus_f["phone"] : 'javascript:void(0);'?>" target="_blank">
        <div class="item">
            <img src="<?=base_url('images/logo/social (3).jpg')?>" alt="<?php echo !empty($contactus_f["phone"]) ? $contactus_f["phone"] : 'IFC Express'?>">
        </div>
    </a>
    <a href="<?php echo !empty($contactus_f["link_instagram"]) ? $contactus_f["link_instagram"] : 'javascript:void(0);'?>" target="_blank">
        <div class="item">
            <img src="<?=base_url('images/logo/social (4).jpg')?>" alt="<?php echo !empty($contactus_f["instagram"]) ? $contactus_f["instagram"] : 'IFC Express'?>">
        </div>
    </a>
</div>
<footer class="notranslate">
    <!--? Footer Start data-background="assets/img/gallery/footer_bg.jpg"-->
    <div class="footer-area section-bg" >
        <div class="container">
            <div class="footer-top footer-padding">
                <div class="row d-flex justify-content-between">
                    <div class="col-md-3 col-12">
                        <div class="single-footer-caption">
                            <div class="footer-tittle">
                                <h4><?php echo !empty($contactus_f['company_name']) ? $contactus_f['company_name'] : "-"?></h4>
                                <p><?php echo !empty($contactus_f['detail']) ? html_entity_decode($contactus_f['detail']) : "-"?></p>
                                <p><?=$this->lang->line('phone_number');?> : <?php echo !empty($contactus_f['phone']) ? $contactus_f['phone'] : "-"?></p>
                                <p><?=$this->lang->line('email');?> : <?php echo !empty($contactus_f['email']) ? $contactus_f['email'] : "-"?></p>

                            </div>
                        </div>
                    </div>
                    <div class="offset-md-2 col-md-2 col-12"><br>
                        <a href="<?=site_url();?>">
                            <p class="link"><?=$this->lang->line('home');?></p>
                        </a>
                        <a href="<?=site_url('product');?>">
                            <p class="link"><?=$this->lang->line('product');?></p>
                        </a>
                        <a href="<?=site_url('service');?>">
                            <p class="link"><?=$this->lang->line('service');?></p>
                        </a>
                        <a href="<?=site_url('news');?>">
                            <p class="link"><?=$this->lang->line('news_articles');?></p>
                        </a>
                    </div>
                    <div class="col-md-2 col-12"><br>
                        <a href="<?=site_url('help');?>">
                            <p class="link"><?=$this->lang->line('help');?></p>
                        </a>
                        <a href="<?=site_url('compact');?>">
                            <p class="link"><?=$this->lang->line('terms_of_use');?></p>
                        </a>
                        <a href="<?=site_url('about');?>">
                            <p class="link"><?=$this->lang->line('about_us');?></p>
                        </a>
                        <a href="<?=site_url('contact');?>">
                            <p class="link"><?=$this->lang->line('contact_us');?></p>
                        </a>
                    </div>
                    <div class="col-md-3 col-12"><br>
                        <a href="<?=site_url('help/index/คำถามที่พบบ่อย');?>">
                            <p class="link"><?=$this->lang->line('question');?></p>
                        </a>
                        <a href="<?=site_url('help/index/วิธีการใช้งาน');?>">
                            <p class="link"><?=$this->lang->line('how_to_website');?></p>
                        </a>
                        <a href="">
                            <!-- <p>นโยบายการคืนเงินและเงื่อนไขต่างๆ</p> -->
                        </a>
                        <hr style="margin: 15px 0;">
                        <!-- <p>
                            <a href=""><i style="margin-right: 8px;" class="fab fa-facebook-f"></i></a>
                            <a href=""><i style="margin-right: 8px;" class="fab fa-line"></i></a>
                            <a href=""><i style="margin-right: 8px;" class="far fa-envelope"></i></a>
                        </p> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Footer End-->
</footer>
<!-- Scroll Up -->
<div id="back-top">
    <a title="Go to Top" href="#"> <i class="fas fa-level-up-alt"></i></a>
</div>

<!-- modal_login -->
<div class="modal animate__animated animate__bounceInDown" id="modal_login">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <div class="modal-body">
                <form id="form_login" method="post">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div class="form-group">
                                    <img class="logo" src="<?=base_url('images/logo/logo.png')?>" alt="IFC Express">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('user_name');?></label>
                                    <input type="text" class="form-control" name="username" maxlength="50" placeholder="<?=$this->lang->line('user_name');?>...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('password');?></label>
                                    <input type="password" class="form-control" name="password" maxlength="50" placeholder="<?=$this->lang->line('password');?>...">
                                </div>
                            </div>
                            <div class="col-12 text-center">
                                <button type="submit" class="btn btn-dufault mr-3"><?=$this->lang->line('login');?></button>
                                <button modal_register type="button" class="btn btn-primary"><?=$this->lang->line('register');?></button>
                            </div>
                            <div class="col-12 mt-3 text-center">
                                <!-- <span><?=$this->lang->line('forget_password');?> ? </span> -->
                                <a href="javascript:;" modal_forgot>
                                    <span class="c_blue"> <?=$this->lang->line('forget_password');?> ?</span>
                                </a>
                            </div>
                            <div class="col-12 text-center">
                                <hr style="margin: 15px 0;">
                                <?php $url_login_face = Modules::run('login/url_login_face'); ?>
                                <a href="<?=$url_login_face?>">
                                    <img src="<?=base_url('images/login-facebook.png');?>" alt="" style="height: 60px;">
                                </a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- modal_forgot -->
<div class="modal animate__animated animate__bounceInDown" id="modal_forgot">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <div class="modal-body">
                <form id="form_forget_pass" method="post">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div class="form-group">
                                    <img class="logo" src="<?=base_url('images/logo/logo.png')?>" alt="IFC Express">
                                </div>
                            </div>
                            <div class="col-md-12 text-center">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('title_forget_password');?></label>
                                    <input type="email" class="form-control" name="email" placeholder="<?=$this->lang->line('email');?>...">
                                </div>
                            </div>
                            <div class="col-12 text-center">
                                <button type="submit" class="btn btn-dufault mr-3"><?=$this->lang->line('confirm');?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- modal_register -->
<?php if (!$this->session->userdata('ses_mem')) { ?>
<div class="modal animate__animated animate__bounceInUp" id="modal_register">
    <div class="modal-dialog">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <div class="modal-body">
                <form id="form_register" method="post">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <div class="form-group">
                                    <img class="logo" src="<?=base_url('images/logo/logo.png')?>" alt="IFC Express">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('user_name');?></label>
                                    <input type="text" class="form-control" name="username" maxlength="50" placeholder="<?=$this->lang->line('user_name');?>...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('password');?></label>
                                    <input type="password" class="form-control" name="password" id="password" maxlength="50" placeholder="<?=$this->lang->line('password');?>...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('confirm_password');?></label>
                                    <input type="password" class="form-control" name="confirm_password" placeholder="<?=$this->lang->line('confirm_password');?>...">
                                </div>
                            </div>
                            <div class="col-12 text-center">
                                <hr style="margin: 10px 0;">
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('fullname');?></label>
                                    <input type="text" class="form-control" name="full_name" placeholder="<?=$this->lang->line('fullname');?>...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('address');?></label>
                                    <input type="text" class="form-control" name="house_number" placeholder="<?=$this->lang->line('address');?>...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('building_village');?></label>
                                    <input type="text" class="form-control" name="building" placeholder="<?=$this->lang->line('building_village');?>...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('street');?></label>
                                    <input type="text" class="form-control" name="road" placeholder="<?=$this->lang->line('street');?>...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('Sub_district');?></label>
                                    <input type="text" class="form-control" name="district" placeholder="<?=$this->lang->line('Sub_district');?>...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('district');?></label>
                                    <input type="text" class="form-control" name="amphoe" placeholder="<?=$this->lang->line('district');?>...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('province');?></label>
                                    <input type="text" class="form-control" name="province" placeholder="<?=$this->lang->line('province');?>...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('postal_code');?></label>
                                    <input type="text" class="form-control" name="zipcode" maxlength="5" placeholder="<?=$this->lang->line('postal_code');?>...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('email');?></label>
                                    <input type="email" class="form-control" name="email" placeholder="<?=$this->lang->line('email');?>...">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for=""><?=$this->lang->line('phone_number');?></label>
                                    <input type="text" class="form-control" name="phone" maxlength="10" placeholder="<?=$this->lang->line('phone_number');?>...">
                                </div>
                            </div>
                            <div class="col-12 text-center">
                                <input type="hidden" name="id" value="">
                                <button type="submit" class="btn btn-dufault mr-3"><?=$this->lang->line('register');?></button>
                                <!-- <a href="<?=site_url('help');?>">
                                    <button type="button" class="btn btn-primary">เข้าสู่ระบบ</button>
                                </a> -->
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php } ?>