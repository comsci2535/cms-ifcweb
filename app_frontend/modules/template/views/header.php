<!--? Preloader Start -->
<!-- <div id="preloader-active">
  <div class="preloader d-flex align-items-center justify-content-center">
    <div class="preloader-inner position-relative">
      <div class="preloader-circle"></div>
      <div class="preloader-img pere-text">
        <img src="assets/img/logo/loder.png" alt="IFC Express">
      </div>
    </div>
  </div>
</div> -->
<!-- Preloader Start -->
<header>
    <!--? Header Start -->
    <div class="header-area notranslate">
        <div class="main-header header-sticky">
            <div class="container">
                <div class="row align-items-center">
                    <!-- Logo -->
                    <div class="col-xl-2 col-lg-2 col-1">
                        <div class="logo">
                            <a href="<?=site_url();?>">
                                <img class="wfull" src="<?=base_url('images/logo/logo.png');?>" alt="IFC Express">
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-10 col-lg-10 col-10">
                        <div class="">
                            <div class="form_search">
                                <div class="blog search min-sm">
                                    <form id="search_url" action="<?=site_url('product/search_url');?>" method="post" autocomplete="off">
                                        <div class="input-group form-control-search">
                                            <input type="text" class="form-control" name="search" placeholder="<?=$this->lang->line('search_term');?>"
                                                value="" required>
                                            <div class="input-group-prepend" >
                                                <span class="input-group-text btn" submit>
                                                    <span class="min-sm"><?=$this->lang->line('search');?></span>
                                                    <span class="max-sm"><i class="fas fa-search"></i></span>
                                                </span>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="blog max-sm">
                                    <a href="javascript:;" id="max-sm-search-click" data-c="0">
                                        <i class="fas fa-search" style="font-size: 15px;"></i>
                                    </a>
                                </div>
                                <div class="blog lang" style="min-width: 60px;">
                                    <a href="<?=site_url('switchlang/lang/th');?>">
                                        <span class="<?=(CURRENT_LANG=='th')?'active':null;?>">TH</span>
                                    </a>
                                    <span> | </span>
                                    <a href="<?=site_url('switchlang/lang/en');?>">
                                        <span class="<?=(CURRENT_LANG=='en')?'active':null;?>">EN</span>
                                    </a>
                                </div>
                                <div class="blog cart">
                                    <a href="<?=site_url('product/cart');?>">
                                        <img src="<?=base_url('images/logo/header (1).jpg');?>" class="img-lo"
                                            alt="IFC Express">
                                        <span id="text-header-cart-qty"> 
                                            <?php $cartQty = Modules::run('carts/get_data_cart_list');?> 
                                            (<span><?php echo !empty($cartQty) ? count($cartQty) : 0;?></span>)</span>
                                    </a>
                                </div>
                                <?php if (!$this->session->userdata('ses_mem')) { ?>
                                <div class="blog">
                                    <a href="javascript:;" data-toggle="modal" data-target="#modal_login">
                                        <img src="<?=base_url('images/logo/header (2).jpg');?>" class="img-lo"
                                            alt="IFC Express">
                                        <span class="min-sm"> <?=$this->lang->line('login');?></span>
                                    </a>
                                </div>
                                <div class="blog min-sm" style="border: 0;">
                                    <a href="javascript:;" data-toggle="modal" data-target="#modal_register">
                                        <img src="<?=base_url('images/logo/header (3).jpg');?>" class="img-lo"
                                            alt="IFC Express">
                                        <span class="min-sm"> <?=$this->lang->line('register');?></span>
                                    </a>
                                </div>
                                <?php }else{
                                $ses_mem = modules::run('member/_get_member'); 
                                function substr_($in) {
                                    $out = mb_strimwidth($in, 0, 8, "...");
                                    return $out;
                                }
                                ?>
                                <div class="blog min-sm" style="border: 0;">
                                    <div class="dropdown">
                                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                            <?php if($ses_mem->picture){ ?>
                                                <img src="<?php echo !empty($ses_mem->picture) ? base_url($ses_mem->picture) : '';?>" class="img-lo"
                                                    onerror="this.src='<?=base_url('images/logo/header (4).jpg');?>'"
                                                    alt="IFC Express">
                                            <?php }else{ ?>
                                                <img src="<?php echo !empty($ses_mem->oauth_picture) ? $ses_mem->oauth_picture : '';?>" class="img-lo"
                                                    onerror="this.src='<?=base_url('images/logo/header (4).jpg');?>'"
                                                    alt="IFC Express">
                                            <?php } ?>
                                            
                                            <span class="min-sm"> <?=substr_($ses_mem->full_name);?></span>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right">
                                            <a class="dropdown-item <?php if($menu=='mem_pro'){echo 'active';}?>"
                                                href="<?=site_url('member/profile');?>"><?=$this->lang->line('profile');?></a>
                                            <a class="dropdown-item <?php if($menu=='mem_imp'){echo 'active';}?>"
                                                href="<?=site_url('member/imported');?>"><?=$this->lang->line('Import_products');?></a>
                                            <a class="dropdown-item <?php if($menu=='mem_his'){echo 'active';}?>"
                                                href="<?=site_url('member/history');?>"><?=$this->lang->line('Order_history');?></a>
                                            <a class="dropdown-item"
                                                href="<?=site_url('member/logout');?>"><?=$this->lang->line('logout');?></a>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="menu-main d-flex align-items-center justify-content-end">
                            <!-- Main-menu -->
                            <div class="main-menu f-right d-none d-lg-block">
                                <nav>
                                    <ul id="navigation">
                                        <li><a class="<?php if($menu=='home'){echo 'active';}?>"
                                                href="<?=site_url();?>"><?=$this->lang->line('home');?></a></li>
                                        <li><a class="<?php if($menu=='product'){echo 'active';}?>"
                                                href="<?=site_url('product');?>"><?=$this->lang->line('product');?></a></li>
                                        <li><a class="<?php if($menu=='service'){echo 'active';}?>"
                                                href="<?=site_url('service');?>"><?=$this->lang->line('service');?></a></li>
                                        <li><a class="<?php if($menu=='news'){echo 'active';}?>"
                                                href="<?=site_url('news');?>"><?=$this->lang->line('news_articles');?></a></li>
                                        <!-- <li><a class="<?php if($menu=='help'){echo 'active';}?>"
                                                href="<?=site_url('help');?>">ช่วยเหลือ</a></li> -->
                                        <li><a href="javascript:;"
                                                class="<?php if($menu=='help'){echo 'active';}?>"><?=$this->lang->line('help');?></a>
                                            <ul class="submenu">
                                                <li><a href="<?=site_url('help/index/คำถามที่พบบ่อย');?>"><?=$this->lang->line('question');?></a></li>
                                                <li><a href="<?=site_url('help/index/วิธีการใช้งาน');?>"><?=$this->lang->line('how_to_website');?></a></li>
                                            </ul>
                                        </li>
                                        <li><a class="<?php if($menu=='compact'){echo 'active';}?>"
                                                href="<?=site_url('compact');?>"><?=$this->lang->line('terms_of_use');?></a></li>
                                        <li><a class="<?php if($menu=='about'){echo 'active';}?>"
                                                href="<?=site_url('about');?>"><?=$this->lang->line('about_us');?></a></li>
                                        <li><a class="<?php if($menu=='contact'){echo 'active';}?>"
                                                href="<?=site_url('contact');?>"><?=$this->lang->line('contact_us');?></a></li>
                                        <?php if (!$this->session->userdata('ses_mem')) { ?>
                                        <li class="max-sm">
                                        <a href="javascript:;" data-toggle="modal" data-target="#modal_login">
                                            <img src="<?=base_url('images/logo/header (2).jpg');?>" style="width: 20px;" alt="IFC Express">
                                            <span> <?=$this->lang->line('login');?></span>
                                        </a>
                                        </li>
                                        <li class="max-sm">
                                        <a href="javascript:;" data-toggle="modal" data-target="#modal_register">
                                            <img src="<?=base_url('images/logo/header (3).jpg');?>" style="width: 20px;" alt="IFC Express">
                                            <span> <?=$this->lang->line('register');?></span>
                                        </a>
                                        </li>
                                        <?php }else{ ?>
                                        <li class="max-sm">
                                            <a href="javascript:;">
                                                <img src="<?=base_url('images/logo/header (4).jpg');?>" style="width: 20px;" alt="IFC Express">
                                                <span> <?=substr_($ses_mem->full_name);?></span>
                                            </a>
                                            <ul class="submenu">
                                                <li><a href="<?=site_url('member/profile');?>"><?=$this->lang->line('profile');?></a></li>
                                                <li><a href="<?=site_url('member/imported');?>"><?=$this->lang->line('Import_products');?></a></li>
                                                <li><a href="<?=site_url('member/history');?>"><?=$this->lang->line('Order_history');?></a></li>
                                                <li><a href="<?=site_url('member/logout');?>"><?=$this->lang->line('logout');?></a></li>
                                            </ul>
                                        </li>
                                    <?php } ?>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <!-- Mobile Menu -->
                    <div class="col-12">
                        <div class="mobile_menu d-block d-lg-none"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header End -->

    <div id="max-sm-search" style="position: fixed;width: 100%;background: rgb(230, 230, 230);padding: 5px 10px;top: 55px;display: none;">
        <form id="search_url_mini" action="<?=site_url('product/search_url');?>" method="post" autocomplete="off">
            <div class="input-group form-control-search">
                <input type="text" class="form-control" id="search"  name="search" placeholder="<?=$this->lang->line('search_term');?>" aria-describedby="<?=$this->lang->line('search_term');?>" required>
                <div class="input-group-prepend">
                <span class="input-group-text btn" submit>
                    <span class="min-sm"><?=$this->lang->line('search');?></span>
                    <span class="max-sm"><i class="fas fa-search"></i></span>
                </span>
                </div>
            </div>
        </form>
    </div>

</header>