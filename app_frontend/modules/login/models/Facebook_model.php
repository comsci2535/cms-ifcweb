<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Facebook_model extends CI_Model {
    function __construct() {
        $this->tableName = 'member';
        $this->primaryKey = 'id';
    }
    
    /*
     * Insert / Update facebook profile data into the database
     * @param array the data for inserting into the table
     */
    public function checkUser($userData = array()){
        if(!empty($userData)){
            //check whether user data already exists in database with same oauth info
            $this->db->select($this->primaryKey);
            $this->db->from($this->tableName);
            $this->db->where(array('email'=>$userData['email']));
            $prevQuery = $this->db->get();
            $prevCheck = $prevQuery->num_rows();
            
            if($prevCheck > 0){
                $prevResult = $prevQuery->row_array();

                $userData_['full_name']         = !empty($prevResult['full_name']) ? $prevResult['full_name'] : $userData['full_name'];
                //update user data
                $userData_['updated_at']        = date("Y-m-d H:i:s");
                $userData_['oauth_provider']    = $userData['oauth_provider'];
                $userData_['oauth_uid']         = $userData['oauth_uid'];
                $userData_['oauth_picture']     = $userData['oauth_picture'];
                $update = $this->db->update($this->tableName, $userData_, array('id' => $prevResult['id']));
                
                //get user ID
                $userID = $prevResult['id'];
            }else{
                //insert user data
                $userData['created_at']  = date("Y-m-d H:i:s");
                $userData['updated_at'] = date("Y-m-d H:i:s");
                $userData['active'] = 1;
                $insert = $this->db->insert($this->tableName, $userData);
                
                //get user ID
                $userID = $this->db->insert_id();
            }
        }
        
        //return user ID
        return $userID?$userID:FALSE;
    }

    public function find_users_by_user_facebook($oauth_uid){

		$query = $this->db->where('oauth_uid',$oauth_uid)
		         ->get('member');
		return $query->row();         
	}

    /*
    <!-- <a href="<?=$url_login_face?>">
    <img src="<?=base_url('images/login-facebook.png');?>" alt="" style="height: 60px;">
    <img src="<?=base_url('images/login-facebook.png');?>" alt="" style="height: 60px;">
    </a> -->
    */


}