<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {

	function __construct() {
		parent::__construct();
		
        $this->load->library('facebook');
        $this->load->model('facebook_model');

    }

    public function check_login_facebook(){
        // Check if user is logged in
        $userData = array();
        if($this->facebook->is_authenticated()){
            // Get user facebook profile details
            $fbUser = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,link,gender,picture.width(200).height(200)');

            // $userData['link']        = !empty($fbUser['link'])?$fbUser['link']:'';
            // $userData['gender']        = !empty($fbUser['gender'])?$fbUser['gender']:'';
            $userData['oauth_provider'] = 'facebook';
            $userData['oauth_uid']    = !empty($fbUser['id'])?$fbUser['id']:'';;
            $userData['full_name']    = $fbUser['first_name'].' '.$fbUser['last_name'];
            $userData['email']        = !empty($fbUser['email'])?$fbUser['email']:'';
            $userData['oauth_picture']    = !empty($fbUser['picture']['data']['url'])?$fbUser['picture']['data']['url']:'';
            $userData['username']        = !empty($fbUser['first_name'])?$fbUser['first_name']:'';

            
            // Insert or update user data
            $userID = $this->facebook_model->checkUser($userData);
            
            // Check user data insert or update status
            if(!empty($userID)){
                $data['userData'] = $userData;
                // $this->session->set_userdata('users', $userData);
                $users = $this->facebook_model->find_users_by_user_facebook($userData['oauth_uid']);
                // $this->_set_session($users);
                $this->session->set_userdata('ses_mem', $users);


            }else{
                $data['userData'] = array();
            }
            
            // Get logout URL
            $data['logoutURL'] = $this->facebook->logout_url();
            redirect(base_url(),'refresh');
            //redirect_back();
        }else{
            // Get login URL
            $data['authURL'] =  $this->facebook->login_url();
            redirect(base_url(),'refresh');
            //redirect_back();
        }
        
    }

    public function url_login_face(){
        
        // $facebook = array(
        //     'data_login' => $this->facebook->login_url(),
        //     'data_id' => 'facebook',
        //     'verion' => 'oa.2 - v9.0',
        // );
        // echo json_encode($facebook);
        return $this->facebook->login_url();
    }


    private function _set_session($users){
        //Login Success
        if(empty($users->oauth_picture) && empty($users->file) ){
            $file='images/users/'.rand(1,10).'.png';
        }else{
            $file=$users->file;
        }
        $set_session =  array(
            'UID' => $users->user_id,
            'oauth_uid' => $users->oauth_uid,
            'Username' => $users->username,
            'Name' => $users->fname.' '.$users->lname, 
            'fullname' => $users->fullname, 
            'couponCode' => $users->couponCode,
            'Imgprofile' => $file,
            'ImgprofileFace' => $users->oauth_picture,
            'UserType' => $users->type,
        );

        $this->session->set_userdata('ses_mem',$set_session);
    }

}
