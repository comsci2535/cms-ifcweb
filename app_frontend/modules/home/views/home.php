  <!-- load banners -->
  <?php echo Modules::run('banners/get_banners');?>

  <!-- load service other -->
  <?php echo Modules::run('service/get_service_home');?>

  <!-- load product home page -->
  <?php echo Modules::run('product/get_product_home');?>

  <!-- load calculate home page -->
  <?php echo Modules::run('calculate/get_calculate');?> 

  <!-- load news other -->
  <?php echo Modules::run('news/get_home');?>

</main>