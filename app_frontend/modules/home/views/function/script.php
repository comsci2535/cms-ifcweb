<script>
  $('.enter').click(function () {
    $('html, body').animate({
      scrollTop: $("#product").offset().top - 100
    }, 1000)
  });
</script>

<script src="<?=base_url('scripts/frontend/lightslider-master/dist/js/lightslider.min.js')?>"></script>
<script>
  $('#slider_banner').lightSlider({
    item: 1,
    controls: false,
    pager: false,
    autoWidth: false,
    slideMargin: 0,

    speed: 1200, //ms'
    auto: true,
    loop: true,
    slideEndAnimation: true,
    pause: 3000,
  });
</script>
<script>
  $(document).ready(function () {
    var slider_service = $('#slider_service').lightSlider({
      item: 3,
      controls: false,
      loop: true,
      slideMargin: 15,
      responsive: [{
          breakpoint: 992,
          settings: {
            item: 3,
          }
        },
        {
          breakpoint: 768,
          settings: {
            item: 2,
          }
        },
      ]
    });
    $('#goToPrevSlide').on('click', function () {
      slider_service.goToPrevSlide();
    });
    $('#goToNextSlide').on('click', function () {
      slider_service.goToNextSlide();
    });
  });
</script>
<script>
  $(document).ready(function () {
    var slider_news = $('#slider_news').lightSlider({
      item: 4,
      controls: false,
      loop: true,
      slideMargin: 15,
      responsive: [{
          breakpoint: 992,
          settings: {
            item: 3,
          }
        },
        {
          breakpoint: 768,
          settings: {
            item: 2,
          }
        },
      ]
    });
    $('#goToPrevSlide.slider_news1').on('click', function () {
        slider_news.goToPrevSlide();
    });
    $('#goToNextSlide.slider_news2').on('click', function () {
        slider_news.goToNextSlide();
    });
  });

  $(document).on('change', '#type_transportations_setting_id', function(){
    var id = $(this).val();

    $('#type_transportation_cust_id').html('<option value="">เลือกประเภทการซื้อ</option>');
    $('#type_transportation_id').html('<option value="">เลือกประเภทขนส่ง</option>');
    $('select').niceSelect('update');
    $.ajax({
        type: "POST",
        url: '<?=base_url('calculate/get_transportations_cust')?>',
        data:{
            code : id,
            type: 'cust'
        },
        success: function (result) { 
            // // show modal 
            var html = '';
            if(result.status == 200){

              $.each(result.info, function(key, val){
                html+=' <option value="'+val['type_transportation_cust_id']+'">'+val['title']+'</option>';
              });

              $('#type_transportation_cust_id').append(html);
              $('select').niceSelect('update');
            }
        },
        error: function (request, status, error) {
            console.log("ajax call went wrong:" + request.responseText);
        }
    });
  });

  $(document).on('change', '#type_transportation_cust_id', function(){
    $('#type_transportation_id').html('<option value="">เลือกประเภทขนส่ง</option>');
    $('select').niceSelect('update');
    var id = $(this).val();
    $.ajax({
        type: "POST",
        url: '<?=base_url('calculate/get_transportations_cust')?>',
        data:{
            code : id,
            type: 'trans'
        },
        success: function (result) { 
            // // show modal 
            var html = '';
            if(result.status == 200){

              $.each(result.info, function(key, val){
                html+=' <option value="'+val['type_transportation_id']+'">'+val['title']+'</option>';
              });

              $('#type_transportation_id').append(html);
              $('select').niceSelect('update');
            }
        },
        error: function (request, status, error) {
            console.log("ajax call went wrong:" + request.responseText);
        }
    });
  });

  $(document).on('click', '#btn-calculate-process', function(){
    
    var wide    = $('#wide').val();
    var long    = $('#long').val();
    var high    = $('#high').val();
    var weight  = $('#weight').val();

    var type_transportations_setting_id  = $('#type_transportations_setting_id').val();
    var type_transportation_cust_id  = $('#type_transportation_cust_id').val();
    var type_transportation_id  = $('#type_transportation_id').val();

    if(wide == ''){
      alert_box('กรุณาระบุความกว้าง'); 
      return false;
    } 
    if(long == ''){
      alert_box('กรุณาระบุความยาว'); 
      return false;
    } 
    if(high == ''){
      alert_box('กรุณาระบุความสูง'); 
      return false;
    } 
    // if(weight == ''){
    //   alert_box('กรุณาระบุน้ำหนัก'); 
    //   return false;
    // } 

    if(type_transportations_setting_id == ''){ 
      alert_box('กรุณาเลือกประเภทสินค้า'); 
      return false;
    }

    if(type_transportation_cust_id == ''){ 
      alert_box('กรุณาเลือกประเภทการซื้อ'); 
      return false;
    }

    if(type_transportation_id == ''){ 
      alert_box('กรุณาเลือกประเภทขนส่ง'); 
      return false;
    }

    

    $.ajax({
        type: "POST",
        url: '<?=base_url('calculate/get_data')?>',
        // dataType: 'json',
        data:{
          wide : wide,
          long : long,
          high : high,
          weight : weight,
          type_transportations_setting_id : type_transportations_setting_id,
          type_transportation_cust_id : type_transportation_cust_id,
          type_transportation_id : type_transportation_id
        },
        success: function (result) { 
            // // show modal 
          $('#calculate-tatal').html(result.info['price_send']+'฿');
        },
        error: function (request, status, error) {
            console.log("ajax call went wrong:" + request.responseText);
        }
    });

  });
</script>

<script>
$('[data-transportations]' ).click(function() { 
  var data = $(this).attr('data-transportations');
  $('[data-transportations]').removeClass('active');
  $(this).addClass('active');
  $('[transportations]').hide();
  $('[transportations="'+data+'"]').slideDown(400);
});
</script>