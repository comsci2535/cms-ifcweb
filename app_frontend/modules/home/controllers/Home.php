<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MX_Controller {

	public function __construct()
	{
		parent::__construct();
        // $this->load->model('DBrecord');
	}

    private function seo($sco = null)
	{
        
		$title          = !empty($sco['siteTitle']) ? $sco['siteTitle'] : "IFC | Express";
		$robots         = !empty($sco['metaKeyword']) ? $sco['metaKeyword'] : "IFC | Express";
		$description    = !empty($sco['metaDescription']) ? $sco['metaDescription'] : "IFC | Express";
		$keywords       = !empty($sco['metaKeyword']) ? $sco['metaKeyword'] : "IFC | Express";
		$meta  			= '<TITLE>'.$title.'</TITLE>';
		$meta 		   .= '<meta name="robots" content="'.$robots.'"/>';
		$meta		   .= '<meta name="description" content="'.$description.'"/>';
        $meta 		   .= '<meta name="keywords" content="'.$keywords.'"/>';
        $meta 		   .= '<meta property="og:url" content="'.site_url().'" />';
        $meta 		   .= '<meta property="og:type" content="website" />';
        $meta 		   .= '<meta property="og:title" content="'.$title.'" />';
        $meta 		   .= '<meta property="og:description" content="'.$description.'" />';
        $meta 		   .= '<meta property="og:image" content="'.base_url('images/logo/logo.png').'" />';
		return $meta;
	}

	public function index()
	{
        $obj_seo = Modules::run('configs/get_setting', 'general');
        
        $data = array(
            'seo'     => $this->seo($obj_seo),
            'menu'    => 'home',
            'header'  => 'header',
            'content' => 'home',
            'footer'  => 'footer',
            'function'=>  array('home'),
        );
        $this->load->view('template/body', $data);
    }

    public function search_urls()
	{
        $input = $this->input->post(null, true);
        $domain = parse_url($input['search'], PHP_URL_HOST);
        $url_r = array('item.taobao.com', 'detail.tmall.com', 'detail.1688.com');

        $data_act = "tmall.detail";
        $data_url = "https://detail.tmall.com/item.htm?spm=a221t.1812074.goodlist.6.2ecd4208c0zCUI&id=625738745684&skuId=4605298594239";


        $url = 'https://dev.getdev.top/teelek/taobao-api/';
        $data = array(
            "act" => $data_act,
            "url" => $data_url,
        );

        $make_call = $this->callAPI('POST', $url, json_encode($data));
        // echo count($make_call);
        // echo htmlspecialchars($make_call, ENT_COMPAT, 'ISO-8859-15');
        //$obj = json_decode('{ "name": "��ɯ����Ů��Ͳ���ɫŮʿ���ഺ�＾���޳�Ͳ��Ӻ��ﶬȫ��Ů��",  "price": "49",  "color": [  {  "name" : "��?˫29.9Ԫ/?˫49.8Ԫ-10˫ʡ10Ԫ+��2˫���Żݡ�",  "photo" : "https://img.alicdn.com/imgextra/i3/272715291/O1CN0118x8yp1oxKCb23OOa_!!272715291.jpg"  }  ,  {  "name" : "150��5˫����������ɫ5",  "photo" : "https://img.alicdn.com/imgextra/i4/272715291/O1CN01LyFeHS1oxKCgIWNX4_!!272715291.jpg"  }  ,  {  "name" : "150��5˫����������ɫ3���2",  "photo" : "https://img.alicdn.com/imgextra/i4/272715291/O1CN01UlKoUD1oxKCv9T5M6_!!272715291.jpg"  }  ,  {  "name" : "150��5˫����������ɫ2ǳ��2��ɫ1",  "photo" : "https://img.alicdn.com/imgextra/i2/272715291/O1CN01iTdvTp1oxKCYWutyM_!!272715291.jpg"  }  ,  {  "name" : "150��5˫����������ɫ5",  "photo" : "https://img.alicdn.com/imgextra/i4/272715291/O1CN01lWHZGi1oxKCb0iL7S_!!272715291.jpg"  }  ,  {  "name" : "150��5˫��������ǳ��2��2��1",  "photo" : "https://img.alicdn.com/imgextra/i2/272715291/O1CN01ZBaWlK1oxKCbhJUMJ_!!272715291.jpg"  }  ,  {  "name" : "150��5˫����������1��1ǳ��1��1��1",  "photo" : "https://img.alicdn.com/imgextra/i2/272715291/O1CN01DwEpxg1oxKCSqBGpF_!!272715291.jpg"  }  ,  {  "name" : "150��5˫������������1����1�̰�1��ɫ1ǳ��1",  "photo" : "https://img.alicdn.com/imgextra/i1/272715291/O1CN01NAFUQH1oxKCYWu1vi_!!272715291.jpg"  }  ,  {  "name" : "150��10˫����������ɫ10",  "photo" : "https://img.alicdn.com/imgextra/i3/272715291/O1CN01j11jiF1oxKCrEwu34_!!272715291.jpg"  }  ,  {  "name" : "150��10˫����������ɫ6���4",  "photo" : "https://img.alicdn.com/imgextra/i2/272715291/O1CN01avB2pW1oxKCu0pZAA_!!272715291.jpg"  }  ,  {  "name" : "150��10˫����������ɫ10",  "photo" : "https://img.alicdn.com/imgextra/i3/272715291/O1CN01hwbwYh1oxKCwxYFAU_!!272715291.jpg"  }  ,  {  "name" : "150��10˫����������ɫ4ǳ��4��ɫ2",  "photo" : "https://img.alicdn.com/imgextra/i3/272715291/O1CN011vTLO11oxKCrF083O_!!272715291.jpg"  }  ,  {  "name" : "150��10˫��������ǳ��4��4��2",  "photo" : "https://img.alicdn.com/imgextra/i3/272715291/O1CN01YGN1eV1oxKCv9WNMW_!!272715291.jpg"  }  ,  {  "name" : "150��10˫����������2��2ǳ��2��2��2",  "photo" : "https://img.alicdn.com/imgextra/i4/272715291/O1CN012T09l21oxKCpXdwXU_!!272715291.jpg"  }  ,  {  "name" : "150��10˫������������2����2�̰�2��ɫ2ǳ��2",  "photo" : "https://img.alicdn.com/imgextra/i2/272715291/O1CN01yUdb4I1oxKCo2P87Y_!!272715291.jpg"  }  ,  {  "name" : "6002��5˫�����ļ����ɫ1ǳ��1��ɫ1����1����1",  "photo" : "https://img.alicdn.com/imgextra/i1/272715291/O1CN014w2IxP1oxKCfxwos7_!!272715291.jpg"  }  ,  {  "name" : "6002��10˫�����ļ����ɫ2ǳ��2��ɫ2����2����2",  "photo" : "https://img.alicdn.com/imgextra/i1/272715291/O1CN01muA4Pj1oxKCrbcu39_!!272715291.jpg"  }  ,  {  "name" : "521��5˫�����ļ����ɫ1�̰�1����1��ɫ1�̻�1",  "photo" : "https://img.alicdn.com/imgextra/i3/272715291/O1CN01kkpyjh1oxKCdN7jse_!!272715291.jpg"  }  ,  {  "name" : "521��10˫�����ļ����ɫ2�̰�2����2��ɫ2�̻�2",  "photo" : "https://img.alicdn.com/imgextra/i2/272715291/O1CN012qYf6S1oxKCpXegSC_!!272715291.jpg"  }  ,  {  "name" : "318��5˫�����ļ��ǳ��1ǳ��1��ɫ1��ɫ1��ɫ1",  "photo" : "https://img.alicdn.com/imgextra/i3/272715291/O1CN01EExlBp1oxKCb2Z9Nu_!!272715291.jpg"  }  ,  {  "name" : "318��10˫�����ļ��ǳ��2ǳ��2��ɫ2��ɫ2��ɫ2",  "photo" : "https://img.alicdn.com/imgextra/i4/272715291/O1CN01XdTgAL1oxKCrY9fG5_!!272715291.jpg"  }  ,  {  "name" : "185��5˫�����ļ����ɫ1��ɫ1���1����1����1",  "photo" : "https://img.alicdn.com/imgextra/i2/272715291/O1CN01VVloj31oxKCSqKT19_!!272715291.jpg"  }  ,  {  "name" : "185��10˫�����ļ����ɫ2��ɫ2���2����2����2",  "photo" : "https://img.alicdn.com/imgextra/i1/272715291/O1CN012CqAPc1oxKCpXfxYA_!!272715291.jpg"  }  ,  {  "name" : "330��5˫�����ļ��ǳ��1ǳ��1ǳ��1ǳ��1ǳ��1",  "photo" : "https://img.alicdn.com/imgextra/i3/272715291/O1CN01la2gya1oxKCSqNQ0n_!!272715291.jpg"  }  ,  {  "name" : "330��10˫�����ļ��ǳ��2ǳ��2ǳ��2ǳ��2ǳ��2",  "photo" : "https://img.alicdn.com/imgextra/i3/272715291/O1CN01v2McQq1oxKCv9f295_!!272715291.jpg"  }  ,  {  "name" : "361��5˫�����ļ����ɫ1��ɫ1��ɫ1��ɫ1ǳ��1",  "photo" : "https://img.alicdn.com/imgextra/i4/272715291/O1CN01DlOkgY1oxKCXWV8Ys_!!272715291.jpg"  }  ,  {  "name" : "361��10˫�����ļ����ɫ2��ɫ2��ɫ2��ɫ2ǳ��2",  "photo" : "https://img.alicdn.com/imgextra/i1/272715291/O1CN01zeD1l51oxKCwd2qd3_!!272715291.jpg"  }  ,  {  "name" : "340��5˫������Ͳ����3��2",  "photo" : "https://img.alicdn.com/imgextra/i3/272715291/O1CN01WNOn4W1oxKCb2gw0a_!!272715291.jpg"  }  ,  {  "name" : "340��5˫������Ͳ����3��2",  "photo" : "https://img.alicdn.com/imgextra/i1/272715291/O1CN01zDePvS1oxKCgIiWuB_!!272715291.jpg"  }  ,  {  "name" : "340��5˫������Ͳ����5",  "photo" : "https://img.alicdn.com/imgextra/i3/272715291/O1CN01ZRZsZM1oxKCb2hKzH_!!272715291.jpg"  }  ,  {  "name" : "340��5˫������Ͳ����2��2ǳ��1",  "photo" : "https://img.alicdn.com/imgextra/i4/272715291/O1CN01NxhJMO1oxKCb2fOUB_!!272715291.jpg"  }  ,  {  "name" : "340��10˫������Ͳ����6��4",  "photo" : "https://img.alicdn.com/imgextra/i1/272715291/O1CN01l516571oxKCo2Vdw6_!!272715291.jpg"  }  ,  {  "name" : "340��10˫������Ͳ����6��4",  "photo" : "https://img.alicdn.com/imgextra/i1/272715291/O1CN01LzH2gy1oxKCvogYlo_!!272715291.jpg"  }  ,  {  "name" : "340��10˫������Ͳ����10",  "photo" : "https://img.alicdn.com/imgextra/i4/272715291/O1CN01jD37xX1oxKCuK8RTv_!!272715291.jpg"  }  ,  {  "name" : "340��10˫������Ͳ����4��4ǳ��2",  "photo" : "https://img.alicdn.com/imgextra/i1/272715291/O1CN015eUoaS1oxKCpXms9A_!!272715291.jpg"  }  ,  {  "name" : "151��5˫�����ļ�������1����1��ɫ1����1�׺�1",  "photo" : "https://img.alicdn.com/imgextra/i1/272715291/O1CN01qlKPq11oxKCfy8IYM_!!272715291.jpg"  }  ,  {  "name" : "151��5˫�����ļ�������2����2�׺�1",  "photo" : "https://img.alicdn.com/imgextra/i1/272715291/O1CN01baQecM1oxKCdiNrG3_!!272715291.jpg"  }  ,  {  "name" : "151��5˫�����ļ�������2����2ǳ��1",  "photo" : "https://img.alicdn.com/imgextra/i3/272715291/O1CN01xXBrdR1oxKCfy8ZEU_!!272715291.jpg"  }  ,  {  "name" : "151��5˫�����ļ�������1����1����2�׺�1",  "photo" : "https://img.alicdn.com/imgextra/i1/272715291/O1CN01hkzMEt1oxKCcnAPAS_!!272715291.jpg"  }  ,  {  "name" : "186��5˫�����ļ����ɫ1��ɫ1���1��ɫ1ǳ��1",  "photo" : "https://img.alicdn.com/imgextra/i2/272715291/O1CN01tcCtGz1oxKCbhZwF2_!!272715291.jpg"  }  ,  {  "name" : "188��5˫�����ļ����ɫ1ǳ��1����1����1���1",  "photo" : "https://img.alicdn.com/imgextra/i2/272715291/O1CN01MiZNZ61oxKCbYqpY5_!!272715291.jpg"  }  ,  {  "name" : "289��5˫��������-���ۡ���ɫ5",  "photo" : "https://img.alicdn.com/imgextra/i3/272715291/O1CN01eqrRLZ1oxKCb2mylw_!!272715291.jpg"  }  ,  {  "name" : "289��5˫��������-���ۡ�ǳ��2��ɫ2ǳ��1",  "photo" : "https://img.alicdn.com/imgextra/i3/272715291/O1CN01zuBYjE1oxKCXWd3oL_!!272715291.jpg"  }  ,  {  "name" : "289��5˫��������-���ۡ���ɫ1��ɫ1ǳ��1ǳ��1ǳ��1",  "photo" : "https://img.alicdn.com/imgextra/i2/272715291/O1CN01rvLxLu1oxKCb2oaYZ_!!272715291.jpg"  }  ,  {  "name" : "289��5˫��������-���ۡ���2��ɫ2ǳ��1",  "photo" : "https://img.alicdn.com/imgextra/i1/272715291/O1CN01j2xltt1oxKCgIv0yG_!!272715291.jpg"  }  ,  {  "name" : "289��5˫��������-���ۡ���ɫ5",  "photo" : "https://img.alicdn.com/imgextra/i3/272715291/O1CN01FQ88IO1oxKCdNO3Ht_!!272715291.jpg"  }  ,  {  "name" : "289��5˫��������-���ۡ���2ǳ��2��ɫ1",  "photo" : "https://img.alicdn.com/imgextra/i4/272715291/O1CN01x7LU6A1oxKCbYsAtc_!!272715291.jpg"  }  ,  {  "name" : "289��5˫��������-���ۡ���ɫ3��ɫ2",  "photo" : "https://img.alicdn.com/imgextra/i1/272715291/O1CN01fTjDQ21oxKCdNN2xD_!!272715291.jpg"  }  ,  {  "name" : "289��5˫��������-���ۡ�ǳ��2��ɫ2��ɫ1",  "photo" : "https://img.alicdn.com/imgextra/i1/272715291/O1CN01FzhYYg1oxKCYXGNrM_!!272715291.jpg"  }  ,  {  "name" : "289��5˫��������-���ۡ�ǳ��2ǳ��2ǳ��1",  "photo" : "https://img.alicdn.com/imgextra/i2/272715291/O1CN01dZiX4o1oxKCbhcHww_!!272715291.jpg"  }  ] ,  "size": [ "���ղؼӹ���-5˫��1˫-10˫��2˫��" ] ,  "photo": [ "https:////img.alicdn.com/imgextra/i2/272715291/O1CN01gJM4yu1oxKCcp4td7_!!0-item_pic.jpg","https:////img.alicdn.com/imgextra/i3/272715291/O1CN01F0KJR41oxKCdPDNMC_!!272715291.jpg","https:////img.alicdn.com/imgextra/i2/272715291/O1CN01UimELp1oxKCb3VxaP_!!272715291-0-lubanu-s.jpg","https:////img.alicdn.com/imgextra/i1/272715291/O1CN01dH2z6P1oxKCYXxgl8_!!272715291-0-lubanu-s.jpg","https:////img.alicdn.com/imgextra/i4/272715291/O1CN01ThHSQP1oxKCb3WMYF_!!272715291-0-lubanu-s.jpg" ], "statusCode": "1",  "message": "success" }');
        //echo $obj->name;
        // $obj = json_decode(html_entity_decode($make_call));
        // echo $obj->name;
        // echo json_last_error();
        echo json_last_error_msg();
        exit();
        // arr($make_call);
        // foreach($make_call as $item){
        //     echo $item->name;
        // }
        // arr($make_call['scalar']->name);
        // $response = json_decode($make_call, true);
        // echo "<pre>".$response."</pre>";
        // $this->load->view('home/search_url', array('data'=> $make_call));
        // print_r($response);
        // exit();

        // if (in_array($domain, $url_r)) {
        //     $data['search_url'] = $input['search'];
        //     $this->load->view('home/search_url', $data);
        // }else{
        //     redirect(site_url("product"), 'refresh');
        // }
    }

    private function callAPI($method, $url, $data)
    {
        $curl = curl_init();
        switch ($method){
           case "POST":
              curl_setopt($curl, CURLOPT_POST, 1);
              if ($data)
                 curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
              break;
           default:
              if ($data)
                 $url = sprintf("%s?%s", $url, http_build_query($data));
        }
        // OPTIONS:
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HEADER, 1);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
           'Content-Type: application/json',
           'Accept: application/json'
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        // EXECUTE:
        $result = curl_exec($curl);
        if(!$result){die("Connection Failure");}
        $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        $header = substr($result, 0, $header_size);
        // arr($header_size);
        $body = substr($result, $header_size);
        curl_close($curl);

        return $body;
    }

    public function search_url()
	{
        $input = $this->input->post(null, true);
        $domain = parse_url($input['search'], PHP_URL_HOST);
        $url_r = array('item.taobao.com', 'detail.tmall.com', 'detail.1688.com');

        $data_act = "tmall.detail";
        $data_url = "https://detail.tmall.com/item.htm?spm=a221t.1812074.goodlist.6.2ecd4208c0zCUI&id=625738745684&skuId=4605298594239";


        $url = 'https://dev.getdev.top/teelek/taobao-api/';
        $data = array(
            "act" => $data_act,
            "url" => $data_url,
        );

        $this->load->view('home/search_url', $data);

        // if (in_array($domain, $url_r)) {
        //     $data['url'] = $input['search'];
        //     $data['act'] = 'tmall.detail';
        //     $this->load->view('home/search_url', $data);
        // }else{
        //     redirect(site_url("product"), 'refresh');
        // }
    }

    public function get_product_item()
	{
        $input = $this->input->post();

        echo json_encode($input); 

    }

        
}