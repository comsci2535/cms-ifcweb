<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Switchlang extends MX_Controller
{
    public function __construct() {
        parent::__construct(); 
    }

    function lang() {
        $language = $this->uri->segment(3);
        $languages = ($language != "") ? $language : "th";
        $this->session->set_userdata('CURRENT_LANG', $languages);

        redirect($_SERVER['HTTP_REFERER']);
    }
}