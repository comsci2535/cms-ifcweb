<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select('a.*')
                        ->from('products a')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->from('products a')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {   
        // START form filter 
        if (!empty($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['keyword'])
                    ->or_like('a.excerpt', $param['keyword'])
                    ->or_like('a.detail', $param['keyword'])
                    ->group_end();
        }
           
        if ( isset($param['active']) && $param['active'] != "" ) {
            $this->db->where('a.active', $param['active']);
        } 
        
        if ( isset($param['recommend']) && $param['recommend'] != "" ) {
            $this->db->where('a.recommend', $param['recommend']);
        } 
        // END form filter 
        
        if ( isset($param['product_id']) ) 
            $this->db->where('a.product_id', $param['product_id']);

        if (!empty($param['product_in']) ):
            $this->db->where_in('a.product_id', $param['product_in']);
        endif;

        if (!empty($param['stars']) ):
            $this->db->where('a.stars', $param['stars']);
        endif; 
        
        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);
        
        if (!empty($param['slug']) )
            $this->db->where('a.slug', $param['slug']);
        
        if (!empty($param['categorie_id']) )
            $this->db->where('a.categorie_id', $param['categorie_id']);
            
        if (!empty($param['order'])){
            $this->db->order_by('a.order', $param['order']);
        }

        if(!empty($param['start_price']) && !empty($param['high_price'])):
            $this->db->where('a.price >= ', $param['start_price']);
            $this->db->where('a.price <=', $param['high_price']);
        endif;
            
    } 

    public function get_products_image($param)
    {
        if ( isset($param['product_id']) ) 
        $this->db->where('a.product_id', $param['product_id']);

        if ( isset($param['id']) ) 
        $this->db->where('a.id', $param['id']);

        $this->db->from('products_image a');
        return $this->db->get();
    }

    public function select_map_products_color($param) 
    { 
        if (!empty($param['product_id'])):
            $this->db->where('product_id', $param['product_id']);
        endif; 
        return $this->db->from('map_products_color')->get();
    } 

    public function select_map_products_size($param) 
    { 
        if (!empty($param['product_id'])):
            $this->db->where('product_id', $param['product_id']);
        endif; 
        return $this->db->from('map_products_size')->get();
    }

    public function insert_product_stars($value) 
    { 
        return $this->db->insert_batch('product_stars', $value);;
    }

    public function select_sql_query($id)
    {
        $query = '';
        if(!empty($id)):
            $query.= " AND a.product_id = '".$id."'";
        endif;
        return $this->db->query("SELECT a.product_id, COUNT( a.user_id ) as user_qty, (COUNT( a.user_id ) * a.star_qty ) AS sum_qty FROM product_stars AS a WHERE a.active = 1 $query GROUP BY 	a.star_qty");
    }

    public function get_categories($param)
    {
        if ( isset($param['categorie_id']) ) 
            $this->db->where('a.categorie_id', $param['categorie_id']);

        if (!empty($param['slug']) ) 
            $this->db->where('a.slug', $param['slug']);

        if ( isset($param['active']) && $param['active'] != "" ) {
            $this->db->where('a.active', $param['active']);
        } 

        if ( isset($param['recycle']) && $param['recycle'] != "" ) {
            $this->db->where('a.recycle', $param['recycle']);
        } 

        $this->db->from('categories a');
        return $this->db->get();
    }

}
