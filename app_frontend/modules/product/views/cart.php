<main>

  <section class="cart">
    <div class="container">

      <div class="row">
        <div class="offset-md-2 col-md-8">
          <!-- <form action="" method="post">
            <div class="input-group form-control-search">
              <input type="text" class="form-control" id="aaa2" placeholder="<?=$this->lang->line('search_term');?>" aria-describedby="aaa" required>
              <div class="input-group-prepend">
                <span class="input-group-text btn" id="aa">
                  <span class="min-sm"><?=$this->lang->line('search');?></span>
                  <span class="max-sm"><i class="fas fa-search"></i></span>
                </span>
              </div>
            </div>
          </form> -->
        </div>
        <div class="col-md-12 mt-4">
          <div class="form-group form-check">
            <label class="form-check-label">
              <input class="form-check-input" type="checkbox" id="check1"> <?=$this->lang->line('product_list');?>
            </label>
          </div>
          <form id="form-submit-car" action="<?=site_url('product/cart_payment');?>" method="post">
            <div class="table-responsive">
              
              <table class="table table-hover">
                <tbody>
                  <?php  
                  $total = 0;
                  $i     = 0;
                  if(!empty($cart)):
                    foreach($cart as $keys => $item):
                      $total += !empty($item['subtotal']) ? $item['subtotal'] : 0;
                  ?> 
                  <tr class="tr-<?=$keys?>">
                    <td>
                      <div class="blog">
                        <div class="box first">
                          <div class="form-check">
                            <label class="form-check-label" for="check2">
                              <input type="checkbox" 
                                class="form-check-input" 
                                name="car_rowid[]"
                                value="<?php echo !empty($item['rowid']) ? $item['rowid'] : 0;?>" 
                                style="top: 0;">
                            </label>
                          </div>
                          <img src="<?php echo !empty($item['file']) ? $item['file'] : '';?>" 
                                class="wfull" 
                                alt="<?php echo !empty($item['name']) ? $item['name'] : null;?>" 
                                onerror="this.src='<?=base_url('template/frontend/images/no-image.png');?>'" >
                        </div>
                        <div class="box">
                          <div class="detail">
                            <p><?php echo !empty($item['name']) ? $item['name'] : null;?></p>
                          </div>
                          <div class="spect">
                            <div>
                              <?php
                              if(!empty($item['options'])):
                                foreach($item['options'] as $key => $option):
                                
                                  
                                  if(!empty($item['type'])):
                                    if(!empty($option)): 
                                      if($key > 0):
                                    ?>
                                    ,<span>
                                      <?php echo !empty($option['attribute_title']) ? $option['attribute_title'] : '';?> : <span class="c_pink"><?php echo !empty($option['attribute']) ? $option['attribute'] : '';?></span>
                                    </span>
                                    <?php
                                      else:
                                    ?>
                                    <span><small>(ลักษณะสินค้า)</small>
                                      <?php echo !empty($option['attribute_title']) ? $option['attribute_title'] : '';?> : <span class="c_pink"><?php echo !empty($option['attribute']) ? $option['attribute'] : '';?></span>
                                    </span>
                                    <?php
                                      endif;
                                    endif;
                                  endif;

                                  if($key > 0): 
                                    if(!empty($option['size'])):
                                    ?>
                                      , <span><?=$this->lang->line('size');?> : <span class="c_pink"><?php echo !empty($option['size'])? $option['size'] : null;?></span></span>
                                    <?php
                                    endif; 
                                    if(!empty($option['color'])):
                                    ?>
                                      , <span><?=$this->lang->line('color');?> : <span class="c_pink"><?php echo !empty($option['color'])? $option['color'] : null;?></span></span>
                                    <?php
                                    endif; 
                                  else:
                                    if(!empty($option['size'])):
                                      ?>
                                        <span><?=$this->lang->line('size');?> : <span class="c_pink"><?php echo !empty($option['size'])? $option['size'] : null;?></span></span>
                                      <?php
                                      endif; 
                                      if(!empty($option['color'])):
                                      ?>
                                        <span><?=$this->lang->line('color');?> : <span class="c_pink"><?php echo !empty($option['color'])? $option['color'] : null;?></span></span>
                                      <?php
                                      endif; 
                                  endif;
                                endforeach;
                              endif;
                              ?> 
                            </div>
                            
                          </div>
                        </div>
                      </div>
                    </td>
                    <td style="min-width: 100px;">
                      <div class="blog">
                        <div class="box">
                          <div class="spect">
                            <div class="right">
                                <div class="input-group minus_plus">
                                  <span class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-number" data-type="minus"
                                      data-field="quant[<?=$i?>]">
                                      <span><i class="fas fa-minus"></i></span>
                                    </button>
                                  </span>
                                  <input type="text" name="quant[<?=$i?>]" class="form-control input-number" value="<?php echo !empty($item['qty']) ? $item['qty'] : 1;?>" min="1"
                                    max="100" disabled>
                                  <input type="hidden" class="text-row-id" value="<?php echo !empty($item['rowid']) ? $item['rowid'] : 0;?>">
                                  <span class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-number" data-type="plus"
                                      data-field="quant[<?=$i?>]">
                                      <span><i class="fas fa-plus"></i></span>
                                    </button>
                                  </span>
                                </div>
                              </div>
                            </div>
                          </div>
                      </div>
                    </td>
                    <td style="min-width: 100px;">
                      <div class="blog">
                        <div class="box">
                          <span><?=$this->lang->line('price');?> : </span>
                          <br>
                          <span class="c_pink"><?php echo !empty($item['price']) ? number_format($item['price'],2) : 0;?></span> <span>บาท</span>
                        </div>
                      </div>
                    </td>
                    <td style="min-width: 100px;">
                      <div class="blog">
                        <div class="box">
                          <span><?=$this->lang->line('total_price');?> : </span>
                          <br>
                          <span class="c_pink"><?php echo !empty($item['subtotal']) ? number_format($item['subtotal'],2) : 0;?></span> <span>บาท</span>
                        </div>
                      </div>
                    </td>
                    <td class="text-right" style="min-width: 108px;">
                      <?php
                      if(empty($item['type'])):
                      ?>
                      <a href="javascript:;" id="modal_cart_ajax" 
                        data-url="<?=site_url('product/cart_ajax');?>" 
                        data-id="<?php echo !empty($item['id']) ? $item['id'] : 0;?>"
                        data-rowid="<?php echo !empty($item['rowid']) ? $item['rowid'] : 0;?>"
                        >
                        <p class="c_blue"><i class="far fa-edit"></i> <?=$this->lang->line('edit');?></p>
                      </a>
                      <?php
                      endif;
                      ?>
                      <a href="<?=site_url('carts/delete_cart_row?rowid='.(!empty($item['rowid']) ? $item['rowid'] : 0));?>">
                        <p class="c_pink ml-2 mt-2"><i class="far fa-trash-alt"></i> <?=$this->lang->line('delete');?></p>
                      </a>
                    </td>
                  </tr> 
                  <?php
                    $i++;
                    endforeach;
                  else:
                  ?>
                  <tr>
                    <td class="text-center"><?=$this->lang->line('text_lang_10');?></td>
                  </tr>
                  <?php
                  endif; 
                  ?>
                </tbody>
              </table>
            </div>
            <div class="text-right">
              <h5><?=$this->lang->line('total_price');?> <span class="c_pink"><?php echo number_format($total, 2);?></span> <?=$this->lang->line('bath');?></h5>
              <?php
              if($total > 0):
              ?>
                <button type="button" id="btn-submit-form-car" class="btn btn-dufault mt-1"><?=$this->lang->line('confirm_order');?></button>
              <?php
              endif;
              ?>
              <hr>
            </div>
          </form>
        </div>
      </div>

    </div>
  </section>

</main>