

<script src="<?=base_url('scripts/frontend/lightslider-master/dist/js/lightslider.min.js')?>"></script>

<section class="product_detail">
    <div class="container">

        <div class="row">
            <div class="col-md-5 mb-3">
                <ul id="product-gallery" class="gallery list-unstyled cS-hidden">
                <?php 
                    if(!empty($product->file)):
                    foreach($product->file as $file): 
                    ?> 
                    <li data-thumb="<?php echo !empty($file->full_path) ? base_url($file->full_path) : base_url('template/frontend/images/no-image.png');?>">
                        <img src="<?php echo !empty($file->full_path) ? base_url($file->full_path) : '';?>" 
                        onerror="this.src='<?=base_url('template/frontend/images/no-image.png');?>'"
                        style="width: 100%;" />
                    </li>
                <?php  
                    endforeach;
                    endif;
                ?>
                </ul>
            </div>

            <div class="col-md-7">
                <?php
                // arr($product);
                ?>
                <div>
                    <div class="title">
                        <h5><?php echo !empty($product->title) ? $product->title : null;?></h5>
                    </div>
                    <div class="expert">
                        <p><?php echo !empty($product->excerpt) ? $product->excerpt : null;?></p> 
                    </div>
                    <div class="rating">
                     <?php
                        for($i = 1; $i <= 5; $i++):
                            $select = '';
                            if(!empty($product->total_stars)):
                            if($i <= $product->total_stars):
                                $select = 'checked';
                            endif;
                            endif;
                        ?>
                        <span class="fa fa-star <?=$select?>"></span>
                        <?php
                        endfor;
                        ?> 
                    </div>
                    <div class="price">
                        <div>
                            <span><?=$this->lang->line('price');?> </span>
                            <span class="p1">฿<?php echo !empty($product->price) ? number_format($product->price) : 0.0;?></span> 
                            <span class="def">(<?php echo !empty($product->start_price) ? number_format($product->start_price) : 0.0;?>)</span>
                        </div>
                        <div>
                            <span><?=$this->lang->line('price');?> </span>
                            <span class="p2">¥<?php echo !empty($product->yuan) ? number_format($product->yuan) : 0.0;?></span> 
                            <span class="def">(<?php echo !empty($product->start_yuan) ? number_format($product->start_yuan) : 0.0;?>)</span>
                        </div>
                    </div>
                    <?php
                    if(!empty($product->products_color)):  
                    ?>
                    <style>
                    .btn-select-color.active{
                        background: linear-gradient(to left, #b22d6c, #b22d6c);
                        color: #ffffff;
                        border: 2px solid #b22d6c;
                        border-radius: 14px;
                    } 

                    .btn-select-color{ 
                        background: linear-gradient(to right, white, white), linear-gradient(15deg, #b22d6c, #b22d6c);
                        color: #b22d6c;
                        border: 2px solid #b22d6c;
                        border-radius: 14px;
                    } 

                    .btn-select-size.active{
                        background: linear-gradient(to left, #b22d6c, #b22d6c);
                        color: #ffffff;
                        border: 2px solid #b22d6c;
                        border-radius: 14px;
                    } 

                    .btn-select-size{ 
                        background: linear-gradient(to right, white, white), linear-gradient(15deg, #b22d6c, #b22d6c);
                        color: #b22d6c;
                        border: 2px solid #b22d6c;
                        border-radius: 14px;
                    }   

                    </style>
                    
                    <div class="row py-2">
                    <div class="col-md-3">
                        <span>จำนวนแถบสี</span>
                    </div>
                    <div class="col-md-9">
                        <div class="tap_color">
                        <?php  
                        foreach($product->products_color as $key => $color):
                            $color_title = !empty($color->title) ? $color->title : null;
                            $color_active = '';
                            if(!empty($options_arr[$color_title])):
                                if($options_arr[$color_title] == $color->title):
                                $color_active = 'active';
                                endif;
                            endif;
                            if($key > 0):
                        ?> 
                            , <button type="button" class="btn-select-color <?=$color_active?>" 
                                data-value="<?php echo !empty($color->title) ? $color->title : null;?>"> 
                                <?php echo !empty($color->title) ? $color->title : null;?>
                             </button>  
                            <?php 
                            else:
                            ?> 
                            <button type="button" class="btn-select-color <?=$color_active?>" 
                                data-value="<?php echo !empty($color->title) ? $color->title : null;?>"> 
                                <?php echo !empty($color->title) ? $color->title : null;?>
                            </button> 
                        <?php  
                            endif; 
                        endforeach; 
                        ?>
                        </div>
                    </div>
                    </div>
                    <?php 
                    endif;
                    ?>
                    <?php
                    if(!empty($product->products_size)):  
                    ?>
                    <div class="row py-2">
                    <div class="col-md-3">
                        <span>ไซส์</span>
                    </div>
                    <div class="col-md-9">
                        <div class="tap_color">
                        <?php  
                        foreach($product->products_size as $keys => $size):
                            $size_title = !empty($size->title) ? $size->title : null;
                            $size_active = '';
                            if(!empty($options_arr[$size->title])):
                                if($options_arr[$size->title] == $size_title):
                                $size_active = 'active';
                                endif;
                            endif;
                            if($keys > 0):
                        ?>
                        , <button type="button" class="btn-select-size <?=$size_active?>" 
                                data-value="<?php echo !empty($size->title) ? $size->title : null;?>"> 
                                <?php echo !empty($size->title) ? $size->title : null;?>
                            </button>  
                            <?php 
                            else:
                            ?> 
                            <button type="button" class="btn-select-size <?=$size_active?>" 
                                data-value="<?php echo !empty($size->title) ? $size->title : null;?>"> 
                                <?php echo !empty($size->title) ? $size->title : null;?>
                            </button> 
                        <?php  
                            endif; 
                        endforeach; 
                        ?>
                        </div>
                    </div>
                    </div>
                    <?php 
                    endif;
                    ?>
                    <div class="row py-2">
                        <div class="col-md-3">
                            <span>ปริมาณ</span>
                        </div>
                        <div class="col-md-9"> 
                            <div class="input-group minus_plus">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-number-modal" data-type="minus"
                                        data-field="quant[modal]">
                                        <span><i class="fas fa-minus"></i></span>
                                    </button>
                                </span>
                                <input type="text" name="quant[modal]" class="form-control input-number-modal" 
                                    value="<?php echo !empty($cartDetail['qty']) ? $cartDetail['qty'] : 1;?>" 
                                    min="1"
                                    max="100" disabled>
                                <input type="hidden" class="text-row-id" value="<?php echo !empty($cartDetail['rowid']) ? $cartDetail['rowid'] : 0;?>">
                                <span class="input-group-btn">
                                    <button type="button" class="btn btn-primary btn-number-modal" data-type="plus"
                                        data-field="quant[modal]">
                                        <span><i class="fas fa-plus"></i></span>
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="detail py-2">
                        <p><b>รายละเอียด : </b></p>
                        <p><?php echo !empty($product->detail) ? html_entity_decode($product->detail) : null;?></p> 
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

<script>
    $(document).ready(function () {
        $('#product-gallery').lightSlider({
            gallery: true,
            item: 1,
            thumbItem: 5,
            slideMargin: 0,
            speed: 1000,
            pause: 3000,
            auto: true,
            loop: true,
            onSliderLoad: function () {
                $('#product-gallery').removeClass('cS-hidden');
            }
        });
    });
</script>


<script>
    // $('.btn-number').click(function (e) {
    //     e.preventDefault();

    //     fieldName = $(this).attr('data-field');
    //     type = $(this).attr('data-type');
    //     var input = $("input[name='" + fieldName + "']");
    //     var currentVal = parseInt(input.val());
    //     if (!isNaN(currentVal)) {
    //         if (type == 'minus') {

    //             if (currentVal > input.attr('min')) {
    //                 input.val(currentVal - 1).change();
    //             }
    //             if (parseInt(input.val()) == input.attr('min')) {
    //                 $(this).attr('disabled', true);
    //             }

    //         } else if (type == 'plus') {

    //             if (currentVal < input.attr('max')) {
    //                 input.val(currentVal + 1).change();
    //             }
    //             if (parseInt(input.val()) == input.attr('max')) {
    //                 $(this).attr('disabled', true);
    //             }

    //         }
    //     } else {
    //         input.val(0);
    //     }
    // });
    // $('.input-number').focusin(function () {
    //     $(this).data('oldValue', $(this).val());
    // });

    // $('.input-number').change(function () {

    //     minValue = parseInt($(this).attr('min'));
    //     maxValue = parseInt($(this).attr('max'));
    //     valueCurrent = parseInt($(this).val());

    //     name = $(this).attr('name');
    //     if (valueCurrent >= minValue) {
    //         $(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr('disabled')
    //     } else {
    //         alert('Sorry, the minimum value was reached');
    //         $(this).val($(this).data('oldValue'));
    //     }
    //     if (valueCurrent <= maxValue) {
    //         $(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr('disabled')
    //     } else {
    //         alert('Sorry, the maximum value was reached');
    //         $(this).val($(this).data('oldValue'));
    //     }


    // });

    // $(".input-number").keydown(function (e) {
    //     // Allow: backspace, delete, tab, escape, enter and .
    //     if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
    //         // Allow: Ctrl+A
    //         (e.keyCode == 65 && e.ctrlKey === true) ||
    //         // Allow: home, end, left, right
    //         (e.keyCode >= 35 && e.keyCode <= 39)) {
    //         // let it happen, don't do anything
    //         return;
    //     }
    //     // Ensure that it is a number and stop the keypress
    //     if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
    //         e.preventDefault();
    //     }
    // });
</script>