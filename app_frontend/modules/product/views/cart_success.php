<main>

    <section class="cart_payment">
        <div class="container">

            <div class="row">

                <div class="col-md-12 text-center">
                    <img src="<?=base_url('images/success.jpg');?>" style="width: 90px;margin-bottom: 25px;" alt="">

                    <h3>
                    <?=$this->lang->line('we_received_order');?>
                    </h3>
                    <br>
                    <p><?=$this->lang->line('your_order_number_is');?> : <span class="c_pink"><?php echo !empty($order_code) ? $order_code : null;?></span></p>
                    <p><?=$this->lang->line('You_receive_order');?></p>
                    <a class='btn btn-dufault mt-2' href=''><?=$this->lang->line('continue_shopping');?></a>
                </div>

            </div>

        </div>
    </section>

</main>