<script src="<?=base_url('scripts/frontend/elevatezoom/js/jquery.ez-plus.js')?>"></script>
<script>
$("img#zoom").ezPlus({
    // lensSize: 200,
    borderColour:'#b22d6c',
    minZoomLevel: 2
});
</script>

<script src="<?=base_url('scripts/frontend/lightslider-master/dist/js/lightslider.min.js')?>"></script>
<script src="<?=base_url('scripts/frontend/bootstrap_progress/waiting_dialog.js')?>"></script>
<script>
    $(document).ready(function () {
        $('#product-gallery').lightSlider({
            gallery: true,
            item: 1,
            thumbItem: 5,
            slideMargin: 0,
            speed: 1000,
            pause: 3000,
            auto: true,
            loop: true,
            onSliderLoad: function () {
                $('#product-gallery').removeClass('cS-hidden');
            }
        });
    });
</script>

<script>
    $(document).on('click', '.btn-number', function (e) {
        e.preventDefault(); 
        var tr_class =  $(this).parents('.table-hover tr').attr('class'); 
        fieldName = $(this).attr('data-field');
        type = $(this).attr('data-type');
        var input = $("input[name='" + fieldName + "']");
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            if (type == 'minus') {

                if (currentVal > input.attr('min')) {
                    input.val(currentVal - 1).change();
                }
                if (parseInt(input.val()) == input.attr('min')) {
                    $(this).attr('disabled', true);
                }

            } else if (type == 'plus') {

                if (currentVal < input.attr('max')) {
                    input.val(currentVal + 1).change();
                }
                if (parseInt(input.val()) == input.attr('max')) {
                    $(this).attr('disabled', true);
                } 
            }
            
        } else {
            input.val(0);
        }  

    });

    $(document).on('click', '.btn-number-modal', function (e) {
        e.preventDefault(); 
        var tr_class =  $(this).parents('.table-hover tr').attr('class'); 
        fieldName = $(this).attr('data-field');
        type = $(this).attr('data-type');
        var input = $("input[name='" + fieldName + "']");
        var currentVal = parseInt(input.val());
        if (!isNaN(currentVal)) {
            if (type == 'minus') {

                if (currentVal > input.attr('min')) {
                    input.val(currentVal - 1).change();
                }
                if (parseInt(input.val()) == input.attr('min')) {
                    $(this).attr('disabled', true);
                }

            } else if (type == 'plus') {

                if (currentVal < input.attr('max')) {
                    input.val(currentVal + 1).change();
                }
                if (parseInt(input.val()) == input.attr('max')) {
                    $(this).attr('disabled', true);
                } 
            }
            
        } else {
            input.val(0);
        }  

    });

    $('.input-number').focusin(function () {
        $(this).data('oldValue', $(this).val());
    });

    $(document).on('change', '.input-number', function () {
        var tr_class =  $(this).parents('tr').attr('class'); 
        minValue = parseInt($(this).attr('min'));
        maxValue = parseInt($(this).attr('max'));
        valueCurrent = parseInt($(this).val());

        name = $(this).attr('name');
        if (valueCurrent >= minValue) {
            $(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr('disabled')
        } else {
            alert('Sorry, the minimum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        if (valueCurrent <= maxValue) {
            $(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr('disabled')
        } else {
            alert('Sorry, the maximum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        updateCartQty(tr_class);

    });

    $(document).on('change', '.input-number-modal', function () {
        var tr_class =  $(this).parents('tr').attr('class'); 
        minValue = parseInt($(this).attr('min'));
        maxValue = parseInt($(this).attr('max'));
        valueCurrent = parseInt($(this).val());

        name = $(this).attr('name');
        if (valueCurrent >= minValue) {
            $(".input-number-modal[data-type='minus'][data-field='" + name + "']").removeAttr('disabled')
        } else {
            alert('Sorry, the minimum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        if (valueCurrent <= maxValue) {
            $(".input-number-modal[data-type='plus'][data-field='" + name + "']").removeAttr('disabled')
        } else {
            alert('Sorry, the maximum value was reached');
            $(this).val($(this).data('oldValue'));
        }
        updateCartQty(tr_class);

    }); 

    $(".input-number").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
            // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) ||
            // Allow: home, end, left, right
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
</script>

<script>
$(function() {
    $('#check1').click(function() {
        $('input.form-check-input').prop('checked', this.checked);
    });
});
</script>

<script>
    $(document).ready(function () {
        $('input[type="file"]').on("change", function () {
            let filenames = [];
            // let files = document.getElementById("customFile").files;
            // if (files.length > 1) {
            //     filenames.push("Total Files (" + files.length + ")");
            // } else {
            //     for (let i in files) {
            //         if (files.hasOwnProperty(i)) {
            //             filenames.push(files[i].name);
            //         }
            //     }
            // }

            $(this)
                .next(".custom-file-label")
                .html(filenames.join(","));
        });

        $(document).on('change', 'input[type="file"]', function(ee){  
            if(ee.target.files[0].type =="image/png" 
                || ee.target.files[0].type =="image/jpg" 
                || ee.target.files[0].type =="image/jpeg"
            ){ 
                console.log(ee.target.files[0].size);
                var $this = $(this); 
                var reader  = new FileReader();
                reader.onload = function (e) { 
                    var image = new Image();  
                    image.src = e.target.result;  
                    image.onload = function () {  
                        return true; 
                    }; 
                };
                reader.readAsDataURL(ee.target.files[0]);
            }else{
                $('#upload-file-info').text('');
                $(this).val('');
                alert_box('<?=$this->lang->line('text_lang_6');?>'); 
            }
        });
    });
</script>

<script>
    $('a#modal_cart_ajax').click(function () { 
        var url = $(this).data("url");
        var code = $(this).attr('data-id');
        var rowid = $(this).attr('data-rowid');
        $.ajax({
            type: "POST",
            url: url,
            // dataType: 'json',
            data:{
                code : code,
                rowid : rowid
            },
            success: function (data) {
                // show modal
                $('#modal_cart').modal('show');
                $('#modal_cart .modal-body').html(data);
            },
            error: function (request, status, error) {
                console.log("ajax call went wrong:" + request.responseText);
            }
        });
    });

    function updateCartQty(tr_class)
    {    
         
        console.log(tr_class);
        if(tr_class != undefined){
            var qty     = $('.'+tr_class).find('.input-number').val();
            var rowid   = $('.'+tr_class).find('.text-row-id').val();
        }else{ 
            var qty     = $('#modal_cart').find('.input-number-modal').val();
            var rowid   = $('#modal_cart').find('.text-row-id').val();
        }
        
        $.ajax({
            type: "POST",
            url: '<?=base_url('carts/update_row')?>', 
            data:{
                qty : qty,
                rowid : rowid
            },
            success: function (result) {
                // show modal   
                if(result.data == 200){
                    window.location.reload();
                }
            },
            error: function (request, status, error) {
                console.log("ajax call went wrong:" + request.responseText);
            }
        });
    };
</script>

<script> 
  
    $(document).on('click', '.btn-select-color', function(){
        var $this = $(this);
        $('.btn-select-color').removeClass('active').attr('data-active',0);
        $this.addClass('active');
        $this.attr('data-active', 1)
    })

    $(document).on('click', '#btn-submit-form-car', function(){
        var setect = $('.form-check-input').is(':checked');
        if(setect){
            $('#form-submit-car').submit();
        }else{
            alert_box('กรุณาเลือกสินค้าที่ต้องการชำระเงิน');
            return false;
        }
        
    })

    $(document).on('click', '.btn-select-size', function(){
        var $this = $(this);
        $('.btn-select-size').removeClass('active');
        $this.addClass('active');
    })

    $(document).on('click', '#btn-select-cart', function(){
        var $this       = $(this);
        var code        = $this.attr('data-id');
        var qty         = $('.input-text-qty').val();
        var color       = $('.btn-select-color.active').attr('data-value');
        var size        = $('.btn-select-size.active').attr('data-value');
        var error_color = 0;
        $(".btn-select-color").each(function(){
            error_color++; 
        }); 
        var error_size  = 0;
        $(".btn-select-size").each(function(){
            error_size++;
        }); 

        if(error_color > 0){
            if(!$(".btn-select-color").hasClass("active")){ 
                alert_box('<?=$this->lang->line('please_select_a_color');?>');
                return false;
            }
        }

        if(error_size > 0){
            if(!$(".btn-select-size").hasClass("active")){ 
                alert_box('<?=$this->lang->line('please_select_a_size');?>');
                return false;
            }
        } 

        if(size == undefined){ size = ''; }
        if(color == undefined){ color = ''; }

        $.ajax({
            type: "POST",
            url: '<?=base_url('carts/data_cart')?>', 
            data:{
                code    : code,
                qty     : qty,
                size    : size,
                color   : color
            },
            success: function (request) { 
                if(request.data != 500){  
                    if(request.data == 200){  
                        Swal.fire({
                            title: '<?=$this->lang->line('success_add_to_cart');?>', 
                            type: 'success',
                            confirmButtonText: 'ตกลง'
                        }).then(function() {
                            $('#text-header-cart-qty span').html(request.count);  
                        })
                    }else{
                        alert_box('<?=$this->lang->line('text_lang_7');?>');
                    }
                }else{
                    alert_box('<?=$this->lang->line('text_lang_8');?>');
                }
            },
            error: function (request, status, error) {
                console.log("ajax call went wrong:" + request.responseText);
            }
        }); 
    });

    $(document).on('click', '#btn-select-cart-go', function(){
        var $this       = $(this);
        var code        = $this.attr('data-id');
        var qty         = $('.input-text-qty').val();
        var color       = $('.btn-select-color.active').attr('data-value');
        var size        = $('.btn-select-size.active').attr('data-value');
        var error_color = 0;
        $(".btn-select-color").each(function(){
            error_color++; 
        }); 
        var error_size  = 0;
        $(".btn-select-size").each(function(){
            error_size++;
        }); 

        if(error_color > 0){
            if(!$(".btn-select-color").hasClass("active")){ 
                alert_box('<?=$this->lang->line('please_select_a_color');?>');
                return false;
            }
        }

        if(error_size > 0){
            if(!$(".btn-select-size").hasClass("active")){ 
                alert_box('<?=$this->lang->line('please_select_a_size');?>');
                return false;
            }
        } 

        if(size == undefined){ size = ''; }
        if(color == undefined){ color = ''; }

        $.ajax({
            type: "POST",
            url: '<?=base_url('carts/data_cart')?>', 
            data:{
                code    : code,
                qty     : qty,
                size    : size,
                color   : color
            },
            success: function (request) { 
                if(request.data != 500){  
                    if(request.data == 200){  
                        Swal.fire({
                            title: '<?=$this->lang->line('success_add_to_cart');?>', 
                            type: 'success',
                            confirmButtonText: '<?=$this->lang->line('ok');?>'
                        }).then(function() {
                            window.location.href = '<?=base_url('product/cart')?>'
                            // $('#text-header-cart-qty span').html(request.count);  
                        })
                    }else{
                        alert_box('<?=$this->lang->line('text_lang_7');?>');
                    }
                }else{
                    alert_box('<?=$this->lang->line('text_lang_8');?>');
                }
            },
            error: function (request, status, error) {
                console.log("ajax call went wrong:" + request.responseText);
            }
        }); 
    });

    $(document).on('click', '#btn--cart-success', function(){
        var bank_id = $('.bank_id:checked').val();
        var file = $('#file_source').val();    
        if(bank_id != undefined){
            if(file != ''){ 
                Swal.fire({
                    title: '<?=$this->lang->line('order_confirmation');?>',
                    text: '<?=$this->lang->line('Do_continue');?>',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    cancelButtonText: '<?=$this->lang->line('cancel');?>',
                    confirmButtonText: '<?=$this->lang->line('Confirm_the_order');?>'
                }).then(function(result){
                    if (result.value) {
                        $( "#form-cart-success" ).submit();
                    }
                }); 
            }else{
                alert_box('<?=$this->lang->line('Please_elect_image');?>');
            } 
        }
        else{
            alert_box('<?=$this->lang->line('text_lang_9');?>'); 
        } 
    });

    $(document).on('change','#filter_product', function(){ 
        $('#form-filter-submit').submit();
    });

    $(document).on('change','#btn-cate', function(){ 
        $('#form-filter-submit').submit();
    });

    $(document).on('change','.btn-input-price-search', function(){ 
        var start_price = $('#start_price').val();
        var high_price  = $('#high_price').val();
        if(start_price == ''){
            alert_box('กรุณาระบุราคาน้อยสุด');
           return false; 
        }
        if(high_price == ''){
            alert_box('กรุณาระบุราคามากสุด');
           return false; 
        }
        $('#form-filter-submit').submit();
    });

    $(document).on('click','.btn-click-star', function(){ 
        var start_price = $(this).attr('data-star');
        $('#form-filter-submit').append("<input type='hidden' name='star' value='"+start_price+"'/>");  
        $('#form-filter-submit').submit();
    });


</script>
<!-- modal_login -->
<div class="modal animate__animated animate__bounceInLeft" id="modal_cart" aria-hidden="true" tabindex="-1" role="dialog">
    <div class="modal-dialog" style="max-width: 80%;">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">×</button>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>