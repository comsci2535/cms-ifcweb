<main>

  <section class="product_detail">
    <div class="container">

      <div class="row">
        <div class="col-md-5 mb-3">
          <ul id="product-gallery" class="gallery list-unstyled cS-hidden">
            <?php 
            if(!empty($product->file)):
              foreach($product->file as $file): 
            ?> 
              <li data-zoom-image="<?php echo !empty($file->full_path) ? base_url($file->full_path) : '';?>" data-thumb="<?php echo !empty($file->full_path) ? base_url($file->full_path) : base_url('template/frontend/images/no-image.png');?>">
                <img id="zoom" src="<?php echo !empty($file->full_path) ? base_url($file->full_path) : '';?>" 
                onerror="this.src='<?=base_url('template/frontend/images/no-image.png');?>'"
                style="width: 100%;" />
              </li>
            <?php  
              endforeach;
            endif;
            ?>
          </ul>
        </div>

        <div class="col-md-7"> 
          <div>
            <div class="title">
              <h5><?php echo !empty($product->title) ? $product->title : null;?></h5>
            </div>
            <div class="expert">
              <p><?php echo !empty($product->excerpt) ? $product->excerpt : null;?></p> 
            </div>
            <div class="rating">
              <?php
              for($i = 1; $i <= 5; $i++):
                $select = '';
                if(!empty($product->total_stars)):
                  if($i <= $product->total_stars):
                    $select = 'checked';
                  endif;
                endif;
              ?>
              <span class="fa fa-star <?=$select?>"></span>
              <?php
              endfor;
              ?> 
            </div>
            <div class="price">
              <div>
                  <span><?=$this->lang->line('price');?> </span>
                  <span class="p1">฿<?php echo !empty($product->price) ? number_format($product->price,2) : 0.0;?></span> 
                  <span class="def">(<?php echo !empty($product->start_price) ? number_format($product->start_price,2) : 0.0;?>)</span>
                </div>
                <div>
                  <span><?=$this->lang->line('price');?> </span>
                  <span class="p2">¥<?php echo !empty($product->yuan) ? number_format($product->yuan,2) : 0.0;?></span> 
                  <span class="def">(<?php echo !empty($product->start_yuan) ? number_format($product->start_yuan,2) : 0.0;?>)</span>
                </div>
            </div>
            <?php
            if(!empty($product->products_color)):  
            ?>
            
            <div class="row py-2">
              <div class="col-md-3">
                <span><?=$this->lang->line('number_of_color_bars');?></span>
              </div>
              <div class="col-md-9">
                <div class="tap_color">
                  <?php  
                  foreach($product->products_color as $key => $color):
                    if($key > 0):
                  ?> 
                      , <button type="button" class="btn-select-color" data-value="<?php echo !empty($color->title) ? $color->title : null;?>"> <?php echo !empty($color->title) ? $color->title : null;?></button>  
                    <?php 
                    else:?> 
                      <button type="button" class="btn-select-color" data-value="<?php echo !empty($color->title) ? $color->title : null;?>"> <?php echo !empty($color->title) ? $color->title : null;?></button> 
                  <?php  
                    endif;
                  endforeach; 
                  ?>
                </div>
              </div>
            </div>
            <?php 
            endif;
            ?>
            <?php
            if(!empty($product->products_size)):  
            ?>
            <div class="row py-2">
              <div class="col-md-3">
                <span><?=$this->lang->line('size');?></span>
              </div>
              <div class="col-md-9">
                <div class="tap_color">
                  <?php  
                  foreach($product->products_size as $keys => $size):
                    if($keys > 0):
                  ?>
                   , <button type="button" class="btn-select-size" data-value="<?php echo !empty($size->title) ? $size->title : null;?>"> <?php echo !empty($size->title) ? $size->title : null;?></button>  
                    <?php 
                    else:?> 
                      <button type="button" class="btn-select-size" data-value="<?php echo !empty($size->title) ? $size->title : null;?>"> <?php echo !empty($size->title) ? $size->title : null;?></button> 
                  <?php  
                    endif;
                  endforeach; 
                  ?>
                </div>
              </div>
            </div>
            <?php 
            endif;
            ?>
            <div class="row py-2">
              <div class="col-md-3">
                <span><?=$this->lang->line('quantity');?></span>
              </div>
              <div class="col-md-9">
                <div class="input-group minus_plus">
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-primary btn-number" data-type="minus" data-field="quant[2]">
                      <span><i class="fas fa-minus"></i></span>
                    </button>
                  </span>
                  <input type="text" name="quant[2]" class="form-control input-number input-text-qty" value="1" min="1" max="100">
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-primary btn-number" data-type="plus" data-field="quant[2]">
                      <span><i class="fas fa-plus"></i></span>
                    </button>
                  </span>
                </div>
              </div>
            </div>
            <div class="py-3">
              <a href="javascript:void(0)">
                <button type="button" id="btn-select-cart-go" class="btn btn-primary" data-id="<?php echo !empty($product->product_id) ? $product->product_id : null;?> ">
                <?=$this->lang->line('buy_now');?></button>
              </a>
              <a href="javascript:void(0)"> 
                <button type="button" id="btn-select-cart" class="btn btn-dufault" data-id="<?php echo !empty($product->product_id) ? $product->product_id : null;?> "><i class="fas fa-cart-plus"></i>
                <?=$this->lang->line('add_to_cart');?></button>
              </a>
            </div>
            <div class="detail py-2">
              <p><b><?=$this->lang->line('detail');?> : </b></p>
              <p><?php echo !empty($product->detail) ? html_entity_decode($product->detail) : null;?></p> 
            </div>
          </div>
        </div>

      </div>
    </div>
  </section>

</main>