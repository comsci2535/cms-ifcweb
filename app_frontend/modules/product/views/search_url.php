<style>
  section.product_detail .btn-select-color-copy-url.active {
    background: linear-gradient(to left, #b22d6c, #b22d6c);
    color: #ffffff;
  }
  section.product_detail .btn-select-color-copy-url {
    background: linear-gradient(to right, white, white), linear-gradient(15deg, #b22d6c, #b22d6c);
    color: #b22d6c;
    border: 2px solid #b22d6c;
    border-radius: 14px;
  }
</style>
<main>

  <section class="product_detail">
    
    <div class="container">
    <div id="google_translate_element"></div>
      <div class="row">
        <div class="col-md-5 mb-3">
          <ul id="product-gallery" class="gallery list-unstyled cS-hidden">
           
              <li data-thumb="<?=base_url('template/frontend/images/no-image.png');?>">
                <img src="" 
                onerror="this.src='<?=base_url('template/frontend/images/no-image.png');?>'"
                style="width: 100%;" />
              </li>
            
          </ul>
        </div>

        <div class="col-md-7"> 
          <div>
            <div class="title">
              <h5 class="text-title-chana">รอการโหลดข้อมูลจากเว็บไซต์ปลายทาง</h5>
              <input type="hidden" class="text-title-chana" >
            </div>

            <div class="price">
              <div>
                  <span><?=$this->lang->line('price');?> </span>
                  <span class="p1">฿<span class="text-price-th">0</span></span> 
                </div>
                <div>
                  <span><?=$this->lang->line('price');?> </span>
                  <span class="p2">¥<span class="text-price-chana">0</span></span> 
                </div>
            </div>
            <div id="div-display-attribute">
            </div>
            <div class="row py-2">
              <div class="col-md-3">
                <span><?=$this->lang->line('quantity');?></span>
              </div>
              <div class="col-md-9">
                <div class="input-group minus_plus">
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-primary btn-number" data-type="minus" data-field="quant[2]">
                      <span><i class="fas fa-minus"></i></span>
                    </button>
                  </span>
                  <input type="text" name="quant[2]" class="form-control input-number input-text-qty" value="1" min="1" max="100">
                  <span class="input-group-btn">
                    <button type="button" class="btn btn-primary btn-number" data-type="plus" data-field="quant[2]">
                      <span><i class="fas fa-plus"></i></span>
                    </button>
                  </span>
                </div>
              </div>
            </div>
            <div class="py-3">
              <a href="javascript:void(0)">
                <button type="button" id="btn-select-cart-search-go" class="btn btn-primary">
                <?=$this->lang->line('buy_now');?></button>
              </a>
              <a href="javascript:void(0)"> 
                <button type="button" id="btn-select-search-cart" class="btn btn-dufault"><i class="fas fa-cart-plus"></i>
                <?=$this->lang->line('add_to_cart');?></button>
              </a>
            </div>
            <div class="detail py-2">
              <p><b><?=$this->lang->line('detail');?> : </b></p>
              <p class="text-detail-chana">รอการโหลดข้อมูลจากเว็บไซต์ปลายทาง</p> 
            </div>
          </div>
        </div>

      </div>
    </div>
  </section>

</main>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
function googleTranslateElementInit() {
  new google.translate.TranslateElement({
    // pageLanguage: 'th'
  }, 'google_translate_element');
}
</script>
<script src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
<script>
$(function() {
    var money_th      = '<?=$money_th?>';
    var domain        = '<?=$url_type;?>';
    $(window).ready(function() {
      var id = '<?=$code?>';
      waitingDialog.show('กำลังโหลดข้อมุลจากเว็บไซต์ปลายทาง....',{rtl:false});
      $.ajax({
            type: "POST",
            url: '<?=base_url('apis/get')?>',
            data: {
              id : id,
              urlCall : '<?=$url_api?>'
            },
            
            options: {
                useSSL: true
            },
            dataType: "json",
            success: function (response) { 

                if(response.success){
                  $('.text-title-chana').html(response.data.title);
                  $('.text-title-chana').val(response.data.title);

                  var total = response.data.price*money_th;
                  $('.text-price-th').html(total.toFixed(2));
                  $('.text-price-chana').html(response.data.price);
                  $('#btn-select-search-cart').attr('data-price', total.toFixed(2));
                 

                  $('.text-detail-chana').html(response.data.desc);
                  $('#btn-select-search-cart').attr('data-id', id);
                      
                    var html_photo = '';
                    $.each(response.data.item_imgs, function(key, photo){
                        html_photo+='<li data-thumb="'+photo['url']+'">';
                        html_photo+='<img id="zoom" src="'+photo['url']+'" style="width: 100%;" />';
                        html_photo+='</li>';
                    })
                    var html_attribute = '';
                    var n = 0;
                    var attribute_name = '';
                    var attribute_h    = '';
                    
                    $.each(response.data.props_list, function(key, attribute){

                      var attribute_arr = attribute.split(":");
                      var key_arr       = key.split(":");
                      html_attribute+='<div id="tr-'+n+'" class="row py-2">';
                        html_attribute+='<div class="col-md-3">';
                        if(attribute_arr[0] != attribute_name){
                          html_attribute+='<span>'+attribute_arr[0]+'</span>';
                        }
                        html_attribute+='</div>';
                        html_attribute+='<div class="col-md-9">';
                          html_attribute+='<div class="tap_color">';
                            var strs = key+'-'+attribute;
                            var strs_name = strs.replace(/([.*+?^=!:;★#''""&${}() |\[\]\/\\])/g, "-");
                            // var strs2 = strs.replace(/\:/g, "-");
                            // var strs3 = strs2.replace(/\ /g, "-")
                            // var strs_name = strs3.replace(/\+/g, "-")
                            html_attribute+='<input type="hidden" class="text-select-str-attribute" value="'+strs_name+'">'; 
                            html_attribute+='<button type="button" class="btn-select-color-copy-url find_'+key_arr[0]+'" data-row="tr-'+n+'" data-find-row="find_'+key_arr[0]+'" data-title="'+attribute_arr[0]+'" data-value="'+attribute_arr[1]+'" style="margin-bottom: 4px;">'+attribute_arr[1]+'</button>';
                          html_attribute+='</div>';
                        html_attribute+='</div>';
                      html_attribute+='</div>';
                      attribute_name = attribute_arr[0];
                      n++;
                    })

                    $.each(response.data.skus.sku, function(keys, sku){
                      var str = sku.properties_name;
                      // var str2 = str.replace(/\:/g, "-")
                      // var str3 = str2.replace(/\;/g, "-")
                      // var str4 = str3.replace(/\ /g, "-")
                      // var sku_name = str4.replace(/\+/g, "-")
                      var sku_name = str.replace(/([.*+?^=!:;★#''""&${}() |\[\]\/\\])/g, "-");
                      html_attribute+='<input type="hidden" id="'+sku_name+'" value="'+sku.price+'">'; 
                    })

                    $('#div-display-attribute').html(html_attribute);
                    
                    $('#product-gallery').html(html_photo);
                    $('#product-gallery').lightSlider({
                        gallery: true,
                        item: 1,
                        thumbItem: 5,
                        slideMargin: 0,
                        speed: 1000,
                        pause: 3000,
                        auto: true,
                        loop: true,
                        onSliderLoad: function () {
                          $('#product-gallery').removeClass('cS-hidden');
                        }
                    });

                    $("img#zoom").ezPlus({
                        // lensSize: 200,
                        borderColour:'#b22d6c',
                        minZoomLevel: 2
                    });
                    
                    setTimeout(function () { waitingDialog.hide(); }, 1000);
                    
                }else{
                    alert('ไม่สามารถโหลดข้อมุลจากเว็บไซต์ปลายทางได้ กรุณาลองใหม่อีกครั้ง....');
                    // window.location.href = '<?=base_url('product');?>';
                    setTimeout(function () { waitingDialog.hide(); }, 1000);
                }
                
            },
            failure: function (msg) {
              alert('ไม่สามารถโหลดข้อมุลจากเว็บไซต์ปลายทางได้ กรุณาลองใหม่อีกครั้ง....');
              window.location.href = '<?=base_url('product');?>';
            },
            statusCode: {
                504: function() {
                  alert('Gateway Timeout....');
                  window.location.href = '<?=base_url('product');?>';
                }
            }
        });
        
    });

    $(document).on('click', '#btn-select-cart-search-go', function(){
      $('#btn-select-search-cart').click();
    });

    $(document).on('click', '#btn-select-search-cart', function(){
        var $this           = $(this);
        var code            = $this.attr('data-id');
        var price           = $this.attr('data-price');
        var name            = $('.text-title-chana').val();
        var qty             = $('.input-text-qty').val();

        var loop            = 0;
        var loop_row        = 0;
        $("#div-display-attribute .btn-select-color-copy-url").each(function(){ 
          var $this = $(this); 
          var row = $('#div-display-attribute .btn-select-color-copy-url.'+$this.attr('data-find-row')).length;
          var row_active = $('#div-display-attribute .btn-select-color-copy-url.'+$this.attr('data-find-row')+'.active').length;
          if(row > 0 && row_active < 1){
            loop_row = loop_row + row;
          }
          // console.log('=================')
          // console.log(row);
          // console.log(row_active);
          loop++;
        });  
        // console.log(loop_row)
        if(loop > 0 &&  loop_row > 0){
          alert_box('กรุณาเลือกลักษณะสินค้า');
          return false;
        }

        var attribute_arr = []
        $(".btn-select-color-copy-url.active").each(function(){
          attribute_arr.push({
            attribute       : $(this).attr('data-value'),
            attribute_title : $(this).attr('data-title')
          })
        }); 

        var image = []
        $("#product-gallery li").each(function(){
          image.push($(this).attr('data-thumb'))
        }); 

        // return false;

        $.ajax({
            type: "POST",
            url: '<?=base_url('carts/data_api_cart')?>', 
            data:{
                code        : code,
                qty         : qty,
                price       : price,
                attribute   : attribute_arr,
                name        : name,
                image       : image,
                type        : 1,
                from_web    : '<?=$url_type;?>',
                from_web_url : '<?=$from_web_url;?>'
            },
            success: function (request) { 
                if(request.data != 500){  
                    if(request.data == 200){  
                        Swal.fire({
                            title: '<?=$this->lang->line('success_add_to_cart');?>', 
                            type: 'success',
                            confirmButtonText: 'ตกลง'
                        }).then(function() {
                            $('#text-header-cart-qty span').html(request.count);  
                        })
                    }else{
                        alert_box('<?=$this->lang->line('text_lang_7');?>');
                    }
                }else{
                    alert_box('<?=$this->lang->line('text_lang_8');?>');
                }
            },
            error: function (request, status, error) {
                console.log("ajax call went wrong:" + request.responseText);
            }
        }); 
    });

    $(document).on('click', '.btn-select-color-copy-url', function(){
        var $this = $(this);
        $('.btn-select-color-copy-url.'+$this.attr('data-find-row')).removeClass('active').attr('data-active',0);
        $this.addClass('active');
        $this.attr('data-active', 1)

        var str_name = '';
        $(".btn-select-color-copy-url.active").each(function(){
          var valus = $(this).closest('.tap_color').find('.text-select-str-attribute').val();
          if(str_name == ''){
            str_name = str_name+valus
          }else{
            str_name = str_name+'-'+valus
          }
        }); 
        
        var price = $('#'+str_name).val();

        if(price != undefined){
          var total = parseFloat(price)*money_th;
          $('.text-price-th').html(total.toFixed(2));
          $('.text-price-chana').html(price);
          $('#btn-select-search-cart').attr('data-price', total.toFixed(2));
        }
    })
});
</script> 


