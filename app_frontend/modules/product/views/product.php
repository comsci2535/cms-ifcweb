<main>
<style>
.nice-select {
  width: 100%;
  padding: 0 10px;
  font-size: 17px;
  float: none;
}
</style>

  <section class="product">
    <div class="container">

      <div class="row">
        <div class="col-md-3">
          <form id="form-filter-submit" action="<?=base_url('product/index')?>" method="GET" autocomplete="off">
            <div class="row">
              <div class="col-md-12">
                <label for=""><?=$this->lang->line('filter');?></label>
                <div class="fa_search">
                  <input type="text" class="form-control" id="filter_product" value="<?php echo !empty($filter_product) ? $filter_product :'';?>" placeholder="<?=$this->lang->line('search_term');?>..." name="filter_product">
                  <i class="fas fa-search"></i>
                </div>
              </div>
              <div class="col-md-12">
                <hr style="margin: 15px 0;">
              </div>
              <div class="col-md-12">
                <label for=""><?=$this->lang->line('pro_category');?></label>
                <select class="form-control" id="btn-cate" name="cate">
                  <option value="">เลือกประเภทสินค้า</option>
                  <?php
                  if(!empty($categories)):
                    foreach ($categories as $value):
                      $select = '';
                      if($categorie_id == $value->categorie_id):
                        $select = 'selected';
                      endif;
                  ?>
                  <option value="<?php echo !empty($value->slug) ? $value->slug :'';?>" <?=$select?>><?php echo !empty($value->title) ? $value->title :'';?></option>
                  <?php
                    endforeach;
                  endif;
                  ?>
                </select>
              </div>
              <div class="col-md-12">
                <hr style="margin: 15px 0;">
              </div>
              <div class="col-md-12">
                <label for=""><?=$this->lang->line('price_range');?> (สกุนเงินไทย)</label>
                <div class="row">
                  <div class="col-6">
                    <div class="form-group">
                      <input type="text" class="form-control btn-input-price-search" id="start_price"  value="<?php echo !empty($start_price) ? $start_price :'';?>" placeholder="<?=$this->lang->line('least');?>" name="start_price">
                    </div>
                  </div>
                  <div class="kit"></div>
                  <div class="col-6">
                    <div class="form-group">
                      <input type="text" class="form-control btn-input-price-search" id="high_price"  value="<?php echo !empty($high_price) ? $high_price :'';?>" placeholder="<?=$this->lang->line('most');?>" name="high_price">
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-12">
                <hr style="margin: 15px 0;">
              </div>
              <div class="col-md-12 mb-3">
                <label for=""><?=$this->lang->line('popularity');?></label>
                <div class="row"> 
                  <div class="col-md-12">
                    <div class="rating">
                      <a href="javascript:void(0)" class="btn-click-star" data-star="1">
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                        <span> <?=$this->lang->line('go_up');?></span>
                      </a>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="rating">
                      <a href="javascript:void(0)" class="btn-click-star" data-star="2">
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                        <span> <?=$this->lang->line('go_up');?></span>
                      </a>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="rating">
                      <a href="javascript:void(0)" class="btn-click-star" data-star="3">
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                        <span> <?=$this->lang->line('go_up');?></span>
                      </a>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="rating">
                      <a href="javascript:void(0)" class="btn-click-star" data-star="4">
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star"></span>
                        <span> <?=$this->lang->line('go_up');?></span>
                      </a>
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="rating">
                      <a href="javascript:void(0)" class="btn-click-star" data-star="5">
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>

        <div class="col-md-9">
          <div class="row">
            <?php  
            if(!empty($product)): 
              foreach($product as $item):
            ?>
            <div class="col-md-4 col-6">
              <div class="blog-product">
                <a href="<?=site_url('product/detail/'.(!empty($item->slug) ? $item->slug: null));?>">
                  <img src="<?php echo !empty($item->file) ? base_url($item->file) : '';?>" 
                    class="wfull" 
                    alt="<?php echo !empty($item->title) ? $item->title: null;?>" 
                    onerror="this.src='<?=base_url('template/frontend/images/no-image.png');?>'" >
                  <div class="detail">
                    <div class="title">
                      <h5><?php echo !empty($item->title) ? $item->title : null;?></h5>
                    </div>
                    <div class="expert">
                      <p><?php echo !empty($item->excerpt) ? $item->excerpt : null;?></p> 
                    </div>
                    <div class="price">
                      <div>
                        <span><?=$this->lang->line('price');?> </span>
                        <span class="p1">฿<?php echo !empty($item->price) ? number_format($item->price,2) : 0.0;?></span> 
                        <span class="def">(<?php echo !empty($item->start_price) ? number_format($item->start_price,2) : 0.0;?>)</span>
                      </div>
                      <div>
                        <span><?=$this->lang->line('price');?> </span>
                        <span class="p2">¥<?php echo !empty($item->yuan) ? number_format($item->yuan,2) : 0.0;?></span> 
                        <span class="def">(<?php echo !empty($item->start_yuan) ? number_format($item->start_yuan,2) : 0.0;?>)</span>
                      </div>
                    </div>
                  </div>
                </a>
                <div class="detail_cart_add">
                  <div class="detail_cart">
                    <!-- <div class="title">
                      <h5><?php echo !empty($item->title) ? $item->title : null;?></h5>
                    </div> -->
                    <div class="expert">
                      <p><?php echo !empty($item->excerpt) ? $item->excerpt : null;?></p>  
                    </div>
                    <div class="price">
                      <div>
                        <span><?=$this->lang->line('price');?> </span>
                        <span class="p1">฿<?php echo !empty($item->price) ? number_format($item->price,2) : 0.0;?></span> 
                        <span class="def">(<?php echo !empty($item->start_price) ? number_format($item->start_price,2) : 0.0;?>)</span>
                      </div>
                      <div>
                        <span><?=$this->lang->line('price');?> </span>
                        <span class="p2">¥<?php echo !empty($item->yuan) ? number_format($item->yuan,2) : 0.0;?></span> 
                        <span class="def">(<?php echo !empty($item->start_yuan) ? number_format($item->start_yuan,2) : 0.0;?>)</span>
                      </div>
                      <div class="mt-2">
                        <p style="margin-bottom: -10px;"><?=$this->lang->line('discount');?></p>
                        <span class="discount"><?php echo !empty($item->persent) ? number_format($item->persent, 2) : 0.0;?>%</span>
                        <div class="rating">
                          <?php
                            for($i = 1; $i <= 5; $i++):
                              $select = '';
                              if(!empty($item->total_stars)):
                                if($i <= $item->total_stars):
                                  $select = 'checked';
                                endif;
                              endif;
                            ?>
                            <span class="fa fa-star <?=$select?>"></span>
                            <?php
                            endfor;
                            ?> 
                        </div>
                      </div>
                    </div>
                    <div class="text-center">
                      <a href="<?=site_url('product/detail/'.(!empty($item->slug) ? $item->slug: null));?>">
                        <button type="button" class="btn btn-dufault"><i class="fas fa-cart-plus"></i>
                        <?=$this->lang->line('add_to_cart');?></button>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <?php 
              endforeach;
            endif; 
            ?>
          </div>
        </div>

      </div>
    </div>
  </section>
  <div class="container">
    <div class="row">
      <div class="offset-md-3 col-md-9">
        <ul class="pagination justify-content-center py-4">
          <?php echo !empty($pagination) ? $pagination : ''; ?> 
        </ul> 
      </div>
    </div>
  </div>
</main>