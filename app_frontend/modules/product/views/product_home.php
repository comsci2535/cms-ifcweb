<section id="product">
    <div class="container text-center" style="padding:10px">
        <div class="btn btn-dufault wfull" style="width: auto;border-radius: 40px;padding: 3px 3px;">
          <a href="<?=site_url('contact');?>" class="btn btn-dufault wfull" style="min-width: 200px;padding: 16px;color: white;border: 2px solid white;font-size: 22px;">
            <?=$this->lang->line('free_consultation_click');?></a>
        </div>    
      </div>
    <div class="container">
      <div class="blog-title">
        <h2 class="text"><?=$this->lang->line('recommended_products');?></h2>
      </div>
      <div class="row">
        <?php  
        if(!empty($product)):
          foreach($product as $item):
        ?>
        <div class="col-md-3 col-6">
          <div class="blog-product">
            <a href="<?=site_url('product/detail/'.(!empty($item->slug) ? $item->slug: null));?>">
              <img src="<?php echo !empty($item->file) ? base_url($item->file) : '';?>" 
                    class="wfull" 
                    alt="<?php echo !empty($item->title) ? $item->title: null;?>" 
                    onerror="this.src='<?=base_url('template/frontend/images/no-image.png');?>'" >
              <div class="detail">
                <div class="title">
                  <h5><?php echo !empty($item->title) ? $item->title : null;?></h5>
                </div>
                <div class="expert">
                  <p><?php echo !empty($item->excerpt) ? $item->excerpt : null;?></p> 
                </div>
                <div class="price">
                  <div>
                    <span>ราคา </span>
                    <span class="p1">฿<?php echo !empty($item->price) ? number_format($item->price,2) : 0.0;?></span> 
                    <span class="def">(<?php echo !empty($item->start_price) ? number_format($item->start_price,2) : 0.0;?>)</span>
                  </div>
                  <div>
                    <span>ราคา </span>
                    <span class="p2">¥<?php echo !empty($item->yuan) ? number_format($item->yuan,2) : 0.0;?></span> 
                    <span class="def">(<?php echo !empty($item->start_yuan) ? number_format($item->start_yuan,2) : 0.0;?>)</span>
                  </div>
                </div>
              </div>
            </a>
            <div class="detail_cart_add">
              <div class="detail_cart">
                <!-- <div class="title">
                  <h5><?php echo !empty($item->title) ? $item->title : null;?></h5>
                </div> -->
                <div class="expert">
                  <p><?php echo !empty($item->excerpt) ? $item->excerpt : null;?></p>  
                </div>
                <div class="price">
                  <div>
                    <span>ราคา </span>
                    <span class="p1">฿<?php echo !empty($item->price) ? number_format($item->price,2) : 0.0;?></span> 
                    <span class="def">(<?php echo !empty($item->start_price) ? number_format($item->start_price,2) : 0.0;?>)</span>
                  </div>
                  <div>
                    <span>ราคา </span>
                    <span class="p2">¥<?php echo !empty($item->yuan) ? number_format($item->yuan,2) : 0.0;?></span> 
                    <span class="def">(<?php echo !empty($item->start_yuan) ? number_format($item->start_yuan,2) : 0.0;?>)</span>
                  </div>
                  <div class="mt-2">
                    <p style="margin-bottom: -10px;">ได้รับส่วนลด</p>
                    <span class="discount"><?php echo !empty($item->persent) ? number_format($item->persent, 2) : 0.0;?>%</span>
                    <div class="rating">
                      <?php
                        for($i = 1; $i <= 5; $i++):
                          $select = '';
                          if(!empty($item->total_stars)):
                            if($i <= $item->total_stars):
                              $select = 'checked';
                            endif;
                          endif;
                        ?>
                        <span class="fa fa-star <?=$select?>"></span>
                        <?php
                        endfor;
                        ?> 
                    </div>
                  </div>
                </div>
                <div class="text-center">
                  <a href="<?=site_url('product/detail/'.(!empty($item->slug) ? $item->slug: null));?>"> 
                    <button type="button" class="btn btn-dufault"><i class="fas fa-cart-plus"></i>
                    <?=$this->lang->line('add_to_cart');?></button>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php 
          endforeach;
        endif; 
        ?> 
      </div>
    </div>
  </section>