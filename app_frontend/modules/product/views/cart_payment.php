<main>

    <form id="form-cart-success" method="post" action="<?php echo site_url('product/cart_success');?>" enctype="multipart/form-data">
        <section class="cart_payment">
            <div class="container">

                <div class="row">

                    <div class="col-md-6"> 
                        <!-- load banks -->
                        <?php echo Modules::run('banks/get_banks');?> 
                    </div>
                    <div class="col-md-4">
                        <h5><?=$this->lang->line('proof_of_transfer');?></h5>
                        <div class="form-group"> 
                            <div style="position:relative;">
                                <span class='label label-info' id="upload-file-info"></span><br>
                                <a class='btn btn-primary mt-2' href='javascript:;'>
                                    <i class="fas fa-upload"></i> <?=$this->lang->line('Upload_evidence');?>
                                    <input type="file"
                                        style='position:absolute;height: 100%;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;'
                                        id="file_source"
                                        name="file_source" size="40"
                                        onchange='$("#upload-file-info").html($(this).val());'>
                                </a>
                            </div>
                        </div> 
                    </div>

                    <div class="col-md-12 mt-3">
                        <div class="table-redius">
                        <table class="table table-bordered table-hover" style="background: #fff;">
                            <thead>
                                <tr>
                                    <th><?=$this->lang->line('order_number');?></th>
                                    <th><?=$this->lang->line('detail');?></th>
                                    <th><?=$this->lang->line('quatity');?></th>
                                    <th><?=$this->lang->line('price_piece');?></th>
                                    <th><?=$this->lang->line('total_price');?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php  
                                if(!empty($cart)): 
                                    $i       = 1;
                                    $total  = 0;
                                    foreach($cart as $item):
                                        $total += !empty($item['subtotal']) ? $item['subtotal'] : 0;
                                ?>
                                <tr>
                                    <td class="text-center"><?php echo $i++;?></td>
                                    <td>
                                        <p><?php echo !empty($item['name']) ? $item['name'] : null;?></p>
                                        <?php
                                        if(!empty($item['options'])):
                                            foreach($item['options'] as $key => $option):
                                                if(!empty($item['type'])):
                                                    if(!empty($option)): 
                                                        if($key > 0):
                                                    ?>
                                                    ,<span>
                                                    <?php echo !empty($option['attribute_title']) ? $option['attribute_title'] : '';?> : <span class="c_pink"><?php echo !empty($option['attribute']) ? $option['attribute'] : '';?></span>
                                                    </span>
                                                    <?php
                                                    else:
                                                    ?>
                                                    <span><small>(ลักษณะสินค้า)</small>
                                                    <?php echo !empty($option['attribute_title']) ? $option['attribute_title'] : '';?> : <span class="c_pink"><?php echo !empty($option['attribute']) ? $option['attribute'] : '';?></span>
                                                    </span>
                                                    <?php
                                                        endif;
                                                    endif;
                                                endif;
                
                                                if($key > 0): 
                                                    if(!empty($option['size'])):
                                                    ?>
                                                        , <span>ไซส์ : <span class="c_pink"><?php echo !empty($option['size'])? $option['size'] : null;?></span></span>
                                                    <?php
                                                    endif; 
                                                    if(!empty($option['color'])):
                                                    ?>
                                                        , <span>สี : <span class="c_pink"><?php echo !empty($option['color'])? $option['color'] : null;?></span></span>
                                                    <?php
                                                    endif; 
                                                else:
                                                    if(!empty($option['size'])):
                                                        ?>
                                                        <span>ไซส์ : <span class="c_pink"><?php echo !empty($option['size'])? $option['size'] : null;?></span></span>
                                                        <?php
                                                    endif; 
                                                    if(!empty($option['color'])):
                                                        ?>
                                                        <span>สี : <span class="c_pink"><?php echo !empty($option['color'])? $option['color'] : null;?></span></span>
                                                        <?php
                                                    endif; 
                                                endif;
                                            endforeach;
                                        endif;
                                        ?> 
                                        <input type="hidden" name="car_rowid[]" value="<?php echo !empty($item['rowid']) ? $item['rowid'] : 0;?>">
                                    </td>
                                    <td class="text-center"><?php echo !empty($item['qty']) ? $item['qty'] : 0;?></td>
                                    <td class="text-center"><?php echo !empty($item['price']) ? number_format($item['price'], 1) : 0;?></td>
                                    <td class="text-center"><?php echo !empty($item['subtotal']) ? number_format($item['subtotal'], 1) : 0;?></td>
                                </tr>
                                <?php 
                                    endforeach;
                                endif;
                                ?>
                            </tbody>
                        </table>
                        </div>
                    </div>

                </div>
                <div class="text-right">
                    <br>
                    <input type="hidden" name="discount" value="<?=$total?>">
                    <h5><?=$this->lang->line('total_');?> <span class="c_pink"><?php echo number_format($total, 1);?></span> <?=$this->lang->line('bath');?></h5> 
                    <button type="button" id="btn--cart-success" class="btn btn-dufault mt-1"><?=$this->lang->line('order_confirmation');?></button> 
                    <hr>
                </div>
            </div>
        </section>
    </form>

</main>