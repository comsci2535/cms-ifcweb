
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Writer\Pdf\Mpdf;
require APPPATH.'third_party/vendor/autoload.php'; 
require APPPATH.'libraries/barcode/BarcodeGenerator.php'; 
require APPPATH.'libraries/barcode/BarcodeGeneratorHTML.php';
require APPPATH.'libraries/barcode/BarcodeGeneratorPNG.php'; 

class Product extends MX_Controller {

	public function __construct()
	{
        parent::__construct();
        
        $this->load->library('uploadfile_library');
        $this->load->model('product_m');
        $this->load->model('orders/orders_m');
        $this->load->model('info/info_m');  
	}

    private function seo($seo=null)
	{

        $title          = !empty($seo->meta_title) ? $seo->meta_title : "IFC Product";
		$robots         = !empty($seo->meta_title) ? $seo->meta_title : "";
		$description    = !empty($seo->meta_description) ? $seo->meta_description : "";
        $keywords       = !empty($seo->meta_keyword) ? $seo->meta_keyword : "";
        $url            = !empty($seo->file[0]->full_path) ? base_url($seo->file[0]->full_path) : base_url('images/logo/logo.png');
		$meta  			= '<TITLE>'.$title.'</TITLE>';
		$meta 		   .= '<meta name="robots" content="'.$robots.'"/>';
		$meta		   .= '<meta name="description" content="'.$description.'"/>';
        $meta 		   .= '<meta name="keywords" content="'.$keywords.'"/>';
        $meta 		   .= '<meta property="og:url" content="'.site_url().'" />';
        $meta 		   .= '<meta property="og:type" content="website" />';
        $meta 		   .= '<meta property="og:title" content="'.$title.'" />';
        $meta 		   .= '<meta property="og:description" content="'.$description.'" />';
        $meta 		   .= '<meta property="og:image" content="'.$url.'" />'; 
		return $meta;
	}

	public function index()
	{

        $obj_seo = Modules::run('configs/get_setting', 'general');
        $sco = new stdClass();
        $sco->meta_title = !empty($obj_seo['siteTitle']) ? $obj_seo['siteTitle'] : '';
        $sco->meta_description = !empty($obj_seo['metaDescription']) ? $obj_seo['metaDescription'] : '';
        $sco->meta_keyword = !empty($obj_seo['metaKeyword']) ? $obj_seo['metaKeyword'] : '';
        
        $data = array(
            'seo'     => $this->seo($sco),
            'menu'    => 'product',
            'header'  => 'header',
            'content' => 'product',
            'footer'  => 'footer',
            'function'=>  array('product'),
        );

        if(!empty($this->input->get('star'))): 
            $input['stars'] = !empty($this->input->get('star')) ? $this->input->get('star') : '';
        endif;  

        if(!empty($this->input->get('cate'))): 
            $input__['slug'] = !empty($this->input->get('cate')) ? $this->input->get('cate') : '';
            $categories             = $this->product_m->get_categories($input__)->row();
            $input['categorie_id']  = $categories->categorie_id;
            $data['categorie_id']   = $categories->categorie_id;
        endif;  

        // load config money
        $money_setting = Modules::run('configs/get_setting', 'money_setting');  
        $money_th = !empty($money_setting['money_th']) ? $money_setting['money_th'] : 0;

        $filter_product             = !empty($this->input->get('filter_product')) ? $this->input->get('filter_product') :'';
        $start_price                = !empty($this->input->get('start_price')) ? $this->input->get('start_price') :'';
        $high_price                 = !empty($this->input->get('high_price')) ? $this->input->get('high_price') :'';
        $input['keyword']           = $filter_product;
        $input['start_price']       = $start_price;
        $input['high_price']        = $high_price;
        
        $input['active']    = 1;
        $input['recycle']   = 0; 
        $per_page = 9;
		$page = !empty($this->input->get('per_page')) ? ($this->input->get('per_page')-1) : 0; // เลขหน้าที่จะถูกส่งมาเช่น home/member/3
 
		$parameter          = "?filter_product={$_GET['filter_product']}&cate={$_GET['cate']}&start_price={$_GET['start_price']}&high_price={$_GET['high_price']}";
        $uri                = 'product/index'.$parameter; // ชี้หน้าเพจหลักที่จะใช้งานมาที่ home/member
        $segment            = $page;
        $total              = $this->product_m->get_count($input); // จำนวนข้อมูลทั้งหมด
		$config['per_page'] = $per_page; // จำนวนข้อมูลต่อหน้า 
        $data['pagination'] = $this->pagin($uri, $total, $segment, $per_page); // เลขหน้า 
        $input['length']    = $per_page;
        $input['start']     = ($page*$per_page); 
        $product            = $this->product_m->get_rows($input)->result();    
        if(!empty($product)):
            foreach($product as $item): 

                if(CURRENT_LANG=='en'):
                    $item->title     = $item->title_en;
                    $item->excerpt   = $item->excerpt_en;
                    $item->detail    = $item->detail_en;
                endif;

                 //set stars product  
                $item->total_stars      = $item->stars;

                $input_['product_id']   = $item->product_id;
                $img                    = $this->product_m->get_products_image($input_)->row();
                $item->file             = !empty($img->full_path) ? $img->full_path : null; 
                if($item->start_price > 0):
                    $item->persent      = (100 - (($item->price / $item->start_price)) * 100); 
                else:
                    $item->persent      = 0; 
                endif; 
                $item->start_yuan       = ($item->start_price / $money_th);
                $item->yuan             = ($item->price / $money_th);
            endforeach;
        endif; 

        $input_['active']    = 1;
        $input_['recycle']   = 0; 
        $categories          = $this->product_m->get_categories($input_)->result();
        if(!empty($categories)):
            foreach($categories as $categorie): 

                if(CURRENT_LANG=='en'):
                    $categorie->title     = $categorie->title_en;
                    $categorie->excerpt   = $categorie->excerpt_en;
                    $categorie->detail    = $categorie->detail_en;
                endif;
            endforeach;
        endif; 

        $data['categories']  = $categories;
        $data['product']            = $product;
        $data['filter_product']     = $filter_product;
        $data['start_price']        = $start_price;
        $data['high_price']         = $high_price; 
        
        $this->load->view('template/body', $data);
    }

    public function detail($slug)
	{

        // load config money
        $money_setting  = Modules::run('configs/get_setting', 'money_setting');  
        $money_th       = !empty($money_setting['money_th']) ? $money_setting['money_th'] : 0;

        $input['active']    = 1;
        $input['recycle']   = 0;
        $input['slug']      = $slug;
        $product            = $this->product_m->get_rows($input)->row();  
        if(!empty($product)): 

            if(CURRENT_LANG=='en'):
                $product->title     = !empty($product->title_en) ? $product->title_en : $product->title;
                $product->excerpt   = !empty($product->excerpt_en) ? $product->excerpt_en : $product->excerpt;
                $product->detail    = !empty($product->detail_en) ? $product->detail_en : $product->detail;   
            endif; 

            //set stars product 
            $product->total_stars       = $product->stars; 

            $input_['product_id']        = $product->product_id;
            $img = $this->product_m->get_products_image($input_)->result();
            $product->file = $img;  

            // get query  select_map_products_size
            $products_size               = $this->product_m->select_map_products_size($input_)->result(); 
            $product->products_size      = $products_size;

            // get query  products_color
            $products_color             = $this->product_m->select_map_products_color($input_)->result(); 
            $product->products_color    = $products_color;  

            $product->start_yuan       = ($product->start_price / $money_th);
            $product->yuan             = ($product->price / $money_th);

        endif;
        // arr($product);exit();

        $data = array(
            'seo'     => $this->seo($product),
            'menu'    => 'product',
            'header'  => 'header',
            'content' => 'detail',
            'footer'  => 'footer',
            'function'=>  array('product'),
        );

        $data['product']   = $product; 

        $this->load->view('template/body', $data);
    }
    
    public function get_product_home()
    {
         
        // load config money
        $money_setting = Modules::run('configs/get_setting', 'money_setting');  
        $money_th = !empty($money_setting['money_th']) ? $money_setting['money_th'] : 0;
        // load config money
        
        $input['active']        = 1;
        $input['recommend']     = 1; 
        $input['recycle']       = 0;
        $input['length']        = 20;
        $input['start']         = 0; 
        $input['order']         = 'ASC';
        $product                = $this->product_m->get_rows($input)->result();
        if(!empty($product)):
            foreach($product as $item):
                if(CURRENT_LANG=='en'):
                    $item->title     = !empty($item->title_en) ? $item->title_en : $item->title;
                    $item->excerpt   = !empty($item->excerpt_en) ? $item->excerpt_en : $item->excerpt;
                    $item->detail    = !empty($item->detail_en) ? $item->detail_en : $item->detail;  
                endif;

                //set stars product  
                $item->total_stars       = $item->stars;
                
                $input_['product_id']   = $item->product_id;
                $img                    = $this->product_m->get_products_image($input_)->row();
                $item->file             = !empty($img->full_path) ? $img->full_path : null; 
                if($item->start_price > 0):
                    $item->persent          = (100 - (($item->price / $item->start_price)) * 100); 
                else:
                    $item->persent          = 0; 
                endif;  
                $item->start_yuan       = ($item->start_price / $money_th);
                $item->yuan             = ($item->price / $money_th);

            endforeach;
        endif;
        $data['product']        = $product; 
        $this->load->view('product_home', $data);
    }
    
    public function cart()
	{
        $obj_seo = Modules::run('configs/get_setting', 'general');
        $sco = new stdClass();
        $sco->meta_title = !empty($obj_seo['siteTitle']) ? $obj_seo['siteTitle'] : '';
        $sco->meta_description = !empty($obj_seo['metaDescription']) ? $obj_seo['metaDescription'] : '';
        $sco->meta_keyword = !empty($obj_seo['metaKeyword']) ? $obj_seo['metaKeyword'] : '';

        $data = array(
            'seo'     => $this->seo($sco),
            'menu'    => 'product',
            'header'  => 'header',
            'content' => 'cart',
            'footer'  => 'footer',
            'function'=> array('product'),
        );

        $cart       = Modules::run('carts/get_data_cart_list');

        $cart_arr   = array();
        if(!empty($cart)):
            foreach($cart as $key => $item):
                $input_['product_id'] = $item['id'];
                $img             = $this->product_m->get_products_image($input_)->row();
                $file            = !empty($img->full_path) ? base_url($img->full_path) : null;  
                $name            = $item['name'];
                if(CURRENT_LANG=='en'):
                    $product = $this->product_m->get_rows($input_)->row();
                    $name     = !empty($product->title_en) ? $product->title_en : $name;
                endif;

                if(!empty($item['type'])):
                    $file = !empty($item['image'][0]) ? $item['image'][0] : '';
                endif;

                $cart_arr[$key] = array(
                    'id'        => $item['id'],
                    'qty'       => $item['qty'],
                    'price'     => $item['price'],
                    'name'      => $name,
                    'options'   => $item['options'],
                    'type'      => $item['type'],
                    'rowid'     => $item['rowid'],
                    'subtotal'  => $item['subtotal'],
                    'file'      => $file
                ); 
            endforeach;
        endif;  

        $data['cart'] = $cart_arr;

        $this->load->view('template/body', $data);
    }

    public function cart_ajax()
	{

        // load config money
        $money_setting  = Modules::run('configs/get_setting', 'money_setting');  
        $money_th       = !empty($money_setting['money_th']) ? $money_setting['money_th'] : 0;

        $input['active']        = 1;
        $input['recycle']       = 0;
        $rowid                  = $this->input->post('rowid');
        $input['product_id']    = $this->input->post('code');
        $product                = $this->product_m->get_rows($input)->row();  
        if(!empty($product)): 

            if(CURRENT_LANG=='en'):
                $product->title     = !empty($product->title_en) ? $product->title_en : $product->title;
                $product->excerpt   = !empty($product->excerpt_en) ? $product->excerpt_en : $product->excerpt;
                $product->detail    = !empty($product->detail_en) ? $product->detail_en : $product->detail;   
            endif;

            //set stars product  
            $product->total_stars       = $product->stars;

            $input_['product_id']        = $product->product_id;
            $img = $this->product_m->get_products_image($input_)->result();
            $product->file = $img;  

            // get query  select_map_products_size
            $products_size               = $this->product_m->select_map_products_size($input_)->result(); 
            $product->products_size      = $products_size;

            // get query  products_color
            $products_color             = $this->product_m->select_map_products_color($input_)->result(); 
            $product->products_color    = $products_color;  

            $product->start_yuan       = ($product->start_price / $money_th);
            $product->yuan             = ($product->price / $money_th);

        endif; 
        $options_arr = array();
        $cart                   = Modules::run('carts/get_data_cart_list'); 
        if(!empty($cart[$rowid]['options'])):
           
            foreach($cart[$rowid]['options'] as $item):
                if(!empty($item['size'])):
                $options_arr[$item['size']] = $item['size'];
                endif;
                if(!empty($item['color'])):
                $options_arr[$item['color']] = $item['color'];
                endif;
            endforeach;
           
        endif;
        $data['cartDetail']     = !empty($cart[$rowid]) ? $cart[$rowid] : null; 
        $data['options_arr']    = $options_arr; 
        $data['product']        = $product; 

        $this->load->view('product/cart_ajax', $data);
    }

    public function cart_payment()
	{
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'product',
            'header'  => 'header',
            'content' => 'cart_payment',
            'footer'  => 'footer',
            'function'=>  array('product'),
        );

        $input      = $this->input->post();
        // arr($input);

        $cart       = Modules::run('carts/get_data_cart_list');
        // arr($cart);
        $cart_arr   = array();
        if(!empty($cart)):
            foreach($cart as $key => $item):
                if(in_array($key, $input['car_rowid'])):
                    $input_['product_id'] = $item['id'];
                    $img             = $this->product_m->get_products_image($input_)->row();
                    $file            = !empty($img->full_path) ? $img->full_path : null;  
                    $name            = $item['name'];
                    if(CURRENT_LANG=='en'):
                        $product = $this->product_m->get_rows($input_)->row();
                        $name     = !empty($product->title_en) ? $product->title_en : $name;
                    endif;

                    if(!empty($item['type'])):
                        $file = !empty($item['image'][0]) ? $item['image'][0] : '';
                    endif;

                    $cart_arr[$key] = array(
                        'id'        => $item['id'],
                        'qty'       => $item['qty'],
                        'price'     => $item['price'],
                        'name'      => $name,
                        'options'   => $item['options'],
                        'type'      => $item['type'],
                        'rowid'     => $item['rowid'],
                        'subtotal'  => $item['subtotal'],
                        'file'      => $file
                    );  
                endif;
            endforeach;
        endif;  
        // exit();
        $data['cart'] = $cart_arr;

        $this->load->view('template/body', $data);
    }

    public function cart_success()
	{
        $data = array(
            'seo'     => $this->seo(),
            'menu'    => 'product',
            'header'  => 'header',
            'content' => 'cart_success',
            'footer'  => 'footer',
            'function'=>  array('product'),
        );

        $listCart = Modules::run('carts/get_data_cart_list');
        if(!empty($listCart)): 
            $input  = $this->input->post(); 
            $value  = $this->_build_data($input);
            $result = $this->orders_m->insert($value);
            if($result):
                list($detail, $attribute, $quantity, $star, $detail_url) =  $this->_build_detail_data($result, $input); 
                if(!empty($detail)):
                    //insert order detail
                    $this->orders_m->insert_orders_detail($detail);
                    if(!empty($attribute)):
                        //insert order detail attribute
                        $this->orders_m->insert_orders_detail_attribute($attribute); 
                    endif; 
                    if(!empty($star)):
                        $this->product_m->insert_product_stars($star);
                    endif;
                endif; 

                if(!empty($detail_url)):
                    $this->orders_m->insert_orders_detail_url($detail_url);
                endif;

                $orders_payments = $this->_build_data_orders_payments($result, $input);
                $this->orders_m->insert_orders_payments($orders_payments);

                if(empty($input['order_code'])):
                    $value_code['order_code']   = $this->orders_m->set_order_id()->order_code;
                    $value_code['invoice_code'] = 'Rcpt'.$value_code['order_code'];
                    $value_code['order_qty']    = $quantity;
                    $data['order_code']         = $value_code['order_code'];
                    $this->db->update('orders',$value_code, array('order_id' => $result));
                    Modules::run('carts/delete_cart_by_row', $input); // delete row by select id
                    Modules::run('carts/save_cart_history'); // insert row no select to database
                    if(!empty($value['email'])): 
                        $data_arr['name']              = $value['name'].' '.$value['lasname']; 
                        $data_arr['email']             = $value['email']; 
                        $data_arr['url']               = base_url('product/pdf?code='.urlencode(base64_encode($result))); 
                        $this->sentMail($data_arr);
                    endif;
                endif;

                if(!empty($detail)):
                    foreach($detail as $product):
                        if(!empty($product['product_id'])):
                            $total_stars            = $this->_set_stars($product['product_id']);  
                            $stars                  = (int)$total_stars;
                            $prod['stars']          = $stars;
                            $this->db->update('products', $prod, array('product_id' => $product['product_id']));
                        endif;
                    endforeach; 
                endif;

                $data_['order_id']   = $result;
                $data_['status']     = 3;
                $data_['is_process'] = 0;
                $data_['title']      = null;
                $data_['process']    = null;
                $this->set_orders_status_action($data_); 

            endif;
        else:
            redirect(site_url("product"));
        endif;

        $this->load->view('template/body', $data);
    }

    public function import_save()
    {
        $input                  = $this->input->post();
        $input['order_type']    = 1;
        $input['status']        = 1;
        $value                  = $this->_build_data($input);  
        $result                 = $this->orders_m->insert($value);
        if($result):
            $value_ = $this->_build_detail_import_data($result, $input); 
            $this->orders_m->insert_orders_detail_import($value_); 

            if(empty($input['order_code'])):
                $order_code = $this->orders_m->set_order_in_id()->order_code;
                if(!empty($order_code)):
                    $value_code['order_qty']    = !empty($value_['qty_china']) ? $value_['qty_china'] : 0;
                    $order_code                 = '68-'.$order_code;
                    $value_code['order_code']   = $order_code; 
                    $data['order_code']         = $value_code['order_code'];
                    $this->db->update('orders', $value_code, array('order_id' => $result));

                    $value_import['tracking_in_code']   = $order_code; 
                    $this->db->update('orders_detail_import', $value_import, array('order_id' => $result));

                endif;
            endif;  

            $data_['order_id']   = $result;
            $data_['status']     = 1;
            $data_['is_process'] = 0;
            $data_['title']      = null;
            $data_['process']    = null;
            $this->set_orders_status_action($data_); 

            $res['status'] = "success";
            $res['message'] = "เราได้รับคำสั่งซื้อของคุณแล้ว";   
        else: 
            $res['status'] = "warning";
            $res['message'] = "เกิดข้อผิดผลาดกรุณาลองใหม่อีกครั้ง";  
        endif;
        echo json_encode($res);
    }

    private function set_orders_status_action($data)
    {
        $value['order_id']      = $data['order_id'];
        $value['status']        = $data['status'];
        $value['is_process']    = $data['is_process'];
        $value['title']         = $data['title'];
        $value['process']       = $data['process'];
        $value['created_at']    = db_datetime_now();
        $value['created_by']    = 0;

        $this->orders_m->insert_orders_status_action($value);
    }

    private function _build_data($input)
    { 

        $member = Modules::run('member/_get_member'); 
        if(!empty($member)): 
            list($name, $lasname) = explode(' ', $member->full_name);
        endif; 
        $value['name']              = !empty($name) ? $name : '';
        $value['lasname']           = !empty($lasname) ? $lasname : '';
        $value['address']           = !empty($input['address']) ? $input['address'] : '';
        $value['provinces']         = !empty($input['provinces']) ? $input['provinces'] : '';
        $value['amphures']          = !empty($input['amphures']) ? $input['amphures'] : '';
        $value['districts']         = !empty($input['districts']) ? $input['districts'] : '';
        $value['zip_code']          = !empty($input['zip_code']) ? $input['zip_code'] : '';
        $value['tel']               = !empty($input['tel']) ? $input['tel'] : $member->phone;
        $value['email']             = !empty($input['email']) ? $input['email'] : $member->email; 
        $value['status']            = !empty($input['status']) ? $input['status'] : 3;  
        $value['transport_id']      = !empty($input['transport_id']) ? $input['transport_id'] : null;
        $value['order_type']        = !empty($input['order_type']) ? $input['order_type'] : 0; //ประเภท order 
        $value['deliverys_price']   = !empty($input['deliverys_price']) ? $input['deliverys_price'] : 0; 
        $value['active']            = 1;
        
        $is_invoice                 = !empty($input['is_invoice']) ? $input['is_invoice'] : 0;
        $invoice_type               = !empty($input['invoice_type']) ? $input['invoice_type'] : 0;
        if($is_invoice > 0 && $invoice_type < 1):
            $value['invoice_no']            = $input['invoice_no'];
            $value['invoice_address']       = $input['invoice_address'];
            $value['invoice_provinces_id']  = $input['invoice_provinces_id'];
            $value['invoice_amphures_id']   = $input['invoice_amphures_id'];
            $value['invoice_districts_id']  = $input['invoice_districts_id'];
            $value['invoice_tel']           = $input['invoice_tel'];
            $value['invoice_zip_code']      = $input['invoice_zip_code'];
        elseif($is_invoice > 0 && $invoice_type > 0):
            $value['invoice_no']            = $input['invoice_no'];
        endif;

        $value['is_invoice']      = $is_invoice;
        $value['invoice_type']    = $invoice_type; 

        if(!empty($input['deliverys_price'])):
            $value['transfer_amount']   = $input['deliverys_price'];
        endif;

        if(!empty($input['transfer_datetime'])):
            $value['transfer_datetime'] = $input['transfer_datetime'];
        endif; 

        if(!empty($this->session->userdata['ses_mem']->id)):
            $value['user_id'] = !empty($this->session->userdata['ses_mem']->id) ? $this->session->userdata['ses_mem']->id : 0;
        endif;
        $value['created_at'] = db_datetime_now();
        $value['created_by'] = 0;
        $value['updated_at'] = db_datetime_now();
        $value['updated_by'] = 0;
       
        return $value;
    }

    private function _build_data_orders_payments($order_id, $input)
    { 
 
        $value['order_id']          = $order_id; 
        $value['bank_id']           = !empty($input['bank_id']) ? $input['bank_id'] : ''; 
        $value['payment_type']      = !empty($input['payment_type']) ? $input['payment_type'] : 0; 
        $value['discount']          = !empty($input['discount']) ? $input['discount'] : 0; 
        $value['type']              = 0; 
        $value['active']            = 0;  
        // Add file form product
        $path                = 'payment';
        $upload              = $this->uploadfile_library->do_upload('file_source',TRUE,$path);
        $file                = '';
        if(isset($upload['index'])){
            $file = 'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name']; 
            $value['file']      = $file;
        }

        if(!empty($this->session->userdata['ses_mem']->id)):
            $value['user_id'] = !empty($this->session->userdata['ses_mem']->id) ? $this->session->userdata['ses_mem']->id : 0;
        endif;
        $value['created_at'] = db_datetime_now();
        $value['created_by'] = 0;
        $value['updated_at'] = db_datetime_now();
        $value['updated_by'] = 0;
       
        return $value;
    }

    private function _build_detail_data($id, $input)
    { 
        // load config money
        $money_setting  = Modules::run('configs/get_setting', 'money_setting');  
        $money_th       = !empty($money_setting['money_th']) ? $money_setting['money_th'] : 0;
        $money_china    = !empty($money_setting['money_china']) ? $money_setting['money_china'] : 0; 

        $value          = array();
        $options_arr    = array();
        $star_arr       = array();
        $value_url      = array();
        $quantity       = 0;
        $cart           = Modules::run('carts/get_data_cart_list');  
        if(!empty($cart)):
            foreach($cart as $key => $item):

                if(in_array($key, $input['car_rowid'])):
                    if(empty($item['type'])):

                        $input_['product_id'] = $item['id'];
                        $product     = $this->product_m->get_rows($input_)->row(); 
                        $start_price = !empty($product->start_price) ? $product->start_price : 0 ; 
                        $order_detail_uniqid = strtoupper(uniqid());
                        // get data product attribute
                        if(!empty($item['options'])):
                            foreach($item['options'] as $key => $option): 
                                if(!empty($option['size'])): 
                                    array_push($options_arr, array(
                                        'order_detail_uniqid'   => $order_detail_uniqid,
                                        'attribute_name'        => 'size',
                                        'attribute_value'       => $option['size']
                                    ));
                                endif;
                                if(!empty($option['color'])): 
                                    array_push($options_arr, array(
                                        'order_detail_uniqid'   => $order_detail_uniqid,
                                        'attribute_name'        => 'color',
                                        'attribute_value'       => $option['color']
                                    ));
                                endif;
                            endforeach;
                        endif; 

                        $user_id    = !empty($this->session->userdata['ses_mem']->id) ? $this->session->userdata['ses_mem']->id : 0; 
                        $star_qty   = $this->_set_product_stars($item['qty']);

                        array_push($star_arr, array(
                            'order_id'      => $id,
                            'product_id'    => $item['id'],
                            'star_qty'      => $star_qty,
                            'user_id'       => $user_id,
                            'active'        => 1,
                            'created_at'    => db_datetime_now(),
                            'created_by'    => 0,
                            'updated_at'    => db_datetime_now(),
                            'updated_by'    => 0
                        ));

                        $quantity += $item['qty'];

                        array_push($value, array(
                            'order_id'              => $id,
                            'product_id'            => $item['id'],
                            'product_price'         => $item['price'],
                            'product_start_price'   => $start_price,
                            'rating_price_th'       => $money_th,
                            'rating_price_china'    => $money_china,
                            'quantity'              => $item['qty'],
                            'is_attribute'          => 0,
                            'created_at'            => db_datetime_now(),
                            'order_detail_uniqid'   => $order_detail_uniqid
                        ));

                    else:
                        // get data product attribute
                        $attribute = '';
                        if(!empty($item['options'])):
                            foreach($item['options'] as $keys => $option): 
                                if(!empty($option['attribute_title'])):
                                    if($keys == 0):
                                        $attribute.= $option['attribute_title'].':'.$option['attribute'];
                                    else:
                                        $attribute.= '<=>'.$option['attribute_title'].':'.$option['attribute'];
                                    endif;
                                endif;
                            endforeach;
                        endif; 

                        // get data product attribute
                        $image = '';
                        if(!empty($item['image'])):
                            foreach($item['image'] as $keys => $image): 
                                if(!empty($image)):
                                    if($keys == 0):
                                        $image.= $image;
                                    else:
                                        $image.= ','.$image;
                                    endif;
                                endif;
                            endforeach;
                        endif; 

                        $quantity += $item['qty'];

                        array_push($value_url, array(
                            'order_id'              => $id,
                            'product_price'         => $item['price'],
                            'image'                 => $image,
                            'title'                 => $item['name'],
                            'rating_price_th'       => $money_th,
                            'rating_price_china'    => $money_china,
                            'quantity'              => $item['qty'],
                            'attribute'             => $attribute,
                            'from_web'              => $item['from_web'],
                            'from_web_url'          => $item['from_web_url'],
                            'created_at'            => db_datetime_now()
                        ));
                    endif;
                endif;

            endforeach;
        endif;   
        return array($value, $options_arr, $quantity, $star_arr, $value_url);
    }

    private function _set_product_stars($qty)
    { 
        $star_qty = 0;
        if($qty > 0 && $qty <= 5):
            $star_qty = 1;
        endif;
        if($qty > 5 && $qty <= 10):
            $star_qty = 2;
        endif;
        if($qty > 10 && $qty <= 15):
            $star_qty = 3;
        endif;

        if($qty > 15 && $qty <= 20):
            $star_qty = 4;
        endif;
        if($qty > 20):
            $star_qty = 5;
        endif;
        return $star_qty;
    }

    private function _set_stars($product_id)
    {
        $total_stars    = 0;
        $product_stars  = $this->product_m->select_sql_query($product_id)->result();
        if(!empty($product_stars)):
            $user_qty    = 0;
            $sum_qty     = 0;
            foreach($product_stars as $stars):
                $sum_qty    += $stars->sum_qty;
                $user_qty   += $stars->user_qty;
            endforeach;
            $total_stars = $sum_qty / $user_qty;
            if($total_stars > 5):
                $total_stars = 5;
            endif;
        endif;
        return $total_stars;
    }

    private function _build_detail_import_data($id, $input)
    {
        if(!empty($this->session->userdata['ses_mem']->id)):
            $value['user_id'] = !empty($this->session->userdata['ses_mem']->id) ? $this->session->userdata['ses_mem']->id : 0;
        endif;
        
        $value['order_id']                          = $id; 
        $value['tracking_china']                    = !empty($input['tracking_china']) ? $input['tracking_china'] : '';
        $value['address_china']                     = !empty($input['address_china']) ? $input['address_china'] : '';
        $value['qty_china']                         = !empty($input['qty_china']) ? $input['qty_china'] : 0;
        $value['note_china']                        = !empty($input['note_china']) ? $input['note_china'] : '';
        $value['type_transportations_setting_id']   = !empty($input['type_transportations_setting_id']) ? $input['type_transportations_setting_id'] : '';
        $attribute_send_id                          = !empty($input['attribute_send_id']) ? $input['attribute_send_id'] : '';
        $attribute_send_type_id                     = !empty($input['attribute_send_type_id']) ? $input['attribute_send_type_id'] : '';
        $value['note_import']                       = !empty($input['note_import']) ? $input['note_import'] : '';

        $attribute_send = '';
        if(!empty($attribute_send_type_id)):
            foreach($attribute_send_type_id as $attribute_send_type):
                $attribute_send.= ','.$attribute_send_type;
            endforeach;
        endif;
        
        $value['attribute_send_id'] = $attribute_send_id.$attribute_send;
        $value['active']            = 1;

        $value['created_at'] = db_datetime_now();
        $value['created_by'] = $value['user_id'];
        $value['updated_at'] = db_datetime_now();
        $value['updated_by'] = $value['user_id'];
        return $value;
    }

    private function sentMail($data)
    { 

        $temp  = Modules::run('configs/get_setting', 'mail');  

        if(!empty($temp)):
            // load mail config  
            $Host 		= !empty($temp['SMTPserver']) ? $temp['SMTPserver'] : '';
            $Username 	= !empty($temp['SMTPusername']) ? $temp['SMTPusername'] : '';
            $Password 	= !empty($temp['SMTPpassword']) ? $temp['SMTPpassword'] : '';
            $SMTPSecure = 'ssl';
            $Port 		= !empty($temp['SMTPport']) ? $temp['SMTPport'] : '';  
            $viewMail = $this->load->view('email/email', $data, TRUE);

            // load mail send config 
            
            require 'app_frontend/third_party/phpmailer/PHPMailerAutoload.php';
            $mail = new PHPMailer;
            $mail->SMTPDebug = 0;                               	// Enable verbose debug output
            
            $mail->isSMTP();                                      	// Set mailer to use SMTP
            $mail->Host 		= $Host;              				// Specify main and backup SMTP servers
            $mail->SMTPAuth 	= true;                             // Enable SMTP authentication
            $mail->Username 	= $Username;                		// SMTP username
            $mail->Password 	= $Password;                        // SMTP password
            $mail->SMTPSecure 	= $SMTPSecure;                      // Enable TLS encryption, `ssl` also accepted
            $mail->Port 		= $Port;                            // TCP port to connect to
            $mail->CharSet 		= 'UTF-8';
            
            $mail->From 		= $Username;
            $mail->FromName 	= $Username;

            $email_to 			= $data['email'];

            $mail->addAddress($email_to);               			// Name is optional
            $mail->isHTML(false);                                  	// Set email format to HTML

            $mail->Subject = $email_to;
            $mail->Body    = $viewMail;
            $mail->AltBody = $viewMail; 
            $mail->Send();
            // $mail->ErrorInfo; 
        endif;
    }

    public function pdf()
    {  
        $input['order_id']  = urldecode(base64_decode($this->input->get('code')));
        $info               = $this->orders_m->get_orders_param($input); 
        $info               = $info->row();
        $data['info']       = $info;
        
        $infoDetail             = $this->orders_m->get_orders_detail_by_order_id($info->order_id)->result();
        if(!empty($infoDetail)):
            foreach($infoDetail as $item):
                $item->attributes = $this->orders_m->get_orders_detail_attribute_id($item->order_detail_uniqid)->result();
            endforeach;
        endif;
        $data['details']        = $infoDetail;

        //ดึงข้อมูลบริษัท
        $info_com           = $this->info_m->get_rows('');
        $info_com           = $info_com->row();
        $info_com->file     = base_url($info_com->file); 
        $data['company']    = $info_com; 
        header('Content-Type: image/jpeg;');  

        $code                = $info->order_code;
        $generator           = new Picqer\Barcode\BarcodeGeneratorHTML();
        $generator_img       = new Picqer\Barcode\BarcodeGeneratorPNG(); 
        $border              = 2;
        $height              = 50;
        $data['order_code']  = $code; 
        $data['img_barcode'] = '<img src="data:image/png;base64,' . base64_encode($generator_img->getBarcode($code, $generator_img::TYPE_CODE_128,$border,$height)) . '">';
        $data['barcode']     = $generator->getBarcode($code , $generator::TYPE_CODE_128,$border,$height);
        $mpdfConfig = [
            'default_font_size' => 9,
            'default_font'      => 'garuda'
        ];
 
        $mpdf = new \Mpdf\Mpdf($mpdfConfig);
        $content = $this->load->view('print_pdf', $data, true); 
        $mpdf->WriteHTML($content); 
        
        $download = FALSE;
        $saveFile = FALSE;
        $viewFile = TRUE;
        $fileName = "orders.pdf";
        $pathFile = "uploads/pdf/orders/";
        if ( $saveFile ) {
            create_dir($pathFile);
            $fileName = $pathFile.$fileName;
            $mpdf->Output($fileName, 'F');
        }
        if ( $download ) 
            $mpdf->Output($fileName, 'D');
        if ( $viewFile ) 
            $mpdf->Output();
        
        // page detail 
    }

    public function search_url()
	{

        $data = array(
            'seo'       => $this->seo(),
            'menu'      => 'product',
            'header'    => 'header',
            'content'   => 'search_url',
            'footer'    => 'footer',
            'function'  => array('product'),
        );

        $input = $this->input->post(null, true);
        $search = $input['search'];

        // load config money
        $money_setting          = Modules::run('configs/get_setting', 'money_setting');  
        $money_th               = !empty($money_setting['money_th']) ? $money_setting['money_th'] : 0;
        $money_china            = !empty($money_setting['money_china']) ? $money_setting['money_china'] : 0;
        $data['money_th']       = $money_th;
        $data['money_china']    = $money_china;

        $domain                 = parse_url($input['search'], PHP_URL_HOST);
        $domain_arr             = explode('.', $domain);

        // Call Url Share
        list($key, $domainArr) = $this->get_curl_html($input['search']);
        $domain_arr            = array_merge($domain_arr, $domainArr);
        
        if(in_array('tmall', $domain_arr) || in_array('taobao', $domain_arr) || in_array('1688', $domain_arr)):
            $data['from_web_url']   = !empty($input['search']) ? $input['search'] : '';
            if(in_array('tmall', $domain_arr) || in_array('taobao', $domain_arr)):
                $data['url_type']       = 'taobao-tmall';
                $data['url_api']        = 'https://api.openchinaapi.com/v1/taobao/products/';
                if(!empty($key)):
                    $data['code'] = $key;
                else:
                    $url_replace = str_replace('?','&', $input['search']);
                    $url_explode = explode('&', $url_replace);
                    if(!empty($url_explode)):
                        foreach($url_explode as $explode):
                            $url_id = explode('=', $explode);
                            if($url_id[0] == 'id'):
                                $data['code'] = $url_id[1];
                            endif;
                        endforeach;
                    endif;
                endif;
            endif;
            if(in_array('1688', $domain_arr)):
                $data['url_type'] = '1688';
                $data['url_api']  = 'https://api.openchinaapi.com/v1/1688/products/';
                if(!empty($key)):
                    $data['code'] = $key;
                else:
                    $url_explode  = explode('/', $input['search']);
                    $data['code'] = explode('.', explode('?', $url_explode[4])[0])[0];
                endif;
            endif;

            $this->load->view('template/body', $data);
        else:
            redirect(base_url("product/index?filter_product=".urldecode($search)));
        endif;
    }
    
    private function get_curl_html($url)
    {
        $url = trim($url);
        // 1688 $url = 'https://qr.1688.com/share.html?secret=zNjzYh98'; // tamll $url = 'https://m.tb.cn/h.4knO1JQ?sm=f432e7'; // taobao $url = 'https://m.tb.cn/h.4QPr7Dg?sm=ac1ba1';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        curl_setopt($ch, CURLOPT_COOKIESESSION, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 4);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close ($ch);    
        
        $key    = '';
        $data   = explode(".html?share_token", htmlentities($response));
        $data   = explode("offer?id=", $data[0]);
        $domain = array();
        if(!empty($data[1])):
            $key    = $data[1];
        else:
            $data   = explode("sourceType=", htmlentities($response));
            $data   = explode("id=", $data[0]);
            $data   = explode("&", $data[1]);
            if(is_numeric($data[0])):
                $key    = $data[0];
            else:
                $data   = explode(".htm?", htmlentities($response));
                $data   = explode("https://a.m.tmall.com/", $data[0]);
                $key    = substr($data[1], 1);
            endif;
            
            $domain = array('tmall','taobao');
        endif;

        //echo htmlentities($response);
        // arr($domain);
        // arr($key);
        // exit();
        
        return array($key, $domain);
    }

    public function debug_share()
    {
        $this->load->view('product/debug_share');
    }

    public function debug_url_share()
    {
        $url = $this->input->post('url');
        $this->get_curl_debug_html($url);
    }

    private function get_curl_debug_html($url)
    {
        $url = trim($url);
        // 1688 $url = 'https://qr.1688.com/share.html?secret=zNjzYh98'; // tamll $url = 'https://m.tb.cn/h.4knO1JQ?sm=f432e7'; // taobao $url = 'https://m.tb.cn/h.4QPr7Dg?sm=ac1ba1';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        curl_setopt($ch, CURLOPT_COOKIESESSION, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 4);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close ($ch);    
        
        $key    = '';
        $data   = explode(".html?share_token", htmlentities($response));
        $data   = explode("offer?id=", $data[0]);
        $domain = array();
        if(!empty($data[1])):
            $key    = $data[1];
            $domain = array('domain' =>'1688');
        else:
            $data   = explode("sourceType=", htmlentities($response));
            $data   = explode("id=", $data[0]);
            $data   = explode("&", $data[1]);
            if(is_numeric($data[0])):
                $key    = $data[0];
            else:
                $data   = explode(".htm?", htmlentities($response));
                $data   = explode("https://a.m.tmall.com/", $data[0]);
                $key    = substr($data[1], 1);
            endif;
            
            $domain = array(
                'domain' => array('tmall','taobao')
            );
        endif;

        echo htmlentities($response);

        arr($domain);
        arr(array('key' => $key));
        exit();
        
        //return array($key, $domain);
    }

}