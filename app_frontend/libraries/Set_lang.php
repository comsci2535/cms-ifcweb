<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Set_lang {

    public function __construct()
    {
        $CI =& get_instance();
        if($CI->session->userdata('CURRENT_LANG')=='en'){
            define('CURRENT_LANG', 'en');
            $CI->lang->load('custom', 'english');
        }else{
            define('CURRENT_LANG', 'th');
            $CI->lang->load('custom', 'thai');
        }
    }
}
