<?php
// หน้าหลัก
$lang['home'] = 'หน้าแรก'; //หน้าแรก
$lang['product'] = 'สินค้า';  //สินค้า
$lang['service'] = 'บริการของเรา';   //บริการของเรา
$lang['news_articles'] = 'ข่าวและบทความ';  //ข่าวและบทความ
$lang['help'] = 'ช่วยเหลือ';  //ช่วยเหลือ
$lang['question'] = 'คำถามที่พบบ่อย';  //คำถามที่พบบ่อย
$lang['how_to_website'] = 'วิธีการใช้งานเว็ปไซต์';  //วิธีการใช้งานเวปไซต์
$lang['terms_of_use'] = 'ข้อตกลงการใช้งาน'; //ข้อตกลงในการใช้งาน
$lang['about_us'] = 'เกี่ยวกับเรา';  //เกี่ยวกับเรา
$lang['contact_us'] = 'ติดต่อเรา'; //ติดต่อเรา
$lang['login'] = 'เข้าสู่ระบบ';  //เข้าสู่ระบบ
$lang['register'] = 'สมัครสมาชิก';  //สมัครสมาชิก
$lang['profile'] = 'โปรไฟลฺ์';  //โปรไฟลฺ์
$lang['Import_products'] = 'นำเข้าสินค้า'; //นำเข้าสินค้า
$lang['Order_history'] = 'ประวัติการสั่งซื้อ';  //ประวัติการสั่งซื้อ
$lang['logout'] = 'ออกจากระบบ'; //ออกจากระบบ
$lang['recommended_products'] = 'สินค้าแนะนำ'; //สินค้าแนะนำ
$lang['price'] = 'ราคา'; //ราคา
$lang['free_consultation_click'] = 'ให้คำปรึกษาฟรีคลิก'; //ให้คำปรึกษาฟรีคลิก

//คำนวณค่าส่ง หน้า home
$lang['transport_by_car'] = 'ขนส่งทางรถ';  //ขนส่งทางรถ
$lang['transport_by_ship'] = 'ขนส่งทางเรือ';  //ขนส่งทางเรือ
$lang['transport_by_air'] = 'ขนส่งทางเครื่องบิน';  //ขนส่งทางเครื่องบิน
$lang['general_product'] = 'สินค้าทั่วไป';  //สินค้าทั่วไป
$lang['special_product'] = 'สินค้าพิเศษ';  //สินค้าพิเศษ
$lang['fda_product'] = 'สินค้า อย.';  //สินค้า อย.
$lang['tisi_product'] = 'สินค้า มอก.';  //สินค้า มอก.
$lang['total_product_weight_kg'] = 'น้ำหนักสินค้ารวม : กิโลกรัม';  //น้ำหนักสินค้ารวม : กิโลกรัม
$lang['total_product_weight_Q'] = 'น้ำหนักสินค้ารวม : คิว';  //น้ำหนักสินค้ารวม : คิว
$lang['calculation_china_to_thailand'] = '* เป็นการคำนวนจากจีนมาไทยเท่านั้น 4-7 วัน ขนส่งทางเรือจะใช้วเวลา 15-20 ขึ้นไป';  //* เป็นการคำนวนจากจีนมาไทยเท่านั้น 4-7 วัน ขนส่งทางเรือจะใช้วเวลา 15-20 ขึ้นไป

//ปุ่มค้นหา - หน้าหลัก
$lang['search'] = 'ค้นหาสินค้า'; //ค้นหาสินค้า

//คำในปุ่มค้นหา  - หน้าหลัก
$lang['search_term'] = 'คำค้นหา'; //คำค้นหา

//ปุ่มเข้าสู่ระบบ 
$lang['user_name'] = 'ชื่อผู้ใช้'; //ชื่อผู้ใช้
$lang['password'] = 'รหัสผ่าน'; //รหัสผ่าน
$lang['forget_password'] = 'ลืมรหัสผ่าน'; //ลืมรหัสผ่าน
$lang['title_forget_password'] = 'กรอกอีเมลเพื่อทำการเปลี่ยนรหัสผ่าน'; //กรอกอีเมลเพื่อทำการเปลี่ยนรหัสผ่าน

//หน้าสมัครสมาชิก
$lang['confirm_password'] = 'ยืนยันรหัสผ่าน';  //ยืนยันรหัสผ่าน
$lang['fname'] = 'ชื่อ';  //ชื่อ
$lang['lname'] = 'นามสกุล';  //นามสกุล
$lang['fullname'] = 'ชื่อ - นามสกุล';  //ชื่อ - นามสกุล
$lang['address'] = 'บ้านเลขที่';  //บ้านเลขที่
$lang['building_village'] = 'อาคาร / หมุ่บ้าน';  //อาคาร / หมุ่บ้าน
$lang['street'] = 'ถนน';   //ถนน
$lang['Sub_district'] = 'แขวง / ตำบล ';  //แขวง / ตำบล 
$lang['district'] = 'เขต / อำเภอ';  //เขตอำเภอ 
$lang['province'] = 'จังหวัด';  //จังหวัด 
$lang['postal_code'] = 'รหัสไปรษณีย์ ';  //รหัสไปรษณีย์ 
$lang['email'] = 'อีเมล';  //อีเมล
$lang['phone_number'] = 'เบอร์โทร';  //เบอร์โทร
$lang['tax_invoice_no'] = 'เลขประจำตัวผู้เสียภาษีอากร';  //เลขประจำตัวผู้เสียภาษีอากร
$lang['title'] = 'หัวข้อ';  //หัวข้อ
$lang['detail'] = 'รายละเอียด';  //รายละเอียด
$lang['edit'] = 'แก้ไข';  //แก้ไข
$lang['delete'] = 'ลบ';  //ลบ
$lang['cancel'] = 'ยกเลิก';  //ยกเลิก
$lang['save'] = 'บันทึก';  //บันทึก
$lang['confirm'] = 'ยืนยัน';  //ยืนยัน
$lang['calculate'] = 'คำนวณ';  //คำนวณ
$lang['ok'] = 'ตกลง';  //ตกลง

//หน้าโปรไฟล์
$lang['upload_profile'] = 'อัพโหลดรูปโปรไฟล์';  //อัพโหลดรูปโปรไฟล์
$lang['PHOTO_FILES_2_MB'] = 'ไฟล์รูปถ่ายขนาดใหญ่ไม่เกิน 2 mb';  //ไฟล์รูปถ่ายขนาดใหญ่ไม่เกิน 2 mb

//หน้านำเข้าสินค้า
$lang['warehouse_address'] = 'ที่อยู่โกดังจีน';    //ที่อยู่โกดังจีน
$lang['chinese_parcel_number'] = 'เลขพัสดุจีน';  //เลขพัสดุจีน
$lang['quatity'] = 'จำนวน';  //จำนวน
$lang['product_note'] = 'สินค้า';  //สินค้า
$lang['note'] = 'หมายเหตุ';  //หมายเหตุ
$lang['payment'] = 'แจ้งชำระเงิน';  //แจ้งชำระเงิน
$lang['by_car'] = 'ทางรถ';  //ทางรถ
$lang['by_ship'] = 'ทางเรือ';  //ทางเรือ
$lang['hit_crates'] = 'ตีลังไม้';  //ตีลังไม้
$lang['product_qc'] = 'QC สินค้า';  //QC สินค้า

///ตาราง - หน้านำเข้าสินค้า
$lang['no'] = 'ลำดับ';  //ลำดับ
$lang['cabinet_date'] = 'วันที่ขึ้นตู้';  //วันที่ขึ้นตู้
$lang['recording_date'] = 'วันที่บันทึก';  //วันที่บันทึก

//หน้าประวัติการสั่งซื้อ
$lang['bill_number'] = 'เลขที่บิล';  //เลขที่บิล
$lang['cabinet'] = 'ขึ้นตู้';  //ขึ้นตู้
$lang['category'] = 'ประเภท';  //ประเภท
$lang['num_of_pieces'] = 'จำนวนชิ้น';  //จำนวนชิ้น
$lang['total_import_cost'] = 'ยอดรวมค่านำเข้า';  //ยอดรวมค่านำเข้า
$lang['total_deposit_for_purchase'] = 'ยอดรวมค่าฝากซื้อ';  //ยอดรวมค่าฝากซื้อ
$lang['See_details'] = 'ดูรายละเอียด';  //ดูรายละเอียด

///ในหน้ารายละเอียด - หน้าประวัติการสั่งซื้อ
$lang['bill_number'] = 'เลขที่บิล';  //เลขที่บิล
$lang['member_id'] = 'รหัสสมาชิก';  //รหัสสมาชิก
$lang['import_date'] = 'วันที่นำเข้า';  //วันที่นำเข้า
$lang['cabinet'] = 'ขึ้นตู้';  //ขึ้นตู้
$lang['out_cabinet'] = 'ลง';  //ลง
$lang['chinese_parcel_number2'] = 'เลขพัสดุจีน';  //เลขพัสดุจีน
$lang['data'] = 'ข้อมูล';  //ข้อมูล
$lang['pro_category'] = 'หมวดสินค้า';  //ประเภท
$lang['weight'] = 'น้ำหนัก';  //น้ำหนัก
$lang['service_charge'] = 'ค่าบริการ';  //ค่าบริการ
$lang['Import_cost'] = 'ค่านำเข้า';  //ค่านำเข้า
$lang['bath'] = 'บาท';  //บาท
$lang['shipping_fee'] = 'ค่าส่ง';  //ค่าส่ง
$lang['total'] = 'จำนวน';  //จำนวน
$lang['order_confirmation'] = 'ยืนยันการสั่งซื้อ';  //ยืนยันการสั่งซื้อ

//หน้าหลังจากกดยืนยันการสั่งซื้อ 
$lang['shipping_address'] = 'ที่อยู่จัดส่ง';  //ที่อยู่จัดส่ง
$lang['primary_shipping_address'] = 'เลือกเป็นที่อยู๋จัดส่งหลัก';  //เลือกเป็นที่อยู๋จัดส่งหลัก
$lang['new_shipping_address'] = 'เลือกเป็นที่อยู๋จัดส่งใหม่';  //เลือกเป็นที่อยู๋จัดส่งใหม่

///เมื่อคลิกที่อยู่จัดส่งใหม่จะแสดง -หน้าหลังจากกดยืนยันการสั่งซื้อ 
$lang['shipping channel'] = 'ช่องทางการจัดส่ง';  //ช่องทางการจัดส่ง

///ตาราง - หน้าหลังจากกดยืนยันการสั่งซื้อ 
$lang['status'] = 'สถานะ';  //สถานะ
$lang['parcel_number'] = 'เลขพัสดุ';  //เลขพัสดุ
$lang['cabinet_date'] = 'วันที่ขึ้นตู้';  //วันที่ขึ้นตู้
$lang['recording_date'] = 'วันที่บันทึก';  //วันที่บันทึก

$lang['calculate_shipping_cost'] = 'คำนวณค่าส่งเบื้องต้น';  //คำนวณค่าส่ง
$lang['gross_weight'] = 'น้ำหนัก';  //น้ำหนัก
$lang['distance'] = 'ระยะทาง';  //ระยะทาง
$lang['continue'] = 'ดำเนินการต่อ';  //ดำเนินการต่อ
$lang['please_note_shipping'] = '** หมายเหตุ ค่าจัดส่งจะคำนวณเพิ่มอีก 20% เมื่อหักค่าจัดส่งของแล้วค่าส่งคงเหลือ เจ้าหน้าที่ที่จะทำการโอนคืนเงินลูกค้าเต็มจำนวน';  //** หมายเหตุ ค่าจัดส่งจะคำนวณเพิ่มอีก 20% ...

//หน้าเมนูสินค้า
$lang['filter'] = 'ตัวกรอง';  //ตัวกรอง
$lang['price_range'] = 'ช่วงราคา';  //ช่วงราคา
$lang['least'] = 'น้อยสุด';  //น้อยสุด
$lang['most'] = 'มากสุด';  //มากสุด
$lang['popularity'] = 'ความนิยม';  //ความนิยม
$lang['go_up'] = 'ขึ้นไป';  //ขึ้นไป

//popup ก่อนเลือกลงตะกร้า
$lang['discount'] = 'ได้รับส่วนลด';  //ได้รับส่วนลด
$lang['add_to_cart'] = 'เพิ่มลงในตะกร้า';  //เพิ่มลงในตะกร้า

//หน้าตะกร้าสินค้า
$lang['number_of_color_bars'] = 'จำนวนแถบสี';  //จำนวนแถบสี
$lang['color'] = 'สี';  //ไซต์
$lang['size'] = 'ไซต์';  //ไซต์
$lang['buy_now'] = 'ซื้อเลย';  //ซื้อเลย
$lang['quantity'] = 'ปริมาณ';  //ปริมาณ
$lang['please_select_a_color'] = 'กรุณาเลือกสี';  //กรุณาเลือกสี
$lang['please_select_a_size'] = 'กรุณาเลือกไซต์';  //กรุณาเลือกไซต์
$lang['success_add_to_cart'] = 'เพิ่มในตะกร้าเรียบร้อย';  //เพิ่มในตะกร้าเรียบร้อย

///หน้าหลังจาก add สินค้า
$lang['product_list'] = 'รายการสินค้า';  //รายการสินค้า
$lang['total_price'] = 'ยอดรวม';  //ยอดรวม
$lang['total_'] = 'รวม';  //รวม
$lang['confirm_order'] = 'ยืนยันการสั่งซื้อ';  //ยืนยันการสั่งซื้อ

//หลังจากกดยืนยันการสั่งซื้อ
$lang['account'] = 'บันชี';  //บันชี
$lang['account_name'] = 'ชื่อบันชี';  //ชื่อบันชี
$lang['branch'] = 'สาขา';  //สาขา
$lang['proof_of_transfer'] = 'หลักฐานการโอนเงิน';  //หลักฐานการโอนเงิน
$lang['order_number'] = 'เลขที่สั่งซื้อ';  //เลขที่สั่งซื้อ
$lang['price_piece'] = 'ราคา / ชิ้น	';  //ราคา / ชิ้น	

//หน้าหลังจากกดยืนยันการสั่งซื้อ(หน้าแสดงเลข orderสั่งซื้อ)
$lang['we_received_order'] = 'เราได้รับคำสั่งซื้อของคุณแล้ว<br> ขอบคุณที่ไว้วางใจในบริการของเรา	';  //เราได้รับคำสั่งซื้อของคุณแล้วขอบคุณที่ไว้วางใจในบริการของเรา	
$lang['your_order_number_is'] = 'เลขคำสั่งซื้อของคุณคือ';  //เลขคำสั่งซื้อของคุณคือ	
$lang['You_receive_order'] = 'คุณจะได้รับอีเมลการสั่งซื้อ พร้อมรายละเอียดคำสั่งซื้อ และรายละเอียดการชำระเงิน';  //คุณจะได้รับอีเมลการสั่งซื้อ พร้อมรายละเอียดคำสั่งซื้อ และรายละเอียดการชำระเงิน
$lang['continue_shopping'] = 'เลือกซื้อสินค้าต่อ';  //เลือกซื้อสินค้าต่อ

//หน้าเมนูบริการของเรา
$lang['read_more'] = 'อ่านเพิ่มเติม';  //อ่านเพิ่มเติม

///หลังจากกดเพิ่มเติม	-หน้าเมนูบริการของเรา
$lang['other_services'] = 'บริการของเรา อื่นๆ';  //บริการของเรา อื่นๆ
$lang['other_news'] = 'ข่าวและบทความ อื่นๆ';  //ข่าวและบทความ อื่นๆ

//ข้อความอื่นๆ
$lang['upload_profile'] = 'อัพโหลดรูปโปรไฟล์';  //อัพโหลดรูปโปรไฟล์
$lang['Upload_evidence'] = 'อัพโหลดหลักฐาน';
$lang['Please_elect_image'] = 'กรุณาเลือกรูปภาพ';
$lang['Do_continue'] = 'คุณต้องการดำเนินการต่อ หรือไม่ ?';
$lang['Confirm_the_order'] = 'ยื่นยันสั่งซื้อ';
$lang['Ordering_and_importing'] = 'บริการสั่งซื้อและนำเข้าสินค้าจากจีน';
$lang['Today_rate'] = 'เรทวันนี้';

$lang['text_lang_1'] = 'ผู้ใช้บริหารได้รับทราบและยอมรับเงื่อนไขในการใช้บริการ www.ifcexpressshipping.com ตามที่ระบุไว้ดังนี้';
$lang['text_lang_2'] = 'ข้อมูลส่วนตัว';
$lang['text_lang_3'] = 'ยินดีต้อนรับ';
$lang['text_lang_4'] = 'ไฟล์รูปถ่ายขนาดใหญ่ไม่เกิน 2 mb';
$lang['text_lang_5'] = 'ส่งสินค้ามาที่โกดังตามข้อมูลด้านล่าง';
$lang['text_lang_6'] = "กรุณาตรวจสอบประเภทรูปภาพ";
$lang['text_lang_7'] = "กรุณาลองใหม่อีกครั้ง";
$lang['text_lang_8'] = "กรุณาเข้าสู่ระบบ/หรือสมัครสมาชิกก่อนสั่งซื้อสินค้า";
$lang['text_lang_9'] = "กรุณาเลือกบัญชีธนาคารที่ต้องการชำร";
$lang['text_lang_10'] = "ไม่มีรายการในตะกร้า";
$lang['text_lang_11'] = "บันทึกสำเร็จและเข้าสู่ระบบสำเร็จ";
$lang['text_lang_12'] = "ไม่สามารถบันทึกข้อมูลได้";
$lang['text_lang_13'] = "บันทึกสำเร็จ แต่เข้าสู่ระบบไม่ได้เนื่องจากต้องรอผู้ดูแลระบบอนุมัติ";
$lang['text_lang_14'] = "อัพเดทเรียบร้อยแล้ว";
$lang['text_lang_15'] = "ไม่สามารถอัพเดทข้อมูลได้";
$lang['text_lang_16'] = "อัพโหลดรูปโปรไฟล์สำเร็จ";
$lang['text_lang_17'] = "เกิดข้อผิดพลาดกรุณาลองใหม่อีกครั้ง";
$lang['text_lang_18'] = "ไม่สามารถส่งอีเมลได้";
$lang['text_lang_19'] = "เข้าสู่ระบบเรียบร้อยแล้ว";
$lang['text_lang_20'] = "รหัสผ่านไม่ถูกต้อง";
$lang['text_lang_21'] = "เกิดข้อผิดพลาดกรุณาลองใหม่อีกครั้ง";










