<?php
// หน้าหลัก
$lang['home'] = 'HOME'; //หน้าแรก
$lang['product'] = 'PRODUCT';  //สินค้า
$lang['service'] = 'SERVICE';   //บริการของเรา
$lang['news_articles'] = 'NEWS/ARTICLES';  //ข่าวและบทความ
$lang['help'] = 'HELP';  //ช่วยเหลือ
$lang['question'] = 'QUESTION';  //คำถามที่พบบ่อย
$lang['how_to_website'] = 'HOW TO USE THE WEBSITE';  //วิธีการใช้งานเวปไซต์
$lang['terms_of_use'] = 'TERMS OF USE'; //ข้อตกลงในการใช้งาน
$lang['about_us'] = 'ABOUT US';  //เกี่ยวกับเรา
$lang['contact_us'] = 'CONTACT US'; //ติดต่อเรา
$lang['login'] = 'LOGIN';  //เข้าสู่ระบบ
$lang['register'] = 'REGISTER';  //สมัครสมาชิก
$lang['profile'] = 'PROFILE';  //โปรไฟลฺ์
$lang['Import_products'] = 'IMPORT PRODUCT'; //นำเข้าสินค้า
$lang['Order_history'] = 'ORDER HITSTORY';  //ประวัติการสั่งซื้อ
$lang['logout'] = 'LOGOUT'; //ออกจากระบบ
$lang['recommended_products'] = 'RECOMMENDED PRODUCTS'; //สินค้าแนะนำ
$lang['price'] = 'Price'; //ราคา
$lang['free_consultation_click'] = 'Free Consultation Click'; //ให้คำปรึกษาฟรีคลิก

//คำนวณค่าส่ง หน้า home
$lang['transport_by_car'] = 'TRANSPORT BY CAR';  //ขนส่งทางรถ
$lang['transport_by_ship'] = 'TRANSPORT BY SHIP';  //ขนส่งทางเรือ
$lang['transport_by_air'] = 'TRANSPORT BY AIR';  //ขนส่งทางเครื่องบิน
$lang['general_product'] = 'GENERAL PRODUCT';  //สินค้าทั่วไป
$lang['special_product'] = 'SPECIAL PRODUCT';  //สินค้าพิเศษ
$lang['fda_product'] = 'FDA PRODUCT';  //สินค้า อย.
$lang['tisi_product'] = 'TISI PRODUCT';  //สินค้า มอก.
$lang['total_product_weight_kg'] = 'Total product weight : kg';  //น้ำหนักสินค้ารวม : กิโลกรัม
$lang['total_product_weight_Q'] = 'Total product weight : q';  //น้ำหนักสินค้ารวม : คิว
$lang['calculation_china_to_thailand'] = 'It is the calculation from China to Thailand only 4-7 days, shipping by sea takes 15-20 or more.';  //* เป็นการคำนวนจากจีนมาไทยเท่านั้น 4-7 วัน ขนส่งทางเรือจะใช้วเวลา 15-20 ขึ้นไป

//ปุ่มค้นหา
$lang['search'] = 'SEARCH'; //ค้นหาสินค้า

//คำในปุ่มค้นหา
$lang['search_term'] = 'SEARCH TERM'; //คำค้นหา

//ปุ่มเข้าสู่ระบบ
$lang['user_name'] = 'Username'; //ชื่อผู้ใช้
$lang['password'] = 'Password'; //รหัสผ่าน
$lang['forget_password'] = 'Forget Password'; //ลืมรหัสผ่าน
$lang['title_forget_password'] = 'Enter your email to change your password.'; //กรอกอีเมลเพื่อทำการเปลี่ยนรหัสผ่าน

//หน้าสมัครสมาชิก
$lang['confirm_password'] = 'Confirm Password';  //รหัสผ่าน
$lang['fname'] = 'First name';  //ชื่อ
$lang['lname'] = 'Last name';  //นามสกุล
$lang['fullname'] = 'Fullname';  //ชื่อ - นามสกุล
$lang['address'] = 'Address';  //บ้านเลขที่
$lang['building_village'] = 'Building/village';  //อาคาร / หมุ่บ้าน
$lang['street'] = 'Street';   //ถนน
$lang['Sub_district'] = 'Sub-district';  //แขวง / ตำบล 
$lang['district'] = 'District';  //เขตอำเภอ 
$lang['province'] = 'Province';  //จังหวัด 
$lang['postal_code'] = 'Postal code';  //รหัสไปรษณีย์ 
$lang['email'] = 'Email';  //อีเมล
$lang['phone_number'] = 'Phone number';  //เบอร์โทร
$lang['tax_invoice_no'] = 'Tax Identification Number';  //เลขประจำตัวผู้เสียภาษีอากร
$lang['title'] = 'Title';  //หัวข้อ
$lang['detail'] = 'Detail';  //รายละเอียด
$lang['edit'] = 'Edit';  //แก้ไข
$lang['delete'] = 'Delete';  //ลบ
$lang['cancel'] = 'Cancel';  //ยกเลิก
$lang['save'] = 'Save';  //บันทึก
$lang['confirm'] = 'Confirm';  //ยืนยัน
$lang['calculate'] = 'Calculate';  //คำนวณ
$lang['ok'] = 'Ok';  //ตกลง

//หน้าโปรไฟล์
$lang['upload_profile'] = 'UPLOAD PROFILE';  //อัพโหลดรูปโปรไฟล์
$lang['PHOTO_FILES_2_MB'] = 'PHOTO FILES ARE LARGER THAN 2 MB';  //ไฟล์รูปถ่ายขนาดใหญ่ไม่เกิน 2 mb

//หน้านำเข้าสินค้า
$lang['warehouse_address'] = 'Warehouse Address';    //ที่อยู่โกดัง
$lang['chinese_parcel_number'] = 'Chinese parcel number';  //เลขพัสดุจีน
$lang['quatity'] = 'QUATITY';  //จำนวน
$lang['product_note'] = 'PRODUCT';  //สินค้า
$lang['note'] = 'NOTE';  //หมายเหตุ
$lang['payment'] = 'PAYMENT';  //แจ้งชำระเงิน
$lang['by_car'] = 'BY CAR';  //ทางรถ
$lang['by_ship'] = 'BY SHIP';  //ทางเรือ
$lang['hit_crates'] = 'HIT CRATES';  //ตีลังไม้
$lang['product_qc'] = 'PRODUCT QC';  //QC สินค้า

///ตาราง - หน้านำเข้าสินค้า
$lang['no'] = 'NO';  //ลำดับ
$lang['cabinet_date'] = 'CABINET DATE';  //วันที่ขึ้นตู้
$lang['recording_date'] = 'RECORDING DATE';  //วันที่บันทึก

//หน้าประวัติการสั่งซื้อ
$lang['bill_number'] = 'BILL_NUMBER';  //เลขที่บิล
$lang['cabinet'] = 'CABINET';  //ขึ้นตู้
$lang['category'] = 'CATEGORY';  //ประเภท
$lang['num_of_pieces'] = 'NUMBER OF PIECES';  //จำนวนชิ้น
$lang['total_import_cost'] = 'TOTAL IMPORT COST';  //ยอดรวมค่านำเข้า
$lang['total_deposit_for_purchase'] = 'TOTAL DEPOSIT FOR PURCHASE';  //ยอดรวมค่าฝากซื้อ
$lang['See_details'] = 'SEE DETAILS';  //ดูรายละเอียด

///ในหน้ารายละเอียด - หน้าประวัติการสั่งซื้อ
$lang['bill_number'] = 'BILL NUMBER';  //เลขที่บิล
$lang['member_id'] = 'MEMBER ID';  //รหัสสมาชิก
$lang['import_date'] = 'IMPORT DATE';  //วันที่นำเข้า
$lang['cabinet'] = 'CABINET';  //ขึ้นตู้
$lang['out_cabinet'] = 'OUT CABINET';  //ลง
$lang['chinese_parcel_number2'] = 'CHINESE PARCEL NUMBER';  //เลขพัสดุจีน
$lang['data'] = 'DATA';  //ข้อมูล
$lang['pro_category'] = 'CATEGORY';  //ประเภท
$lang['weight'] = 'WEIGHT';  //น้ำหนัก
$lang['service_charge'] = 'SERVICE CHARGE';  //ค่าบริการ
$lang['Import_cost'] = 'IMPORT COST';  //ค่านำเข้า
$lang['bath'] = 'Bath';  //บาท
$lang['shipping_fee'] = 'SHIPPING FEE';  //ค่าส่ง
$lang['total'] = 'TOTAL';  //จำนวน
$lang['order_confirmation'] = 'ORDER CONFIRMATION';  //ยืนยันการสั่งซื้อ

//หน้าหลังจากกดยืนยันการสั่งซื้อ 
$lang['shipping_address'] = 'SHIPPING ADDRESS';  //ที่อยู่จัดส่ง
$lang['primary_shipping_address'] = 'PRIMARY SHIPPING ADDRESS';  //เลือกเป็นที่อยู๋จัดส่งหลัก
$lang['new_shipping_address'] = 'NEW SHIPPING ADDRESS';  //เลือกเป็นที่อยู๋จัดส่งใหม่

///เมื่อคลิกที่อยู่จัดส่งใหม่จะแสดง -หน้าหลังจากกดยืนยันการสั่งซื้อ 
$lang['shipping channel'] = 'SHIPPING CHANNEL';  //ช่องทางการจัดส่ง

///ตาราง - หน้าหลังจากกดยืนยันการสั่งซื้อ 
$lang['status'] = 'STATUS';  //สถานะ
$lang['parcel_number'] = 'PARCEL NUMBER';  //เลขพัสดุ
$lang['cabinet_date'] = 'CABINET DATE';  //วันที่ขึ้นตู้
$lang['recording_date'] = 'RECORDING DATE';  //วันที่บันทึก

$lang['calculate_shipping_cost'] = 'CALCULATE SHIPPING COST';  //คำนวณค่าส่ง
$lang['gross_weight'] = 'GROSS WEIGHT';  //น้ำหนัก
$lang['distance'] = 'DISTANCE';  //ระยะทาง
$lang['continue'] = 'CONTINUE';  //ดำเนินการต่อ
$lang['please_note_shipping'] = '** PLEASE NOTE THE SHIPPING COST WILL BE CALCULATED AN ADDITIONAL 20% AFTER DEDUCTING THE SHIPPING COST. THE OFFICER WILL TRANSFER A FULL REFUND TO THE CUSTOMER.';  //** หมายเหตุ ค่าจัดส่งจะคำนวณเพิ่มอีก 20% ...

//หน้าเมนูสินค้า
$lang['filter'] = 'FILTER';  //ตัวกรอง
$lang['price_range'] = 'PRICE RANGE';  //ช่วงราคา
$lang['least'] = 'LEAST';  //น้อยสุด
$lang['most'] = 'MOST';  //มากสุด
$lang['popularity'] = 'POPULARITY';  //ความนิยม
$lang['go_up'] = 'GO UP';  //ขึ้นไป

//popup ก่อนเลือกลงตะกร้า
$lang['discount'] = 'DISCOUNT';  //ได้รับส่วนลด
$lang['add_to_cart'] = 'ADD TO CART';  //เพิ่มลงในตะกร้า

//หน้าตะกร้าสินค้า
$lang['number_of_color_bars'] = 'Number of color bars';  //จำนวนแถบสี
$lang['color'] = 'Color';  //ไซต์
$lang['size'] = 'SIZE';  //ไซต์
$lang['buy_now'] = 'BUY NOW';  //ซื้อเลย
$lang['quantity'] = 'Quantity';  //ปริมาณ
$lang['please_select_a_color'] = 'Please select a color';  //กรุณาเลือกสี
$lang['please_select_a_size'] = 'Please select a size';  //กรุณาเลือกไซต์
$lang['success_add_to_cart'] = 'uccess add to cart';  //เพิ่มในตะกร้าเรียบร้อย

///หน้าหลังจาก add สินค้า
$lang['product_list'] = 'PRODUCT LIST';  //รายการสินค้า
$lang['total_price'] = 'Total price';  //ยอดรวม
$lang['total_'] = 'Total';  //รวม
$lang['confirm_order'] = 'CONFIRM ORDER';  //ยืนยันการสั่งซื้อ

//หลังจากกดยืนยันการสั่งซื้อ
$lang['account'] = 'Account';  //บันชี
$lang['account_name'] = 'Account NAnameME';  //ชื่อบันชี
$lang['branch'] = 'Branch';  //สาขา
$lang['proof_of_transfer'] = 'Proof of transfer';  //หลักฐานการโอนเงิน
$lang['order_number'] = 'Order number';  //เลขที่สั่งซื้อ
$lang['price_piece'] = 'Price / Piece';  //ราคา / ชิ้น	

//หน้าหลังจากกดยืนยันการสั่งซื้อ(หน้าแสดงเลข orderสั่งซื้อ)
$lang['we_received_order'] = 'We have received your order.<br> Thank you for trusting our service.';  //เราได้รับคำสั่งซื้อของคุณแล้วขอบคุณที่ไว้วางใจในบริการของเรา	
$lang['your_order_number_is'] = 'Your order number is';  //เลขคำสั่งซื้อของคุณคือ	
$lang['You_receive_order'] = 'You will receive an email with the order. With order details And payment details';  //คุณจะได้รับอีเมลการสั่งซื้อ พร้อมรายละเอียดคำสั่งซื้อ และรายละเอียดการชำระเงิน
$lang['continue_shopping'] = 'Continue to shopping';  //เลือกซื้อสินค้าต่อ

//หน้าเมนูบริการของเรา
$lang['read_more'] = 'Read more';  //อ่านเพิ่มเติม

///หลังจากกดเพิ่มเติม	-หน้าเมนูบริการของเรา
$lang['other_services'] = 'Other services';  //บริการของเรา อื่นๆ
$lang['other_news'] = 'Other news';  //ข่าวและบทความ อื่นๆ

//ข้อความอื่นๆ
$lang['upload_profile'] = 'UPLOAD PROFILE';  //อัพโหลดรูปโปรไฟล์
$lang['Upload_evidence'] = 'Upload evidence';
$lang['Please_elect_image'] = 'Please select an image';
$lang['Do_continue'] = 'Do continue ?';
$lang['Confirm_the_order'] = 'Confirm the order';
$lang['Ordering_and_importing'] = 'Ordering and importing services from China';
$lang['Today_rate'] = 'Today rate';

$lang['text_lang_1'] = 'Management users acknowledge and accept the terms of service. www.ifcexpressshipping.com As stated as follows';
$lang['text_lang_2'] = 'Personal information';
$lang['text_lang_3'] = 'Welcome';
$lang['text_lang_4'] = 'Large photo file no more than 2 mb.';
$lang['text_lang_5'] = 'Send products to the warehouse according to the information below.';
$lang['text_lang_6'] = 'Please check the image type.';
$lang['text_lang_7'] = "Please try again.";
$lang['text_lang_8'] = "Please login / or register before placing an order.";
$lang['text_lang_9'] = "Please select the bank account you wish to pay for.";
$lang['text_lang_10'] = "There are no items in the basket.";
$lang['text_lang_11'] = "Successfully saved and logged in successfully";
$lang['text_lang_12'] = "The data cannot be saved.";
$lang['text_lang_13'] = "Successful, but unable to log in due to admin approval.";
$lang['text_lang_14'] = "Successfully updated";
$lang['text_lang_15'] = "Could not update information";
$lang['text_lang_16'] = "Successfully uploaded profile picture";
$lang['text_lang_17'] = "Something went wrong, please try again.";
$lang['text_lang_18'] = "Unable to send email";
$lang['text_lang_19'] = "Logged in successfully.";
$lang['text_lang_20'] = "Password is incorrect";
$lang['text_lang_21'] = "Something went wrong, please try again.";








