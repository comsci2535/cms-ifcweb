<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed'); 
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php'; 

class Items extends REST_Controller 
{     
    //private $token;
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->helper('utils_api_helper');
        // $this->token = "Token b25a6cd1095b059ed599ecf653946b058c7f88d7";
    }

	public function get_get()
    { 
          
        $id = $this->get('id');

        // $url = "https://api.openchinaapi.com/v1/taobao/products/".$id;

        // $curl = curl_init(); curl_setopt_array($curl, array(
        //     CURLOPT_URL => $url, CURLOPT_RETURNTRANSFER => true,
        //     CURLOPT_ENCODING => "",
        //     CURLOPT_MAXREDIRS => 10,
        //     CURLOPT_TIMEOUT => 0,
        //     CURLOPT_FOLLOWLOCATION => true, CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1, CURLOPT_CUSTOMREQUEST => "GET", CURLOPT_HTTPHEADER => array(
        //     "Authorization: '.$this->token.'" /*Token วรรค <key จาก Openchinaapi>*/
        //     ), ));
            
        // $response           = curl_exec($curl);
        // curl_close($curl); 
        // $data['response']   = $response;
        $data['status']     = status(200); 
        $this->response($data, REST_Controller::HTTP_OK);
    }


}
