-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 30, 2021 at 01:58 PM
-- Server version: 5.7.26
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ifc`
--

-- --------------------------------------------------------

--
-- Table structure for table `aboutus`
--

CREATE TABLE `aboutus` (
  `aboutus_id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `title_en` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `excerpt_en` text,
  `detail` text,
  `detail_en` text,
  `file` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  `meta_title` text,
  `meta_description` text,
  `meta_keyword` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `aboutus`
--

INSERT INTO `aboutus` (`aboutus_id`, `slug`, `lang`, `title`, `title_en`, `excerpt`, `excerpt_en`, `detail`, `detail_en`, `file`, `active`, `order`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(1, 'เราคือผู้นำชิปปิ้งจีนครบวงจร-ให้บริการขนส่งทั้งทางรถและทางเรือ-ด้วยเรทค่าขนส่งที่ได้มาตรฐาน-และให้บริการอย่างมีคุณธรรม-ไม่ว่าคุณจะเป็นผู้ประกอบการนำเข้ามือใหม่หรือมืออาชีพ-ifc-express-shipping-ยินดีให้คำแนะนำและปรึกษาก่อนใช้บริการ', NULL, 'เราคือผู้นำชิปปิ้งจีนครบวงจร ให้บริการขนส่งทั้งทางรถและทางเรือ ด้วยเรทค่าขนส่งที่ได้มาตรฐาน และให้บริการอย่างมีคุณธรรม ไม่ว่าคุณจะเป็นผู้ประกอบการนำเข้ามือใหม่หรือมืออาชีพ IFC Express Shipping ยินดีให้คำแนะนำและปรึกษาก่อนใช้บริการ', 'We are the leader in complete Chinese shipping. Provides transportation services both by car and by boat. With standard shipping rates And serve with virtue Whether you are a beginner or a professional import operator, IFC Express Shipping is happy t', '', '', NULL, NULL, NULL, 1, 0, '2020-06-30 21:49:04', '1', '2020-08-29 18:32:15', '1', 0, NULL, NULL, 'เราคือผู้นำชิปปิ้งจีนครบวงจร ให้บริการขนส่งทั้งทางรถและทางเรือ ด้วยเรทค่าขนส่งที่ได้มาตรฐาน และให้บริการอย่างมีคุณธรรม ไม่ว่าคุณจะเป็นผู้ประกอบการนำเข้ามือใหม่หรือมืออาชีพ IFC Express Shipping ยินดีให้คำแนะนำและปรึกษาก่อนใช้บริการ', '', 'เราคือผู้นำชิปปิ้งจีนครบวงจร ให้บริการขนส่งทั้งทางรถและทางเรือ ด้วยเรทค่าขนส่งที่ได้มาตรฐาน และให้บริการอย่างมีคุณธรรม ไม่ว่าคุณจะเป็นผู้ประกอบการนำเข้ามือใหม่หรือมืออาชีพ IFC Express Shipping ยินดีให้คำแนะนำและปรึกษาก่อนใช้บริการ'),
(2, 'ประวัติความเป็นมา', NULL, 'ประวัติความเป็นมา', 'History', '&lt;p&gt;บริการชิปปิ้งที่รับส่งสินค้าจากจีนกลับไทย ดําเนินกิจการมาแล้วเป็นเวลายาวนาน ผ่านทีมงานทั้งไทยและเทศ ซึ่งเป็นทีมงานทั้งทางจีนและทางไทย &lt;span xss=&quot;removed&quot; style=&quot;font-size: 22px;&quot;&gt;ทําให้ได้รับความไว้วางใจจากผู้ใช้บริการมาอย่างยาวนาน IFC มีบริการขนส่ง สินค้า โดยทั้งทางรถ และทางเรือ&lt;/span&gt;&lt;br&gt;&lt;/p&gt;', '<p>Shipping service that sends products from China to Thailand It has been operating for a long time. Through both Thai and international teams Which is a team both in China and in Thailand This has gained<span xss=removed> the trust of users for a long time. IFC provides transportation services by both car and boat.</span><br></p>', NULL, NULL, 'uploads/aboutus/2020/06/c66dd16bfe762c074fd8039c35820d2e.JPG', 1, 1, '2020-06-30 21:50:07', '1', '2020-09-02 12:23:25', '1', 0, NULL, NULL, 'ประวัติความเป็นมา', '<p>บริการชิปปิ้งที่รับส่งสินค้าจากจีนกลับไทย ดําเนินกิจการมาแล้วเป็นเวลายาวนาน ผ่านทีมงานทั้งไทยและเทศ ซึ่งเป็นทีมงานทั้งทางจีนและทางไทย <span xss=\"removed\" style=\"font-size: 22px;\">ทําให้ได้รับความไว้วางใจจากผู้ใช้บริการมาอย่างยาวนาน IFC มีบริการขนส่ง สินค้า โดยทั้งทางรถ และทางเรือ</span><br></p>', 'ประวัติความเป็นมา'),
(3, 'ทำไมต้อง-ifc-express-shipping', NULL, 'ทำไมต้อง IFC Express Shipping', 'Why IFC Express Shipping?', '<p>บริการชิปปิ้งที่รับส่งสินค้าจากจีนกลับไทย ดําเนินกิจการมาแล้วเป็นเวลายาวนาน ผ่านทีมงานทั้งไทยและเทศ ซึ่งเป็นทีมงานทั้งทางจีนและทางไทย ทําให้ได้รับความไว้วางใจจากผู้ใช้บริการมาอย่างยาวนาน IFC มีบริการขนส่ง สินค้า โดยทั้งทางรถ และทางเรือ</p>', '<p>Shipping service that sends products from China to Thailand It has been operating for a long time. Through both Thai and international teams Which is a team both in China and in Thailand This has gained the trust of users for a long time. IFC provides transportation services by both car and boat.<br></p>', NULL, NULL, 'uploads/aboutus/2020/06/411666dd650e692607934074bf8cda5b.JPG', 1, 2, '2020-06-30 21:52:51', '1', '2020-08-29 18:33:15', '1', 0, NULL, NULL, 'ทำไมต้อง IFC Express Shipping', '<p>บริการชิปปิ้งที่รับส่งสินค้าจากจีนกลับไทย ดําเนินกิจการมาแล้วเป็นเวลายาวนาน ผ่านทีมงานทั้งไทยและเทศ ซึ่งเป็นทีมงานทั้งทางจีนและทางไทย ทําให้ได้รับความไว้วางใจจากผู้ใช้บริการมาอย่างยาวนาน IFC มีบริการขนส่ง สินค้า โดยทั้งทางรถ และทางเรือ</p>', 'ทำไมต้อง IFC Express Shipping'),
(4, 'เป้าหมายของเรา', NULL, 'เป้าหมายของเรา', 'Our goal', '<p>เพื่อสร้างมาตรฐานให้ทัดเทียมในระดับสากล และเพื่อเป้าหมายสำคัญของเรา ก็คือความพึงพอใจของลูกค้า อยากนำเข้าหรือส่งออกสินค้า แต่ไม่รู้จะทำอย่างไร เรายินดีให้คำปรึกษาขั้นตอนวิธีการ ซึ่งทำให้เราเป็นที่ไว้วางใจแก่บริษัทชั้นนำ พร้อมทั้งมีทีมงานผู้เชี่ยวชาญที่สามารถให้บริการงานสิทธิประโยชน์ด้านภาษี</p>', '<p>To create a standard to be equal in international level And for our important goals Is customer satisfaction. Want to import or export products But don\'t know what to do We are happy to advise you on the algorithm. Which makes us a trustworthy company Along with a team of experts who can provide tax benefits services<br></p>', NULL, NULL, 'uploads/aboutus/2020/06/b6ff4988717cf9c9a52fd089450a8054.JPG', 1, 3, '2020-06-30 21:54:26', '1', '2020-08-29 18:33:52', '1', 0, NULL, NULL, 'เป้าหมายของเรา', '<p>เพื่อสร้างมาตรฐานให้ทัดเทียมในระดับสากล และเพื่อเป้าหมายสำคัญของเรา ก็คือความพึงพอใจของลูกค้า อยากนำเข้าหรือส่งออกสินค้า แต่ไม่รู้จะทำอย่างไร เรายินดีให้คำปรึกษาขั้นตอนวิธีการ ซึ่งทำให้เราเป็นที่ไว้วางใจแก่บริษัทชั้นนำ พร้อมทั้งมีทีมงานผู้เชี่ยวชาญที่สามารถให้บริการงานสิทธิประโยชน์ด้านภาษี</p>', 'เป้าหมายของเรา');

-- --------------------------------------------------------

--
-- Table structure for table `attribute_sends`
--

CREATE TABLE `attribute_sends` (
  `attribute_send_id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `type` int(1) DEFAULT '0',
  `excerpt` text,
  `detail` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  `meta_title` text,
  `meta_description` text,
  `meta_keyword` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `attribute_sends`
--

INSERT INTO `attribute_sends` (`attribute_send_id`, `slug`, `lang`, `title`, `type`, `excerpt`, `detail`, `active`, `order`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(1, '', NULL, 'ทางรถ', 0, NULL, NULL, 1, 0, '2020-08-11 13:36:29', '1', '2020-08-11 13:36:31', '1', 0, NULL, NULL, NULL, NULL, NULL),
(2, '', NULL, 'ทางเรือ', 0, NULL, NULL, 1, 0, '2020-08-11 13:36:38', '1', '2020-08-11 13:37:11', '1', 0, '2020-08-11 13:37:02', 1, NULL, NULL, NULL),
(3, '', NULL, 'ตีลังไม้', 1, NULL, NULL, 1, 0, '2020-08-11 13:36:46', '1', '2020-08-23 22:37:33', '1', 0, NULL, NULL, NULL, NULL, NULL),
(4, '', NULL, 'QC สินค้า', 1, NULL, NULL, 1, 0, '2020-08-11 13:36:53', '1', '2020-08-23 22:37:15', '1', 0, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `auto_order_id`
--

CREATE TABLE `auto_order_id` (
  `id` char(16) CHARACTER SET ascii NOT NULL,
  `cdate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `auto_order_id`
--

INSERT INTO `auto_order_id` (`id`, `cdate`) VALUES
('202008000001', '2020-08-24 20:40:11'),
('202008000002', '2020-08-24 20:49:31'),
('202008000003', '2020-08-24 20:52:39'),
('202008000004', '2020-08-28 16:02:16'),
('202008000005', '2020-08-28 22:17:24'),
('202008000006', '2020-08-28 22:51:03'),
('202008000007', '2020-08-28 22:55:15'),
('202008000008', '2020-08-28 23:01:56'),
('202008000009', '2020-08-28 23:24:36'),
('202008000010', '2020-08-28 23:44:59'),
('202008000011', '2020-08-29 00:12:28'),
('202009000001', '2020-09-01 22:25:07'),
('202009000002', '2020-09-02 20:48:02'),
('202009000003', '2020-09-02 20:50:10'),
('202009000004', '2020-09-02 20:54:59'),
('202009000005', '2020-09-02 20:57:10'),
('202009000006', '2020-09-02 21:18:05'),
('202009000007', '2020-09-02 21:22:06'),
('202009000008', '2020-09-02 21:23:00'),
('202009000009', '2020-09-02 21:23:42'),
('202009000010', '2020-09-02 22:03:34'),
('202009000011', '2020-09-16 21:42:28'),
('202009000012', '2020-09-16 21:44:44'),
('202010000001', '2020-10-31 23:05:21'),
('202010000002', '2020-10-31 23:16:13'),
('202011000001', '2020-11-01 21:46:13'),
('202011000002', '2020-11-01 22:02:33'),
('202011000003', '2020-11-02 11:40:04'),
('202011000004', '2020-11-02 21:57:19'),
('202011000005', '2020-11-02 23:15:45'),
('202011000006', '2020-11-02 23:23:33'),
('202011000007', '2020-11-10 19:48:00'),
('202011000008', '2020-11-10 19:48:59'),
('202012000001', '2020-12-12 22:11:32'),
('202101000001', '2021-01-18 21:28:26'),
('202103000001', '2021-03-01 15:45:38'),
('202103000002', '2021-03-01 22:42:17');

-- --------------------------------------------------------

--
-- Table structure for table `auto_order_in_id`
--

CREATE TABLE `auto_order_in_id` (
  `id` char(16) CHARACTER SET ascii NOT NULL,
  `cdate` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `auto_order_in_id`
--

INSERT INTO `auto_order_in_id` (`id`, `cdate`) VALUES
('202102000001', '2021-02-28 12:35:44'),
('202102000002', '2021-02-28 12:36:13'),
('202103000001', '2021-03-01 16:31:25'),
('202103000002', '2021-03-01 16:32:43'),
('202103000003', '2021-03-01 22:20:58'),
('202103000004', '2021-03-01 22:22:09'),
('202104000001', '2021-04-29 22:13:24');

-- --------------------------------------------------------

--
-- Table structure for table `banks`
--

CREATE TABLE `banks` (
  `bank_id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `title_en` varchar(250) DEFAULT NULL,
  `account_number` varchar(250) DEFAULT NULL,
  `account_name` varchar(250) DEFAULT NULL,
  `account_name_en` varchar(250) DEFAULT NULL,
  `account_branch` varchar(255) DEFAULT NULL,
  `account_branch_en` varchar(255) DEFAULT NULL,
  `file_logo` text,
  `file` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  `meta_title` text,
  `meta_description` text,
  `meta_keyword` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `banks`
--

INSERT INTO `banks` (`bank_id`, `slug`, `lang`, `title`, `title_en`, `account_number`, `account_name`, `account_name_en`, `account_branch`, `account_branch_en`, `file_logo`, `file`, `active`, `order`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(1, '', NULL, 'กสิกรไทย', 'Kasikorn Thai', '1111111111', 'สมชาย ใจดี', 'Somchai Jaidee', 'สาขารามคำแหง', 'Ramkhamhaeng', 'uploads/banks/2020/07/811551ab1d2342c63f98fd4b2c45c150.JPG', 'uploads/banks/2020/07/397b27c72e7856cb89cbf4fd0b4d307f.JPG', 1, 0, '2020-07-01 14:13:26', '1', '2021-02-22 23:02:08', '1', 0, '2020-07-01 14:16:42', 1, NULL, NULL, NULL),
(2, '', NULL, 'กรุงไทย', 'Krung Thai', '1111111112', 'สมชาย ใจดี', 'Somchai Jaidee', 'สาขารามคำแหง', 'Ramkhamhaeng', 'uploads/banks/2020/08/dfa5f19237881013824040abaf896a3a.jpg', 'uploads/banks/2020/07/91dad422f8972ccb50cac2fa8e0157d9.JPG', 1, 0, '2020-07-17 15:05:34', '1', '2021-02-22 23:02:01', '1', 0, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `banner_id` int(11) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `title_en` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `excerpt_en` text,
  `file` varchar(255) NOT NULL,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `banners`
--

INSERT INTO `banners` (`banner_id`, `title`, `title_en`, `excerpt`, `excerpt_en`, `file`, `active`, `order`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`) VALUES
(1, 'ยินดีต้อนรับสู่', 'WELCOME TO', '&lt;p&gt;ยินดีต้อนรับสู่&lt;/p&gt;\r\n&lt;h2&gt;ไอเอฟซีเอ็กซ์เพรส&lt;/h2&gt;\r\n&lt;h1&gt;การจัดส่ง&lt;/h1&gt;', '&lt;p&gt;WELCOME TO&lt;/p&gt;&lt;h2 xss=removed&gt;IFC EXPRESS&lt;/h2&gt;&lt;h1 xss=removed&gt;PHIPPING&lt;/h1&gt;', 'uploads/banners/2020/09/6ebb005bc0a6eb1f94279d478bc01b85.jpg', 1, 0, '2020-09-02 22:39:37', '1', '2020-09-02 23:08:53', '1', 0, NULL, NULL),
(2, 'ยินดีต้อนรับสู่แบนเนอร์', 'WELCOME TO BANNER', '&lt;p&gt;ยินดีต้อนรับสู่แบนเนอร์&lt;/p&gt;&lt;h2 xss=&quot;removed&quot;&gt;ไอเอฟซีเอ็กซ์เพรส&lt;/h2&gt;&lt;h1 xss=&quot;removed&quot;&gt;การจัดส่ง&lt;/h1&gt;', '&lt;p&gt;WELCOME TO BANNER&lt;/p&gt;&lt;h2 xss=&quot;removed&quot; xss=removed&gt;IFC EXPRESS&lt;/h2&gt;&lt;h1 xss=&quot;removed&quot; xss=removed&gt;PHIPPING&lt;/h1&gt;', 'uploads/banners/2020/09/b5155efcf1be0b730927d8781b33dd71.jpg', 1, 0, '2020-09-02 22:46:09', '1', '2020-09-02 23:17:36', '1', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `categorie_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT '0',
  `slug` varchar(255) NOT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `title_en` varchar(255) NOT NULL,
  `excerpt` text,
  `excerpt_en` text NOT NULL,
  `detail` text,
  `detail_en` text NOT NULL,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  `meta_title` text,
  `meta_description` text,
  `meta_keyword` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`categorie_id`, `parent_id`, `slug`, `lang`, `title`, `title_en`, `excerpt`, `excerpt_en`, `detail`, `detail_en`, `active`, `order`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(9, 0, 'เส้อผ้า', NULL, 'เส้อผ้า', '', 'เส้อผ้า', '', NULL, '', 1, 0, '2020-12-25 19:35:42', '1', '2020-12-26 20:40:40', '1', 0, NULL, NULL, NULL, NULL, NULL),
(10, 0, 'กระเป๋า', NULL, 'กระเป๋า', '', 'กระเป๋า', '', NULL, '', 1, 0, '2020-12-25 19:36:18', '1', '2020-12-26 20:40:01', '1', 0, NULL, NULL, NULL, NULL, NULL),
(11, 0, 'ของใช้ในครัว', NULL, 'ของใช้ในครัว', '', 'ของใช้ในครัว', '', NULL, '', 1, 0, '2020-12-25 19:36:59', '1', '2020-12-26 20:40:22', '1', 0, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(255) NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `timestamp` int(11) DEFAULT NULL,
  `data` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('31vab5489r2fl7qda5ful09cbn8gv53h', '::1', 1619708190, '__ci_last_regenerate|i:1619708190;FBRLH_state|s:32:\"ac64de7f5e8ee756bcb703c7c9c98be4\";ses_mem|O:8:\"stdClass\":29:{s:2:\"id\";s:1:\"1\";s:9:\"member_id\";s:8:\"RL-00016\";s:8:\"username\";s:6:\"ruslee\";s:8:\"password\";s:60:\"$2y$10$u5/RGEwVN..Rmm/h2k.Mheb7SCaXnKEHl04L1WsXKta9/7eO5aPN6\";s:9:\"full_name\";s:12:\"ruslee seebu\";s:7:\"picture\";s:58:\"uploads/users/2020/08/9a64c19a40f3c71f7034727b520a9bfb.png\";s:5:\"phone\";s:10:\"0807055971\";s:5:\"email\";s:20:\"comsci2535@gmail.com\";s:12:\"house_number\";s:4:\"13/2\";s:8:\"building\";s:1:\"-\";s:4:\"road\";s:1:\"-\";s:8:\"district\";s:30:\"คลองต้นไทร\";s:6:\"amphoe\";s:21:\"คลองสาน\";s:8:\"province\";s:39:\"กรุงเทพมหานคร\";s:7:\"zipcode\";s:5:\"10600\";s:10:\"invoice_no\";s:12:\"231231231234\";s:7:\"created\";s:19:\"2020-08-10 10:12:37\";s:8:\"modified\";s:19:\"2021-03-01 15:29:34\";s:6:\"active\";s:1:\"1\";s:14:\"oauth_provider\";N;s:9:\"oauth_uid\";N;s:13:\"oauth_picture\";N;s:10:\"created_at\";s:19:\"2020-08-21 16:45:02\";s:10:\"created_by\";N;s:10:\"updated_at\";s:19:\"2021-03-01 15:29:34\";s:10:\"updated_by\";s:1:\"1\";s:7:\"recycle\";s:1:\"0\";s:10:\"recycle_at\";N;s:10:\"recycle_by\";N;}'),
('5mr16rjp4pb6ljvkos4omv5uoh8qgm8c', '::1', 1619709273, '__ci_last_regenerate|i:1619709273;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:17:\"super_super_admin\";s:4:\"Name\";s:39:\"ST โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:43:\"http://localhost/cms-ifcweb/images/user.png\";s:5:\"roles\";a:0:{}}condition|a:1:{s:15:\"separate_orders\";a:9:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:11:\"detail_send\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:14:\"tracking_china\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:6:\"detail\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:14:\"service_charge\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:11:\"import_cost\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:4:\"code\";s:12:\"YjY0NDNAODA=\";s:7:\"recycle\";i:0;s:8:\"order_id\";s:2:\"43\";}}'),
('5p24tov4eko9nfiig2sdg27dgrkv3rjv', '::1', 1619710561, '__ci_last_regenerate|i:1619710561;FBRLH_state|s:32:\"ac64de7f5e8ee756bcb703c7c9c98be4\";ses_mem|O:8:\"stdClass\":29:{s:2:\"id\";s:1:\"1\";s:9:\"member_id\";s:8:\"RL-00016\";s:8:\"username\";s:6:\"ruslee\";s:8:\"password\";s:60:\"$2y$10$u5/RGEwVN..Rmm/h2k.Mheb7SCaXnKEHl04L1WsXKta9/7eO5aPN6\";s:9:\"full_name\";s:12:\"ruslee seebu\";s:7:\"picture\";s:58:\"uploads/users/2020/08/9a64c19a40f3c71f7034727b520a9bfb.png\";s:5:\"phone\";s:10:\"0807055971\";s:5:\"email\";s:20:\"comsci2535@gmail.com\";s:12:\"house_number\";s:4:\"13/2\";s:8:\"building\";s:1:\"-\";s:4:\"road\";s:1:\"-\";s:8:\"district\";s:30:\"คลองต้นไทร\";s:6:\"amphoe\";s:21:\"คลองสาน\";s:8:\"province\";s:39:\"กรุงเทพมหานคร\";s:7:\"zipcode\";s:5:\"10600\";s:10:\"invoice_no\";s:12:\"231231231234\";s:7:\"created\";s:19:\"2020-08-10 10:12:37\";s:8:\"modified\";s:19:\"2021-03-01 15:29:34\";s:6:\"active\";s:1:\"1\";s:14:\"oauth_provider\";N;s:9:\"oauth_uid\";N;s:13:\"oauth_picture\";N;s:10:\"created_at\";s:19:\"2020-08-21 16:45:02\";s:10:\"created_by\";N;s:10:\"updated_at\";s:19:\"2021-03-01 15:29:34\";s:10:\"updated_by\";s:1:\"1\";s:7:\"recycle\";s:1:\"0\";s:10:\"recycle_at\";N;s:10:\"recycle_by\";N;}cart_contents|a:3:{s:10:\"cart_total\";d:2800;s:11:\"total_items\";d:1;s:32:\"244de90b96545a4e3a1caeb111c22f33\";a:7:{s:2:\"id\";s:1:\"1\";s:3:\"qty\";d:1;s:5:\"price\";d:2800;s:4:\"name\";s:200:\"กระเป๋าสัมภาระสุทธิกระเป๋าเดินทางกรณี รถเข็นสีแดง (แต่ละ ID จำกัด จำนวน 5)\";s:7:\"options\";a:2:{i:0;a:1:{s:4:\"size\";s:1:\"L\";}i:1;a:1:{s:5:\"color\";s:15:\"สีฟ้า\";}}s:5:\"rowid\";s:32:\"244de90b96545a4e3a1caeb111c22f33\";s:8:\"subtotal\";d:2800;}}'),
('8737lnjoa8oqame5t73np1p52dqvfob8', '::1', 1619716524, '__ci_last_regenerate|i:1619716524;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:17:\"super_super_admin\";s:4:\"Name\";s:39:\"ST โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:43:\"http://localhost/cms-ifcweb/images/user.png\";s:5:\"roles\";a:0:{}}condition|a:1:{s:13:\"news_articles\";a:7:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:6:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:10:\"created_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:10:\"updated_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:7:\"recycle\";i:0;}}'),
('9o5evaaj5om2tbiqrvmk1d02tef8hhgb', '::1', 1619709748, '__ci_last_regenerate|i:1619709748;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:17:\"super_super_admin\";s:4:\"Name\";s:39:\"ST โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:43:\"http://localhost/cms-ifcweb/images/user.png\";s:5:\"roles\";a:0:{}}condition|a:1:{s:15:\"separate_orders\";a:9:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:11:\"detail_send\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:14:\"tracking_china\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:6:\"detail\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:14:\"service_charge\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:11:\"import_cost\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:4:\"code\";s:12:\"YjY0NDNAOTM=\";s:7:\"recycle\";i:0;s:8:\"order_id\";s:2:\"43\";}}'),
('aapddpuhtmv7egulmhuduj2gh97ej74k', '::1', 1619716541, '__ci_last_regenerate|i:1619716541;FBRLH_state|s:32:\"ac64de7f5e8ee756bcb703c7c9c98be4\";ses_mem|O:8:\"stdClass\":29:{s:2:\"id\";s:1:\"1\";s:9:\"member_id\";s:8:\"RL-00016\";s:8:\"username\";s:6:\"ruslee\";s:8:\"password\";s:60:\"$2y$10$u5/RGEwVN..Rmm/h2k.Mheb7SCaXnKEHl04L1WsXKta9/7eO5aPN6\";s:9:\"full_name\";s:12:\"ruslee seebu\";s:7:\"picture\";s:58:\"uploads/users/2020/08/9a64c19a40f3c71f7034727b520a9bfb.png\";s:5:\"phone\";s:10:\"0807055971\";s:5:\"email\";s:20:\"comsci2535@gmail.com\";s:12:\"house_number\";s:4:\"13/2\";s:8:\"building\";s:1:\"-\";s:4:\"road\";s:1:\"-\";s:8:\"district\";s:30:\"คลองต้นไทร\";s:6:\"amphoe\";s:21:\"คลองสาน\";s:8:\"province\";s:39:\"กรุงเทพมหานคร\";s:7:\"zipcode\";s:5:\"10600\";s:10:\"invoice_no\";s:12:\"231231231234\";s:7:\"created\";s:19:\"2020-08-10 10:12:37\";s:8:\"modified\";s:19:\"2021-03-01 15:29:34\";s:6:\"active\";s:1:\"1\";s:14:\"oauth_provider\";N;s:9:\"oauth_uid\";N;s:13:\"oauth_picture\";N;s:10:\"created_at\";s:19:\"2020-08-21 16:45:02\";s:10:\"created_by\";N;s:10:\"updated_at\";s:19:\"2021-03-01 15:29:34\";s:10:\"updated_by\";s:1:\"1\";s:7:\"recycle\";s:1:\"0\";s:10:\"recycle_at\";N;s:10:\"recycle_by\";N;}cart_contents|a:3:{s:10:\"cart_total\";d:2800;s:11:\"total_items\";d:1;s:32:\"244de90b96545a4e3a1caeb111c22f33\";a:7:{s:2:\"id\";s:1:\"1\";s:3:\"qty\";d:1;s:5:\"price\";d:2800;s:4:\"name\";s:200:\"กระเป๋าสัมภาระสุทธิกระเป๋าเดินทางกรณี รถเข็นสีแดง (แต่ละ ID จำกัด จำนวน 5)\";s:7:\"options\";a:2:{i:0;a:1:{s:4:\"size\";s:1:\"L\";}i:1;a:1:{s:5:\"color\";s:15:\"สีฟ้า\";}}s:5:\"rowid\";s:32:\"244de90b96545a4e3a1caeb111c22f33\";s:8:\"subtotal\";d:2800;}}'),
('cp2i5ih71u6k0mghpgn4enfrq667d349', '::1', 1619708539, '__ci_last_regenerate|i:1619708539;FBRLH_state|s:32:\"ac64de7f5e8ee756bcb703c7c9c98be4\";ses_mem|O:8:\"stdClass\":29:{s:2:\"id\";s:1:\"1\";s:9:\"member_id\";s:8:\"RL-00016\";s:8:\"username\";s:6:\"ruslee\";s:8:\"password\";s:60:\"$2y$10$u5/RGEwVN..Rmm/h2k.Mheb7SCaXnKEHl04L1WsXKta9/7eO5aPN6\";s:9:\"full_name\";s:12:\"ruslee seebu\";s:7:\"picture\";s:58:\"uploads/users/2020/08/9a64c19a40f3c71f7034727b520a9bfb.png\";s:5:\"phone\";s:10:\"0807055971\";s:5:\"email\";s:20:\"comsci2535@gmail.com\";s:12:\"house_number\";s:4:\"13/2\";s:8:\"building\";s:1:\"-\";s:4:\"road\";s:1:\"-\";s:8:\"district\";s:30:\"คลองต้นไทร\";s:6:\"amphoe\";s:21:\"คลองสาน\";s:8:\"province\";s:39:\"กรุงเทพมหานคร\";s:7:\"zipcode\";s:5:\"10600\";s:10:\"invoice_no\";s:12:\"231231231234\";s:7:\"created\";s:19:\"2020-08-10 10:12:37\";s:8:\"modified\";s:19:\"2021-03-01 15:29:34\";s:6:\"active\";s:1:\"1\";s:14:\"oauth_provider\";N;s:9:\"oauth_uid\";N;s:13:\"oauth_picture\";N;s:10:\"created_at\";s:19:\"2020-08-21 16:45:02\";s:10:\"created_by\";N;s:10:\"updated_at\";s:19:\"2021-03-01 15:29:34\";s:10:\"updated_by\";s:1:\"1\";s:7:\"recycle\";s:1:\"0\";s:10:\"recycle_at\";N;s:10:\"recycle_by\";N;}cart_contents|a:3:{s:10:\"cart_total\";d:2800;s:11:\"total_items\";d:1;s:32:\"244de90b96545a4e3a1caeb111c22f33\";a:7:{s:2:\"id\";s:1:\"1\";s:3:\"qty\";d:1;s:5:\"price\";d:2800;s:4:\"name\";s:200:\"กระเป๋าสัมภาระสุทธิกระเป๋าเดินทางกรณี รถเข็นสีแดง (แต่ละ ID จำกัด จำนวน 5)\";s:7:\"options\";a:2:{i:0;a:1:{s:4:\"size\";s:1:\"L\";}i:1;a:1:{s:5:\"color\";s:15:\"สีฟ้า\";}}s:5:\"rowid\";s:32:\"244de90b96545a4e3a1caeb111c22f33\";s:8:\"subtotal\";d:2800;}}'),
('d862tajfrdqc6ti1k5k6rgdp9tll4lla', '::1', 1619708887, '__ci_last_regenerate|i:1619708887;FBRLH_state|s:32:\"ac64de7f5e8ee756bcb703c7c9c98be4\";ses_mem|O:8:\"stdClass\":29:{s:2:\"id\";s:1:\"1\";s:9:\"member_id\";s:8:\"RL-00016\";s:8:\"username\";s:6:\"ruslee\";s:8:\"password\";s:60:\"$2y$10$u5/RGEwVN..Rmm/h2k.Mheb7SCaXnKEHl04L1WsXKta9/7eO5aPN6\";s:9:\"full_name\";s:12:\"ruslee seebu\";s:7:\"picture\";s:58:\"uploads/users/2020/08/9a64c19a40f3c71f7034727b520a9bfb.png\";s:5:\"phone\";s:10:\"0807055971\";s:5:\"email\";s:20:\"comsci2535@gmail.com\";s:12:\"house_number\";s:4:\"13/2\";s:8:\"building\";s:1:\"-\";s:4:\"road\";s:1:\"-\";s:8:\"district\";s:30:\"คลองต้นไทร\";s:6:\"amphoe\";s:21:\"คลองสาน\";s:8:\"province\";s:39:\"กรุงเทพมหานคร\";s:7:\"zipcode\";s:5:\"10600\";s:10:\"invoice_no\";s:12:\"231231231234\";s:7:\"created\";s:19:\"2020-08-10 10:12:37\";s:8:\"modified\";s:19:\"2021-03-01 15:29:34\";s:6:\"active\";s:1:\"1\";s:14:\"oauth_provider\";N;s:9:\"oauth_uid\";N;s:13:\"oauth_picture\";N;s:10:\"created_at\";s:19:\"2020-08-21 16:45:02\";s:10:\"created_by\";N;s:10:\"updated_at\";s:19:\"2021-03-01 15:29:34\";s:10:\"updated_by\";s:1:\"1\";s:7:\"recycle\";s:1:\"0\";s:10:\"recycle_at\";N;s:10:\"recycle_by\";N;}cart_contents|a:3:{s:10:\"cart_total\";d:2800;s:11:\"total_items\";d:1;s:32:\"244de90b96545a4e3a1caeb111c22f33\";a:7:{s:2:\"id\";s:1:\"1\";s:3:\"qty\";d:1;s:5:\"price\";d:2800;s:4:\"name\";s:200:\"กระเป๋าสัมภาระสุทธิกระเป๋าเดินทางกรณี รถเข็นสีแดง (แต่ละ ID จำกัด จำนวน 5)\";s:7:\"options\";a:2:{i:0;a:1:{s:4:\"size\";s:1:\"L\";}i:1;a:1:{s:5:\"color\";s:15:\"สีฟ้า\";}}s:5:\"rowid\";s:32:\"244de90b96545a4e3a1caeb111c22f33\";s:8:\"subtotal\";d:2800;}}'),
('ggbjo7cr1biuamiofkf5r7n7acj0t63n', '::1', 1619715526, '__ci_last_regenerate|i:1619715526;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:17:\"super_super_admin\";s:4:\"Name\";s:39:\"ST โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:43:\"http://localhost/cms-ifcweb/images/user.png\";s:5:\"roles\";a:0:{}}condition|a:1:{s:15:\"separate_orders\";a:9:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:11:\"detail_send\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:14:\"tracking_china\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:6:\"detail\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:14:\"service_charge\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:11:\"import_cost\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:4:\"code\";s:12:\"YjY0NDNAMTQ=\";s:7:\"recycle\";i:0;s:8:\"order_id\";s:2:\"43\";}}'),
('j4aspshjtbai5ormlgb9reh6screug4v', '::1', 1619708957, '__ci_last_regenerate|i:1619708957;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:17:\"super_super_admin\";s:4:\"Name\";s:39:\"ST โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:43:\"http://localhost/cms-ifcweb/images/user.png\";s:5:\"roles\";a:0:{}}condition|a:1:{s:6:\"orders\";a:8:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:10:\"created_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:10:\"order_code\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:4:\"name\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:3:\"qty\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:8:\"discount\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:4:\"DESC\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"25\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:6:\"status\";a:12:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";i:4;s:1:\"5\";i:5;s:1:\"6\";i:6;s:1:\"7\";i:7;s:1:\"8\";i:8;s:1:\"9\";i:9;s:2:\"10\";i:10;s:2:\"11\";i:11;s:2:\"12\";}s:7:\"recycle\";i:0;}}'),
('k2idp94ialevdu8uh90vn087ka0jn3o1', '::1', 1619709206, '__ci_last_regenerate|i:1619709206;FBRLH_state|s:32:\"ac64de7f5e8ee756bcb703c7c9c98be4\";ses_mem|O:8:\"stdClass\":29:{s:2:\"id\";s:1:\"1\";s:9:\"member_id\";s:8:\"RL-00016\";s:8:\"username\";s:6:\"ruslee\";s:8:\"password\";s:60:\"$2y$10$u5/RGEwVN..Rmm/h2k.Mheb7SCaXnKEHl04L1WsXKta9/7eO5aPN6\";s:9:\"full_name\";s:12:\"ruslee seebu\";s:7:\"picture\";s:58:\"uploads/users/2020/08/9a64c19a40f3c71f7034727b520a9bfb.png\";s:5:\"phone\";s:10:\"0807055971\";s:5:\"email\";s:20:\"comsci2535@gmail.com\";s:12:\"house_number\";s:4:\"13/2\";s:8:\"building\";s:1:\"-\";s:4:\"road\";s:1:\"-\";s:8:\"district\";s:30:\"คลองต้นไทร\";s:6:\"amphoe\";s:21:\"คลองสาน\";s:8:\"province\";s:39:\"กรุงเทพมหานคร\";s:7:\"zipcode\";s:5:\"10600\";s:10:\"invoice_no\";s:12:\"231231231234\";s:7:\"created\";s:19:\"2020-08-10 10:12:37\";s:8:\"modified\";s:19:\"2021-03-01 15:29:34\";s:6:\"active\";s:1:\"1\";s:14:\"oauth_provider\";N;s:9:\"oauth_uid\";N;s:13:\"oauth_picture\";N;s:10:\"created_at\";s:19:\"2020-08-21 16:45:02\";s:10:\"created_by\";N;s:10:\"updated_at\";s:19:\"2021-03-01 15:29:34\";s:10:\"updated_by\";s:1:\"1\";s:7:\"recycle\";s:1:\"0\";s:10:\"recycle_at\";N;s:10:\"recycle_by\";N;}cart_contents|a:3:{s:10:\"cart_total\";d:2800;s:11:\"total_items\";d:1;s:32:\"244de90b96545a4e3a1caeb111c22f33\";a:7:{s:2:\"id\";s:1:\"1\";s:3:\"qty\";d:1;s:5:\"price\";d:2800;s:4:\"name\";s:200:\"กระเป๋าสัมภาระสุทธิกระเป๋าเดินทางกรณี รถเข็นสีแดง (แต่ละ ID จำกัด จำนวน 5)\";s:7:\"options\";a:2:{i:0;a:1:{s:4:\"size\";s:1:\"L\";}i:1;a:1:{s:5:\"color\";s:15:\"สีฟ้า\";}}s:5:\"rowid\";s:32:\"244de90b96545a4e3a1caeb111c22f33\";s:8:\"subtotal\";d:2800;}}'),
('njgcq3okmsurv9nc1q1esohpjj9ffqtm', '::1', 1619715796, '__ci_last_regenerate|i:1619715796;FBRLH_state|s:32:\"ac64de7f5e8ee756bcb703c7c9c98be4\";ses_mem|O:8:\"stdClass\":29:{s:2:\"id\";s:1:\"1\";s:9:\"member_id\";s:8:\"RL-00016\";s:8:\"username\";s:6:\"ruslee\";s:8:\"password\";s:60:\"$2y$10$u5/RGEwVN..Rmm/h2k.Mheb7SCaXnKEHl04L1WsXKta9/7eO5aPN6\";s:9:\"full_name\";s:12:\"ruslee seebu\";s:7:\"picture\";s:58:\"uploads/users/2020/08/9a64c19a40f3c71f7034727b520a9bfb.png\";s:5:\"phone\";s:10:\"0807055971\";s:5:\"email\";s:20:\"comsci2535@gmail.com\";s:12:\"house_number\";s:4:\"13/2\";s:8:\"building\";s:1:\"-\";s:4:\"road\";s:1:\"-\";s:8:\"district\";s:30:\"คลองต้นไทร\";s:6:\"amphoe\";s:21:\"คลองสาน\";s:8:\"province\";s:39:\"กรุงเทพมหานคร\";s:7:\"zipcode\";s:5:\"10600\";s:10:\"invoice_no\";s:12:\"231231231234\";s:7:\"created\";s:19:\"2020-08-10 10:12:37\";s:8:\"modified\";s:19:\"2021-03-01 15:29:34\";s:6:\"active\";s:1:\"1\";s:14:\"oauth_provider\";N;s:9:\"oauth_uid\";N;s:13:\"oauth_picture\";N;s:10:\"created_at\";s:19:\"2020-08-21 16:45:02\";s:10:\"created_by\";N;s:10:\"updated_at\";s:19:\"2021-03-01 15:29:34\";s:10:\"updated_by\";s:1:\"1\";s:7:\"recycle\";s:1:\"0\";s:10:\"recycle_at\";N;s:10:\"recycle_by\";N;}cart_contents|a:3:{s:10:\"cart_total\";d:2800;s:11:\"total_items\";d:1;s:32:\"244de90b96545a4e3a1caeb111c22f33\";a:7:{s:2:\"id\";s:1:\"1\";s:3:\"qty\";d:1;s:5:\"price\";d:2800;s:4:\"name\";s:200:\"กระเป๋าสัมภาระสุทธิกระเป๋าเดินทางกรณี รถเข็นสีแดง (แต่ละ ID จำกัด จำนวน 5)\";s:7:\"options\";a:2:{i:0;a:1:{s:4:\"size\";s:1:\"L\";}i:1;a:1:{s:5:\"color\";s:15:\"สีฟ้า\";}}s:5:\"rowid\";s:32:\"244de90b96545a4e3a1caeb111c22f33\";s:8:\"subtotal\";d:2800;}}'),
('ob0ief812snfgl47dnst512f024a29be', '::1', 1619707653, '__ci_last_regenerate|i:1619707653;FBRLH_state|s:32:\"ac64de7f5e8ee756bcb703c7c9c98be4\";ses_mem|O:8:\"stdClass\":29:{s:2:\"id\";s:1:\"1\";s:9:\"member_id\";s:8:\"RL-00016\";s:8:\"username\";s:6:\"ruslee\";s:8:\"password\";s:60:\"$2y$10$u5/RGEwVN..Rmm/h2k.Mheb7SCaXnKEHl04L1WsXKta9/7eO5aPN6\";s:9:\"full_name\";s:12:\"ruslee seebu\";s:7:\"picture\";s:58:\"uploads/users/2020/08/9a64c19a40f3c71f7034727b520a9bfb.png\";s:5:\"phone\";s:10:\"0807055971\";s:5:\"email\";s:20:\"comsci2535@gmail.com\";s:12:\"house_number\";s:4:\"13/2\";s:8:\"building\";s:1:\"-\";s:4:\"road\";s:1:\"-\";s:8:\"district\";s:30:\"คลองต้นไทร\";s:6:\"amphoe\";s:21:\"คลองสาน\";s:8:\"province\";s:39:\"กรุงเทพมหานคร\";s:7:\"zipcode\";s:5:\"10600\";s:10:\"invoice_no\";s:12:\"231231231234\";s:7:\"created\";s:19:\"2020-08-10 10:12:37\";s:8:\"modified\";s:19:\"2021-03-01 15:29:34\";s:6:\"active\";s:1:\"1\";s:14:\"oauth_provider\";N;s:9:\"oauth_uid\";N;s:13:\"oauth_picture\";N;s:10:\"created_at\";s:19:\"2020-08-21 16:45:02\";s:10:\"created_by\";N;s:10:\"updated_at\";s:19:\"2021-03-01 15:29:34\";s:10:\"updated_by\";s:1:\"1\";s:7:\"recycle\";s:1:\"0\";s:10:\"recycle_at\";N;s:10:\"recycle_by\";N;}'),
('olebt5h4lklgaqeubgbu1nvrfha5i5u2', '::1', 1619716712, '__ci_last_regenerate|i:1619716541;FBRLH_state|s:32:\"ac64de7f5e8ee756bcb703c7c9c98be4\";ses_mem|O:8:\"stdClass\":29:{s:2:\"id\";s:1:\"1\";s:9:\"member_id\";s:8:\"RL-00016\";s:8:\"username\";s:6:\"ruslee\";s:8:\"password\";s:60:\"$2y$10$u5/RGEwVN..Rmm/h2k.Mheb7SCaXnKEHl04L1WsXKta9/7eO5aPN6\";s:9:\"full_name\";s:12:\"ruslee seebu\";s:7:\"picture\";s:58:\"uploads/users/2020/08/9a64c19a40f3c71f7034727b520a9bfb.png\";s:5:\"phone\";s:10:\"0807055971\";s:5:\"email\";s:20:\"comsci2535@gmail.com\";s:12:\"house_number\";s:4:\"13/2\";s:8:\"building\";s:1:\"-\";s:4:\"road\";s:1:\"-\";s:8:\"district\";s:30:\"คลองต้นไทร\";s:6:\"amphoe\";s:21:\"คลองสาน\";s:8:\"province\";s:39:\"กรุงเทพมหานคร\";s:7:\"zipcode\";s:5:\"10600\";s:10:\"invoice_no\";s:12:\"231231231234\";s:7:\"created\";s:19:\"2020-08-10 10:12:37\";s:8:\"modified\";s:19:\"2021-03-01 15:29:34\";s:6:\"active\";s:1:\"1\";s:14:\"oauth_provider\";N;s:9:\"oauth_uid\";N;s:13:\"oauth_picture\";N;s:10:\"created_at\";s:19:\"2020-08-21 16:45:02\";s:10:\"created_by\";N;s:10:\"updated_at\";s:19:\"2021-03-01 15:29:34\";s:10:\"updated_by\";s:1:\"1\";s:7:\"recycle\";s:1:\"0\";s:10:\"recycle_at\";N;s:10:\"recycle_by\";N;}cart_contents|a:3:{s:10:\"cart_total\";d:2800;s:11:\"total_items\";d:1;s:32:\"244de90b96545a4e3a1caeb111c22f33\";a:7:{s:2:\"id\";s:1:\"1\";s:3:\"qty\";d:1;s:5:\"price\";d:2800;s:4:\"name\";s:200:\"กระเป๋าสัมภาระสุทธิกระเป๋าเดินทางกรณี รถเข็นสีแดง (แต่ละ ID จำกัด จำนวน 5)\";s:7:\"options\";a:2:{i:0;a:1:{s:4:\"size\";s:1:\"L\";}i:1;a:1:{s:5:\"color\";s:15:\"สีฟ้า\";}}s:5:\"rowid\";s:32:\"244de90b96545a4e3a1caeb111c22f33\";s:8:\"subtotal\";d:2800;}}'),
('pfau9a40qged3romv577p1ujof1t4e0e', '::1', 1619714749, '__ci_last_regenerate|i:1619714749;FBRLH_state|s:32:\"ac64de7f5e8ee756bcb703c7c9c98be4\";ses_mem|O:8:\"stdClass\":29:{s:2:\"id\";s:1:\"1\";s:9:\"member_id\";s:8:\"RL-00016\";s:8:\"username\";s:6:\"ruslee\";s:8:\"password\";s:60:\"$2y$10$u5/RGEwVN..Rmm/h2k.Mheb7SCaXnKEHl04L1WsXKta9/7eO5aPN6\";s:9:\"full_name\";s:12:\"ruslee seebu\";s:7:\"picture\";s:58:\"uploads/users/2020/08/9a64c19a40f3c71f7034727b520a9bfb.png\";s:5:\"phone\";s:10:\"0807055971\";s:5:\"email\";s:20:\"comsci2535@gmail.com\";s:12:\"house_number\";s:4:\"13/2\";s:8:\"building\";s:1:\"-\";s:4:\"road\";s:1:\"-\";s:8:\"district\";s:30:\"คลองต้นไทร\";s:6:\"amphoe\";s:21:\"คลองสาน\";s:8:\"province\";s:39:\"กรุงเทพมหานคร\";s:7:\"zipcode\";s:5:\"10600\";s:10:\"invoice_no\";s:12:\"231231231234\";s:7:\"created\";s:19:\"2020-08-10 10:12:37\";s:8:\"modified\";s:19:\"2021-03-01 15:29:34\";s:6:\"active\";s:1:\"1\";s:14:\"oauth_provider\";N;s:9:\"oauth_uid\";N;s:13:\"oauth_picture\";N;s:10:\"created_at\";s:19:\"2020-08-21 16:45:02\";s:10:\"created_by\";N;s:10:\"updated_at\";s:19:\"2021-03-01 15:29:34\";s:10:\"updated_by\";s:1:\"1\";s:7:\"recycle\";s:1:\"0\";s:10:\"recycle_at\";N;s:10:\"recycle_by\";N;}cart_contents|a:3:{s:10:\"cart_total\";d:2800;s:11:\"total_items\";d:1;s:32:\"244de90b96545a4e3a1caeb111c22f33\";a:7:{s:2:\"id\";s:1:\"1\";s:3:\"qty\";d:1;s:5:\"price\";d:2800;s:4:\"name\";s:200:\"กระเป๋าสัมภาระสุทธิกระเป๋าเดินทางกรณี รถเข็นสีแดง (แต่ละ ID จำกัด จำนวน 5)\";s:7:\"options\";a:2:{i:0;a:1:{s:4:\"size\";s:1:\"L\";}i:1;a:1:{s:5:\"color\";s:15:\"สีฟ้า\";}}s:5:\"rowid\";s:32:\"244de90b96545a4e3a1caeb111c22f33\";s:8:\"subtotal\";d:2800;}}'),
('pupa8va75srpr8rgdp3t937n6dute439', '::1', 1619711248, '__ci_last_regenerate|i:1619711248;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:17:\"super_super_admin\";s:4:\"Name\";s:39:\"ST โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:43:\"http://localhost/cms-ifcweb/images/user.png\";s:5:\"roles\";a:0:{}}condition|a:1:{s:15:\"separate_orders\";a:9:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:11:\"detail_send\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:14:\"tracking_china\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:6:\"detail\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:14:\"service_charge\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:11:\"import_cost\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:4:\"code\";s:12:\"YjY0NDNAMTQ=\";s:7:\"recycle\";i:0;s:8:\"order_id\";s:2:\"43\";}}'),
('rftdqf0k7e29n679o2fk3bkok35hs2cp', '::1', 1619710123, '__ci_last_regenerate|i:1619710123;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:17:\"super_super_admin\";s:4:\"Name\";s:39:\"ST โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:43:\"http://localhost/cms-ifcweb/images/user.png\";s:5:\"roles\";a:0:{}}condition|a:1:{s:15:\"separate_orders\";a:9:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:11:\"detail_send\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:14:\"tracking_china\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:6:\"detail\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:14:\"service_charge\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:11:\"import_cost\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:4:\"code\";s:12:\"YjY0NDNAOTM=\";s:7:\"recycle\";i:0;s:8:\"order_id\";s:2:\"43\";}}'),
('s58tm5480ul5a8nqk4tvmo1pdb8469k9', '::1', 1619716818, '__ci_last_regenerate|i:1619716524;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:17:\"super_super_admin\";s:4:\"Name\";s:39:\"ST โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:43:\"http://localhost/cms-ifcweb/images/user.png\";s:5:\"roles\";a:0:{}}condition|a:1:{s:8:\"products\";a:7:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:5:\"title\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:7:\"excerpt\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:10:\"created_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:10:\"updated_at\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:4:\"true\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:6:\"active\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:7:\"recycle\";i:0;}}'),
('t7ld35e8qu1kg3ej07ipklb6sjlc7blg', '::1', 1619710565, '__ci_last_regenerate|i:1619710565;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:17:\"super_super_admin\";s:4:\"Name\";s:39:\"ST โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:43:\"http://localhost/cms-ifcweb/images/user.png\";s:5:\"roles\";a:0:{}}condition|a:1:{s:15:\"separate_orders\";a:9:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:11:\"detail_send\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:14:\"tracking_china\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:6:\"detail\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:14:\"service_charge\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:11:\"import_cost\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:4:\"code\";s:12:\"YjY0NDNAMTQ=\";s:7:\"recycle\";i:0;s:8:\"order_id\";s:2:\"43\";}}'),
('tda5tl9avsuh5arv61esne6jhkbpkk30', '::1', 1619710919, '__ci_last_regenerate|i:1619710919;users|a:10:{s:3:\"UID\";s:1:\"1\";s:8:\"Username\";s:17:\"super_super_admin\";s:4:\"Name\";s:39:\"ST โปรแกรมเมอร์\";s:6:\"RoleId\";s:1:\"1\";s:4:\"type\";s:9:\"developer\";s:5:\"email\";s:15:\"admin@admin.com\";s:9:\"isBackend\";b:1;s:7:\"user_id\";s:1:\"1\";s:5:\"image\";s:43:\"http://localhost/cms-ifcweb/images/user.png\";s:5:\"roles\";a:0:{}}condition|a:1:{s:15:\"separate_orders\";a:9:{s:4:\"draw\";s:1:\"1\";s:7:\"columns\";a:7:{i:0;a:5:{s:4:\"data\";s:8:\"checkbox\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:1;a:5:{s:4:\"data\";s:11:\"detail_send\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:2;a:5:{s:4:\"data\";s:14:\"tracking_china\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:3;a:5:{s:4:\"data\";s:6:\"detail\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:4;a:5:{s:4:\"data\";s:14:\"service_charge\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:5;a:5:{s:4:\"data\";s:11:\"import_cost\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}i:6;a:5:{s:4:\"data\";s:6:\"action\";s:4:\"name\";s:0:\"\";s:10:\"searchable\";s:4:\"true\";s:9:\"orderable\";s:5:\"false\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}}}s:5:\"order\";a:1:{i:0;a:2:{s:6:\"column\";s:1:\"1\";s:3:\"dir\";s:3:\"asc\";}}s:5:\"start\";s:1:\"0\";s:6:\"length\";s:2:\"10\";s:6:\"search\";a:2:{s:5:\"value\";s:0:\"\";s:5:\"regex\";s:5:\"false\";}s:4:\"code\";s:12:\"YjY0NDNAMTQ=\";s:7:\"recycle\";i:0;s:8:\"order_id\";s:2:\"43\";}}');

-- --------------------------------------------------------

--
-- Table structure for table `conditions`
--

CREATE TABLE `conditions` (
  `condition_id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `title_en` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `excerpt_en` text,
  `detail` text,
  `detail_en` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  `meta_title` text,
  `meta_description` text,
  `meta_keyword` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `conditions`
--

INSERT INTO `conditions` (`condition_id`, `slug`, `lang`, `title`, `title_en`, `excerpt`, `excerpt_en`, `detail`, `detail_en`, `active`, `order`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(1, '', NULL, 'เงื่อนไขการใช้บริการ', 'Terms of service', '<p>1. ทางลูกค้าตกลงเป็นผู้รับผิดชอบต่อความเสียหายที่เกิดขึ้น กรณีความเสียหายของสินค้าเกิดมาจากร้านค้าจีน เช่น สินค้าไม่ครบจำนวน ผิดสี ผิดแบบ การจัดส่งล่าช้า สินค้าไม่ได้จัดส่งจากร้าน</p><p>  สินค้าที่ได้รับไม่เหมือนที่แสดงที่หน้าเว็บไซด์ เป็นต้น ทั้งนี้บริษัทฯเป็นเพียงตัวกลางในการช่วยสั่งซื้อสินค้า และขนส่งสินค้าจากจีนให้กับลูกค้า โดยทางบริษัทฯยินดีที่จะช่วยติดตาม ประสานงานกับ</p><p>  ร้านค้าให้อย่างเต็มที่กรณีที่เกิดปัญหาดังกล่าว</p><p>2. รูปภาพ หรือข้อมูลของสินค้าที่แสดงบน www.ifcexpressshipping.com เป็นข้อมูลที่เชื่อมโยงมาจากฐานข้อมูลจากร้านค้าจีนโดยตรง หากมีความผิดพลาดของข้อมูลจะเป็นความผิดพลาดที่</p><p>  เกิดจากทางร้านค้าจีน เช่น กรณีสินค้ามาไม่ตรงตามที่แสดงไว้</p><p>3. กรณีลูกค้าชำระค่าสินค้าในการสั่งซื้อแล้วจะไม่สามารถยกเลิกคำสั่งซื้อได้</p>', '<p>1. The customer agrees to be responsible for any damage incurred. In case of product damage caused by Chinese stores such as incomplete items, wrong color, wrong delivery delay The product is not delivered from the store, the received product is not the same as the one shown on the website, etc. The company is just an intermediary to help order products. And transport the goods from China for customers The company is happy to help follow. To fully coordinate with the store in case of such a problem</p><p>2. Pictures or information of the products displayed on www.ifcexpressshipping.com It is linked from a database from a Chinese shop directly. If there is an error in the information, it will be the error caused by the Chinese store, such as the case that the product does not come as shown.</p><p>3. In case the customer has paid for the products in the order, the order cannot be canceled.</p>', NULL, NULL, 1, 0, '2020-06-30 16:37:04', '1', '2020-08-29 18:29:32', '1', 0, NULL, NULL, NULL, NULL, NULL),
(2, '', NULL, 'เงื่อนไขการคิดค่าขนส่ง และค่าใช้จ่ายอื่น', 'Conditions of the freight charge And other expenses', '<p>1. ลูกค้ายอมรับหลักเกณฑ์ในการคิดค่าขนส่งจากโกดังจีนมาโกดังไทย ว่าจะใช้นน้ำหนักที่มากกว่าในการคิดค่าขนส่ง ที่ได้จากการการเปรียบเทียบระหว่างน้ำหนักจริงที่ชั่งได้กับน้ำหนักเชิงปริมาตร </p><p>  และตกลงที่จะชำระค่าขนส่งตามหลักเกณฑ์นี้</p><p>2. การส่งสินค้าจากโกดังไทยถึงมือลูกค้าทางบริษัทฯจะดำเนินการส่งต่อให้กับผู้บริการขนส่งเอกชนตามที่ลูกค้าเลือกเป็นผู้ดำเนินการ โดยจะคิดค่าขนส่งตามอัตราจริงตามแต่ผู้บริการขนส่งเอกชน</p><p>  กำหนด บวกค่าบริการดำเนินการในการส่งสินค้าต่อไปยังผู้บริการขนส่งเอกชน โดยลูกค้าตกลงที่จะชำระค่าดำเนินการพัสดุตามอัตราที่ทางบริษัทฯกำหนด</p><p>3. กรณีมีค่าใช้จ่ายในการขนส่งสินค้าจากร้านค้าจีนมาโกดังจีน เช่น ค่าไปรษณีย์ ค่าขนส่งระหว่างเมือง ค่าใช้จ่ายที่ทางผู้ขนส่งขนส่งในจีนเรียกเก็บ หรือค่าใช้จ่ายอื่นใดที่ทางผู้ขนส่งได้จ่ายไปเพื่อ</p><p>  ให้ได้รับสินค้าของลูกค้าจากทางร้านค้า ทางลูกค้าตกลงชำระค่าใช้จ่ายนั้นให้แก่ผู้ขนส่งตามอัตราที่ทางผู้ขนส่งได้จ่ายไป</p><p>4. กรณีเกิดค่าใช้จ่ายใด ๆ จากหีบห่อที่ใช้บรรจุไม่แข็งแรงมั่นคงเพียงพอ หรือไม่เหมาะสมกับสินค้าและการขนส่ง ซึ่งบรรจุโดยผู้ส่งสินค้าต้นทาง ทำให้ทางผู้ขนส่งต้องทำการบรรจุใหม่ หรือเสริม</p><p>  ให้แข็งแรง  ทางลูกค้าตกลงชำระค่าใช้จ่ายนั้นตามอัตราที่ผู้ขนส่งกำหนด ทั้งนี้กรณีที่มีการบรรจุใหม่หรือเสริมให้แข็งแรงแล้ว ความรับผิดชอบของผู้ขนส่งจะยังอยู่ในเงื่อนไขกรณีสินค้าสูญหาย</p><p>  หรือเสียหายตามปกติ</p><p>5. ผู้รับสินค้าจะต้องชำระเงินค่าขนส่ง และไม่มียอดค้างชำระค่าขนส่ง หรือค่าบริการอื่นๆ ก่อนรับสินค้า</p><p>6. เมื่อผู้ขนส่งได้แจ้งให้ลูกค้าทราบ ลูกค้าจะต้องมารับสินค้า หรือนัดรับสินค้าภายใน 7 วันนับจากวันที่ได้รับแจ้ง หากเกินกำหนดผู้รับยินยอมเสียค่าฝากสินค้าในราคา 30 บาท/วัน/ชิ้น</p><p>7. กรณีไม่มีผู้มารับสินค้าเกินกว่า 90 วันนับแต่วันที่ผู้ขนส่งได้แจ้งให้ลูกค้าทราบ ลูกค้ายินยอมให้ผู้ขนส่งนำสินค้าขายทอดตลาดและนำเงินที่ได้หักชำระค่าบริการรับฝากสินค้า และค่าใช้จ่ายอื่นๆ</p><p>  เกี่ยวกับการจัดการสินค้านั้น</p><p>8. ราคาค่าขนส่งที่แจ้งอาจมีการเปลี่ยนแปลงได้ ขึ้นอยู่กับรายการส่งเสริมการขาย และประกาศที่บริษัทฯกำหนด</p>', '<p>1. The customer accepts the criteria for calculating shipping costs from a Chinese warehouse to a Thai warehouse. Whether it will take more weight to calculate the shipping cost Obtained by comparing the actual weight to the scale and the volumetric weight. And agree to pay for the freight in accordance with this rule</p><p>2. Delivery from the Thai warehouse to the customer, the company will proceed forwarding to the private transport service provider as the customer chooses as the operator. The freight will be charged according to the actual rate as specified by the private carrier. Plus a service charge for forwarding products to a private carrier The customer agrees to pay the parcel handling fee at the rate specified by the company.</p><p>3. In the case of shipping costs from Chinese stores to Chinese warehouses, such as postage, intercity shipping Costs levied by shippers in China Or any other expenses paid by the carrier in order to receive the products of the customer from the store The customer agrees to pay such expenses to the carrier at the rate paid by the carrier.</p><p>4.In case of any expenses incurred from packaging which is not strong and stable enough Or unsuitable for goods and transportation Which is packed by the original shipper Causing the carrier to repack Or strengthen The customer agrees to pay such expenses at the rates specified by the carrier. In the event that it is new or strengthened The responsibility of the carrier remains in the event of loss of goods. Or damaged normally</p><p>5. The consignee must pay the freight. And there is no outstanding balance payment for shipping Or other service charges Before receiving the goods</p><p>6. When the carrier has notified the customer Customers have to come to pick up the product. Or arrange to receive the product within 7 days from the date notified If it exceeds the limit, the recipient agrees to pay the deposit at the price of 30 baht / day / piece.</p><p>7. In the case that no one comes to receive the product more than 90 days from the date the carrier notifies the customer The customer agrees to allow the carrier to sell the product by auction and to pay the deposit for the consignment service. And other expenses related to product handling</p><p>8. The shipping price quoted is subject to change. Depending on the promotion And announcements set by the company</p>', NULL, NULL, 1, 1, '2020-06-30 16:38:04', '1', '2020-08-29 18:28:49', '1', 0, NULL, NULL, NULL, NULL, NULL),
(3, '', NULL, 'เงื่อนไขในการจัดส่ง', 'Conditions of delivery', '<p>1. ระยะเวลาการจัดส่งเป็นระยะเวลาโดยเฉลี่ย อาจเร็วหรือช้ากว่าที่แจ้ง ซึ่งทางผู้ขนส่งไม่มีการรับประกันถึงความเสียหายต่อความล่าช้าของสินค้า</p><p>2. สินค้าที่ถึงไทยแล้วจะเข้าสู่กระบวนการจัดส่งภายในประเทศ ก็ต่อเมื่อทางผู้ขนส่งได้รับหลักฐานการชำระเงินค่าจัดส่ง ค่าสินค้า หรือค่าใช้จ่ายอื่นๆ ในรอบนั้นๆเรียบร้อยแล้ว โดยจะใช้ระยะเวลา</p><p>  ประมาณ 1-3 วัน</p><p>3. ผู้ขนส่งมีสิทธิที่จะยึดหน่วงสินค้าที่ขนส่งไว้จนกว่าจะได้รับชำระค่าบริการ และค่าใช้จ่ายอื่น ๆ จนครบถ้วน</p><p>4. ผู้ขนส่งไม่รับขนส่งสินค้าที่ผิดกฎหมายทุกชนิด หากสินค้าส่งเข้ามาแล้วถูกอายัด ทางผู้ขนส่งจะไม่รับผิดชอบในทุกกรณี หรือหากมีความเสียหายเกิดขึ้น ทางลูกค้าจะต้องรับผิดทั้งทางแพ่งและ</p><p>  อาญา แต่เพียงฝ่ายเดียว</p><p>5. ในบางกรณีผู้ขนส่งอาจจำเป็นต้องใช้สิทธิในการตรวจสอบบรรจุภัณฑ์ของผู้ส่งไม่ว่าจะก่อนหรือหลังจากผู้ส่งได้ทำการส่ง โดยลูกค้ายินยอมด้วยในการดังกล่าว</p><p>6. ผู้ขนส่งสงวนสิทธิในการปฏิเสธการขนส่งหากพบว่า ของที่จะขนส่งนั้นอาจเป็นสาเหตุให้ของอื่น หรืออุปกรณ์ใด ๆ เกิดความเสียหาย หรือเป็นอันตรายต่อบุคคล หรือทรัพย์ของผู้อื่น หรืออาจ</p><p>  เป็นอันตรายแก่ผู้ขนส่ง หรือทำให้เกิดความล่าช้าในการขนส่ง หรือเป็นของต้องห้าม หรือผิดกฎหมาย หรือเป็นการฝ่าฝืนต่อข้อกำหนดและเงื่อนไขในการขนส่งโดยประการอื่น</p>', '<p>1. The delivery time is an average time. It may be faster or slower than notified. The carrier has no guarantee of damage to product delays.</p><p>2. Products that arrive in Thailand will go through the domestic delivery process. Only when the carrier has received proof of payment for shipping, product or other expenses. In that round already It takes about 1-3 days.</p><p>3.The carrier has the right to withhold the carriage until payment has been received. And other expenses until complete</p><p>4. The carrier does not accept the transport of illegal goods of any kind. If the product has been delivered and was seized The carrier is not responsible for all cases. Or if damage occurs The customer will be liable for both civil and criminal. Unilaterally</p><p>5. In some cases, the carrier may be required to exercise the right to inspect the sender\'s packaging, either before or after the sender has made it. In which the customer agrees to do so</p><p>6.The carrier reserves the right to refuse a carriage if it is found that The goods that will be transported may cause other things. Or any device damage Or harmful to persons Or the property of others Or it may be dangerous for the carrier Or cause transportation delays Or is it prohibited Or illegal Or is a violation of the terms and conditions of carriage in any other way</p>', NULL, NULL, 1, 2, '2020-06-30 16:38:50', '1', '2020-08-29 18:30:22', '1', 0, NULL, NULL, NULL, NULL, NULL),
(4, '', NULL, 'เงื่อนไขกรณีสินค้าสูญหายหรือเสียหาย', 'Conditions in the event of loss or damage', '<p>1. หากมีสินค้าเสียหายหรือสูญหายจากการจัดส่งสินค้าที่เกิดขึ้นจากความผิดของทางผู้ขนส่ง ผู้ขนส่งจะรับผิดชอบความเสียหายตามราคาซื้อจริงของสินค้า แต่ทั้งหมดต้องไม่เกินกว่าค่าขนส่ง </p><p>  ของสินค้าที่เสียหายหรือสูญหายชิ้นนั้นๆ เช่น มูลค่าสินค้าที่เสียหายหรือสูญหายเป็นจำนวนเงิน 1000 บาท แต่ค่าขนส่งจากโกดังจีนมาโกดังไทยของสินค้าชิ้นนั้น เป็นจำนวนเงิน 500 บาท ทาง</p><p>  ผู้ขนส่งจะรับผิดชอบเป็นจำนวนเงินทั้งหมดไม่เกิน 500 บาท แต่หากมูลค่าสินค้าของที่สูญหายไม่ถึง 500 บาท ทางผู้ขนส่งก็จะรับผิดชอบตามตามราคาซื้อจริงของสินค้า</p><p>2. การแจ้งความเสียหาย หรือสูญหาย จะต้องแจ้งเป็นลายลักษณ์อักษรแก่ผู้ขนส่ง โดยแจ้งให้กับทางผู้ขนส่งทราบภายในวันที่ได้รับสินค้า พร้อมส่งภาพถ่ายหีบห่อของสินค้ากรณีอยู่ในสภาพไม่</p><p>  สมบูรณ์ หรือภาพถ่ายของสินค้ากรณีเสียหาย และแจ้งเลขที่เอกสารการจัดส่ง หรือเอกสารการสั่งซื้อ แก่ผู้ขนส่ง</p><p>3. ผู้ขนส่งจะไม่รับผิดชอบต่อการสูญหายหรือเสียหาย หรือค่าใช้จ่ายที่เกิดขึ้น ในกรณีดังต่อไปนี้</p><p><span xss=removed>  </span>- พบว่า สินค้าเป็นของต้องห้าม ผิดกฎหมาย หรือผิดข้อบังคับของกรมศุลกากร และหน่วยงานราชการต่างๆ</p><p><span xss=removed>  </span>- มีสาเหตุจากหีบห่อที่ใช้บรรจุไม่แข็งแรงมั่นคงเพียงพอ หรือไม่เหมาะสมกับสินค้าและการขนส่ง ซึ่งบรรจุโดยผู้ส่งต้นทาง </p><p><span xss=removed>  </span>- สินค้าที่จัดส่งเป็นสินค้าดังต่อไปนี้  เอกสารที่มีมูลค่า สิ่งของแตกหักเสียหายง่าย เครื่องแก้ว กระจก เซรามิค พลาสติก หินอ่อน กระเบื้อง สินค้าประเภทเฟอร์นิเจอร์ งานไม้ เช่น เกิด</p><p>  <span xss=removed>  </span>  รอยบุบ รอยขีดข่วน เครื่องเพชร เครื่องประดับ (ทอง เงิน พลอย) สิ่งของผิดกฎหมาย วัตถุอันตราย งานศิลปะ สินค้าวัตถุโบราณ สิ่งมีชีวิต ของสด ประเภทอาหารและเครื่องดื่มทุก</p><p><span xss=removed>  </span>  ชนิด สินค้าที่เสื่อมสภาพได้โดยตัวสินค้าเอง เช่น การระเหย ละลาย ขึ้นสนิม สีซีด สีตก ขึ้นรา</p><p><span xss=removed>  </span>- มีสาเหตุจากความล่าช้าในการจัดส่ง</p><p><span xss=removed>  </span>- เสียหายเฉพาะบรรจุภัณฑ์ด้านนอกไม่ได้ถึงตัวสินค้า</p>', '<p>1.If there is any damage or loss from delivery caused by the fault of the carrier The carrier will be responsible for the damage according to the actual purchase price of the item. But the total must not exceed the freight.</p><p>  For example, the value of the damaged or lost product is 1000 baht, but the shipping cost from the Chinese warehouse to the Thai warehouse of that item Is an amount of 500 baht.</p><p>  The carrier will be responsible for the total amount not exceeding 500 baht, but if the value of the lost goods is less than 500 baht, the carrier will be responsible according to the actual purchase price of the product.</p><p>2. The notice of damage or loss must be reported in writing to the carrier. By informing the carrier within the day of receiving the product Ready to send photos of the package of the product in case it is in no condition</p><p>  Complete or damaged product photos And inform the shipping document number Or order documents to the carrier</p><p>3. The carrier is not responsible for any loss or damage. Or expenses incurred In the following cases</p><p>- Found that the product is prohibited, illegal or illegal of the Customs Department. And various government agencies</p><p>- Caused by the packaging which is not strong and stable enough Or unsuitable for goods and transportation Packed by the originating sender</p><p>- Products delivered are as follows Value document Items that are fragile and easily damaged, glassware, ceramics, plastics, marble, tiles, furniture, wood products such as birth</p><p>    Dents, scratches, jewelry, jewelry (gold, silver, precious stones), illegal items, dangerous goods, works of art, merchandise, antiques, living things, fresh goods, food and beverages of all kinds.</p><p>  Products that can deteriorate by the product itself, such as evaporation, melting, rust, color, mold</p><p>- caused by delivery delay</p><p>- Damaged only the outer packaging does not reach the product.</p>', NULL, NULL, 1, 3, '2020-06-30 16:39:52', '1', '2020-08-29 18:27:35', '1', 0, NULL, NULL, NULL, NULL, NULL),
(5, '', NULL, 'เงื่อนไขในการรับสินค้า', 'Conditions for receiving products', '<p>1. การติดต่อรับสินค้าที่ ”สำนักงาน” (สาขา) หรือ “คลังสินค้า” ผู้รับต้องแสดงบัตรประจำตัวประชาชน/ใบขับขี่ ซึ่งทางราชการออกให้ ทั้งนี้กรณีมอบอำนาจให้ผู้อื่นมารับสินค้าทาง ต้องเพิ่มเอกสาร </p><p>  ได้แก่ หนังสือมอบอำนาจ, บัตรประจำตัวประชาชนของผู้รับมอบ, กรณีนิติบุคคล หนังสือมอบอำนาจจะต้องมีอายุไม่เกิน 3 เดือน</p><p>2. ก่อนลงชื่อรับสินค้า ควรตรวจสอบสภาพภายนอกของกล่องสินค้า ว่าอยู่ในสภาพเรียบร้อยดีหรือไม่ทุกประการ ควรปฏิเสธการรับของหากกล่องบรรจุฉีกขาด หรืออยู่ในสภาพไม่เรียบร้อยหรือมี</p><p>  ร่องรอยการเปิดออกก่อน อย่างไรก็ตาม หากท่าประสงค์จะรับของไว้ ควรเปิดกล่องออกตรวจรับต่อหน้าผู้ขนส่งหากมีของสูญหายหรือเสียหาย ให้ระบุไว้ในการเซ็นรับของด้วย</p><p>3. ลูกค้าตกลงใช้บริการ และทำรายการสั่งซื้อสินค้ากับทางบริษัทฯ ให้ถือว่าลูกค้ายินยอมและยอมรับในข้อตกลงของทางบริษัทฯ ข้างต้นทุกประการ</p>', '<p>1. Contact to receive the product at \"office\" (branch) or \"warehouse\". The recipient must show ID card / driving license. Which the government issued for In the case of authorizing others to pick up the product via Must add documents such as power of attorney, identity card of the recipient, in the case of a juristic person The power of attorney must not be older than 3 months.</p><p>2. Before signing to receive the product Should check the external condition of the product box. That it is in good condition or not in all respects Refuse to pick up if the package is torn Or in a bad condition or have signs of opening first, however, if you intend to receive the item Should open the box and check in the presence of the carrier if there is any missing or damaged item. Please specify in the signature.</p><p>3. Customers agree to use the service. And make an order list with the company It is considered that the customer agrees and accepts the terms of the company. Above in all respects</p>', NULL, NULL, 1, 4, '2020-06-30 16:40:26', '1', '2020-08-29 18:31:03', '1', 0, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE `config` (
  `variable` varchar(50) NOT NULL DEFAULT '',
  `value` text,
  `lang` varchar(2) NOT NULL DEFAULT 'th',
  `description` varchar(250) DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `type` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`variable`, `value`, `lang`, `description`, `updateDate`, `type`) VALUES
('china_address', '&lt;p&gt;&lt;span style=&quot;color: rgb(63, 64, 71); text-align: right; font-size: 14px;&quot;&gt;โกดังจีน&amp;nbsp;&lt;/span&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;318/16-17 ซ.เจริญสุข ถนนพระราม 4 แขวงคลองเตย&lt;/span&gt;&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;เขตคลองเตย จังหวัดกรุงเทพมหานคร 10110 ประเทศไทย&lt;/span&gt;&lt;/p&gt;', 'th', NULL, '2020-09-02 16:16:57', 'setting_china_addres'),
('china_address_en', '&lt;p&gt;Chinese warehouse 318 / 16-17 Soi Charoen Suk, Rama 4 Road, Khlong Toei&lt;/p&gt;&lt;p&gt;Klongtoey District, Bangkok 10110, Thailand&lt;/p&gt;', 'th', NULL, '2020-09-02 16:16:57', 'setting_china_addres'),
('company_name', 'IFC Express &amp; Shipping Co.,Ltd', 'th', NULL, '2020-12-01 22:36:19', 'contactus'),
('detail', '&lt;p&gt;318/16-17 ซ.เจริญสุข ถนนพระราม 4 แขวงคลองเตย &lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;เขตคลองเตย จังหวัดกรุงเทพมหานคร  10110 ประเทศไทย&lt;/span&gt; &lt;/p&gt;', 'th', NULL, '2020-12-01 22:36:19', 'contactus'),
('detail_en', '&lt;p&gt;318 / 16-17 Soi Charoensuk, Rama 4 Road, Khlong Toei&lt;/p&gt;&lt;p&gt;Klongtoey District, Bangkok 10110, Thailand&lt;/p&gt;&lt;p&gt;&lt;br&gt;&lt;/p&gt;', 'th', NULL, '2020-12-01 22:36:19', 'contactus'),
('email', 'ifcexpressshipping@gmail.com', 'th', NULL, '2020-12-01 22:36:19', 'contactus'),
('facebook', 'IFC Express &amp; Shipping นำเข้าสินค้าจากจีน', 'th', NULL, '2020-12-01 22:36:19', 'contactus'),
('files', '', 'th', NULL, '2020-12-01 22:36:19', 'contactus'),
('files', 'N389410.jpg', 'th', NULL, '2020-09-02 16:16:57', 'setting_china_addres'),
('google_map', '&lt;iframe src=&quot;https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3259.140422700169!2d100.58438481771171!3d13.730979967890791!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcce032435237b693!2sIFC!5e0!3m2!1sth!2sth!4v1593532855525!5m2!1sth!2sth&quot; width=&quot;600&quot; height=&quot;450&quot; frameborder=&quot;0&quot; style=&quot;border:0;&quot; allowfullscreen=&quot;&quot; aria-hidden=&quot;false&quot; tabindex=&quot;0&quot;&gt;&lt;/iframe&gt;', 'th', NULL, '2020-12-01 22:36:19', 'contactus'),
('instagram', 'ifcexpressshipping@gmail.com', 'th', NULL, '2020-12-01 22:36:19', 'contactus'),
('line', 'IFC Express &amp; Shipping', 'th', NULL, '2020-12-01 22:36:19', 'contactus'),
('link_facebook', 'https://www.facebook.com/ifcexpressshipping', 'th', NULL, '2020-12-01 22:36:19', 'contactus'),
('link_instagram', 'https://www.facebook.com/ifcexpressshipping', 'th', NULL, '2020-12-01 22:36:19', 'contactus'),
('link_line', 'https://lin.ee/tcNHhwf', 'th', NULL, '2020-12-01 22:36:19', 'contactus'),
('mailAuthenticate', '1', 'th', NULL, '2020-07-17 23:28:24', 'mail'),
('mailDefault', 'comsci2535@gmail.com', 'th', NULL, '2020-07-17 23:28:24', 'mail'),
('mailMethod', '1', 'th', NULL, '2020-07-17 23:28:24', 'mail'),
('metaDescription', 'IFC Express ไอเอฟซีเอ็กซ์เพรส', 'th', NULL, '2020-11-04 21:14:11', 'general'),
('metaKeyword', 'IFC Express ไอเอฟซีเอ็กซ์เพรส', 'th', NULL, '2020-11-04 21:14:11', 'general'),
('money_china', '1', 'th', NULL, '2020-09-02 23:15:54', 'money_setting'),
('money_th', '4.66', 'th', NULL, '2020-09-02 23:15:54', 'money_setting'),
('phone', '0909895494', 'th', NULL, '2020-12-01 22:36:19', 'contactus'),
('senderEmail', 'comsci2535@gmail.com', 'th', NULL, '2020-07-17 23:28:24', 'mail'),
('senderName', 'IFC Shipping', 'th', NULL, '2020-07-17 23:28:24', 'mail'),
('siteTitle', 'ผู้ใช้บริหารได้รับทราบและยอมรับเงื่อนไขในการใช้บริการ www.ifcexpressshipping.com ตามที่ระบุไว้ดังนี้', 'th', NULL, '2021-03-25 22:22:21', 'conditions'),
('siteTitle', 'IFC Express ไอเอฟซีเอ็กซ์เพรส', 'th', NULL, '2020-11-04 21:14:11', 'general'),
('siteTitle_en', 'Management users acknowledge and accept the terms of service. www.ifcexpressshipping.com As stated as follows', 'th', NULL, '2021-03-25 22:22:21', 'conditions'),
('SMTPpassword', '5411425016lee', 'th', NULL, '2020-07-17 23:28:24', 'mail'),
('SMTPport', '465', 'th', NULL, '2020-07-17 23:28:24', 'mail'),
('SMTPserver', 'smtp.gmail.com', 'th', NULL, '2020-07-17 23:28:24', 'mail'),
('SMTPusername', 'comsci2535@gmail.com', 'th', NULL, '2020-07-17 23:28:24', 'mail'),
('time', '', 'th', NULL, '2020-12-01 22:36:19', 'contactus');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `contact_id` int(11) NOT NULL,
  `fname` varchar(250) DEFAULT '',
  `lname` varchar(250) DEFAULT '',
  `email` varchar(150) DEFAULT '',
  `phone` varchar(25) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `detail` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`contact_id`, `fname`, `lname`, `email`, `phone`, `title`, `detail`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`) VALUES
(1, 'สมชาย', 'ใจดี', 'test@hotmail.com', '0865038800', 'ยังไม่ได้รับสินค้า', 'ยังไม่ได้รับสินค้า', 0, '2020-01-29 20:55:24', '0', '2020-06-30 23:05:23', '1', 0, '2020-06-30 23:05:15', 1);

-- --------------------------------------------------------

--
-- Table structure for table `delivery_companys`
--

CREATE TABLE `delivery_companys` (
  `delivery_company_id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `title_en` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `excerpt_en` text,
  `detail` text,
  `detail_en` text,
  `file` text,
  `is_send` int(1) DEFAULT '0',
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  `meta_title` text,
  `meta_description` text,
  `meta_keyword` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `delivery_companys`
--

INSERT INTO `delivery_companys` (`delivery_company_id`, `slug`, `lang`, `title`, `title_en`, `excerpt`, `excerpt_en`, `detail`, `detail_en`, `file`, `is_send`, `active`, `order`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(1, '', NULL, 'Kerry', NULL, 'Kerry', NULL, NULL, NULL, 'uploads/delivery_companys/2020/07/ba678322e00fa6f9fb3cbf624a4a06f8.JPG', 1, 1, 2, '2020-07-01 14:46:36', '1', '2020-07-02 22:55:20', '1', 0, NULL, NULL, NULL, NULL, NULL),
(2, '', NULL, 'Ems', 'Ems en', 'Ems', 'Ems en', NULL, NULL, 'uploads/delivery_companys/2020/07/1de04bd1311be9425549eae8c0db6113.JPG', 1, 1, 0, '2020-07-01 14:48:50', '1', '2020-07-12 15:29:24', '1', 0, NULL, NULL, NULL, NULL, NULL),
(3, '', NULL, 'J&T', NULL, 'J&T', NULL, NULL, NULL, 'uploads/delivery_companys/2020/07/c139deb1729589ce93cec3d10da3abf5.JPG', 1, 1, 3, '2020-07-01 14:49:19', '1', '2020-07-02 22:55:03', '1', 0, NULL, NULL, NULL, NULL, NULL),
(4, '', NULL, 'Flash', NULL, 'Flash', NULL, NULL, NULL, 'uploads/delivery_companys/2020/07/57cedacd2ff091724161f4af1667a4b1.JPG', 1, 1, 1, '2020-07-01 14:49:41', '1', '2020-07-02 22:54:57', '1', 0, NULL, NULL, NULL, NULL, NULL),
(5, '', NULL, 'รับของด้วยตัวเอง', NULL, 'รับของด้วยตัวเอง', NULL, NULL, NULL, 'uploads/delivery_companys/2020/07/36985c482ff1a5f86a7dcc4a0cd82084.jpg', 2, 1, 4, '2020-07-02 22:52:53', '1', '2020-07-02 22:52:59', '1', 0, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `delivery_settings`
--

CREATE TABLE `delivery_settings` (
  `delivery_setting_id` int(11) NOT NULL,
  `delivery_company_id` int(11) NOT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `weight_min` varchar(100) DEFAULT NULL,
  `weight_max` varchar(100) DEFAULT NULL,
  `price_in` double(100,0) DEFAULT '0',
  `price_out` double(100,0) DEFAULT '0',
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `delivery_settings`
--

INSERT INTO `delivery_settings` (`delivery_setting_id`, `delivery_company_id`, `lang`, `title`, `weight_min`, `weight_max`, `price_in`, `price_out`, `active`, `order`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`) VALUES
(1, 1, NULL, 'Envelop / Mini', '0', '1', 35, 45, 1, 0, '2020-07-02 16:40:52', '1', '2020-07-02 22:37:21', '1', 0, NULL, NULL),
(2, 1, NULL, 'S', '2', '5', 65, 80, 1, 0, '2020-07-02 16:51:38', '1', '2020-07-02 22:38:40', '1', 0, NULL, NULL),
(3, 1, NULL, 'M', '5', '10', 85, 115, 1, 0, '2020-07-02 16:52:33', '1', '2020-07-02 22:38:16', '1', 0, NULL, NULL),
(4, 1, NULL, 'L', '11', '15', 185, 205, 1, 0, '2020-07-02 16:53:38', '1', '2020-07-02 22:38:05', '1', 0, NULL, NULL),
(5, 1, NULL, 'XL', '16', '20', 290, 330, 1, 0, '2020-07-02 16:54:06', '1', '2020-08-26 23:56:02', '1', 0, NULL, NULL),
(6, 1, NULL, 'Over size', '21', '1000', 380, 420, 1, 0, '2020-07-02 16:54:53', '1', '2020-08-26 23:56:16', '1', 0, NULL, NULL),
(7, 1, NULL, 'Envelop / Mini', '0', '1', 45, NULL, 0, 0, '2020-07-02 16:55:51', '1', '2020-07-02 22:39:11', '1', 2, '2020-07-02 22:37:58', 1),
(8, 0, NULL, 'L', '11', '15', 205, NULL, 0, 0, '2020-07-02 16:56:29', '1', '2020-07-02 22:39:11', '1', 2, '2020-07-02 22:37:58', 1),
(9, 1, NULL, 'M', '5', '10', 115, NULL, 0, 0, '2020-07-02 16:57:19', '1', '2020-07-02 22:39:11', '1', 2, '2020-07-02 22:37:58', 1),
(10, 1, NULL, 'Over size', '21', '25', 420, NULL, 0, 0, '2020-07-02 16:58:21', '1', '2020-07-02 22:39:11', '1', 2, '2020-07-02 22:37:58', 1),
(11, 1, NULL, 'S', '2', '5', 80, NULL, 0, 0, '2020-07-02 16:58:41', '1', '2020-07-02 22:39:11', '1', 2, '2020-07-02 22:37:58', 1),
(12, 1, NULL, 'XL', '16', '20', 330, NULL, 0, 0, '2020-07-02 16:59:21', '1', '2020-07-02 22:39:11', '1', 2, '2020-07-02 22:37:58', 1),
(13, 2, NULL, 'S', '0', '10', 20, 50, 1, 0, '2020-08-09 15:58:17', '1', '2020-08-09 15:58:20', '1', 0, NULL, NULL),
(14, 2, NULL, 'M', '11', '1000', 30, 60, 1, 0, '2020-08-09 15:58:57', '1', '2020-08-26 23:51:18', '1', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `info`
--

CREATE TABLE `info` (
  `info_id` int(11) NOT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `title_en` varchar(255) NOT NULL,
  `slug` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `detail` text,
  `tax_id` int(20) NOT NULL,
  `tel` varchar(12) NOT NULL,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `file` text NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `info`
--

INSERT INTO `info` (`info_id`, `lang`, `title`, `title_en`, `slug`, `excerpt`, `detail`, `tax_id`, `tel`, `active`, `order`, `file`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`) VALUES
(1, NULL, 'บริษัท ไอเอฟซี เอ็กซ์เพรส แอนด์ ชิปปิ้ง จำกัด', 'IFC Express & Shipping Co.,Ltd', 'บริษัท-ไอเอฟซี-เอ็กซ์เพรส-แอนด์-ชิปปิ้ง-จำกัด', '318/16-17 ซ.เจริญสุข ถนนพระราม 4 แขวงคลองเตย เขตคลองเตย จังหวัดกรุงเทพมหานคร 10110 ประเทศไทย', '', 2147483647, '0909895494', 0, 0, 'uploads/logo/2020/06/7df49385d1091283b4c8d42caf3c8db7.jpg', '2019-03-11 17:32:42', '', '2021-02-21 21:15:19', '1', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `map_products_color`
--

CREATE TABLE `map_products_color` (
  `product_id` int(11) DEFAULT NULL,
  `title` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `map_products_color`
--

INSERT INTO `map_products_color` (`product_id`, `title`) VALUES
(1, 'สีฟ้า'),
(1, 'สีแดง'),
(2, 'สีชมพู่'),
(2, 'สีฟ้า');

-- --------------------------------------------------------

--
-- Table structure for table `map_products_size`
--

CREATE TABLE `map_products_size` (
  `product_id` int(11) DEFAULT NULL,
  `title` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `map_products_size`
--

INSERT INTO `map_products_size` (`product_id`, `title`) VALUES
(1, 'L'),
(1, 'M');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id` int(11) NOT NULL,
  `member_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `full_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `house_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `building` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `road` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `district` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `amphoe` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `province` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zipcode` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `invoice_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `active` int(11) DEFAULT '0',
  `oauth_provider` enum('','facebook','google','twitter') COLLATE utf8_unicode_ci DEFAULT NULL,
  `oauth_uid` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `oauth_picture` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `recycle` varchar(255) COLLATE utf8_unicode_ci DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id`, `member_id`, `username`, `password`, `full_name`, `picture`, `phone`, `email`, `house_number`, `building`, `road`, `district`, `amphoe`, `province`, `zipcode`, `invoice_no`, `created`, `modified`, `active`, `oauth_provider`, `oauth_uid`, `oauth_picture`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`) VALUES
(1, 'RL-00016', 'ruslee', '$2y$10$u5/RGEwVN..Rmm/h2k.Mheb7SCaXnKEHl04L1WsXKta9/7eO5aPN6', 'ruslee seebu', 'uploads/users/2020/08/9a64c19a40f3c71f7034727b520a9bfb.png', '0807055971', 'comsci2535@gmail.com', '13/2', '-', '-', 'คลองต้นไทร', 'คลองสาน', 'กรุงเทพมหานคร', '10600', '231231231234', '2020-08-10 10:12:37', '2021-03-01 15:29:34', 1, NULL, NULL, NULL, '2020-08-21 16:45:02', NULL, '2021-03-01 15:29:34', 1, '0', NULL, NULL),
(3, 'RL-00017', 'ruslee1', '$2y$10$DJ9GK73.d32EPgMPJ2/xEeHyLbdtV0zvZ3Aasb/4QKDl9Vkp6OjWe', 'Ruslee Seebu1', NULL, '0909895494', 'comsci12535@gmail.com', '12', '-', '-', 'ยาบี', 'หนองจิก', 'ปัตตานี', '94170', '231233453445', '2020-08-21 17:55:13', '0000-00-00 00:00:00', 0, NULL, NULL, NULL, '2020-08-21 21:39:29', NULL, '2020-09-29 21:38:35', 1, '1', '2020-09-29 21:38:35', 1),
(4, 'RL-00017', 'ruslee1', '$2y$10$LmaphUPmFxFdZzG.T8Eo8.8OU1P8X3uA1vsKLSNrkF48idxT2d2kG', 'Ruslee Seebu', NULL, '0807055971', 'comsci12535@gmail.com', '12', '-', '-', 'ยาบี', 'หนองจิก', 'ปัตตานี', '94170', '345345345353', '2020-09-29 21:44:06', '2020-09-29 21:44:19', 1, NULL, NULL, NULL, '2020-09-29 21:44:06', NULL, '2020-09-29 22:20:43', 1, '0', NULL, NULL),
(5, NULL, 'Ruslee', NULL, 'Ruslee Comsci', NULL, NULL, 'lee.yam_1214@hotmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'facebook', '4963689493704379', 'https://platform-lookaside.fbsbx.com/platform/profilepic/?asid=4963689493704379&height=200&width=200&ext=1617357105&hash=AeRYAZRygirxGl8_oIU', '2021-02-25 22:09:30', NULL, '2021-03-03 16:51:45', NULL, '0', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `member_cart_history`
--

CREATE TABLE `member_cart_history` (
  `id` int(11) NOT NULL,
  `cart_json` json NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `member_cart_history`
--

INSERT INTO `member_cart_history` (`id`, `cart_json`, `created_at`) VALUES
(1, '[]', '2021-03-01 22:42:17');

-- --------------------------------------------------------

--
-- Table structure for table `module`
--

CREATE TABLE `module` (
  `moduleId` int(11) NOT NULL,
  `parentId` smallint(6) DEFAULT NULL,
  `type` int(11) DEFAULT '1' COMMENT '1menu, 2separate',
  `active` tinyint(1) DEFAULT '0' COMMENT '0Disabled, 1Enabled',
  `title` varchar(150) DEFAULT NULL,
  `directory` varchar(200) DEFAULT NULL,
  `class` varchar(50) DEFAULT NULL,
  `order` int(10) DEFAULT '0',
  `method` varchar(50) DEFAULT NULL,
  `param` varchar(150) DEFAULT NULL,
  `isSidebar` int(11) DEFAULT '1' COMMENT '0:hide,1show',
  `isDev` tinyint(1) DEFAULT '0',
  `icon` varchar(50) DEFAULT 'fa fa-angle-double-right',
  `createBy` int(11) DEFAULT '0',
  `updateBy` int(11) DEFAULT '0',
  `createDate` datetime DEFAULT NULL,
  `updateDate` datetime DEFAULT NULL,
  `modify` int(11) DEFAULT '0',
  `view` int(11) DEFAULT '0',
  `print` int(11) DEFAULT '0',
  `import` int(11) DEFAULT '0',
  `export` int(11) DEFAULT '0',
  `descript` text,
  `recycle` int(11) DEFAULT '0',
  `recycleDate` datetime DEFAULT NULL,
  `recycleBy` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `module`
--

INSERT INTO `module` (`moduleId`, `parentId`, `type`, `active`, `title`, `directory`, `class`, `order`, `method`, `param`, `isSidebar`, `isDev`, `icon`, `createBy`, `updateBy`, `createDate`, `updateDate`, `modify`, `view`, `print`, `import`, `export`, `descript`, `recycle`, `recycleDate`, `recycleBy`) VALUES
(1, 0, 1, 1, 'แผงควบคุม', 'admin', 'dashboard', 1, '', NULL, 1, 0, 'flaticon-line-graph', 0, 3, '2016-07-23 08:54:42', '2019-02-04 16:46:17', 0, 1, 0, 0, 0, '', 0, NULL, NULL),
(2, 0, 1, 1, 'ตั้งค่า', 'admin', '', 17, '', '', 1, 0, 'fa fa-cogs', 0, 7, '2016-07-23 08:54:42', '2016-12-10 10:37:56', 0, 1, 0, 0, 0, '', 0, NULL, NULL),
(3, 2, 1, 1, 'ผู้ใช้งาน', 'admin', 'users', 4, '', '', 1, 0, 'fa fa-user', 0, 1, '2016-07-23 08:54:42', '2019-03-25 22:00:37', 1, 1, 0, 0, 0, '', 0, NULL, NULL),
(4, 2, 1, 1, 'พื้นฐาน', 'admin', 'config_general', 6, NULL, '', 1, 0, 'fa fa-circle-o', 0, 1, '2016-07-23 08:54:42', '2019-03-02 15:28:20', 1, 1, 0, 0, 0, '', 0, NULL, NULL),
(5, 2, 1, 1, 'อีเมล์', 'admin', 'config_mail', 7, '', '', 1, 0, 'fa fa-circle-o', 0, 1, '2016-07-23 08:54:42', '2019-03-02 15:28:21', 1, 1, 0, 0, 0, '', 0, NULL, NULL),
(6, 2, 1, 1, 'กลุ่มผู้ใช้งาน', 'admin', 'roles', 5, '', '', 1, 0, 'fa fa-users', 0, NULL, '2016-07-23 08:54:42', '2019-02-06 22:47:00', 1, 1, 0, 0, 0, '', 0, NULL, NULL),
(7, 0, 1, 1, 'โมดูล', 'admin', 'module', 19, '', '', 1, 1, 'fa fa-cog', 0, 7, '2016-07-23 08:54:42', '2016-07-16 13:04:45', 0, 0, 0, 0, 0, '', 0, NULL, NULL),
(8, 0, 1, 1, 'คลังไฟล์', 'admin', 'library', 18, '', '', 0, 0, 'fa fa-briefcase', 7, 1, '2016-07-23 08:54:42', '2018-03-23 14:48:52', 0, 0, 0, 0, 0, '', 0, NULL, NULL),
(10, 0, 1, 0, 'ประวัติการใช้งาน', 'admin', 'stat_admin', 14, '', '', 1, 0, 'fa fa-calendar-o', 7, 1, '2017-01-19 13:11:57', '2020-06-29 14:50:46', 0, 0, 0, 0, 0, '', 0, NULL, NULL),
(11, 0, 1, 0, 'Repo', 'admin', 'repo', 21, NULL, NULL, 1, 1, 'fa fa-circle-o', 1, 3, '2018-03-16 21:32:34', '2018-07-03 04:28:33', 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับrepo', 0, NULL, NULL),
(17, 2, 1, 1, 'เกี่ยวกับข้อมูลทั่วไป', '', 'info', 3, NULL, NULL, 1, 0, 'fa fa-angle-right', 1, 1, '2019-07-16 22:41:36', '2020-04-24 15:42:32', 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับเกี่ยวกับข้อมูลทั่วไป', 0, NULL, NULL),
(295, 2, 1, 1, 'ตั้งค่าแจ้งเตือนไลน์', '', 'setting_line', 1, NULL, NULL, 1, 0, 'fa fa-angle-right', 1, 0, '2019-12-18 16:14:52', NULL, 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับตั้งค่าแจ้งเตือนไลน์', 0, NULL, NULL),
(296, 0, 1, 1, 'บริการของเรา', '', 'services', 9, NULL, NULL, 1, 0, 'fa fa-handshake', 1, 1, '2020-06-29 15:01:59', '2020-06-30 23:24:08', 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับบริการของเรา', 0, NULL, NULL),
(297, 0, 1, 1, 'คำถามที่พบบ่อย', '', 'questions', 10, NULL, NULL, 1, 0, 'fa fa-tag', 1, 1, '2020-06-29 18:23:01', '2020-06-30 23:19:22', 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับคำถามที่พบบ่อย', 0, NULL, NULL),
(298, 0, 1, 1, 'วิธีการใช้งานเว็บ', '', 'uses', 11, NULL, NULL, 1, 0, 'fa fa-hand-pointer', 1, 1, '2020-06-29 23:14:45', '2020-06-30 23:25:35', 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับวิธีการใช้งานเว็บ', 0, NULL, NULL),
(299, 0, 1, 1, 'ข้อตกลงการใช้บริการ', '', 'conditions', 12, NULL, NULL, 1, 0, 'fa fa-check-square', 1, 1, '2020-06-30 16:29:26', '2020-06-30 23:21:23', 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับข้อตกลงการใช้บริการ', 0, NULL, NULL),
(300, 0, 1, 1, 'ข่าวสารและบทความ', '', 'news_articles', 13, NULL, NULL, 1, 0, 'fa fa-bullhorn', 1, 1, '2020-06-30 17:10:53', '2020-06-30 23:21:50', 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับข่าวสารและบทความ', 0, NULL, NULL),
(301, 0, 1, 1, 'เกี่ยวกับเรา', '', 'aboutus', 14, NULL, NULL, 1, 0, 'fa fa-building', 1, 1, '2020-06-30 21:38:45', '2020-06-30 23:22:18', 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับเกี่ยวกับเรา', 0, NULL, NULL),
(302, 304, 1, 1, 'ตั้งค่าติดต่อเรา', '', 'config_contactus', 1, NULL, NULL, 1, 0, 'fa fa-angle-right', 1, 0, '2020-06-30 22:43:16', NULL, 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับตั้งค่าติดต่อเรา', 0, NULL, NULL),
(303, 304, 1, 1, 'ข้อความติดต่อเรา', '', 'contactus', 2, NULL, NULL, 1, 0, 'fa fa-angle-right', 1, 0, '2020-06-30 22:44:28', NULL, 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับข้อความติดต่อเรา', 0, NULL, NULL),
(304, 0, 1, 1, 'ติดต่อเรา', '', '', 16, NULL, NULL, 1, 0, 'flaticon-envelope', 1, 0, '2020-06-30 23:03:29', NULL, 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับติดต่อเรา', 0, NULL, NULL),
(305, 0, 1, 1, 'จัดการข้อมูลตั้งต้น', '', '', 8, NULL, NULL, 1, 0, 'fa fa-cube', 1, 0, '2020-06-30 23:17:33', NULL, 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับจัดการข้อมูล', 0, NULL, NULL),
(306, 305, 1, 1, 'ตั้งค่าอัตราเงิน', '', 'money_setting', 5, NULL, NULL, 1, 0, 'fa fa-angle-right', 1, 1, '2020-06-30 23:35:34', '2020-06-30 23:42:00', 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับตั้งค่าอัตราเงิน', 0, NULL, NULL),
(307, 305, 1, 1, 'ตั้งค่าข้อมูลธนาคาร', '', 'banks', 6, NULL, NULL, 1, 0, 'fa fa-angle-right', 1, 0, '2020-06-30 23:59:55', NULL, 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับตั้งค่าข้อมูลธนาคาร', 0, NULL, NULL),
(308, 305, 1, 1, 'จัดการบริษัทขนส่ง', '', 'delivery_companys', 2, NULL, NULL, 1, 0, 'fa fa-angle-right', 1, 0, '2020-07-01 14:31:40', NULL, 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับจัดการบริษัทขนส่ง', 0, NULL, NULL),
(309, 305, 1, 1, 'ตั้งค่าราคาจัดส่งสินค้า', '', 'delivery_setting', 4, NULL, NULL, 0, 0, 'fa fa-angle-right', 1, 1, '2020-07-01 22:50:16', '2020-07-02 23:21:43', 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับตั้งค่าราคาจัดส่งสินค้า', 0, NULL, NULL),
(310, 322, 1, 1, 'ตั้งค่าประเภทช่องทางขนส่ง', '', 'type_transportations', 2, NULL, NULL, 1, 0, 'fa fa-angle-right', 1, 1, '2020-07-02 23:21:13', '2020-12-25 21:05:02', 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับตั้งค่าประเภทช่องทางขนส่ง', 0, NULL, NULL),
(311, 322, 1, 1, 'ตั้งค่าประเภทส่งสินค้าขนส่ง', '', 'type_transportations_setting', 3, NULL, NULL, 1, 0, 'fa fa-angle-right', 1, 1, '2020-07-02 23:38:24', '2020-12-25 21:03:57', 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับตั้งค่าประเภทส่งสินค้าขนส่ง', 0, NULL, NULL),
(312, 0, 1, 1, 'จัดการข้อมูลสินค้า', '', 'products', 3, NULL, NULL, 1, 0, 'fa fa-box', 1, 0, '2020-07-03 16:41:37', NULL, 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับจัดการข้อมูลสินค้า', 0, NULL, NULL),
(313, 0, 1, 1, 'จัดการข้อมูลสั่งซื้อสินค้า', '', 'orders', 2, NULL, NULL, 1, 0, 'fa fa-cart-plus', 1, 1, '2020-07-05 11:30:13', '2020-09-29 22:21:44', 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับจัดการข้อมูลสั่งซื้อสินค้า', 0, NULL, NULL),
(314, 2, 1, 1, 'จัดการสถานะ', '', 'status', 2, NULL, NULL, 1, 0, 'fa fa-angle-right', 1, 0, '2020-07-05 12:34:34', NULL, 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับจัดการสถานะ', 0, NULL, NULL),
(315, 305, 1, 1, 'ลักษณะแพ็คสินค้า', '', 'attribute_sends', 3, NULL, NULL, 1, 0, 'fa fa-angle-right', 1, 0, '2020-08-11 13:32:01', NULL, 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับลักษณะแพ็คสินค้า', 0, NULL, NULL),
(316, 305, 1, 1, 'ตั้งค่าข้อมูลที่อยู่โกดังจีน', '', 'china_address', 1, NULL, NULL, 1, 0, 'fa fa-angle-right', 1, 0, '2020-08-15 23:56:05', NULL, 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับตั้งค่าข้อมูลที่อยู่โกดังจีน', 0, NULL, NULL),
(317, 0, 1, 1, 'จัดการแยกรายสินค้า', '', 'separate_orders', 5, NULL, NULL, 0, 0, 'fa fa-angle-right', 1, 1, '2020-08-20 22:38:48', '2020-08-20 22:49:15', 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับจัดการแยกรายสินค้า', 0, NULL, NULL),
(318, 0, 1, 1, 'จัดการสมาชิก', '', 'members', 6, NULL, NULL, 1, 0, 'fa fa-users', 1, 1, '2020-08-21 16:33:52', '2020-08-21 16:34:50', 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับจัดการสมาชิก', 0, NULL, NULL),
(319, 322, 1, 1, 'อัตราค่าบริการขนส่ง', '', 'type_transportations_setting_list', 4, NULL, NULL, 1, 0, 'fa fa-angle-right', 1, 1, '2020-08-29 23:32:26', '2020-12-25 20:54:06', 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับอัตราค่าบริการขนส่ง', 0, NULL, NULL),
(320, 0, 1, 1, 'จัดการ Banner', '', 'banners', 15, NULL, NULL, 1, 0, 'fa fa-file-image', 1, 1, '2020-09-02 22:19:59', '2020-09-02 22:20:59', 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับจัดการ banner', 0, NULL, NULL),
(321, 0, 1, 1, 'จัดการประเภทสินค้า', '', 'categories', 4, NULL, NULL, 1, 0, 'fa fa-book', 1, 1, '2020-12-25 16:03:30', '2020-12-25 19:46:31', 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับจัดการประเภทสินค้า', 0, NULL, NULL),
(322, 0, 1, 1, 'จัดการค่าขนส่งเบื้องต้น', '', 'transportations', 7, NULL, NULL, 1, 0, 'fa fa-truck', 1, 1, '2020-12-25 20:22:48', '2020-12-25 20:23:50', 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับจัดการค่าขนส่งเบื้องต้น', 0, NULL, NULL),
(323, 322, 1, 1, 'ตั้งค่าประเภทลูกค้าขนส่ง', '', 'type_transportations_cust', 1, NULL, NULL, 1, 0, 'fa fa-angle-right', 1, 0, '2020-12-25 20:39:48', NULL, 0, 0, 0, 0, 0, 'การจัดการข้อมูลเกี่ยวกับตั้งค่าประเภทลูกค้าขนส่ง', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `news_articles`
--

CREATE TABLE `news_articles` (
  `news_article_id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `title_en` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `excerpt_en` text,
  `detail` text,
  `detail_en` text,
  `file` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  `meta_title` text,
  `meta_description` text,
  `meta_keyword` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `news_articles`
--

INSERT INTO `news_articles` (`news_article_id`, `slug`, `lang`, `title`, `title_en`, `excerpt`, `excerpt_en`, `detail`, `detail_en`, `file`, `active`, `order`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(1, 'รีวิวอาณาจักร-อาลีบาบา-อีคอมเมิร์ซสุดล้ำ-สะดวกสบายไปอีก', NULL, 'รีวิวอาณาจักร \'อาลีบาบา\' อีคอมเมิร์ซสุดล้ำ สะดวกสบายไปอีก', 'Review of the state-of-the-art \'Alibaba\' e-commerce empire More convenient', 'ชวนทำความรู้จัก อาลีบาบา กรุ๊ป ผู้นำด้านอีคอมเมิร์ซและเทคโนโลยีด้านไอทีสุดล้ำของประเทศจีน ที่ไฮเทคสุดๆ จนอาจจะทำให้คุณต้องอึ้ง ถ้าอยากรู้จักอาณาจักรอันยิ่งใหญ่แห่งนี้ของ  แจ็ค หม่า ให้มากขึ้นละก็ ต้องตามมาดู', 'Let\'s get to know Alibaba Group, China\'s most advanced e-commerce and IT technology leader, that is extremely high-tech. If you want to get to know more about Jack Ma\'s great empire, you must follow along.', '<p xss=removed>1. อาณาจักรอาลีบาบา เมืองหางโจว ประเทศจีน</p><p xss=removed>เริ่มต้นด้วยการไปเยี่ยมเยือนกันถึงที่บริษัทอาลีบาบา สำนักงานใหญ่ในเมืองหางโจว ประเทศจีน ออฟฟิศที่นี่ทันสมัยมาก สะดวกสบาย มีบริการจักรยานให้พนักงานปั่นไปทำงานระหว่างอาคารได้ด้วย ภาย</p><p xss=removed>ในบริเวณบริษัทก็มีทั้งร้านอาหาร ร้านเครื่องดื่ม คาเฟ่ และร้านขายของที่ระลึกที่เป็นร้านเอาไว้ทดลองระบบเทคโนโลยีด้านการใช้จ่ายต่างๆ ที่บริษัทพัฒนาขึ้น แถมยังมีพิพิธภัณฑ์อาลีบาบาให้หน่วยงาน</p><p xss=removed>ภายนอก เข้ามาศึกษาดูงานได้ตลอดทุกวัน สำหรับเมืองไทยเองก็มีทางคณะรัฐมนตรีกระทรวงพาณิชย์ ของไทยเคยเดินทางมาดูงาน และเยี่ยมเยือนพิพิธภัณฑ์แห่งนี้เช่นกัน</p><p xss=removed>2. Futuremart @Alibaba Xixi Campus</p><p xss=removed>อย่างที่บอกไปข้างต้นว่าภายในบริษัทของอาลีบาบา มีร้านค้าที่ทำขึ้นมาเพื่อทดลองระบบเทคโนโลยีต่างๆ ในการช็อปปิ้ง ใช้จ่ายซื้อสิ่งของ ร้านค้านั้นมีชื่อว่า Futuremart @Alibaba Xixi Campus เป็นต้น</p><p xss=removed>แบบของร้านค้าที่ใช้เทคโนโลยีดิจิทัลมาช่วยทำให้เกิด New Retail เพื่อเชื่อมโยงอุตสาหกรรมค้าปลีกกับโลกออนไลน์ </p>', '<p xss=\"removed\">1. Alibaba Empire, Hangzhou City, China</p><p xss=\"removed\">Beginning with a visit to Alibaba. Headquartered in Hangzhou, China, the office here is very modern and comfortable. There is a bicycle service for employees to cycle between the buildings as well.</p><p xss=\"removed\">There are also restaurants in the company area. Beverage shops, cafes, and souvenir shops that are stores to try out various spending technology systems. Developed by the company There is also an Alibaba Museum for the department.</p><p xss=\"removed\">Outside to study and see the work all day. For Thailand, there is also a cabinet of the Ministry of Commerce. Thai people have traveled to see the fair And visit this museum as well</p><p xss=\"removed\">2. Futuremart @lan Xixi Campus</p><p xss=\"removed\">As I said above that within the company of Alibaba. There is a store made to test different technology systems. In shopping Spend to buy things That store is called Futuremart @lan Xixi Campus.</p><p xss=\"removed\">A type of store that uses digital technology to help create New Retail to connect the retail industry with the online world.</p>', 'uploads/news_articles/2020/08/5aa27202e7ba63b073b7b37711570443.jpg', 1, 0, '2020-06-30 21:26:29', '1', '2020-08-29 18:05:10', '1', 0, NULL, NULL, 'รีวิวอาณาจักร \'อาลีบาบา\' อีคอมเมิร์ซสุดล้ำ สะดวกสบายไปอีก', 'ชวนทำความรู้จัก อาลีบาบา กรุ๊ป ผู้นำด้านอีคอมเมิร์ซและเทคโนโลยีด้านไอทีสุดล้ำของประเทศจีน ที่ไฮเทคสุดๆ จนอาจจะทำให้คุณต้องอึ้ง ถ้าอยากรู้จักอาณาจักรอันยิ่งใหญ่แห่งนี้ของ  แจ็ค หม่า ให้มากขึ้นละก็ ต้องตามมาดู', 'รีวิวอาณาจักร \'อาลีบาบา\' อีคอมเมิร์ซสุดล้ำ สะดวกสบายไปอีก'),
(2, 'รีวิวอาณาจักร-อาลีบาบา-อีคอมเมิร์ซสุดล้ำ-สะดวกสบายไปอีก-2', NULL, 'รีวิวอาณาจักร \'อาลีบาบา\' อีคอมเมิร์ซสุดล้ำ สะดวกสบายไปอีก 2', 'Review of the state-of-the-art \'Alibaba\' e-commerce empire Convenient to another 2', 'ชวนทำความรู้จัก อาลีบาบา กรุ๊ป ผู้นำด้านอีคอมเมิร์ซและเทคโนโลยีด้านไอทีสุดล้ำของประเทศจีน ที่ไฮเทคสุดๆ จนอาจจะทำให้คุณต้องอึ้ง ถ้าอยากรู้จักอาณาจักรอันยิ่งใหญ่แห่งนี้ของ  แจ็ค หม่า ให้มากขึ้นละก็ ต้องตามมาดู', 'Let\'s get to know Alibaba Group, China\'s most advanced e-commerce and IT technology leader, that is extremely high-tech. If you want to get to know more about Jack Ma\'s great empire, you must follow along.', '<p xss=\"removed\">1. อาณาจักรอาลีบาบา เมืองหางโจว ประเทศจีน</p><p xss=\"removed\">เริ่มต้นด้วยการไปเยี่ยมเยือนกันถึงที่บริษัทอาลีบาบา สำนักงานใหญ่ในเมืองหางโจว ประเทศจีน ออฟฟิศที่นี่ทันสมัยมาก สะดวกสบาย มีบริการจักรยานให้พนักงานปั่นไปทำงานระหว่างอาคารได้ด้วย ภาย</p><p xss=\"removed\">ในบริเวณบริษัทก็มีทั้งร้านอาหาร ร้านเครื่องดื่ม คาเฟ่ และร้านขายของที่ระลึกที่เป็นร้านเอาไว้ทดลองระบบเทคโนโลยีด้านการใช้จ่ายต่างๆ ที่บริษัทพัฒนาขึ้น แถมยังมีพิพิธภัณฑ์อาลีบาบาให้หน่วยงาน</p><p xss=\"removed\">ภายนอก เข้ามาศึกษาดูงานได้ตลอดทุกวัน สำหรับเมืองไทยเองก็มีทางคณะรัฐมนตรีกระทรวงพาณิชย์ ของไทยเคยเดินทางมาดูงาน และเยี่ยมเยือนพิพิธภัณฑ์แห่งนี้เช่นกัน</p><p xss=\"removed\">2. Futuremart @Alibaba Xixi Campus</p><p xss=\"removed\">อย่างที่บอกไปข้างต้นว่าภายในบริษัทของอาลีบาบา มีร้านค้าที่ทำขึ้นมาเพื่อทดลองระบบเทคโนโลยีต่างๆ ในการช็อปปิ้ง ใช้จ่ายซื้อสิ่งของ ร้านค้านั้นมีชื่อว่า Futuremart @Alibaba Xixi Campus เป็นต้น</p><p xss=\"removed\">แบบของร้านค้าที่ใช้เทคโนโลยีดิจิทัลมาช่วยทำให้เกิด New Retail เพื่อเชื่อมโยงอุตสาหกรรมค้าปลีกกับโลกออนไลน์ </p>', '<p xss=\"removed\">1. Alibaba Empire, Hangzhou City, China</p><p xss=\"removed\">Beginning with a visit to Alibaba. Headquartered in Hangzhou, China, the office here is very modern and comfortable. There is a bicycle service for employees to cycle between the buildings as well.</p><p xss=\"removed\">There are also restaurants in the company area. Beverage shops, cafes, and souvenir shops that are stores to try out various spending technology systems. Developed by the company There is also an Alibaba Museum for the department.</p><p xss=\"removed\">Outside to study and see the work all day. For Thailand, there is also a cabinet of the Ministry of Commerce. Thai people have traveled to see the fair And visit this museum as well</p><p xss=\"removed\">2. Futuremart @lan Xixi Campus</p><p xss=\"removed\">As I said above that within the company of Alibaba. There is a store made to test different technology systems. In shopping Spend to buy things That store is called Futuremart @lan Xixi Campus.</p><p xss=\"removed\">A type of store that uses digital technology to help create New Retail to connect the retail industry with the online world.</p>', 'uploads/news_articles/2020/08/257613634b15255f5e7d90bb9aa9294c.jpg', 1, 0, '2020-08-06 22:46:02', '1', '2020-08-29 18:05:51', '1', 0, NULL, NULL, 'รีวิวอาณาจักร \'อาลีบาบา\' อีคอมเมิร์ซสุดล้ำ สะดวกสบายไปอีก 2', 'ชวนทำความรู้จัก อาลีบาบา กรุ๊ป ผู้นำด้านอีคอมเมิร์ซและเทคโนโลยีด้านไอทีสุดล้ำของประเทศจีน ที่ไฮเทคสุดๆ จนอาจจะทำให้คุณต้องอึ้ง ถ้าอยากรู้จักอาณาจักรอันยิ่งใหญ่แห่งนี้ของ  แจ็ค หม่า ให้มากขึ้นละก็ ต้องตามมาดู', 'รีวิวอาณาจักร \'อาลีบาบา\' อีคอมเมิร์ซสุดล้ำ สะดวกสบายไปอีก 2');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `order_code` varchar(150) DEFAULT NULL,
  `order_type` int(1) DEFAULT '0',
  `name` varchar(255) DEFAULT '',
  `lasname` varchar(255) DEFAULT NULL,
  `address` text,
  `provinces` varchar(255) DEFAULT NULL,
  `amphures` varchar(255) DEFAULT NULL,
  `districts` varchar(255) DEFAULT NULL,
  `zip_code` varchar(15) DEFAULT NULL,
  `tel` varchar(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `transport_id` int(11) DEFAULT NULL,
  `tracking_code` varchar(100) DEFAULT NULL,
  `deliverys_price` float(15,2) DEFAULT '0.00',
  `deliverys_price_extra` float(15,2) DEFAULT '0.00',
  `deliverys_repack_sack` float(11,2) NOT NULL DEFAULT '0.00',
  `note` text NOT NULL,
  `order_qty` int(11) DEFAULT '0',
  `active` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  `status` int(1) DEFAULT '1' COMMENT '0=ยังไม่ชำระเงิน,1=ชำระเงินแล้ว',
  `reserve_code` varchar(100) DEFAULT NULL,
  `is_invoice` int(1) DEFAULT '0',
  `invoice_type` int(1) DEFAULT '0',
  `invoice_code` varchar(255) DEFAULT NULL,
  `invoice_no` varchar(100) DEFAULT NULL,
  `invoice_address` varchar(255) DEFAULT NULL,
  `invoice_provinces_id` varchar(255) DEFAULT NULL,
  `invoice_amphures_id` varchar(255) DEFAULT NULL,
  `invoice_districts_id` varchar(255) DEFAULT NULL,
  `invoice_zip_code` varchar(45) DEFAULT NULL,
  `invoice_tel` varchar(20) DEFAULT NULL,
  `user_id` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`order_id`, `order_code`, `order_type`, `name`, `lasname`, `address`, `provinces`, `amphures`, `districts`, `zip_code`, `tel`, `email`, `transport_id`, `tracking_code`, `deliverys_price`, `deliverys_price_extra`, `deliverys_repack_sack`, `note`, `order_qty`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`, `status`, `reserve_code`, `is_invoice`, `invoice_type`, `invoice_code`, `invoice_no`, `invoice_address`, `invoice_provinces_id`, `invoice_amphures_id`, `invoice_districts_id`, `invoice_zip_code`, `invoice_tel`, `user_id`) VALUES
(1, '202008000006', 0, 'ruslee', 'seebu', '', '', '', '', '', '', '', NULL, NULL, 0.00, 0.00, 0.00, '', 2, 1, '2020-08-28 22:51:03', 0, '2020-08-28 22:51:03', 0, '0', NULL, NULL, 3, NULL, 0, 0, 'Rcpt202008000006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(2, '202008000007', 0, 'ruslee', 'seebu', ' - -', 'ปัตตานี', 'หนองจิก', 'ยาบี', '94170', '0807055971', 'comsci2535@gmail.com', 1, NULL, 330.00, 66.00, 0.00, '', 2, 1, '2020-08-28 22:55:15', 0, '2020-08-28 22:55:15', 0, '0', NULL, NULL, 3, NULL, 0, 0, 'Rcpt202008000007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(3, '202008000008', 1, 'ruslee', 'seebu', '', '', '', '', '', '', '', NULL, NULL, 0.00, 0.00, 0.00, '', 50, 1, '2020-08-28 23:01:56', 0, '2020-08-28 23:11:09', 1, '0', NULL, NULL, 2, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(4, '202008000009', 0, 'สมชาย', 'ใจดี', '23/44 ราม 53 รามคำแหง', 'กรุงเทพมหานคร', 'วังทองหลาง', 'วังทองหลาง', '10310', '0807055971', 'comsci2535@gmail.com', 1, NULL, 115.00, 23.00, 0.00, '', 1, 1, '2020-08-28 23:24:36', 0, '2020-08-28 23:25:37', 1, '0', NULL, NULL, 3, NULL, 0, 0, 'Rcpt202008000009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(5, '202008000010', 0, 'ruslee', 'seebu', '', '', '', '', '', '', '', NULL, NULL, 0.00, 0.00, 0.00, '', 1, 1, '2020-08-28 23:44:59', 0, '2020-08-28 23:44:59', 0, '0', NULL, NULL, 3, NULL, 0, 0, 'Rcpt202008000010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(6, '202008000011', 0, 'ruslee', 'seebu', '', '', '', '', '', '', '', NULL, NULL, 0.00, 0.00, 0.00, '', 1, 1, '2020-08-29 00:12:28', 0, '2020-08-29 00:12:28', 0, '0', NULL, NULL, 3, NULL, 0, 0, 'Rcpt202008000011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(7, '202009000001', 0, 'ruslee', 'seebu', '', '', '', '', '', '', '', NULL, NULL, 0.00, 0.00, 0.00, '', 1, 1, '2020-09-01 22:25:07', 0, '2020-09-01 22:25:07', 0, '0', NULL, NULL, 3, NULL, 0, 0, 'Rcpt202009000001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(8, NULL, 0, 'ruslee', 'seebu', '', '', '', '', '', '', '', NULL, NULL, 0.00, 0.00, 0.00, '', 0, 1, '2020-09-02 20:44:16', 0, '2020-09-02 20:44:16', 0, '0', NULL, NULL, 3, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(9, '202009000002', 0, 'ruslee', 'seebu', '', '', '', '', '', '', '', NULL, NULL, 0.00, 0.00, 0.00, '', 7, 1, '2020-09-02 20:48:02', 0, '2020-09-02 20:48:02', 0, '0', NULL, NULL, 3, NULL, 0, 0, 'Rcpt202009000002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(10, '202009000003', 0, 'ruslee', 'seebu', '', '', '', '', '', '', '', NULL, NULL, 0.00, 0.00, 0.00, '', 2, 1, '2020-09-02 20:50:10', 0, '2020-09-02 20:50:10', 0, '0', NULL, NULL, 3, NULL, 0, 0, 'Rcpt202009000003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(11, '202009000004', 0, 'ruslee', 'seebu', '', '', '', '', '', '', '', NULL, NULL, 0.00, 0.00, 0.00, '', 6, 1, '2020-09-02 20:54:59', 0, '2020-09-02 20:54:59', 0, '0', NULL, NULL, 3, NULL, 0, 0, 'Rcpt202009000004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(12, '202009000005', 0, 'ruslee', 'seebu', '', '', '', '', '', '', '', NULL, NULL, 0.00, 0.00, 0.00, '', 4, 1, '2020-09-02 20:57:10', 0, '2020-09-02 20:57:10', 0, '0', NULL, NULL, 3, NULL, 0, 0, 'Rcpt202009000005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(13, '202009000006', 0, 'ruslee', 'seebu', '', '', '', '', '', '', '', NULL, NULL, 0.00, 0.00, 0.00, '', 1, 1, '2020-09-02 21:18:05', 0, '2020-09-02 21:18:05', 0, '0', NULL, NULL, 3, NULL, 0, 0, 'Rcpt202009000006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(14, '202009000007', 0, 'ruslee', 'seebu', '', '', '', '', '', '', '', NULL, NULL, 0.00, 0.00, 0.00, '', 12, 1, '2020-09-02 21:22:06', 0, '2020-09-02 21:22:06', 0, '0', NULL, NULL, 3, NULL, 0, 0, 'Rcpt202009000007', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(15, '202009000008', 0, 'ruslee', 'seebu', '', '', '', '', '', '', '', NULL, NULL, 0.00, 0.00, 0.00, '', 13, 1, '2020-09-02 21:23:00', 0, '2020-09-02 21:23:00', 0, '0', NULL, NULL, 3, NULL, 0, 0, 'Rcpt202009000008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(16, '202009000009', 0, 'ruslee', 'seebu', '', '', '', '', '', '', '', NULL, NULL, 0.00, 0.00, 0.00, '', 6, 1, '2020-09-02 21:23:42', 0, '2020-09-02 21:23:42', 0, '0', NULL, NULL, 3, NULL, 0, 0, 'Rcpt202009000009', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(17, '202009000010', 0, 'ruslee', 'seebu', '', '', '', '', '', '', '', NULL, NULL, 0.00, 0.00, 0.00, '', 4, 1, '2020-09-02 22:03:34', 0, '2020-09-02 22:03:34', 0, '0', NULL, NULL, 3, NULL, 0, 0, 'Rcpt202009000010', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(18, '202009000011', 0, 'ruslee', 'seebu', '', '', '', '', '', '', '', NULL, NULL, 0.00, 0.00, 0.00, '', 1, 1, '2020-09-16 21:42:28', 0, '2020-09-16 21:42:28', 0, '0', NULL, NULL, 3, NULL, 0, 0, 'Rcpt202009000011', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(19, '202009000012', 0, 'ruslee', 'seebu', '', '', '', '', '', '', '', NULL, NULL, 0.00, 0.00, 0.00, '', 1, 1, '2020-09-16 21:44:44', 0, '2020-09-16 21:44:44', 0, '0', NULL, NULL, 3, NULL, 0, 0, 'Rcpt202009000012', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(20, '202010000001', 0, 'ruslee', 'seebu', '', '', '', '', '', '', '', NULL, NULL, 0.00, 0.00, 0.00, '', 1, 1, '2020-10-31 23:05:21', 0, '2020-10-31 23:05:21', 0, '0', NULL, NULL, 3, NULL, 0, 0, 'Rcpt202010000001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(21, '202010000002', 0, 'ruslee', 'seebu', '', '', '', '', '', '', '', NULL, NULL, 0.00, 0.00, 0.00, '', 3, 1, '2020-10-31 23:16:13', 0, '2020-10-31 23:16:13', 0, '0', NULL, NULL, 3, NULL, 0, 0, 'Rcpt202010000002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(22, '202011000001', 0, 'ruslee', 'seebu', ' - -', 'กรุงเทพมหานคร', 'คลองสาน', 'คลองต้นไทร', '10600', '0807055971', 'comsci2535@gmail.com', 2, NULL, 30.00, 6.00, 0.00, '', 12, 1, '2020-11-01 21:46:13', 0, '2020-11-02 14:58:57', 1, '0', NULL, NULL, 3, NULL, 0, 0, 'Rcpt202011000001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(23, '202011000002', 0, 'ruslee', 'seebu', '', '', '', '', '', '', '', NULL, NULL, 0.00, 0.00, 0.00, '', 3, 1, '2020-11-01 22:02:33', 0, '2020-11-01 22:02:33', 0, '0', NULL, NULL, 3, NULL, 0, 0, 'Rcpt202011000002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(24, '202011000003', 0, 'ruslee', 'seebu', '', '', '', '', '', '', '', NULL, NULL, 0.00, 0.00, 0.00, '', 1, 1, '2020-11-02 11:40:04', 0, '2020-11-02 11:40:04', 0, '0', NULL, NULL, 3, NULL, 0, 0, 'Rcpt202011000003', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(25, '202011000004', 0, 'ruslee', 'seebu', '', '', '', '', '', '', '', NULL, NULL, 0.00, 0.00, 0.00, '', 2, 1, '2020-11-02 21:57:19', 0, '2020-11-02 21:57:19', 0, '0', NULL, NULL, 3, NULL, 0, 0, 'Rcpt202011000004', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(26, '202011000005', 0, 'ruslee', 'seebu', '', '', '', '', '', '', '', NULL, NULL, 0.00, 0.00, 0.00, '', 3, 1, '2020-11-02 23:15:45', 0, '2020-11-02 23:15:45', 0, '0', NULL, NULL, 3, NULL, 0, 0, 'Rcpt202011000005', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(27, '202011000006', 0, 'ruslee', 'seebu', '', '', '', '', '', '', '', NULL, NULL, 0.00, 0.00, 0.00, '', 3, 1, '2020-11-02 23:23:33', 0, '2020-11-02 23:23:33', 0, '0', NULL, NULL, 3, NULL, 0, 0, 'Rcpt202011000006', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(28, '202011000007', 1, 'ruslee', 'seebu', '', '', '', '', '', '', '', NULL, NULL, 0.00, 0.00, 0.00, '', 0, 1, '2020-11-10 19:48:00', 0, '2020-11-10 19:48:00', 0, '0', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(29, '202011000008', 0, 'ruslee', 'seebu', ' - -', 'กรุงเทพมหานคร', 'คลองสาน', 'คลองต้นไทร', '10600', '0807055971', 'lee.yam_1214@hotmail.com', 2, NULL, 30.00, 6.00, 0.00, '', 2, 1, '2020-11-10 19:48:59', 0, '2020-11-16 21:18:31', 1, '0', NULL, NULL, 6, NULL, 0, 0, 'Rcpt202011000008', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(30, '202012000001', 0, 'ruslee', 'seebu', '', '', '', '', '', '0807055971', 'comsci2535@gmail.com', NULL, NULL, 0.00, 0.00, 0.00, '', 5, 1, '2020-12-12 22:11:32', 0, '2020-12-12 22:11:32', 0, '0', NULL, NULL, 3, NULL, 0, 0, 'Rcpt202012000001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(31, '202101000001', 0, 'ruslee', 'seebu', '', '', '', '', '', '0807055971', 'comsci2535@gmail.com', NULL, NULL, 0.00, 0.00, 0.00, '', 9, 1, '2021-01-18 21:28:26', 0, '2021-01-18 21:28:26', 0, '0', NULL, NULL, 3, NULL, 0, 0, 'Rcpt202101000001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(32, '68-210200001', 1, 'ruslee', 'seebu', '', '', '', '', '', '0807055971', 'comsci2535@gmail.com', NULL, NULL, 0.00, 0.00, 0.00, 'แสดงความคิดเห็น', 2, 1, '2021-02-02 09:27:39', 0, '2021-02-24 22:58:08', 1, '0', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(33, '68-210200002', 1, 'ruslee', 'seebu', '', '', '', '', '', '0807055971', 'comsci2535@gmail.com', NULL, NULL, 0.00, 0.00, 0.00, '', 34, 1, '2021-02-04 21:17:16', 0, '2021-02-04 21:17:16', 0, '0', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(34, '68-210200003', 1, 'ruslee', 'seebu', ' ', 'กรุงเทพมหานคร', 'คลองสาน', 'คลองต้นไทร', '10600', '0807055971', 'comsci2535@gmail.com', 1, NULL, 380.00, 76.00, 0.00, 'คุณลูกค้าจำเป็นต้องชำระเงินให้เรียบร้อยก่อน', 32, 1, '2021-02-04 21:34:00', 0, '2021-02-24 22:57:45', 1, '0', NULL, NULL, 3, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(35, '68-202102000001', 1, 'ruslee', 'seebu', '', '', '', '', '', '0807055971', 'comsci2535@gmail.com', NULL, NULL, 0.00, 0.00, 0.00, '', 34, 1, '2021-02-28 12:35:44', 0, '2021-02-28 12:35:44', 0, '0', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(36, '68-202102000002', 1, 'ruslee', 'seebu', ' - -', 'กรุงเทพมหานคร', 'คลองสาน', 'คลองต้นไทร', '10600', '0807055971', 'comsci2535@gmail.com', 1, NULL, 380.00, 76.00, 250.00, '', 12, 1, '2021-02-28 12:36:13', 0, '2021-02-28 12:36:13', 0, '0', NULL, NULL, 3, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(37, '202103000001', 0, 'ruslee', 'seebu', '', '', '', '', '', '0807055971', 'comsci2535@gmail.com', NULL, NULL, 0.00, 0.00, 0.00, '', 1, 1, '2021-03-01 15:45:38', 0, '2021-03-01 15:45:38', 0, '0', NULL, NULL, 3, NULL, 0, 0, 'Rcpt202103000001', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(38, '68-202103000001', 1, 'ruslee', 'seebu', '', '', '', '', '', '0807055971', 'comsci2535@gmail.com', NULL, NULL, 0.00, 0.00, 0.00, '', 32, 1, '2021-03-01 16:31:25', 0, '2021-03-01 16:31:25', 0, '0', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(39, '68-202103000002', 1, 'ruslee', 'seebu', '', '', '', '', '', '0807055971', 'comsci2535@gmail.com', NULL, NULL, 0.00, 0.00, 0.00, '', 232, 1, '2021-03-01 16:32:43', 0, '2021-03-01 16:32:43', 0, '0', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(40, '68-202103000003', 1, 'ruslee', 'seebu', '', '', '', '', '', '0807055971', 'comsci2535@gmail.com', NULL, NULL, 0.00, 0.00, 0.00, '', 34, 1, '2021-03-01 22:20:58', 0, '2021-03-01 22:20:58', 0, '0', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(41, '68-202103000004', 1, 'ruslee', 'seebu', '', '', '', '', '', '0807055971', 'comsci2535@gmail.com', NULL, NULL, 0.00, 0.00, 0.00, '', 34, 1, '2021-03-01 22:22:09', 0, '2021-03-01 22:22:09', 0, '0', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(42, '202103000002', 0, 'ruslee', 'seebu', '', '', '', '', '', '0807055971', 'comsci2535@gmail.com', NULL, NULL, 0.00, 0.00, 0.00, '', 1, 1, '2021-03-01 22:42:17', 0, '2021-03-01 22:42:17', 0, '0', NULL, NULL, 3, NULL, 0, 0, 'Rcpt202103000002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1'),
(43, '68-202104000001', 1, 'ruslee', 'seebu', '', '', '', '', '', '0807055971', 'comsci2535@gmail.com', NULL, NULL, 0.00, 0.00, 0.00, '', 1231, 1, '2021-04-29 22:13:24', 0, '2021-04-29 22:13:24', 0, '0', NULL, NULL, 1, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1');

-- --------------------------------------------------------

--
-- Table structure for table `orders_detail`
--

CREATE TABLE `orders_detail` (
  `order_detail_id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `product_price` double(11,2) DEFAULT '0.00',
  `product_start_price` double(11,2) DEFAULT '0.00',
  `rating_price_th` double(11,2) DEFAULT '0.00',
  `rating_price_china` double(11,2) DEFAULT '0.00',
  `quantity` int(11) DEFAULT NULL,
  `is_attribute` int(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `order_detail_uniqid` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `orders_detail`
--

INSERT INTO `orders_detail` (`order_detail_id`, `order_id`, `product_id`, `product_price`, `product_start_price`, `rating_price_th`, `rating_price_china`, `quantity`, `is_attribute`, `created_at`, `order_detail_uniqid`) VALUES
(1, 1, 1, 2800.00, 4800.00, 4.66, 1.00, 1, 0, '2020-08-28 22:51:03', '5F4927E722396'),
(2, 1, 6, 4000.00, 5000.00, 4.66, 1.00, 1, 0, '2020-08-28 22:51:03', '5F4927E722519'),
(3, 2, 1, 2800.00, 4800.00, 4.66, 1.00, 2, 0, '2020-08-28 22:55:15', '5F4928E34FF2E'),
(4, 4, 7, 1500.00, 3000.00, 4.66, 1.00, 1, 0, '2020-08-28 23:24:36', '5F492FC485E65'),
(5, 5, 8, 200.00, 250.00, 4.66, 1.00, 1, 0, '2020-08-28 23:44:59', '5F49348B9692E'),
(6, 6, 7, 1500.00, 3000.00, 4.66, 1.00, 1, 0, '2020-08-29 00:12:28', '5F493AFC6A5A5'),
(7, 7, 1, 2800.00, 4800.00, 4.66, 1.00, 1, 0, '2020-09-01 22:25:07', '5F4E67D356AE0'),
(8, 8, 1, 2800.00, 4800.00, 4.66, 1.00, 1, 0, '2020-09-02 20:44:16', '5F4FA1B087A43'),
(9, 9, 1, 2800.00, 4800.00, 4.66, 1.00, 7, 0, '2020-09-02 20:48:02', '5F4FA292DF68C'),
(10, 10, 1, 2800.00, 4800.00, 4.66, 1.00, 2, 0, '2020-09-02 20:50:10', '5F4FA312DE76E'),
(11, 11, 2, 1390.00, 2390.00, 4.66, 1.00, 6, 0, '2020-09-02 20:54:59', '5F4FA43328CD9'),
(12, 12, 2, 1390.00, 2390.00, 4.66, 1.00, 4, 0, '2020-09-02 20:57:10', '5F4FA4B61C079'),
(13, 13, 1, 2800.00, 4800.00, 4.66, 1.00, 1, 0, '2020-09-02 21:18:05', '5F4FA99DC89E6'),
(14, 14, 2, 1390.00, 2390.00, 4.66, 1.00, 12, 0, '2020-09-02 21:22:06', '5F4FAA8E265F3'),
(15, 15, 1, 2800.00, 4800.00, 4.66, 1.00, 13, 0, '2020-09-02 21:23:00', '5F4FAAC44D624'),
(16, 16, 1, 2800.00, 4800.00, 4.66, 1.00, 6, 0, '2020-09-02 21:23:42', '5F4FAAEED9E6B'),
(17, 17, 1, 2800.00, 4800.00, 4.66, 1.00, 1, 0, '2020-09-02 22:03:34', '5F4FB44643126'),
(18, 17, 2, 1390.00, 2390.00, 4.66, 1.00, 3, 0, '2020-09-02 22:03:34', '5F4FB4464327C'),
(19, 18, 1, 2800.00, 4800.00, 4.66, 1.00, 1, 0, '2020-09-16 21:42:28', '5F6224544724D'),
(20, 19, 1, 2800.00, 4800.00, 4.66, 1.00, 1, 0, '2020-09-16 21:44:44', '5F6224DC56E70'),
(21, 20, 2, 1390.00, 2390.00, 4.66, 1.00, 1, 0, '2020-10-31 23:05:21', '5F9D8B4172458'),
(22, 21, 4, 500.00, 500.00, 4.66, 1.00, 1, 0, '2020-10-31 23:16:13', '5F9D8DCD8B26D'),
(23, 22, 2, 1390.00, 2390.00, 4.66, 1.00, 1, 0, '2020-11-01 21:46:13', '5F9ECA35ED69A'),
(24, 22, 4, 500.00, 500.00, 4.66, 1.00, 1, 0, '2020-11-01 21:46:13', '5F9ECA35ED7DE'),
(25, 25, 2, 1390.00, 2390.00, 4.66, 1.00, 1, 0, '2020-11-02 21:57:19', '5FA01E4F76C6C'),
(26, 25, 4, 500.00, 500.00, 4.66, 1.00, 1, 0, '2020-11-02 21:57:19', '5FA01E4F76D78'),
(27, 26, 4, 500.00, 500.00, 4.66, 1.00, 1, 0, '2020-11-02 23:15:45', '5FA030B14FC1B'),
(28, 27, 2, 1390.00, 2390.00, 4.66, 1.00, 1, 0, '2020-11-02 23:23:33', '5FA0328542E2B'),
(29, 29, 1, 2800.00, 0.00, 4.66, 1.00, 1, 0, '2020-11-10 19:48:59', '5FAA8C3B62E5C'),
(30, 29, 2, 1390.00, 2390.00, 4.66, 1.00, 1, 0, '2020-11-10 19:48:59', '5FAA8C3B62F55'),
(31, 30, 1, 2800.00, 0.00, 4.66, 1.00, 1, 0, '2020-12-12 22:11:32', '5FD4DDA4C1CF5'),
(32, 37, 1, 2800.00, 0.00, 4.66, 1.00, 1, 0, '2021-03-01 15:45:38', '603CA9B2E6B92'),
(33, 42, 2, 1390.00, 2390.00, 4.66, 1.00, 1, 0, '2021-03-01 22:42:17', '603D0B591B2ED');

-- --------------------------------------------------------

--
-- Table structure for table `orders_detail_attribute`
--

CREATE TABLE `orders_detail_attribute` (
  `order_detail_uniqid` varchar(255) DEFAULT NULL,
  `attribute_name` varchar(255) DEFAULT NULL,
  `attribute_value` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders_detail_attribute`
--

INSERT INTO `orders_detail_attribute` (`order_detail_uniqid`, `attribute_name`, `attribute_value`) VALUES
('5F4927E722396', 'size', 'M'),
('5F4927E722396', 'color', 'สีแดง'),
('5F4928E34FF2E', 'size', 'M'),
('5F4928E34FF2E', 'color', 'สีแดง'),
('5F4E67D356AE0', 'size', 'L'),
('5F4E67D356AE0', 'color', 'สีฟ้า'),
('5F4FA1B087A43', 'size', 'M'),
('5F4FA1B087A43', 'color', 'สีแดง'),
('5F4FA292DF68C', 'size', 'M'),
('5F4FA292DF68C', 'color', 'สีแดง'),
('5F4FA312DE76E', 'size', 'M'),
('5F4FA312DE76E', 'color', 'สีแดง'),
('5F4FA43328CD9', 'color', 'สีฟ้า'),
('5F4FA4B61C079', 'color', 'สีฟ้า'),
('5F4FA99DC89E6', 'size', 'M'),
('5F4FA99DC89E6', 'color', 'สีแดง'),
('5F4FAA8E265F3', 'color', 'สีชมพู่'),
('5F4FAAC44D624', 'size', 'M'),
('5F4FAAC44D624', 'color', 'สีฟ้า'),
('5F4FAAEED9E6B', 'size', 'M'),
('5F4FAAEED9E6B', 'color', 'สีแดง'),
('5F4FB44643126', 'size', 'M'),
('5F4FB44643126', 'color', 'สีแดง'),
('5F4FB4464327C', 'color', 'สีชมพู่'),
('5F6224544724D', 'size', 'M'),
('5F6224544724D', 'color', 'สีแดง'),
('5F6224DC56E70', 'size', 'M'),
('5F6224DC56E70', 'color', 'สีแดง'),
('5F9D8B4172458', 'color', 'สีชมพู่'),
('5F9ECA35ED69A', 'color', 'สีชมพู่'),
('5FA01E4F76C6C', 'color', 'สีชมพู่'),
('5FA0328542E2B', 'color', 'สีชมพู่'),
('5FAA8C3B62E5C', 'size', 'L'),
('5FAA8C3B62E5C', 'color', 'สีฟ้า'),
('5FAA8C3B62F55', 'color', 'สีชมพู่'),
('5FD4DDA4C1CF5', 'size', 'M'),
('5FD4DDA4C1CF5', 'color', 'สีฟ้า'),
('603CA9B2E6B92', 'size', 'M'),
('603CA9B2E6B92', 'color', 'สีฟ้า'),
('603D0B591B2ED', 'color', 'สีชมพู่');

-- --------------------------------------------------------

--
-- Table structure for table `orders_detail_import`
--

CREATE TABLE `orders_detail_import` (
  `orders_detail_import_id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `address_china` text,
  `tracking_china` varchar(255) DEFAULT '',
  `tracking_code` varchar(255) DEFAULT NULL,
  `tracking_in_code` varchar(255) NOT NULL,
  `tracking_receive` varchar(255) NOT NULL,
  `qty_china` varchar(255) DEFAULT NULL,
  `detail` text,
  `date_up` date DEFAULT NULL,
  `date_down` date DEFAULT NULL,
  `weight` float(11,2) DEFAULT NULL,
  `size` varchar(255) DEFAULT NULL,
  `wide` float(11,2) NOT NULL,
  `longs` float(11,2) NOT NULL,
  `high` float(11,2) NOT NULL,
  `repack_sack` float(11,2) NOT NULL,
  `service_charge` float(11,2) DEFAULT '0.00',
  `import_cost` float(11,2) DEFAULT '0.00',
  `type_note` text,
  `note_china` text,
  `note_import` text NOT NULL,
  `attribute_send_id` varchar(11) DEFAULT '0',
  `user_id` varchar(100) DEFAULT NULL,
  `active` int(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders_detail_import`
--

INSERT INTO `orders_detail_import` (`orders_detail_import_id`, `order_id`, `address_china`, `tracking_china`, `tracking_code`, `tracking_in_code`, `tracking_receive`, `qty_china`, `detail`, `date_up`, `date_down`, `weight`, `size`, `wide`, `longs`, `high`, `repack_sack`, `service_charge`, `import_cost`, `type_note`, `note_china`, `note_import`, `attribute_send_id`, `user_id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`) VALUES
(1, 3, '', '1234567890', '', '', '', '', 'ผลไม้และเครื่องใช้', '2020-08-28', '2020-08-28', 45.00, '56', 0.00, 0.00, 0.00, 0.00, 6000.00, 4000.00, 'ผลไม้และเครื่องใช้', '', '', '2,3', '1', 1, '2020-08-28 23:01:56', 1, '2020-08-28 23:10:01', 1, '0', NULL, NULL),
(2, 2, NULL, NULL, '', '', '', '', 'เสื้อผ้า', '2020-09-01', '2020-08-30', 20.00, '10', 0.00, 0.00, 0.00, 0.00, 300.00, 200.00, 'เสื้อผ้า', NULL, '', '0', '1', 1, '2020-08-28 23:12:08', 1, '2020-08-28 23:12:08', 1, '0', NULL, NULL),
(3, 1, NULL, NULL, '', '', '', '', 'เสื้อผ้า', '2020-09-02', '2020-09-05', 12.00, '20', 0.00, 0.00, 0.00, 0.00, 200.00, 400.00, 'เสื้อผ้า', NULL, '', '0', '1', 1, '2020-08-28 23:19:47', 1, '2020-08-28 23:19:47', 1, '0', NULL, NULL),
(4, 4, NULL, NULL, '', '', '', '', 'อาหารเสริม', '2020-08-19', '2020-08-28', 10.00, '20', 0.00, 0.00, 0.00, 0.00, 550.00, 600.00, 'อาหารเสริม', NULL, '', '0', '1', 1, '2020-08-28 23:26:22', 1, '2020-08-28 23:26:22', 1, '0', NULL, NULL),
(5, 7, NULL, NULL, '', '', '', '', 'ร้องเท้า', '2020-09-01', '2020-09-04', 20.00, '40', 0.00, 0.00, 0.00, 0.00, 400.00, 600.00, 'ร้องเท้า', NULL, '', '0', '1', 1, '2020-09-01 22:27:14', 1, '2020-09-01 22:27:14', 1, '0', NULL, NULL),
(6, 22, NULL, NULL, '', '', '', '', 'ของใช้', '2020-11-02', '2020-11-04', 400.00, '30', 0.00, 0.00, 0.00, 0.00, 500.00, 500.00, 'ของใช้', NULL, '', '0', '1', 1, '2020-11-02 15:00:01', 1, '2020-11-02 15:00:01', 1, '0', NULL, NULL),
(7, 28, '', '34234234234', '', '68-08089008', '', '', 'ผ้า', '2020-11-11', '2020-11-13', 600.00, '45', 12.00, 22.00, 10.00, 50.00, 350.00, 30.00, 'ผ้า', '', '', '2,3', '1', 1, '2020-11-10 19:48:00', 1, '2021-02-01 22:39:12', 1, '0', NULL, NULL),
(8, 29, NULL, NULL, 'TH3423423423', '68-65756756757', '', '', 'เสื้อผ้า', '2020-11-10', '2020-11-13', 40.00, '34', 0.00, 0.00, 0.00, 0.00, 150.00, 50.00, 'เสื้อผ้า', NULL, '', '0', '', 1, '2020-11-10 20:44:40', 1, '2020-11-10 20:49:58', 1, '0', NULL, NULL),
(9, 32, '', 'TG234234234', NULL, '', '', '2', NULL, NULL, NULL, NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, NULL, '', '', '1,3', '1', 1, '2021-02-02 09:27:39', 1, '2021-02-02 09:27:39', 1, '0', NULL, NULL),
(10, 33, '', 'SD342342343', NULL, '', '', '34', NULL, NULL, NULL, NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, NULL, '', '', '1,3', '1', 1, '2021-02-04 21:17:16', 1, '2021-02-04 21:17:16', 1, '0', NULL, NULL),
(11, 34, '', 'SDE324234234', '3342342342TH', '68-675675675', '343242342342', '2', 'ผ้า', '2021-02-20', '2021-02-27', 23.00, '34', 61.00, 41.00, 52.00, 300.00, 500.00, 100.00, 'ผ้า', '', '', '2,4', '1', 1, '2021-02-04 21:34:00', 1, '2021-02-25 15:30:29', 1, '0', NULL, NULL),
(12, 34, NULL, 'SDE324234234', '7867867867TH', '68-6786786786', '343242342342', '2', 'ของใช้', '2021-02-20', '2021-02-28', 234.00, '23', 23.00, 12.00, 11.00, 300.00, 233.00, 244.00, 'ของใช้', NULL, '', '2,4', '1', 1, '2021-02-20 15:13:11', 1, '2021-02-20 15:13:11', 1, '0', NULL, NULL),
(13, 35, '', '3242323423423', NULL, '', '', '34', NULL, NULL, NULL, NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, NULL, '', '', '1,4', '1', 1, '2021-02-28 12:35:44', 1, '2021-02-28 12:35:44', 1, '0', NULL, NULL),
(14, 36, '', '3242342342334', '45634534534DF', '68-63453453453', '456435345345TH', '12', 'ผ้า', '2021-02-28', '2021-03-03', 64.00, '333', 23.00, 54.00, 12.00, 100.00, 300.00, 150.00, 'ผ้า', '', '', '1,3', '1', 1, '2021-02-28 12:36:13', 1, '2021-03-01 22:57:02', 1, '0', NULL, NULL),
(15, 36, NULL, '3242342342334', '4534534534GG', '68-654345435', '4653453453TH', '12', 'ของใช้', '2021-02-28', '2021-03-03', 23.00, '12', 32.00, 53.00, 67.00, 150.00, 200.00, 150.00, 'ของใช้', NULL, '', '0', '1', 1, '2021-02-28 13:15:55', 1, '2021-03-01 22:56:30', 1, '0', NULL, NULL),
(16, 37, NULL, NULL, 'ของใช้', '68-6453453453', '45345345345GH', '34', 'ของใช้', '2021-03-03', '2021-03-05', 222.00, '500', 220.00, 12.00, 50.00, 50.00, 300.00, 150.00, 'ของใช้', NULL, '', '0', '1', 1, '2021-03-01 16:10:38', 1, '2021-03-01 16:27:48', 1, '0', NULL, NULL),
(17, 37, NULL, NULL, '234234234TH', '68-3234234234', '234234234234CH', '34', 'ผ้า', '2021-03-03', '2021-03-05', 23.00, '123', 1231.00, 123.00, 123.00, 502.00, 122.00, 231.00, 'ผ้า', NULL, '', '0', '1', 1, '2021-03-01 16:29:00', 1, '2021-03-01 16:29:00', 1, '0', NULL, NULL),
(18, 38, '', '23423423412CH', NULL, '', '', '32', NULL, NULL, NULL, NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, NULL, '', '', '2,3', '1', 1, '2021-03-01 16:31:25', 1, '2021-03-01 16:31:25', 1, '0', NULL, NULL),
(19, 39, '', '345345345CH', '231231231TH', '68-345345345', '342323423DS', '232', 'สินค้าทั่วไป', '2021-03-01', '2021-03-04', 23.00, '123', 343.00, 232.00, 434.00, 150.00, 324.00, 222.00, 'สินค้าทั่วไป', '', '', '1,4', '1', 1, '2021-03-01 16:32:43', 1, '2021-03-01 20:30:16', 1, '0', NULL, NULL),
(20, 39, NULL, '345345345CH', '4323423423TH', '68-345345435', '345345345DH', '232', 'สินค้าทั่วไป', '2021-03-01', '2021-03-04', 213.00, '12', 543.00, 234.00, 553.00, 200.00, 440.00, 250.00, 'สินค้าทั่วไป', NULL, '', '1,4', '1', 1, '2021-03-01 20:31:42', 1, '2021-03-01 20:31:42', 1, '0', NULL, NULL),
(21, 39, NULL, '345345345CH', '342342342TH', '68-343423423', '342234234SD', '232', 'สินของทั่วไป', '2021-03-01', '2021-03-04', 234.00, '454', 42.00, 34.00, 23.00, 150.00, 340.00, 230.00, 'สินของทั่วไป', NULL, '', '1,4', '1', 1, '2021-03-01 20:38:43', 1, '2021-03-01 20:38:43', 1, '0', NULL, NULL),
(22, 40, '', '324234324DD', NULL, '', '', '34', NULL, NULL, NULL, NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, NULL, 'สินของ', '', '1,4', '1', 1, '2021-03-01 22:20:58', 1, '2021-03-01 22:20:58', 1, '0', NULL, NULL),
(23, 41, '', '3423423423FF', '234234234TH', '68-202103000004', '324342343TH', '34', 'สินของ', '2021-03-01', '2021-03-12', 234.00, '234', 23.00, 12.00, 23.00, 50.00, 360.00, 400.00, 'สินของ', 'สินของ', '', '2,3', '1', 1, '2021-03-01 22:22:09', 1, '2021-03-01 22:34:47', 1, '0', NULL, NULL),
(24, 41, NULL, '3423423423FF', '3423123213TH', '68-202103000004', '234324343CH', '34', 'สินของ', '2021-03-01', '2021-03-12', 534.00, '23', 32.00, 343.00, 45.00, 50.00, 600.00, 230.00, 'สินของ', NULL, '', '2,3', '1', 1, '2021-03-01 22:40:51', 1, '2021-03-01 22:41:14', 1, '0', NULL, NULL),
(25, 42, NULL, NULL, '4353234232TH', '68-756443535', '4353453453CH', '23', 'สินค้า', '2021-03-01', '2021-03-18', 60.00, '0.066', 110.00, 50.00, 12.00, 100.00, 324.00, 2400.00, 'สินค้า', NULL, '', '0', '1', 1, '2021-03-01 22:44:11', 1, '2021-03-25 21:29:25', 1, '0', NULL, NULL),
(26, 43, '', '12312312312', NULL, '68-202104000001', '', '1231', NULL, NULL, NULL, NULL, NULL, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, NULL, 'asdasd', 'zxczxczxczxc', '1,3,4', '1', 1, '2021-04-29 22:13:24', 1, '2021-04-29 22:13:24', 1, '0', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `orders_detail_url`
--

CREATE TABLE `orders_detail_url` (
  `order_detail_id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `title` varchar(2000) NOT NULL,
  `product_price` double(11,2) DEFAULT '0.00',
  `rating_price_th` double(11,2) DEFAULT '0.00',
  `rating_price_china` double(11,2) DEFAULT '0.00',
  `from_web` varchar(255) NOT NULL,
  `from_web_url` text NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `image` text,
  `attribute` text,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `orders_detail_url`
--

INSERT INTO `orders_detail_url` (`order_detail_id`, `order_id`, `title`, `product_price`, `rating_price_th`, `rating_price_china`, `from_web`, `from_web_url`, `quantity`, `image`, `attribute`, `created_at`) VALUES
(1, 20, '打底裤女裤秋冬季大码胖mm高腰显瘦黑色魔术紧身小脚加厚加绒外穿', 62.80, 4.66, 1.00, 'tmall,taobao', '', 1, 'https://img.alicdn.com/imgextra/i4/1579139371/O1CN01EYPRSc2J5y9HegYcO_!!1579139371-0-lubanu-s.jpg,https://img.alicdn.com/imgextra/i4/1579139371/O1CN01EYPRSc2J5y9HegYcO_!!1579139371-0-lubanu-s.jpg', '尺寸:XL 适合106-120斤<=>尺寸:XL 适合106-120斤', '2020-10-31 23:05:21'),
(2, 21, '打底裤女裤秋冬季大码胖mm高腰显瘦黑色魔术紧身小脚加厚加绒外穿', 62.80, 4.66, 1.00, 'tmall,taobao', '', 2, 'https://img.alicdn.com/imgextra/i4/1579139371/O1CN01EYPRSc2J5y9HegYcO_!!1579139371-0-lubanu-s.jpg,https://img.alicdn.com/imgextra/i4/1579139371/O1CN01EYPRSc2J5y9HegYcO_!!1579139371-0-lubanu-s.jpg', '尺寸:6XL 适合176-195斤<=>尺寸:6XL 适合176-195斤', '2020-10-31 23:16:13'),
(3, 22, '浪莎袜子女中筒袜黑色女士棉袜春秋季纯棉长筒袜加厚秋冬全棉女袜', 49.00, 4.66, 1.00, 'detail.tmall.com', '', 4, 'https://img.alicdn.com/imgextra/i1/272715291/O1CN01jdo5Kr1oxKDX77xaJ_!!272715291-0-picasso.jpg,https://img.alicdn.com/imgextra/i1/272715291/O1CN01jdo5Kr1oxKDX77xaJ_!!272715291-0-picasso.jpg', '颜色分类:340【5双】【长筒】黑2白2浅灰1<=>颜色分类:340【5双】【长筒】黑2白2浅灰1', '2020-11-01 21:46:13'),
(4, 22, '浪莎袜子女中筒袜黑色女士棉袜春秋季纯棉长筒袜加厚秋冬全棉女袜', 49.00, 4.66, 1.00, 'detail.tmall.com', '', 3, 'https://img.alicdn.com/imgextra/i1/272715291/O1CN01jdo5Kr1oxKDX77xaJ_!!272715291-0-picasso.jpg,https://img.alicdn.com/imgextra/i1/272715291/O1CN01jdo5Kr1oxKDX77xaJ_!!272715291-0-picasso.jpg', '颜色分类:361【10双】【四季款】黑色2白色2灰色2粉色2浅蓝2<=>颜色分类:361【10双】【四季款】黑色2白色2灰色2粉色2浅蓝2', '2020-11-01 21:46:13'),
(5, 22, '浪莎袜子女中筒袜黑色女士棉袜春秋季纯棉长筒袜加厚秋冬全棉女袜', 49.00, 4.66, 1.00, 'detail.tmall.com', '', 1, 'https://img.alicdn.com/imgextra/i1/272715291/O1CN01jdo5Kr1oxKDX77xaJ_!!272715291-0-picasso.jpg,https://img.alicdn.com/imgextra/i1/272715291/O1CN01jdo5Kr1oxKDX77xaJ_!!272715291-0-picasso.jpg', '颜色分类:521【5双】【四季款】白色1奶白1卡其1粉色1烟灰1<=>颜色分类:521【5双】【四季款】白色1奶白1卡其1粉色1烟灰1', '2020-11-01 21:46:13'),
(6, 22, '浪莎袜子女中筒袜黑色女士棉袜春秋季纯棉长筒袜加厚秋冬全棉女袜', 49.00, 4.66, 1.00, 'detail.tmall.com', '', 2, 'https://img.alicdn.com/imgextra/i1/272715291/O1CN01jdo5Kr1oxKDX77xaJ_!!272715291-0-picasso.jpg,https://img.alicdn.com/imgextra/i1/272715291/O1CN01jdo5Kr1oxKDX77xaJ_!!272715291-0-picasso.jpg', '颜色分类:151【5双】【四季】白蓝1深蓝1白绿2白黑1<=>颜色分类:151【5双】【四季】白蓝1深蓝1白绿2白黑1', '2020-11-01 21:46:13'),
(7, 23, '浪莎袜子女中筒袜黑色女士棉袜春秋季纯棉长筒袜加厚秋冬全棉女袜', 49.00, 4.66, 1.00, 'detail.tmall.com', 'https://detail.tmall.com/item.htm?spm=a221t.1812074.goodlist.6.2ecd4208c0zCUI&id=625738745684&skuId=4605298594239', 3, 'https://img.alicdn.com/imgextra/i1/272715291/O1CN01jdo5Kr1oxKDX77xaJ_!!272715291-0-picasso.jpg,https://img.alicdn.com/imgextra/i1/272715291/O1CN01jdo5Kr1oxKDX77xaJ_!!272715291-0-picasso.jpg', '颜色分类:289【5双】【薄款-网眼】黑2浅粉2白色1<=>颜色分类:289【5双】【薄款-网眼】黑2浅粉2白色1', '2020-11-01 22:02:33'),
(8, 24, '浪莎袜子女中筒袜黑色女士棉袜春秋季纯棉长筒袜加厚秋冬全棉女袜', 228.34, 4.66, 1.00, 'detail.tmall.com', 'https://detail.tmall.com/item.htm?spm=a221t.1812074.goodlist.6.2ecd4208c0zCUI&id=625738745684&skuId=4605298594239', 1, 'https://img.alicdn.com/imgextra/i1/272715291/O1CN01jdo5Kr1oxKDX77xaJ_!!272715291-0-picasso.jpg,https://img.alicdn.com/imgextra/i1/272715291/O1CN01jdo5Kr1oxKDX77xaJ_!!272715291-0-picasso.jpg', '颜色分类:289【5双】【薄款-网眼】黑2白色2浅灰1<=>尺码:【多款多色 舒适百搭】【收藏加购享优先发货】', '2020-11-02 11:40:04'),
(9, 26, '浪莎袜子女中筒袜黑色女士棉袜春秋季纯棉长筒袜加厚秋冬全棉女袜', 228.34, 4.66, 1.00, 'detail.tmall.com', 'https://detail.tmall.com/item.htm?spm=a221t.1812074.goodlist.6.2ecd4208c0zCUI&id=625738745684&skuId=4605298594239', 2, 'https://img.alicdn.com/imgextra/i1/272715291/O1CN01jdo5Kr1oxKDX77xaJ_!!272715291-0-picasso.jpg,https://img.alicdn.com/imgextra/i1/272715291/O1CN01jdo5Kr1oxKDX77xaJ_!!272715291-0-picasso.jpg', '颜色分类:150【10双】【春秋款】白色10<=>尺码:【多款多色 舒适百搭】【收藏加购享优先发货】', '2020-11-02 23:15:45'),
(10, 27, '浪莎袜子女中筒袜黑色女士棉袜春秋季纯棉长筒袜加厚秋冬全棉女袜', 228.34, 4.66, 1.00, 'detail.tmall.com', 'https://detail.tmall.com/item.htm?spm=a221t.1812074.goodlist.6.2ecd4208c0zCUI&id=625738745684&skuId=4605298594239', 2, 'https://img.alicdn.com/imgextra/i1/272715291/O1CN01jdo5Kr1oxKDX77xaJ_!!272715291-0-picasso.jpg,https://img.alicdn.com/imgextra/i1/272715291/O1CN01jdo5Kr1oxKDX77xaJ_!!272715291-0-picasso.jpg', '颜色分类:289【5双】【薄款-网眼】白色3粉色2<=>尺码:【多款多色 舒适百搭】【收藏加购享优先发货】', '2020-11-02 23:23:33'),
(11, 30, '厨房长柄清洁刷 家用去污洗锅刷洗碗刷可挂式水槽灶台清洁刷子', 2.33, 4.66, 1.00, 'detail.1688.com', 'https://detail.1688.com/offer/618272962065.html?spm=a261y.7663282.combination.5.67ce463aYMnkyh', 4, 'https://cbu01.alicdn.com/img/ibank/2020/290/323/15491323092_1359988319.jpg,https://cbu01.alicdn.com/img/ibank/2020/290/323/15491323092_1359988319.jpg', '产地:浙江金华<=>货号:锅刷A-13<=>品牌:自主<=>产品类别:清洁刷子<=>价格段:5元以内<=>产品上市时间:2013年夏<=>规格:米色<=>包装:裸妆<=>只数:1<=>重量:40<=>材质:其他<=>贸易属性:内贸<=>是否跨境出口专供货源:否<=>箱装数量:500<=>类别:清洁球<=>专利:否<=>是否进口:否', '2020-12-12 22:11:32'),
(12, 31, '厂家直销小米有品Qin2 Pro智能手机学生机老人机备用机小爱同学4G', 3769.94, 4.66, 1.00, '1688', 'https://detail.1688.com/offer/610947572360.html', 2, 'https://cbu01.alicdn.com/img/ibank/2020/063/830/13235038360_979836901.jpg,https://cbu01.alicdn.com/img/ibank/2020/063/830/13235038360_979836901.jpg', 'Body memory:64GB<=>colour:Porcelain white', '2021-01-18 21:28:26'),
(13, 31, '厂家直销小米有品Qin2 Pro智能手机学生机老人机备用机小爱同学4G', 3723.34, 4.66, 1.00, '1688', 'https://detail.1688.com/offer/610947572360.html', 6, 'https://cbu01.alicdn.com/img/ibank/2020/063/830/13235038360_979836901.jpg,https://cbu01.alicdn.com/img/ibank/2020/063/830/13235038360_979836901.jpg', 'Body memory:64GB<=>colour:Iron gray', '2021-01-18 21:28:26'),
(14, 31, 'Three edge wood folding knife security check creative Mini Key chain key knife military knife portable multi-functional knife package mail', 297.31, 4.66, 1.00, 'taobao-tmall', 'https://item.taobao.com/item.htm?id=520813250866', 1, '//img.alicdn.com/imgextra/i4/2596264565/O1CN01mbmuAB1jaogMUWhv8_!!2596264565.jpg,//img.alicdn.com/imgextra/i4/2596264565/O1CN01mbmuAB1jaogMUWhv8_!!2596264565.jpg', 'Color classification:Bright diamond package with tool card + Chain', '2021-01-18 21:28:26');

-- --------------------------------------------------------

--
-- Table structure for table `orders_log`
--

CREATE TABLE `orders_log` (
  `order_log_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `total_old` varchar(250) DEFAULT NULL,
  `total_new` varchar(250) DEFAULT NULL,
  `remark` text,
  `type` varchar(250) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `orders_log`
--

INSERT INTO `orders_log` (`order_log_id`, `order_id`, `total_old`, `total_new`, `remark`, `type`, `created_at`, `created_by`) VALUES
(1, 25, '50.00', '100.00', 'แก้ไขค่าบริการรีแพ็คลัง/กระสอบ', 'edit_repack_sack', '2021-03-22 22:45:58', '1'),
(2, 25, '212.00', '0', 'แก้ไขน้ำหนัก', 'edit_weight', '2021-03-23 22:46:52', '1'),
(3, 25, '90.00', '462.0', 'แก้ไขค่านำเข้า', 'edit_import_cost', '2021-03-23 22:46:52', '1'),
(4, 25, '234.00', '110', 'แก้ไขความกว้าง', 'edit_wide', '2021-03-23 22:46:52', '1'),
(5, 25, '32.00', '50', 'แก้ไขความยาว', 'edit_longs', '2021-03-23 22:46:52', '1'),
(6, 25, '0.00', '60', 'แก้ไขน้ำหนัก', 'edit_weight', '2021-03-25 21:25:52', '1'),
(7, 25, '462.00', '2,400.0', 'แก้ไขค่านำเข้า', 'edit_import_cost', '2021-03-25 21:25:52', '1'),
(8, 25, '2.00', '2400', 'แก้ไขค่านำเข้า', 'edit_import_cost', '2021-03-25 21:29:25', '1');

-- --------------------------------------------------------

--
-- Table structure for table `orders_payments`
--

CREATE TABLE `orders_payments` (
  `orders_payments_id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `bank_id` int(11) DEFAULT NULL,
  `payment_type` int(11) DEFAULT NULL,
  `file` text,
  `discount` float(15,2) DEFAULT NULL,
  `user_id` varchar(100) DEFAULT NULL,
  `type` int(1) DEFAULT '0',
  `active` int(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders_payments`
--

INSERT INTO `orders_payments` (`orders_payments_id`, `order_id`, `bank_id`, `payment_type`, `file`, `discount`, `user_id`, `type`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`) VALUES
(1, 1, 2, 0, 'uploads/payment/2020/08/12d0b5a35da38dd4203f4e9424753d67.jpg', 6800.00, '1', 0, 0, '2020-08-28 22:51:03', 0, '2020-08-28 22:51:03', 0, '0', NULL, NULL),
(2, 2, 2, 0, 'uploads/payment/2020/08/1d613f19b9a6e156826a79dbd0fa20b9.jpg', 5600.00, '1', 0, 0, '2020-08-28 22:55:15', 0, '2020-08-28 22:55:15', 0, '0', NULL, NULL),
(3, 4, 2, 0, 'uploads/payment/2020/08/860536c845e65d9ba27b3bac2bd801f8.jpg', 1500.00, '1', 0, 0, '2020-08-28 23:24:36', 0, '2020-08-28 23:24:36', 0, '0', NULL, NULL),
(4, 4, 1, 0, 'uploads/payment/2020/08/b53d676847b8a810c2d534ff92f8cbeb.jpg', 1288.00, '1', 0, 1, '2020-08-28 23:39:08', 0, '2020-08-28 23:39:08', 0, '0', NULL, NULL),
(5, 5, 2, 0, 'uploads/payment/2020/08/11ab87d24e325a76dc94189a3ac731bb.jpg', 200.00, '1', 0, 0, '2020-08-28 23:44:59', 0, '2020-08-28 23:44:59', 0, '0', NULL, NULL),
(6, 6, 2, 0, 'uploads/payment/2020/08/1ec7e65a1e718f965c0fbdd588fd6704.jpg', 1500.00, '1', 0, 0, '2020-08-29 00:12:28', 0, '2020-08-29 00:12:28', 0, '0', NULL, NULL),
(7, 2, 2, 0, 'uploads/payment/2020/08/6dc95a7c790111014ca9ff2b95fef235.jpg', 896.00, '1', 0, 1, '2020-08-29 00:15:15', 0, '2020-08-29 00:15:15', 0, '0', NULL, NULL),
(8, 7, 1, 0, 'uploads/payment/2020/09/1d8dfe9fcaf3944fdc21c667ff897fbe.jpg', 2800.00, '1', 0, 0, '2020-09-01 22:25:07', 0, '2020-09-01 22:25:07', 0, '0', NULL, NULL),
(9, 9, 2, 0, 'uploads/payment/2020/09/c66152f1a9a8e3e83b44d36a11c0c8c6.jpg', 19600.00, '1', 0, 0, '2020-09-02 20:48:02', 0, '2020-09-02 20:48:02', 0, '0', NULL, NULL),
(10, 10, 1, 0, 'uploads/payment/2020/09/3484f42f19b18693cc6e89ab03880bc3.jpg', 5600.00, '1', 0, 0, '2020-09-02 20:50:10', 0, '2020-09-02 20:50:10', 0, '0', NULL, NULL),
(11, 11, 2, 0, 'uploads/payment/2020/09/02e239200f6cbee41fd2cd819bd841de.jpg', 8340.00, '1', 0, 0, '2020-09-02 20:54:59', 0, '2020-09-02 20:54:59', 0, '0', NULL, NULL),
(12, 12, 2, 0, 'uploads/payment/2020/09/d14efaa4e2687bf127641b0baf2941e9.jpg', 5560.00, '1', 0, 0, '2020-09-02 20:57:10', 0, '2020-09-02 20:57:10', 0, '0', NULL, NULL),
(13, 13, 1, 0, 'uploads/payment/2020/09/0afb51a67e81323a4ed4c0090544104b.jpg', 2800.00, '1', 0, 0, '2020-09-02 21:18:05', 0, '2020-09-02 21:18:05', 0, '0', NULL, NULL),
(14, 14, 1, 0, 'uploads/payment/2020/09/d8939f23a1ff635631d732792fd7af05.jpg', 16680.00, '1', 0, 0, '2020-09-02 21:22:06', 0, '2020-09-02 21:22:06', 0, '0', NULL, NULL),
(15, 15, 1, 0, 'uploads/payment/2020/09/f3ad735cebbca2b829975d1689dae7ed.jpg', 36400.00, '1', 0, 0, '2020-09-02 21:23:00', 0, '2020-09-02 21:23:00', 0, '0', NULL, NULL),
(16, 16, 1, 0, 'uploads/payment/2020/09/9f1bb5ecf34e04d70747ea2012b87626.jpg', 16800.00, '1', 0, 0, '2020-09-02 21:23:42', 0, '2020-09-02 21:23:42', 0, '0', NULL, NULL),
(17, 17, 1, 0, 'uploads/payment/2020/09/54c27744b86e513ca89cab9f2d4085d9.jpg', 6970.00, '1', 0, 0, '2020-09-02 22:03:34', 0, '2020-09-02 22:03:34', 0, '0', NULL, NULL),
(18, 18, 1, 0, 'uploads/payment/2020/09/e362186dcc432db5a1e717b68e73a602.jpg', 2800.00, '1', 0, 0, '2020-09-16 21:42:28', 0, '2020-09-16 21:42:28', 0, '0', NULL, NULL),
(19, 19, 1, 0, 'uploads/payment/2020/09/53f5a6258bf20dc0ebd235ca50eabfce.jpg', 2800.00, '1', 0, 0, '2020-09-16 21:44:44', 0, '2020-09-16 21:44:44', 0, '0', NULL, NULL),
(20, 20, 1, 0, 'uploads/payment/2020/10/5f5088c03b7946f8412bafadd4342d76.png', 1452.80, '1', 0, 0, '2020-10-31 23:05:21', 0, '2020-10-31 23:05:21', 0, '0', NULL, NULL),
(21, 21, 1, 0, 'uploads/payment/2020/10/27ba5e703e2552c6a9bab4636878ef64.jpg', 625.60, '1', 0, 0, '2020-10-31 23:16:13', 0, '2020-10-31 23:16:13', 0, '0', NULL, NULL),
(22, 22, 1, 0, 'uploads/payment/2020/11/0b65b6c4851d1f0ee6bb56c71b289003.jpg', 2380.00, '1', 0, 0, '2020-11-01 21:46:13', 0, '2020-11-01 21:46:13', 0, '0', NULL, NULL),
(23, 23, 1, 0, 'uploads/payment/2020/11/8be1f74770f4704513be1feca965853e.jpg', 147.00, '1', 0, 0, '2020-11-01 22:02:33', 0, '2020-11-01 22:02:33', 0, '0', NULL, NULL),
(24, 24, 1, 0, 'uploads/payment/2020/11/5d954e45e7fe7dfff574421f1b808990.jpg', 228.34, '1', 0, 0, '2020-11-02 11:40:04', 0, '2020-11-02 11:40:04', 0, '0', NULL, NULL),
(25, 22, 1, 0, 'uploads/payment/2020/11/5950eccdc557910d97b353b7d0679828.jpg', 1036.00, '1', 0, 1, '2020-11-02 15:02:00', 0, '2020-11-02 15:02:00', 0, '0', NULL, NULL),
(26, 25, 1, 0, 'uploads/payment/2020/11/365d63a86891ceefeb104a800aee96d6.jpg', 1890.00, '1', 0, 0, '2020-11-02 21:57:19', 0, '2020-11-02 21:57:19', 0, '0', NULL, NULL),
(27, 26, 1, 0, 'uploads/payment/2020/11/900722b56b13429892b01cf2c055d587.jpg', 956.68, '1', 0, 0, '2020-11-02 23:15:45', 0, '2020-11-02 23:15:45', 0, '0', NULL, NULL),
(28, 27, 1, 0, 'uploads/payment/2020/11/3fb186c5c4d3e69094012e4c56e19ab3.jpg', 1846.68, '1', 0, 0, '2020-11-02 23:23:33', 0, '2020-11-02 23:23:33', 0, '0', NULL, NULL),
(29, 29, 1, 0, 'uploads/payment/2020/11/3ca4f37a2cd61519cb680a3f0fdefb10.jpg', 4190.00, '1', 0, 0, '2020-11-10 19:48:59', 0, '2020-11-10 19:48:59', 0, '0', NULL, NULL),
(30, 29, 2, 0, 'uploads/payment/2020/11/8fe30f5b0a5e9dd289ebb9fa4467feea.png', 236.00, '1', 1, 1, '2020-11-10 21:00:13', 0, '2020-11-10 21:00:13', 0, '0', NULL, NULL),
(31, 30, 1, 0, 'uploads/payment/2020/12/13b05b9c8a67ae3534c746529372764c.jpg', 2809.32, '1', 0, 0, '2020-12-12 22:11:32', 0, '2020-12-12 22:11:32', 0, '0', NULL, NULL),
(32, 31, 1, 0, 'uploads/payment/2021/01/74c4c721619a50a24651a10da7898e3a.jpg', 30177.23, '1', 0, 0, '2021-01-18 21:28:26', 0, '2021-01-18 21:28:26', 0, '0', NULL, NULL),
(33, 34, 1, 0, 'uploads/payment/2021/02/0e3be1ca93b7e4097709a353c795db32.jpg', 1533.00, '1', 1, 1, '2021-02-22 21:37:01', 0, '2021-02-22 21:37:01', 0, '0', NULL, NULL),
(34, 36, 1, 0, 'uploads/payment/2021/02/43555f3f93de32d78ad7fcb091faba0c.jpg', 1506.00, '1', 1, 1, '2021-02-28 13:27:45', 0, '2021-02-28 13:27:45', 0, '0', NULL, NULL),
(35, 37, 1, 0, 'uploads/payment/2021/03/bdf9850d0d4dff89a5d9f613ce1e4af5.jpg', 2800.00, '1', 0, 0, '2021-03-01 15:45:38', 0, '2021-03-01 15:45:38', 0, '0', NULL, NULL),
(36, 42, 1, 0, 'uploads/payment/2021/03/7a129cb296a081b1da49c0713aa076e9.jpg', 1390.00, '1', 0, 0, '2021-03-01 22:42:17', 0, '2021-03-01 22:42:17', 0, '0', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `orders_status_action`
--

CREATE TABLE `orders_status_action` (
  `order_status_action_id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `is_process` int(1) DEFAULT '0',
  `process` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `orders_status_action`
--

INSERT INTO `orders_status_action` (`order_status_action_id`, `order_id`, `title`, `status`, `is_process`, `process`, `created_at`, `created_by`) VALUES
(1, 1, NULL, 3, 0, NULL, '2020-08-28 22:51:03', '0'),
(2, 2, NULL, 3, 0, NULL, '2020-08-28 22:55:15', '0'),
(3, 3, NULL, 1, 0, NULL, '2020-08-28 23:01:56', '0'),
(4, 3, NULL, 2, 0, NULL, '2020-08-28 23:11:09', '1'),
(5, 4, NULL, 3, 0, NULL, '2020-08-28 23:24:36', '0'),
(6, 4, NULL, 4, 0, NULL, '2020-08-28 23:25:37', '1'),
(7, 4, NULL, 3, 0, NULL, '2020-08-28 23:39:08', '0'),
(8, 5, NULL, 3, 0, NULL, '2020-08-28 23:44:59', '0'),
(9, 6, NULL, 3, 0, NULL, '2020-08-29 00:12:28', '0'),
(10, 2, NULL, 3, 0, NULL, '2020-08-29 00:15:15', '0'),
(11, 7, NULL, 3, 0, NULL, '2020-09-01 22:25:07', '0'),
(12, 9, NULL, 3, 0, NULL, '2020-09-02 20:48:02', '0'),
(13, 10, NULL, 3, 0, NULL, '2020-09-02 20:50:10', '0'),
(14, 11, NULL, 3, 0, NULL, '2020-09-02 20:54:59', '0'),
(15, 12, NULL, 3, 0, NULL, '2020-09-02 20:57:10', '0'),
(16, 13, NULL, 3, 0, NULL, '2020-09-02 21:18:05', '0'),
(17, 14, NULL, 3, 0, NULL, '2020-09-02 21:22:06', '0'),
(18, 15, NULL, 3, 0, NULL, '2020-09-02 21:23:00', '0'),
(19, 16, NULL, 3, 0, NULL, '2020-09-02 21:23:42', '0'),
(20, 17, NULL, 3, 0, NULL, '2020-09-02 22:03:34', '0'),
(21, 18, NULL, 3, 0, NULL, '2020-09-16 21:42:28', '0'),
(22, 19, NULL, 3, 0, NULL, '2020-09-16 21:44:44', '0'),
(23, 20, NULL, 3, 0, NULL, '2020-10-31 23:05:21', '0'),
(24, 21, NULL, 3, 0, NULL, '2020-10-31 23:16:13', '0'),
(25, 22, NULL, 3, 0, NULL, '2020-11-01 21:46:13', '0'),
(26, 23, NULL, 3, 0, NULL, '2020-11-01 22:02:33', '0'),
(27, 24, NULL, 3, 0, NULL, '2020-11-02 11:40:04', '0'),
(28, 22, NULL, 4, 0, NULL, '2020-11-02 14:58:11', '1'),
(29, 22, NULL, 2, 0, NULL, '2020-11-02 14:58:57', '1'),
(30, 22, NULL, 3, 0, NULL, '2020-11-02 15:02:00', '0'),
(31, 25, NULL, 3, 0, NULL, '2020-11-02 21:57:19', '0'),
(32, 26, NULL, 3, 0, NULL, '2020-11-02 23:15:45', '0'),
(33, 27, NULL, 3, 0, NULL, '2020-11-02 23:23:33', '0'),
(34, 28, NULL, 1, 0, NULL, '2020-11-10 19:48:00', '0'),
(35, 29, NULL, 3, 0, NULL, '2020-11-10 19:48:59', '0'),
(36, 29, NULL, 3, 0, NULL, '2020-11-10 21:00:13', '0'),
(37, 29, NULL, 6, 0, NULL, '2020-11-10 22:20:49', '1'),
(38, 29, NULL, 6, 0, NULL, '2020-11-16 20:59:50', '1'),
(39, 29, NULL, 6, 0, NULL, '2020-11-16 21:01:26', '1'),
(40, 29, NULL, 6, 0, NULL, '2020-11-16 21:02:11', '1'),
(41, 29, NULL, 6, 0, NULL, '2020-11-16 21:02:27', '1'),
(42, 29, NULL, 6, 0, NULL, '2020-11-16 21:03:44', '1'),
(43, 29, NULL, 6, 0, NULL, '2020-11-16 21:06:46', '1'),
(44, 29, NULL, 6, 0, NULL, '2020-11-16 21:07:08', '1'),
(45, 29, NULL, 6, 0, NULL, '2020-11-16 21:08:44', '1'),
(46, 29, NULL, 6, 0, NULL, '2020-11-16 21:09:26', '1'),
(47, 29, NULL, 6, 0, NULL, '2020-11-16 21:12:15', '1'),
(48, 29, NULL, 6, 0, NULL, '2020-11-16 21:13:15', '1'),
(49, 29, NULL, 4, 0, NULL, '2020-11-16 21:16:19', '1'),
(50, 29, NULL, 6, 0, NULL, '2020-11-16 21:18:31', '1'),
(51, 30, NULL, 3, 0, NULL, '2020-12-12 22:11:35', '0'),
(52, 31, NULL, 3, 0, NULL, '2021-01-18 21:28:28', '0'),
(53, 32, NULL, 1, 0, NULL, '2021-02-02 09:27:39', '0'),
(54, 33, NULL, 1, 0, NULL, '2021-02-04 21:17:16', '0'),
(55, 34, NULL, 1, 0, NULL, '2021-02-04 21:34:00', '0'),
(56, 34, 'ปริ้นใบสั่งสินค้า/ใบติดกล่อง', 0, 1, 'print', '2021-02-04 21:40:13', '1'),
(57, 34, NULL, 2, 0, NULL, '2021-02-22 21:35:21', '1'),
(58, 34, NULL, 3, 0, NULL, '2021-02-22 21:37:01', '0'),
(59, 35, NULL, 1, 0, NULL, '2021-02-28 12:35:44', '0'),
(60, 36, NULL, 1, 0, NULL, '2021-02-28 12:36:13', '0'),
(61, 36, NULL, 3, 0, NULL, '2021-02-28 13:27:45', '0'),
(62, 37, NULL, 3, 0, NULL, '2021-03-01 15:45:40', '0'),
(63, 38, NULL, 1, 0, NULL, '2021-03-01 16:31:25', '0'),
(64, 39, NULL, 1, 0, NULL, '2021-03-01 16:32:43', '0'),
(65, 40, NULL, 1, 0, NULL, '2021-03-01 22:20:58', '0'),
(66, 41, NULL, 1, 0, NULL, '2021-03-01 22:22:09', '0'),
(67, 42, NULL, 3, 0, NULL, '2021-03-01 22:42:19', '0'),
(68, 43, NULL, 1, 0, NULL, '2021-04-29 22:13:24', '0');

-- --------------------------------------------------------

--
-- Table structure for table `orders_user_address_send`
--

CREATE TABLE `orders_user_address_send` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT '',
  `lasname` varchar(255) DEFAULT NULL,
  `address` text,
  `provinces` varchar(255) DEFAULT NULL,
  `amphures` varchar(255) DEFAULT NULL,
  `districts` varchar(255) DEFAULT NULL,
  `zip_code` varchar(15) DEFAULT NULL,
  `tel` varchar(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT '0',
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT '0',
  `recycle` varchar(255) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `permission_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL COMMENT 'ชื่อสิทธิ์',
  `name_en` varchar(255) DEFAULT NULL COMMENT 'ชื่อสิทธิ์',
  `created_at` timestamp NULL DEFAULT NULL COMMENT 'เวลาสร้าง',
  `updated_at` timestamp NULL DEFAULT NULL COMMENT 'เวลาแก้ไข'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`permission_id`, `name`, `name_en`, `created_at`, `updated_at`) VALUES
(1, 'เข้าถึง', 'view', '2018-01-21 10:24:49', '2018-01-21 10:24:52'),
(2, 'สร้าง', 'create', '2018-01-21 10:27:13', '2018-01-21 10:27:15'),
(3, 'แก้ไข', 'edit', '2018-01-21 10:27:40', '2018-01-21 10:27:42'),
(4, 'ลบ', 'delete', '2018-01-21 10:28:20', '2018-01-21 10:28:22');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `role_id` int(11) NOT NULL,
  `moduleId` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`role_id`, `moduleId`, `permission_id`) VALUES
(16, 1, 1),
(16, 1, 2),
(16, 1, 3),
(16, 1, 4),
(16, 295, 1),
(16, 295, 2),
(16, 295, 3),
(16, 295, 4);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `product_id` int(11) NOT NULL,
  `categorie_id` int(11) NOT NULL DEFAULT '0',
  `recommend` int(1) DEFAULT '0',
  `slug` varchar(255) NOT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `title_en` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `excerpt_en` text,
  `detail` text,
  `detail_en` text,
  `start_price` double(11,2) DEFAULT '0.00',
  `price` double(11,2) DEFAULT '0.00',
  `stars` int(1) DEFAULT '0',
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  `meta_title` text,
  `meta_description` text,
  `meta_keyword` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`product_id`, `categorie_id`, `recommend`, `slug`, `lang`, `title`, `title_en`, `excerpt`, `excerpt_en`, `detail`, `detail_en`, `start_price`, `price`, `stars`, `active`, `order`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(1, 10, 1, 'กระเป๋าสัมภาระสุทธิกระเป๋าเดินทางกรณี-รถเข็นสีแดง-แต่ละ-id-จำกัด-จำนวน-5', NULL, 'กระเป๋าสัมภาระสุทธิกระเป๋าเดินทางกรณี รถเข็นสีแดง (แต่ละ ID จำกัด จำนวน 5)', 'Net luggage, luggage, trolley case, red (each ID limited to 5)', 'เอี๊ยมซิลิโคนอ่อนนุ่มมีสุขภาพดีรสจืดง่ายต่อการล้างและแห้ง', 'Soft silicone bib, healthy, tasteless, easy to wash and dry.', '&lt;p&gt;&lt;span xss=&quot;removed&quot;&gt;รายละเอียดสินค้า&lt;/span&gt;&amp;nbsp;: สินค้านี้เข้าร่วมในโครงการเด็กการกุศลและสัญญาที่ผู้ขายได้ว่าการทำธุรกรรมทุกจะบริจาค&lt;/p&gt;&lt;p&gt;RMB 0.02 สำหรับการดูแลเด็กที่น่าสงสาร ผลิตภัณฑ์นี้มียอดบริจาคบริจาค 1,259,463 รายการ บทนำของการ&lt;/p&gt;&lt;p&gt;ใช้การกุศล โครงการนี้เปิดตัวโดยมูลนิธิจีนเพื่อการแก้ไขปัญหาความยากจนในปี 2552 และอาลีบาบาสวัสดิการ&lt;/p&gt;&lt;p&gt;&lt;span xss=&quot;removed&quot; style=&quot;font-size: 20px;&quot;&gt;สาธารณะร่วมกันเปิดตัวเวอร์ชันสากลในปี 2562 โครงการได้เปิดตัวเพื่อสวัสดิการสังคมสำหรับเด็ก&lt;/span&gt;&lt;/p&gt;', '&lt;p&gt;Product Details: This product participates in the Kids Charity Program and promises that every seller has a donation.&lt;/p&gt;&lt;p&gt;RMB 0.02 for the care of poor children. This product has 1,259,463 donations.&lt;/p&gt;&lt;p&gt;Use Charity This project was launched by the China Foundation for Tackling Poverty in 2009 and Alibaba Welfare.&lt;/p&gt;&lt;p&gt;The public jointly launched the international version in 2019, the project was launched for children&#039;s social welfare.&lt;/p&gt;', 0.00, 2800.00, 1, 1, 0, '2020-07-03 20:52:41', '1', '2020-12-25 20:01:16', '1', 0, NULL, NULL, 'กระเป๋าสัมภาระสุทธิกระเป๋าเดินทางกรณี รถเข็นสีแดง (แต่ละ ID จำกัด จำนวน 5)', 'เอี๊ยมซิลิโคนอ่อนนุ่มมีสุขภาพดีรสจืดง่ายต่อการล้างและแห้ง', 'กระเป๋าสัมภาระสุทธิกระเป๋าเดินทางกรณี รถเข็นสีแดง (แต่ละ ID จำกัด จำนวน 5)'),
(2, 10, 1, 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก-กระเป๋าน้ำลายขนาดใหญ่ทิ้ง', NULL, 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง', 'Silicone Baby Bib Three-Dimensional Waterproof Super Soft Food Pouch Baby Bag Large disposable saliva bag', 'เอี๊ยมซิลิโคนอ่อนนุ่มมีสุขภาพดีรสจืดง่ายต่อการล้างและแห้ง', 'Soft silicone bib, healthy, tasteless, easy to wash and dry.', '&lt;p&gt;&lt;span xss=removed&gt;รายละเอียดสินค้า&lt;/span&gt; : สินค้านี้เข้าร่วมในโครงการเด็กการกุศลและสัญญาที่ผู้ขายได้ว่าการทำธุรกรรมทุกจะบริจาค&lt;/p&gt;&lt;p&gt;RMB 0.02 สำหรับการดูแลเด็กที่น่าสงสาร ผลิตภัณฑ์นี้มียอดบริจาคบริจาค 1,259,463 รายการ บทนำของการ&lt;/p&gt;&lt;p&gt;ใช้การกุศล โครงการนี้เปิดตัวโดยมูลนิธิจีนเพื่อการแก้ไขปัญหาความยากจนในปี 2552 และอาลีบาบาสวัสดิการ&lt;/p&gt;&lt;p&gt;สาธารณะร่วมกันเปิดตัวเวอร์ชันสากลในปี 2562 โครงการได้เปิดตัวเพื่อสวัสดิการสังคมสำหรับเด็ก&lt;/p&gt;', '&lt;p&gt;Product Details: This product participates in the Kids Charity Program and promises that every seller has a donation.&lt;/p&gt;&lt;p&gt;RMB 0.02 for the care of poor children. This product has 1,259,463 donations.&lt;/p&gt;&lt;p&gt;Use Charity This project was launched by the China Foundation for Tackling Poverty in 2009 and Alibaba Welfare.&lt;/p&gt;&lt;p&gt;The public jointly launched the international version in 2019, the project was launched for children&#039;s social welfare.&lt;/p&gt;', 2390.00, 1390.00, 1, 1, 0, '2020-07-04 12:11:44', '1', '2020-12-26 20:29:38', '1', 0, NULL, NULL, 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง', 'เอี๊ยมซิลิโคนอ่อนนุ่มมีสุขภาพดีรสจืดง่ายต่อการล้างและแห้ง', 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง'),
(3, 0, 1, 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก-กระเป๋าน้ำลายขนาดใหญ่ทิ้ง-1', NULL, 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง-1', 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง-1 en', 'เอี๊ยมซิลิโคนอ่อนนุ่มมีสุขภาพดีรสจืดง่ายต่อการล้างและแห้ง', 'เอี๊ยมซิลิโคนอ่อนนุ่มมีสุขภาพดีรสจืดง่ายต่อการล้างและแห้ง en', '<p><span xss=\"removed\">รายละเอียดสินค้า</span> : สินค้านี้เข้าร่วมในโครงการเด็กการกุศลและสัญญาที่ผู้ขายได้ว่าการทำธุรกรรมทุกจะบริจาค</p><p>RMB 0.02 สำหรับการดูแลเด็กที่น่าสงสาร ผลิตภัณฑ์นี้มียอดบริจาคบริจาค 1,259,463 รายการ บทนำของการ</p><p>ใช้การกุศล โครงการนี้เปิดตัวโดยมูลนิธิจีนเพื่อการแก้ไขปัญหาความยากจนในปี 2552 และอาลีบาบาสวัสดิการ</p><p>สาธารณะร่วมกันเปิดตัวเวอร์ชันสากลในปี 2562 โครงการได้เปิดตัวเพื่อสวัสดิการสังคมสำหรับเด็ก</p>', '<p><span xss=\"removed\">รายละเอียดสินค้า</span> : สินค้านี้เข้าร่วมในโครงการเด็กการกุศลและสัญญาที่ผู้ขายได้ว่าการทำธุรกรรมทุกจะบริจาค</p><p>RMB 0.02 สำหรับการดูแลเด็กที่น่าสงสาร ผลิตภัณฑ์นี้มียอดบริจาคบริจาค 1,259,463 รายการ บทนำของการ</p><p>ใช้การกุศล โครงการนี้เปิดตัวโดยมูลนิธิจีนเพื่อการแก้ไขปัญหาความยากจนในปี 2552 และอาลีบาบาสวัสดิการ</p><p>สาธารณะร่วมกันเปิดตัวเวอร์ชันสากลในปี 2562 โครงการได้เปิดตัวเพื่อสวัสดิการสังคมสำหรับเด็ก</p>', 1500.00, 1250.00, 0, 0, 0, '2020-07-13 18:30:48', '1', '2020-08-29 17:51:05', '1', 0, NULL, NULL, 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง-1', 'เอี๊ยมซิลิโคนอ่อนนุ่มมีสุขภาพดีรสจืดง่ายต่อการล้างและแห้ง', 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง-1'),
(4, 9, 1, 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก-กระเป๋าน้ำลายขนาดใหญ่ทิ้ง-2', NULL, 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง 2', 'Silicone Baby Bib Three-Dimensional Waterproof Super Soft Food Pouch Baby Bag Large disposable saliva bags 2', 'เอี๊ยมซิลิโคนอ่อนนุ่มมีสุขภาพดีรสจืดง่ายต่อการล้างและแห้ง', 'Soft silicone bib, healthy, tasteless, easy to wash and dry.', '&lt;p&gt;&lt;span xss=&quot;removed&quot;&gt;รายละเอียดสินค้า&lt;/span&gt; : สินค้านี้เข้าร่วมในโครงการเด็กการกุศลและสัญญาที่ผู้ขายได้ว่าการทำธุรกรรมทุกจะบริจาค&lt;/p&gt;&lt;p&gt;RMB 0.02 สำหรับการดูแลเด็กที่น่าสงสาร ผลิตภัณฑ์นี้มียอดบริจาคบริจาค 1,259,463 รายการ บทนำของการ&lt;/p&gt;&lt;p&gt;ใช้การกุศล โครงการนี้เปิดตัวโดยมูลนิธิจีนเพื่อการแก้ไขปัญหาความยากจนในปี 2552 และอาลีบาบาสวัสดิการ&lt;/p&gt;&lt;p&gt;สาธารณะร่วมกันเปิดตัวเวอร์ชันสากลในปี 2562 โครงการได้เปิดตัวเพื่อสวัสดิการสังคมสำหรับเด็ก&lt;/p&gt;', '&lt;p&gt;Product Details: This product participates in the Kids Charity Program and promises that every seller has a donation.&lt;/p&gt;&lt;p&gt;RMB 0.02 for the care of poor children. This product has 1,259,463 donations.&lt;/p&gt;&lt;p&gt;Use Charity This project was launched by the China Foundation for Tackling Poverty in 2009 and Alibaba Welfare.&lt;/p&gt;&lt;p&gt;The public jointly launched the international version in 2019, the project was launched for children&#039;s social welfare.&lt;/p&gt;', 500.00, 500.00, 1, 1, 0, '2020-07-13 18:32:54', '1', '2020-12-26 20:29:30', '1', 0, NULL, NULL, 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง 2', 'เอี๊ยมซิลิโคนอ่อนนุ่มมีสุขภาพดีรสจืดง่ายต่อการล้างและแห้ง', 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง 2'),
(5, 0, 1, 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก-กระเป๋าน้ำลายขนาดใหญ่ทิ้ง-3', NULL, 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง 3', 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง 3 en', 'เอี๊ยมซิลิโคนอ่อนนุ่มมีสุขภาพดีรสจืดง่ายต่อการล้างและแห้ง', 'เอี๊ยมซิลิโคนอ่อนนุ่มมีสุขภาพดีรสจืดง่ายต่อการล้างและแห้ง en', '<p><span xss=\"removed\">รายละเอียดสินค้า</span> : สินค้านี้เข้าร่วมในโครงการเด็กการกุศลและสัญญาที่ผู้ขายได้ว่าการทำธุรกรรมทุกจะบริจาค</p><p>RMB 0.02 สำหรับการดูแลเด็กที่น่าสงสาร ผลิตภัณฑ์นี้มียอดบริจาคบริจาค 1,259,463 รายการ บทนำของการ</p><p>ใช้การกุศล โครงการนี้เปิดตัวโดยมูลนิธิจีนเพื่อการแก้ไขปัญหาความยากจนในปี 2552 และอาลีบาบาสวัสดิการ</p><p>สาธารณะร่วมกันเปิดตัวเวอร์ชันสากลในปี 2562 โครงการได้เปิดตัวเพื่อสวัสดิการสังคมสำหรับเด็ก</p>', '<p><span xss=\"removed\">รายละเอียดสินค้า</span> : สินค้านี้เข้าร่วมในโครงการเด็กการกุศลและสัญญาที่ผู้ขายได้ว่าการทำธุรกรรมทุกจะบริจาค</p><p>RMB 0.02 สำหรับการดูแลเด็กที่น่าสงสาร ผลิตภัณฑ์นี้มียอดบริจาคบริจาค 1,259,463 รายการ บทนำของการ</p><p>ใช้การกุศล โครงการนี้เปิดตัวโดยมูลนิธิจีนเพื่อการแก้ไขปัญหาความยากจนในปี 2552 และอาลีบาบาสวัสดิการ</p><p>สาธารณะร่วมกันเปิดตัวเวอร์ชันสากลในปี 2562 โครงการได้เปิดตัวเพื่อสวัสดิการสังคมสำหรับเด็ก</p>', 1590.00, 1100.00, 0, 0, 0, '2020-07-13 18:33:48', '1', '2020-08-29 17:51:01', '1', 0, NULL, NULL, 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง 3', 'เอี๊ยมซิลิโคนอ่อนนุ่มมีสุขภาพดีรสจืดง่ายต่อการล้างและแห้ง', 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง 3'),
(6, 0, 1, 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก-กระเป๋าน้ำลายขนาดใหญ่ทิ้ง-4', NULL, 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง 4', 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง 4 en', 'เอี๊ยมซิลิโคนอ่อนนุ่มมีสุขภาพดีรสจืดง่ายต่อการล้างและแห้ง', 'เอี๊ยมซิลิโคนอ่อนนุ่มมีสุขภาพดีรสจืดง่ายต่อการล้างและแห้ง en', '<p><span xss=\"removed\">รายละเอียดสินค้า</span> : สินค้านี้เข้าร่วมในโครงการเด็กการกุศลและสัญญาที่ผู้ขายได้ว่าการทำธุรกรรมทุกจะบริจาค</p><p>RMB 0.02 สำหรับการดูแลเด็กที่น่าสงสาร ผลิตภัณฑ์นี้มียอดบริจาคบริจาค 1,259,463 รายการ บทนำของการ</p><p>ใช้การกุศล โครงการนี้เปิดตัวโดยมูลนิธิจีนเพื่อการแก้ไขปัญหาความยากจนในปี 2552 และอาลีบาบาสวัสดิการ</p><p>สาธารณะร่วมกันเปิดตัวเวอร์ชันสากลในปี 2562 โครงการได้เปิดตัวเพื่อสวัสดิการสังคมสำหรับเด็ก</p>', '<p><span xss=\"removed\">รายละเอียดสินค้า</span> : สินค้านี้เข้าร่วมในโครงการเด็กการกุศลและสัญญาที่ผู้ขายได้ว่าการทำธุรกรรมทุกจะบริจาค</p><p>RMB 0.02 สำหรับการดูแลเด็กที่น่าสงสาร ผลิตภัณฑ์นี้มียอดบริจาคบริจาค 1,259,463 รายการ บทนำของการ</p><p>ใช้การกุศล โครงการนี้เปิดตัวโดยมูลนิธิจีนเพื่อการแก้ไขปัญหาความยากจนในปี 2552 และอาลีบาบาสวัสดิการ</p><p>สาธารณะร่วมกันเปิดตัวเวอร์ชันสากลในปี 2562 โครงการได้เปิดตัวเพื่อสวัสดิการสังคมสำหรับเด็ก</p>', 5000.00, 4000.00, 0, 0, 0, '2020-07-13 18:35:24', '1', '2020-08-29 17:51:02', '1', 0, NULL, NULL, 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง 4', 'เอี๊ยมซิลิโคนอ่อนนุ่มมีสุขภาพดีรสจืดง่ายต่อการล้างและแห้ง', 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง 4'),
(7, 0, 0, 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก-กระเป๋าน้ำลายขนาดใหญ่ทิ้ง-5', NULL, 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง 5', 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง 5 en', 'เอี๊ยมซิลิโคนอ่อนนุ่มมีสุขภาพดีรสจืดง่ายต่อการล้างและแห้ง', 'เอี๊ยมซิลิโคนอ่อนนุ่มมีสุขภาพดีรสจืดง่ายต่อการล้างและแห้ง en', '<p><span xss=\"removed\">รายละเอียดสินค้า</span> : สินค้านี้เข้าร่วมในโครงการเด็กการกุศลและสัญญาที่ผู้ขายได้ว่าการทำธุรกรรมทุกจะบริจาค</p><p>RMB 0.02 สำหรับการดูแลเด็กที่น่าสงสาร ผลิตภัณฑ์นี้มียอดบริจาคบริจาค 1,259,463 รายการ บทนำของการ</p><p>ใช้การกุศล โครงการนี้เปิดตัวโดยมูลนิธิจีนเพื่อการแก้ไขปัญหาความยากจนในปี 2552 และอาลีบาบาสวัสดิการ</p><p>สาธารณะร่วมกันเปิดตัวเวอร์ชันสากลในปี 2562 โครงการได้เปิดตัวเพื่อสวัสดิการสังคมสำหรับเด็ก</p>', '<p><span xss=\"removed\">รายละเอียดสินค้า</span> : สินค้านี้เข้าร่วมในโครงการเด็กการกุศลและสัญญาที่ผู้ขายได้ว่าการทำธุรกรรมทุกจะบริจาค</p><p>RMB 0.02 สำหรับการดูแลเด็กที่น่าสงสาร ผลิตภัณฑ์นี้มียอดบริจาคบริจาค 1,259,463 รายการ บทนำของการ</p><p>ใช้การกุศล โครงการนี้เปิดตัวโดยมูลนิธิจีนเพื่อการแก้ไขปัญหาความยากจนในปี 2552 และอาลีบาบาสวัสดิการ</p><p>สาธารณะร่วมกันเปิดตัวเวอร์ชันสากลในปี 2562 โครงการได้เปิดตัวเพื่อสวัสดิการสังคมสำหรับเด็ก</p>', 3000.00, 1500.00, 0, 0, 0, '2020-07-13 18:36:30', '1', '2020-08-29 17:51:02', '1', 0, NULL, NULL, 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง 5', 'เอี๊ยมซิลิโคนอ่อนนุ่มมีสุขภาพดีรสจืดง่ายต่อการล้างและแห้ง', 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง 5'),
(8, 0, 1, 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก-กระเป๋าน้ำลายขนาดใหญ่ทิ้ง-6', NULL, 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง 6', 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง 6 en', 'เอี๊ยมซิลิโคนอ่อนนุ่มมีสุขภาพดีรสจืดง่ายต่อการล้างและแห้ง', 'เอี๊ยมซิลิโคนอ่อนนุ่มมีสุขภาพดีรสจืดง่ายต่อการล้างและแห้ง en', '<p><span xss=\"removed\">รายละเอียดสินค้า</span> : สินค้านี้เข้าร่วมในโครงการเด็กการกุศลและสัญญาที่ผู้ขายได้ว่าการทำธุรกรรมทุกจะบริจาค</p><p>RMB 0.02 สำหรับการดูแลเด็กที่น่าสงสาร ผลิตภัณฑ์นี้มียอดบริจาคบริจาค 1,259,463 รายการ บทนำของการ</p><p>ใช้การกุศล โครงการนี้เปิดตัวโดยมูลนิธิจีนเพื่อการแก้ไขปัญหาความยากจนในปี 2552 และอาลีบาบาสวัสดิการ</p><p>สาธารณะร่วมกันเปิดตัวเวอร์ชันสากลในปี 2562 โครงการได้เปิดตัวเพื่อสวัสดิการสังคมสำหรับเด็ก</p>', '<p><span xss=\"removed\">รายละเอียดสินค้า</span> : สินค้านี้เข้าร่วมในโครงการเด็กการกุศลและสัญญาที่ผู้ขายได้ว่าการทำธุรกรรมทุกจะบริจาค</p><p>RMB 0.02 สำหรับการดูแลเด็กที่น่าสงสาร ผลิตภัณฑ์นี้มียอดบริจาคบริจาค 1,259,463 รายการ บทนำของการ</p><p>ใช้การกุศล โครงการนี้เปิดตัวโดยมูลนิธิจีนเพื่อการแก้ไขปัญหาความยากจนในปี 2552 และอาลีบาบาสวัสดิการ</p><p>สาธารณะร่วมกันเปิดตัวเวอร์ชันสากลในปี 2562 โครงการได้เปิดตัวเพื่อสวัสดิการสังคมสำหรับเด็ก</p>', 250.00, 200.00, 0, 0, 0, '2020-07-13 18:37:24', '1', '2020-08-29 17:51:03', '1', 0, NULL, NULL, 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง 6', 'เอี๊ยมซิลิโคนอ่อนนุ่มมีสุขภาพดีรสจืดง่ายต่อการล้างและแห้ง', 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง 6'),
(9, 0, 1, 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก-กระเป๋าน้ำลายขนาดใหญ่ทิ้ง-7', NULL, 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง 7', 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง 7 en', 'เอี๊ยมซิลิโคนอ่อนนุ่มมีสุขภาพดีรสจืดง่ายต่อการล้างและแห้ง', 'เอี๊ยมซิลิโคนอ่อนนุ่มมีสุขภาพดีรสจืดง่ายต่อการล้างและแห้ง en', '<p><span xss=\"removed\">รายละเอียดสินค้า</span> : สินค้านี้เข้าร่วมในโครงการเด็กการกุศลและสัญญาที่ผู้ขายได้ว่าการทำธุรกรรมทุกจะบริจาค</p><p>RMB 0.02 สำหรับการดูแลเด็กที่น่าสงสาร ผลิตภัณฑ์นี้มียอดบริจาคบริจาค 1,259,463 รายการ บทนำของการ</p><p>ใช้การกุศล โครงการนี้เปิดตัวโดยมูลนิธิจีนเพื่อการแก้ไขปัญหาความยากจนในปี 2552 และอาลีบาบาสวัสดิการ</p><p>สาธารณะร่วมกันเปิดตัวเวอร์ชันสากลในปี 2562 โครงการได้เปิดตัวเพื่อสวัสดิการสังคมสำหรับเด็ก</p>', '<p><span xss=\"removed\">รายละเอียดสินค้า</span> : สินค้านี้เข้าร่วมในโครงการเด็กการกุศลและสัญญาที่ผู้ขายได้ว่าการทำธุรกรรมทุกจะบริจาค</p><p>RMB 0.02 สำหรับการดูแลเด็กที่น่าสงสาร ผลิตภัณฑ์นี้มียอดบริจาคบริจาค 1,259,463 รายการ บทนำของการ</p><p>ใช้การกุศล โครงการนี้เปิดตัวโดยมูลนิธิจีนเพื่อการแก้ไขปัญหาความยากจนในปี 2552 และอาลีบาบาสวัสดิการ</p><p>สาธารณะร่วมกันเปิดตัวเวอร์ชันสากลในปี 2562 โครงการได้เปิดตัวเพื่อสวัสดิการสังคมสำหรับเด็ก</p>', 100.00, 100.00, 0, 0, 0, '2020-07-13 18:38:22', '1', '2020-08-29 17:51:04', '1', 0, NULL, NULL, 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง 7', 'เอี๊ยมซิลิโคนอ่อนนุ่มมีสุขภาพดีรสจืดง่ายต่อการล้างและแห้ง', 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง 7'),
(10, 0, 1, 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก-กระเป๋าน้ำลายขนาดใหญ่ทิ้ง-8', NULL, 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง 8', 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง 8 en', 'เอี๊ยมซิลิโคนอ่อนนุ่มมีสุขภาพดีรสจืดง่ายต่อการล้างและแห้ง', 'เอี๊ยมซิลิโคนอ่อนนุ่มมีสุขภาพดีรสจืดง่ายต่อการล้างและแห้ง en', '<p><span xss=\"removed\">รายละเอียดสินค้า</span> : สินค้านี้เข้าร่วมในโครงการเด็กการกุศลและสัญญาที่ผู้ขายได้ว่าการทำธุรกรรมทุกจะบริจาค</p><p>RMB 0.02 สำหรับการดูแลเด็กที่น่าสงสาร ผลิตภัณฑ์นี้มียอดบริจาคบริจาค 1,259,463 รายการ บทนำของการ</p><p>ใช้การกุศล โครงการนี้เปิดตัวโดยมูลนิธิจีนเพื่อการแก้ไขปัญหาความยากจนในปี 2552 และอาลีบาบาสวัสดิการ</p><p>สาธารณะร่วมกันเปิดตัวเวอร์ชันสากลในปี 2562 โครงการได้เปิดตัวเพื่อสวัสดิการสังคมสำหรับเด็ก</p>', '<p><span xss=\"removed\">รายละเอียดสินค้า</span> : สินค้านี้เข้าร่วมในโครงการเด็กการกุศลและสัญญาที่ผู้ขายได้ว่าการทำธุรกรรมทุกจะบริจาค</p><p>RMB 0.02 สำหรับการดูแลเด็กที่น่าสงสาร ผลิตภัณฑ์นี้มียอดบริจาคบริจาค 1,259,463 รายการ บทนำของการ</p><p>ใช้การกุศล โครงการนี้เปิดตัวโดยมูลนิธิจีนเพื่อการแก้ไขปัญหาความยากจนในปี 2552 และอาลีบาบาสวัสดิการ</p><p>สาธารณะร่วมกันเปิดตัวเวอร์ชันสากลในปี 2562 โครงการได้เปิดตัวเพื่อสวัสดิการสังคมสำหรับเด็ก</p>', 400.00, 350.00, 0, 0, 0, '2020-07-13 18:39:19', '1', '2020-08-29 17:51:05', '1', 0, NULL, NULL, 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง 8', 'เอี๊ยมซิลิโคนอ่อนนุ่มมีสุขภาพดีรสจืดง่ายต่อการล้างและแห้ง', 'ซิลิโคนเด็กทารกเอี๊ยมสามมิติกันน้ำซุปเปอร์อ่อนอาหารอาหารกระเป๋าเด็ก กระเป๋าน้ำลายขนาดใหญ่ทิ้ง 8');

-- --------------------------------------------------------

--
-- Table structure for table `products_image`
--

CREATE TABLE `products_image` (
  `id` int(11) NOT NULL,
  `product_id` int(11) DEFAULT NULL,
  `file_type` varchar(255) DEFAULT NULL,
  `full_path` varchar(255) DEFAULT NULL,
  `file_ext` varchar(255) DEFAULT NULL,
  `file_size` double(11,0) DEFAULT '0',
  `image_width` double(11,0) DEFAULT '0',
  `image_height` double(11,0) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products_image`
--

INSERT INTO `products_image` (`id`, `product_id`, `file_type`, `full_path`, `file_ext`, `file_size`, `image_width`, `image_height`) VALUES
(19, 5, 'image/jpeg', 'uploads/products/2020/07/01c544a1383f8dda9ed6148470b66b39.JPG', '.JPG', 24, 341, 342),
(20, 6, 'image/jpeg', 'uploads/products/2020/07/68afe0430c9756b0f1368733071c2595.JPG', '.JPG', 24, 341, 342),
(15, 2, 'image/jpeg', 'uploads/products/2020/07/4c969c5688bbe29c1a079e081bf3a41c.JPG', '.JPG', 42, 548, 547),
(17, 3, 'image/jpeg', 'uploads/products/2020/07/a249c18aa3fa0dbdd2f60d22d65171f3.JPG', '.JPG', 24, 341, 342),
(18, 4, 'image/jpeg', 'uploads/products/2020/07/6799ab5630acc9b1a69180743fac133c.JPG', '.JPG', 42, 548, 547),
(26, 1, 'image/jpeg', 'uploads/products/2020/08/6e734242093d34ee05db22ce52bb87f5.jpg', '.jpg', 42, 548, 547),
(21, 7, 'image/jpeg', 'uploads/products/2020/07/201857349ac4601e97e014e9103a485c.JPG', '.JPG', 42, 548, 547),
(22, 8, 'image/jpeg', 'uploads/products/2020/07/2bfa5cac517fdf172b36c858284d6444.JPG', '.JPG', 42, 548, 547),
(23, 9, 'image/jpeg', 'uploads/products/2020/07/4c2ed63dfe325e6bf01bdeda416433cd.JPG', '.JPG', 24, 341, 342),
(24, 10, 'image/jpeg', 'uploads/products/2020/07/970499f73b64c24b1a4573c4524c63b7.JPG', '.JPG', 24, 341, 342),
(27, 1, 'image/jpeg', 'uploads/products/2020/08/61f1b118d8daeba50b62e121a3f1821a.jpg', '.jpg', 24, 341, 342);

-- --------------------------------------------------------

--
-- Table structure for table `product_stars`
--

CREATE TABLE `product_stars` (
  `product_star_id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `star_qty` int(11) DEFAULT '0',
  `user_id` int(11) DEFAULT NULL,
  `active` int(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_stars`
--

INSERT INTO `product_stars` (`product_star_id`, `order_id`, `product_id`, `star_qty`, `user_id`, `active`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 9, 1, 2, 1, 1, '2020-09-02 20:48:02', '0', '2020-09-02 20:48:02', '0'),
(2, 10, 1, 1, 1, 1, '2020-09-02 20:50:10', '0', '2020-09-02 20:50:10', '0'),
(3, 11, 2, 2, 1, 1, '2020-09-02 20:54:59', '0', '2020-09-02 20:54:59', '0'),
(4, 12, 2, 1, 1, 1, '2020-09-02 20:57:10', '0', '2020-09-02 20:57:10', '0'),
(5, 13, 1, 1, 1, 1, '2020-09-02 21:18:05', '0', '2020-09-02 21:18:05', '0'),
(6, 14, 2, 3, 1, 1, '2020-09-02 21:22:06', '0', '2020-09-02 21:22:06', '0'),
(7, 15, 1, 3, 1, 1, '2020-09-02 21:23:00', '0', '2020-09-02 21:23:00', '0'),
(8, 16, 1, 2, 1, 1, '2020-09-02 21:23:42', '0', '2020-09-02 21:23:42', '0'),
(9, 17, 1, 1, 1, 1, '2020-09-02 22:03:34', '0', '2020-09-02 22:03:34', '0'),
(10, 17, 2, 1, 1, 1, '2020-09-02 22:03:34', '0', '2020-09-02 22:03:34', '0'),
(11, 18, 1, 1, 1, 1, '2020-09-16 21:42:28', '0', '2020-09-16 21:42:28', '0'),
(12, 19, 1, 1, 1, 1, '2020-09-16 21:44:44', '0', '2020-09-16 21:44:44', '0'),
(13, 20, 2, 1, 1, 1, '2020-10-31 23:05:21', '0', '2020-10-31 23:05:21', '0'),
(14, 21, 4, 1, 1, 1, '2020-10-31 23:16:13', '0', '2020-10-31 23:16:13', '0'),
(15, 22, 2, 1, 1, 1, '2020-11-01 21:46:13', '0', '2020-11-01 21:46:13', '0'),
(16, 22, 4, 1, 1, 1, '2020-11-01 21:46:13', '0', '2020-11-01 21:46:13', '0'),
(17, 25, 2, 1, 1, 1, '2020-11-02 21:57:19', '0', '2020-11-02 21:57:19', '0'),
(18, 25, 4, 1, 1, 1, '2020-11-02 21:57:19', '0', '2020-11-02 21:57:19', '0'),
(19, 26, 4, 1, 1, 1, '2020-11-02 23:15:45', '0', '2020-11-02 23:15:45', '0'),
(20, 27, 2, 1, 1, 1, '2020-11-02 23:23:33', '0', '2020-11-02 23:23:33', '0'),
(21, 29, 1, 1, 1, 1, '2020-11-10 19:48:59', '0', '2020-11-10 19:48:59', '0'),
(22, 29, 2, 1, 1, 1, '2020-11-10 19:48:59', '0', '2020-11-10 19:48:59', '0'),
(23, 30, 1, 1, 1, 1, '2020-12-12 22:11:32', '0', '2020-12-12 22:11:32', '0'),
(24, 37, 1, 1, 1, 1, '2021-03-01 15:45:38', '0', '2021-03-01 15:45:38', '0'),
(25, 42, 2, 1, 1, 1, '2021-03-01 22:42:17', '0', '2021-03-01 22:42:17', '0');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `question_id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `title_en` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `excerpt_en` text,
  `detail` text,
  `detail_en` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  `meta_title` text,
  `meta_description` text,
  `meta_keyword` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`question_id`, `slug`, `lang`, `title`, `title_en`, `excerpt`, `excerpt_en`, `detail`, `detail_en`, `active`, `order`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(1, '', NULL, 'มีวิธีการสมัครสมาชิก การสั่งซื้อสินค้า การแจ้งเติมเงิน ฯลฯ สอนหรือไม่ ?', 'Is there a way to subscribe Order Notifications, top-up, etc. Teach or not?', '<p>คุณลูกค้าสามารถพิมพ์ชื่อสินค้าเป็นภาษาไทยที่ช่องค้นหาบนหน้าเว็บ www.ifcexpressshipping.com ได้เลยค่ะ หรือ หากคุณลูกค้าต้องการสอบถามคำศัพท์ภาษาจีน สามารถสอบถามได้ทาง </p><p>E-Mail : ifcexpressshipping@gmail.com, Line id : @409schql</p>', '<p>Customers can type the name of the product in Thai at the search box on the web page. www.ifcexpressshipping.com, or if you want to inquire Chinese words Can inquire via</p><p>E-Mail: ifcexpressshipping@gmail.com, Line id: @ 409schql</p>', NULL, NULL, 1, 0, '2020-06-29 18:30:38', '1', '2020-08-29 18:16:09', '1', 0, NULL, NULL, NULL, NULL, NULL),
(2, '', NULL, 'ไม่รู้ภาษาจีน จะสั่งได้ไหม ต้องทำยังไง ?', 'I don\'t know Chinese, can I order it? How to do it?', '<p>คุณลูกค้าสามารถพิมพ์ชื่อสินค้าเป็นภาษาไทยที่ช่องค้นหาบนหน้าเว็บ www.ifcexpressshipping.com ได้เลยค่ะ หรือ หากคุณลูกค้าต้องการสอบถามคำศัพท์ภาษาจีน สามารถสอบถามได้ทาง </p><p>E-Mail : ifcexpressshipping@gmail.com, Line id : @409schql</p>', '<p>Customers can type the name of the product in Thai at the search box on the web page. www.ifcexpressshipping.com, or if you want to inquire Chinese words Can inquire via</p><p>E-Mail: ifcexpressshipping@gmail.com, Line id: @ 409schql</p>', NULL, NULL, 1, 9, '2020-06-29 18:31:41', '1', '2020-08-29 18:18:17', '1', 0, NULL, NULL, NULL, NULL, NULL),
(3, '', NULL, 'จะได้รับสินค้าเมื่อใด ?', 'When will I receive the products?', '<p>คุณลูกค้าสามารถพิมพ์ชื่อสินค้าเป็นภาษาไทยที่ช่องค้นหาบนหน้าเว็บ www.ifcexpressshipping.com ได้เลยค่ะ หรือ หากคุณลูกค้าต้องการสอบถามคำศัพท์ภาษาจีน สามารถสอบถามได้ทาง </p><p>E-Mail : ifcexpressshipping@gmail.com, Line id : @409schql</p>', '<p>Customers can type the name of the product in Thai at the search box on the web page. www.ifcexpressshipping.com, or if you want to inquire Chinese words Can inquire via</p><p>E-Mail: ifcexpressshipping@gmail.com, Line id: @ 409schql</p>', NULL, NULL, 1, 2, '2020-06-29 18:32:14', '1', '2020-08-29 18:14:31', '1', 0, NULL, NULL, NULL, NULL, NULL),
(4, '', NULL, 'บริษัทรับชำระเงินผ่านช่องทางไหนบ้าง ?', 'What are the payment methods that the company accepts?', '<p>คุณลูกค้าสามารถพิมพ์ชื่อสินค้าเป็นภาษาไทยที่ช่องค้นหาบนหน้าเว็บ www.ifcexpressshipping.com ได้เลยค่ะ หรือ หากคุณลูกค้าต้องการสอบถามคำศัพท์ภาษาจีน สามารถสอบถามได้ทาง </p><p>E-Mail : ifcexpressshipping@gmail.com, Line id : @409schql</p>', '<p>Customers can type the name of the product in Thai at the search box on the web page. www.ifcexpressshipping.com, or if you want to inquire Chinese words Can inquire via</p><p>E-Mail: ifcexpressshipping@gmail.com, Line id: @ 409schql</p>', NULL, NULL, 1, 5, '2020-06-29 18:32:42', '1', '2020-08-29 18:15:32', '1', 0, NULL, NULL, NULL, NULL, NULL),
(5, '', NULL, 'ต้องชำระเงินตอนไหน ?', 'When do I have to pay?', '<p>คุณลูกค้าสามารถพิมพ์ชื่อสินค้าเป็นภาษาไทยที่ช่องค้นหาบนหน้าเว็บ www.ifcexpressshipping.com ได้เลยค่ะ หรือ หากคุณลูกค้าต้องการสอบถามคำศัพท์ภาษาจีน สามารถสอบถามได้ทาง </p><p>E-Mail : ifcexpressshipping@gmail.com, Line id : @409schql</p>', '<p>Customers can type the name of the product in Thai at the search box on the web page. www.ifcexpressshipping.com, or if you want to inquire Chinese words Can inquire via</p><p>E-Mail: ifcexpressshipping@gmail.com, Line id: @ 409schql</p>', NULL, NULL, 1, 1, '2020-06-29 18:33:05', '1', '2020-08-29 18:15:12', '1', 0, NULL, NULL, NULL, NULL, NULL),
(6, '', NULL, 'จะทราบตอนไหนว่าสินค้าสั่งซื้อได้หรือไม่ได้ ?', 'When will I know whether the product can be ordered or not?', '<p>คุณลูกค้าสามารถพิมพ์ชื่อสินค้าเป็นภาษาไทยที่ช่องค้นหาบนหน้าเว็บ www.ifcexpressshipping.com ได้เลยค่ะ หรือ หากคุณลูกค้าต้องการสอบถามคำศัพท์ภาษาจีน สามารถสอบถามได้ทาง </p><p>E-Mail : ifcexpressshipping@gmail.com, Line id : @409schql</p>', '<p>Customers can type the name of the product in Thai at the search box on the web page. www.ifcexpressshipping.com, or if you want to inquire Chinese words Can inquire via</p><p>E-Mail: ifcexpressshipping@gmail.com, Line id: @ 409schql</p>', NULL, NULL, 1, 4, '2020-06-29 18:33:28', '1', '2020-08-29 18:14:02', '1', 0, NULL, NULL, NULL, NULL, NULL),
(7, '', NULL, 'การคิดค่าขนส่งจากจีนมาไทย คิดยังไง ?', 'Is there a way to subscribe Order Notifications, top-up, etc. Teach or not?', '&lt;p&gt;คุณลูกค้าสามารถพิมพ์ชื่อสินค้าเป็นภาษาไทยที่ช่องค้นหาบนหน้าเว็บ www.ifcexpressshipping.com ได้เลยค่ะ หรือ หากคุณลูกค้าต้องการสอบถามคำศัพท์ภาษาจีน สามารถสอบถามได้ทาง&amp;nbsp;&lt;/p&gt;&lt;p&gt;E-Mail : ifcexpressshipping@gmail.com, Line id : &lt;span style=&quot;font-size: 14px;&quot;&gt;@409schql&lt;/span&gt;&lt;/p&gt;', '&lt;p&gt;Customers can type the name of the product in Thai at the search box on the web page. www.ifcexpressshipping.com, or if you want to inquire Chinese words Can inquire via&lt;/p&gt;&lt;p&gt;E-Mail: ifcexpressshipping@gmail.com, Line id: @ 409schql&lt;/p&gt;', NULL, NULL, 1, 3, '2020-06-29 18:33:47', '1', '2020-09-02 16:24:26', '1', 0, NULL, NULL, NULL, NULL, NULL),
(8, '', NULL, 'สินค้าอะไรบ้างที่บริษัทฯ ไม่รับสั่งซื้อ ?', 'What products the company Do not accept orders?', '<p>คุณลูกค้าสามารถพิมพ์ชื่อสินค้าเป็นภาษาไทยที่ช่องค้นหาบนหน้าเว็บ www.ifcexpressshipping.com ได้เลยค่ะ หรือ หากคุณลูกค้าต้องการสอบถามคำศัพท์ภาษาจีน สามารถสอบถามได้ทาง </p><p>E-Mail : ifcexpressshipping@gmail.com, Line id : @409schql</p>', '<p>คุณลูกค้าสามารถพิมพ์ชื่อสินค้าเป็นภาษาไทยที่ช่องค้นหาบนหน้าเว็บ www.ifcexpressshipping.com ได้เลยค่ะ หรือ หากคุณลูกค้าต้องการสอบถามคำศัพท์ภาษาจีน สามารถสอบถามได้ทาง </p><p>E-Mail : ifcexpressshipping@gmail.com, Line id : @409schql</p>', NULL, NULL, 1, 6, '2020-06-29 18:34:08', '1', '2020-08-29 18:16:44', '1', 0, NULL, NULL, NULL, NULL, NULL),
(9, '', NULL, 'หากได้รับสินค้าไม่ครบ หรือ ได้รับสินค้าผิดแบบ ผิดสี ผิดไซส์ ต้องทำอย่างไร ?', 'If I received a missing item or received a wrong item in a wrong color, size, what should I do?', '<p>คุณลูกค้าสามารถพิมพ์ชื่อสินค้าเป็นภาษาไทยที่ช่องค้นหาบนหน้าเว็บ www.ifcexpressshipping.com ได้เลยค่ะ หรือ หากคุณลูกค้าต้องการสอบถามคำศัพท์ภาษาจีน สามารถสอบถามได้ทาง </p><p>E-Mail : ifcexpressshipping@gmail.com, Line id : @409schql</p>', '<p>Customers can type the name of the product in Thai at the search box on the web page. www.ifcexpressshipping.com, or if you want to inquire Chinese words Can inquire via</p><p>E-Mail: ifcexpressshipping@gmail.com, Line id: @ 409schql</p>', NULL, NULL, 1, 8, '2020-06-29 18:34:36', '1', '2020-08-29 18:17:48', '1', 0, NULL, NULL, NULL, NULL, NULL),
(10, '', NULL, 'หากต้องการร้องเรียน หรือมีข้อเสนอแนะต้องทำอย่างไร ?', 'If you want to make a complaint Or have suggestions, what to do?', '<p>คุณลูกค้าสามารถพิมพ์ชื่อสินค้าเป็นภาษาไทยที่ช่องค้นหาบนหน้าเว็บ www.ifcexpressshipping.com ได้เลยค่ะ หรือ หากคุณลูกค้าต้องการสอบถามคำศัพท์ภาษาจีน สามารถสอบถามได้ทาง </p><p>E-Mail : ifcexpressshipping@gmail.com, Line id : @409schql</p>', '<p>Customers can type the name of the product in Thai at the search box on the web page. www.ifcexpressshipping.com, or if you want to inquire Chinese words Can inquire via</p><p>E-Mail: ifcexpressshipping@gmail.com, Line id: @ 409schql</p>', NULL, NULL, 1, 7, '2020-06-29 18:34:59', '1', '2020-08-29 18:17:16', '1', 0, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `repo`
--

CREATE TABLE `repo` (
  `repoId` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `detail` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  `meta_title` text,
  `meta_description` text,
  `meta_keyword` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `role_id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  `remark` text,
  `active` tinyint(1) UNSIGNED DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `create_by` int(11) UNSIGNED DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_by` int(11) UNSIGNED DEFAULT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  `visible` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_id`, `name`, `remark`, `active`, `created_at`, `create_by`, `updated_at`, `update_by`, `recycle`, `recycle_at`, `recycle_by`, `visible`) VALUES
(1, 'ผู้ดูแลระบบ', 'บริหารจัดการระบบ', 1, '2016-07-13 10:55:21', NULL, '2019-05-16 07:40:14', 1, 0, NULL, NULL, 0),
(16, 'ผู้อัพเดทข้อมูลเว็บไซต์', 'บริหารจัดการข้อมูลเว็บไซต์', 1, '2018-08-05 00:20:00', 22, '2019-12-26 05:13:17', 1, 0, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `service_id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `title_en` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `excerpt_en` text,
  `detail` text,
  `detail_en` text,
  `file` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  `meta_title` text,
  `meta_description` text,
  `meta_keyword` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`service_id`, `slug`, `lang`, `title`, `title_en`, `excerpt`, `excerpt_en`, `detail`, `detail_en`, `file`, `active`, `order`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(1, 'โลจิสติกส์เพื่อการพาณิชย์อิเล็กทรอนิกส์', NULL, 'โลจิสติกส์เพื่อการพาณิชย์อิเล็กทรอนิกส์', 'Logistics for e-commerce', 'การจัดการโลจิสติกส์เพื่อการส่งออก หมายถึง กระบวนการจัดการวางแผน การปฎิบัติงานและการควบคุมอย่างมีประสิทธิภาพ และประสิทธิผลทั้งล่วงหน้า และย้อนกลับของการเคลื่อนย้าย และการจัดเก็บสินค้า การบริการและสารสนเทศ ที่เกี่ยวข้องตั้งแต่จุดกำเนิดจนถึงจุดการบริโภคสินค้า เพื่อตอบสนองความต้องการของลูกค้าและการส่งออกไปยังต่างประเทศให้มีศักยภาพในการแข่งขันกับต่างประเทศ', 'Export logistics management refers to the planning management process. Operation and control efficiency And effectiveness both in advance And reverse of moving And product storage Service and Information Related from the point of origin to the point of consumption To meet the needs of customers and export to foreign countries to have the potential to compete with foreign countries.', '&lt;p&gt;การจัดการโลจิสติกส์เพื่อการส่งออก หมายถึง กระบวนการจัดการวางแผน การปฎิบัติงานและการควบคุมอย่างมี&lt;/p&gt;&lt;p&gt;ประสิทธิภาพ และประสิทธิผลทั้งล่วงหน้า และย้อนกลับของการเคลื่อนย้าย และการจัดเก็บสินค้า การบริการและ&lt;/p&gt;&lt;p&gt;สารสนเทศ ที่เกี่ยวข้องตั้งแต่จุดกำเนิดจนถึงจุดการบริโภคสินค้า เพื่อตอบสนองความต้องการของลูกค้าและ&lt;/p&gt;&lt;p&gt;&lt;span style=&quot;font-size: 14px;&quot;&gt;การส่งออกไปยังต่างประเทศให้มีศักยภาพในการแข่งขันกับต่างประเทศ&lt;/span&gt;&lt;/p&gt;', '&lt;p&gt;Export logistics management refers to the planning management process. Operation and control&lt;/p&gt;&lt;p&gt;Efficiency and efficiency both in advance And reverse of moving And storage of products, services and&lt;/p&gt;&lt;p&gt;Relevant information from the point of origin to the point of consumption To meet the needs of customers and&lt;/p&gt;&lt;p&gt;Export to foreign countries to have the potential to compete with foreign countries&lt;/p&gt;', 'uploads/services/2020/06/ba59c556deacd7d978efb88c48a935ee.JPG', 1, 0, '2020-06-29 15:49:57', '1', '2020-09-02 16:22:49', '1', 0, NULL, NULL, 'โลจิสติกส์เพื่อการพาณิชย์อิเล็กทรอนิกส์', ' การจัดการโลจิสติกส์เพื่อการส่งออก หมายถึง กระบวนการจัดการวางแผน การปฎิบัติงานและการควบคุมอย่างมีประสิทธิภาพ และประสิทธิผลทั้งล่วงหน้า และย้อนกลับของการเคลื่อนย้าย และการจัดเก็บสินค้า การบริการและสารสนเทศ ที่เกี่ยวข้องตั้งแต่จุดกำเนิดจนถึงจุดการบริโภคสินค้า เพื่อตอบสนองความต้องการของลูกค้าและการส่งออกไปยังต่างประเทศให้มีศักยภาพในการแข่งขันกับต่างประเทศ', 'โลจิสติกส์เพื่อการพาณิชย์อิเล็กทรอนิกส์'),
(2, 'บริการโอนเงินหยวน-เติมเงิน-alipay-โอนไปธนาคารจีน', NULL, 'บริการโอนเงินหยวน , เติมเงิน Alipay, โอนไปธนาคารจีน', 'RMB remittance, Alipay top up, Chinese bank transfer', 'การจัดการโลจิสติกส์เพื่อการส่งออก หมายถึง กระบวนการจัดการวางแผน การปฎิบัติงานและการควบคุมอย่างมีประสิทธิภาพ และประสิทธิผลทั้งล่วงหน้า และย้อนกลับของการเคลื่อนย้าย และการจัดเก็บสินค้า การบริการและสารสนเทศ ที่เกี่ยวข้องตั้งแต่จุดกำเนิดจนถึงจุดการบริโภคสินค้า เพื่อตอบสนองความต้องการของลูกค้าและการส่งออกไปยังต่างประเทศให้มีศักยภาพในการแข่งขันกับต่างประเทศ', 'Export logistics management refers to the planning management process. Operation and control efficiency And effectiveness both in advance And reverse of moving And product storage Service and Information Related from the point of origin to the point of consumption To meet the needs of customers and export to foreign countries to have the potential to compete with foreign countries.', 'การจัดการโลจิสติกส์เพื่อการส่งออก หมายถึง กระบวนการจัดการวางแผน การปฎิบัติงานและการควบคุมอย่างมีประสิทธิภาพ และประสิทธิผลทั้งล่วงหน้า และย้อนกลับของการเคลื่อนย้าย และการจัดเก็บสินค้า การบริการและสารสนเทศ ที่เกี่ยวข้องตั้งแต่จุดกำเนิดจนถึงจุดการบริโภคสินค้า เพื่อตอบสนองความต้องการของลูกค้าและการส่งออกไปยังต่างประเทศให้มีศักยภาพในการแข่งขันกับต่างประเทศ', '<p>Export logistics management refers to the planning management process. Operation and control efficiency And effectiveness both in advance And reverse of moving And product storage Service and Information Related from the point of origin to the point of consumption To meet the needs of customers and export to foreign countries to have the potential to compete with foreign countries.<br></p>', 'uploads/services/2020/06/c7ae2941b5c12fc683ea97908447516f.JPG', 1, 0, '2020-06-29 18:08:07', '1', '2020-08-29 18:01:45', '1', 0, NULL, NULL, 'บริการโอนเงินหยวน , เติมเงิน Alipay, โอนไปธนาคารจีน', 'การจัดการโลจิสติกส์เพื่อการส่งออก หมายถึง กระบวนการจัดการวางแผน การปฎิบัติงานและการควบคุมอย่างมีประสิทธิภาพ และประสิทธิผลทั้งล่วงหน้า และย้อนกลับของการเคลื่อนย้าย และการจัดเก็บสินค้า การบริการและสารสนเทศ ที่เกี่ยวข้องตั้งแต่จุดกำเนิดจนถึงจุดการบริโภคสินค้า เพื่อตอบสนองความต้องการของลูกค้าและการส่งออกไปยังต่างประเทศให้มีศักยภาพในการแข่งขันกับต่างประเทศ', 'บริการโอนเงินหยวน , เติมเงิน Alipay, โอนไปธนาคารจีน'),
(3, 'นำเข้าแบบเหมาตู้สินค้า-การบริการคลังสินค้า', NULL, 'นำเข้าแบบเหมาตู้สินค้า การบริการคลังสินค้า', 'Imported as a container Warehouse service', 'การจัดการโลจิสติกส์เพื่อการส่งออก หมายถึง กระบวนการจัดการวางแผน การปฎิบัติงานและการควบคุมอย่างมีประสิทธิภาพ และประสิทธิผลทั้งล่วงหน้า และย้อนกลับของการเคลื่อนย้าย และการจัดเก็บสินค้า การบริการและสารสนเทศ ที่เกี่ยวข้องตั้งแต่จุดกำเนิดจนถึงจุดการบริโภคสินค้า เพื่อตอบสนองความต้องการของลูกค้าและการส่งออกไปยังต่างประเทศให้มีศักยภาพในการแข่งขันกับต่างประเทศ', 'Export logistics management refers to the planning management process. Operation and control efficiency And effectiveness both in advance And reverse of moving And product storage Service and Information Related from the point of origin to the point of consumption To meet the needs of customers and export to foreign countries to have the potential to compete with foreign countries.', '<p><span xss=removed>การจัดการโลจิสติกส์เพื่อการส่งออก หมายถึง กระบวนการจัดการวางแผน การปฎิบัติงานและการควบคุมอย่างมีประสิทธิภาพ และประสิทธิผลทั้งล่วงหน้า และย้อนกลับของการเคลื่อนย้าย และการจัดเก็บสินค้า การบริการและสารสนเทศ ที่เกี่ยวข้องตั้งแต่จุดกำเนิดจนถึงจุดการบริโภคสินค้า เพื่อตอบสนองความต้องการของลูกค้าและการส่งออกไปยังต่างประเทศให้มีศักยภาพในการแข่งขันกับต่างประเทศ</span><br></p>', '<p>Export logistics management refers to the planning management process. Operation and control efficiency And effectiveness both in advance And reverse of moving And product storage Service and Information Related from the point of origin to the point of consumption To meet the needs of customers and export to foreign countries to have the potential to compete with foreign countries.<br></p>', 'uploads/services/2020/06/8b75c875d9c0e9e26f2594f0ccb719c8.JPG', 1, 0, '2020-06-29 18:09:35', '1', '2020-08-29 18:02:22', '1', 0, NULL, NULL, 'นำเข้าแบบเหมาตู้สินค้า การบริการคลังสินค้า', 'การจัดการโลจิสติกส์เพื่อการส่งออก หมายถึง กระบวนการจัดการวางแผน การปฎิบัติงานและการควบคุมอย่างมีประสิทธิภาพ และประสิทธิผลทั้งล่วงหน้า และย้อนกลับของการเคลื่อนย้าย และการจัดเก็บสินค้า การบริการและสารสนเทศ ที่เกี่ยวข้องตั้งแต่จุดกำเนิดจนถึงจุดการบริโภคสินค้า เพื่อตอบสนองความต้องการของลูกค้าและการส่งออกไปยังต่างประเทศให้มีศักยภาพในการแข่งขันกับต่างประเทศ', 'นำเข้าแบบเหมาตู้สินค้า การบริการคลังสินค้า');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
  `status_id` int(11) NOT NULL,
  `title` varchar(250) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime NOT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`status_id`, `title`, `active`, `order`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`) VALUES
(1, 'รอประเมินราคา', 1, 0, '2019-07-10 22:36:42', '1', '2020-07-05 12:35:10', '1', 0, NULL, NULL),
(2, 'รอชำระค่าสินค้า / ค่านำเข้า', 1, 0, '2019-07-10 22:37:29', '1', '2020-07-05 12:35:20', '1', 0, NULL, NULL),
(3, 'รอตรวจสอบหลักฐานการชำระ', 1, 0, '2019-07-10 22:37:49', '1', '2020-07-05 12:35:29', '1', 0, NULL, NULL),
(4, 'จัดส่งจากจีนแล้ว', 1, 0, '2019-07-10 22:38:03', '1', '2020-07-05 12:35:41', '1', 0, NULL, NULL),
(5, 'รอการชำระค่าจัดส่งในประเทศไทย', 1, 0, '2019-07-10 22:38:22', '1', '2020-07-05 12:35:53', '1', 0, NULL, NULL),
(6, 'จัดส่งภายในประเทศไทยแล้ว', 1, 0, '2019-07-10 22:38:40', '1', '2020-07-05 12:36:10', '1', 0, NULL, NULL),
(7, 'รายการตีกลับ', 1, 0, '2019-07-10 22:38:53', '1', '2019-07-10 22:40:24', '1', 0, '2019-07-10 22:40:16', 1),
(8, 'ยกเลิกรายการ', 1, 0, '2019-07-10 22:38:53', '1', '2019-07-10 22:40:24', '1', 0, '2019-07-10 22:40:16', 1),
(9, 'จัดส่งสินค้าไปที่อยู่ลูกค้าแล้ว', 1, 0, '2021-02-01 21:57:35', '1', '2021-02-01 22:03:15', '1', 0, NULL, NULL),
(10, 'ติดต่อเจ้าหน้าที่', 1, 0, '2021-02-01 21:58:20', '1', '2021-02-01 22:03:16', '1', 0, NULL, NULL),
(11, 'สินค้ามีปัญหา / ยกเลิกรายการ / รายการสินค้าตีกลับ', 1, 0, '2021-02-01 21:59:20', '1', '2021-02-01 22:03:17', '1', 0, NULL, NULL),
(12, 'ดำเนินการเสร็จสิ้น', 1, 0, '2021-02-01 21:59:45', '1', '2021-02-01 22:03:18', '1', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `stting_line`
--

CREATE TABLE `stting_line` (
  `line_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `token` varchar(250) DEFAULT NULL,
  `line_url` varchar(250) DEFAULT NULL,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `type_transportations`
--

CREATE TABLE `type_transportations` (
  `type_transportation_id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `title_en` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `excerpt_en` text,
  `detail` text,
  `detail_en` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  `meta_title` text,
  `meta_description` text,
  `meta_keyword` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `type_transportations`
--

INSERT INTO `type_transportations` (`type_transportation_id`, `slug`, `lang`, `title`, `title_en`, `excerpt`, `excerpt_en`, `detail`, `detail_en`, `active`, `order`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(1, '', NULL, 'ขนส่งทางรถ', 'Transport by car', 'ขนส่งทางรถ', 'Transport by car', NULL, NULL, 1, 0, '2020-07-02 23:26:09', '1', '2020-08-29 23:09:26', '1', 0, NULL, NULL, NULL, NULL, NULL),
(2, '', NULL, 'ขนส่งทางเรือ', 'Transport ship', 'ขนส่งทางเรือ', 'Transport ship', NULL, NULL, 1, 0, '2020-07-02 23:26:35', '1', '2020-08-29 23:10:11', '1', 0, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `type_transportations_cust`
--

CREATE TABLE `type_transportations_cust` (
  `type_transportation_cust_id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `title_en` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `excerpt_en` text,
  `detail` text,
  `detail_en` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  `meta_title` text,
  `meta_description` text,
  `meta_keyword` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `type_transportations_cust`
--

INSERT INTO `type_transportations_cust` (`type_transportation_cust_id`, `slug`, `lang`, `title`, `title_en`, `excerpt`, `excerpt_en`, `detail`, `detail_en`, `active`, `order`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(1, '', NULL, 'ลูกค้าทั่วไป', 'General customers', 'ลูกค้าทั่วไป', 'General customers', NULL, NULL, 1, 0, '2020-12-25 20:41:18', '1', '2020-12-25 20:41:21', '1', 0, NULL, NULL, NULL, NULL, NULL),
(2, '', NULL, 'ลูกค้า VIP', 'VIP customers', 'ลูกค้า VIP', 'VIP Customers', NULL, NULL, 1, 0, '2020-12-25 20:42:07', '1', '2020-12-25 20:42:10', '1', 0, NULL, NULL, NULL, NULL, NULL),
(3, '', NULL, 'ลูกค้าเก่า', 'Old customers', 'ลูกค้าเก่า', 'Old customers', NULL, NULL, 1, 0, '2020-12-25 20:43:18', '1', '2020-12-25 20:43:20', '1', 0, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `type_transportations_setting`
--

CREATE TABLE `type_transportations_setting` (
  `type_transportations_setting_id` int(11) NOT NULL,
  `type_transportation_id` int(11) NOT NULL DEFAULT '0',
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `title_en` varchar(250) DEFAULT NULL,
  `kg_min` varchar(100) DEFAULT NULL,
  `kg_max` varchar(100) DEFAULT NULL,
  `queue_min` varchar(100) DEFAULT '0',
  `queue_max` varchar(100) DEFAULT '0',
  `price_send` double(100,0) DEFAULT '0',
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `type_transportations_setting`
--

INSERT INTO `type_transportations_setting` (`type_transportations_setting_id`, `type_transportation_id`, `lang`, `title`, `title_en`, `kg_min`, `kg_max`, `queue_min`, `queue_max`, `price_send`, `active`, `order`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`) VALUES
(1, 0, NULL, 'สินค้าทั่วไป', 'General merchandise', '0', '40', '0', '7000', 1000, 1, 0, '2020-07-03 00:12:10', '1', '2020-08-29 23:11:40', '1', 0, NULL, NULL),
(2, 0, NULL, 'สินค้าพิเศษ', 'Special products', '0', '110', '0', '15000', 2000, 1, 0, '2020-07-03 00:14:27', '1', '2020-08-29 23:11:55', '1', 0, NULL, NULL),
(3, 0, NULL, 'สินค้า อย.', 'FDA products', '0', '110', '0', '11000', 5000, 0, 0, '2020-07-03 00:15:13', '1', '2020-12-25 22:03:44', '1', 1, '2020-12-25 22:03:44', 1),
(4, 0, NULL, 'สินค้า มอก.', 'TIS product', '0', '60', '0', '9000', 4000, 0, 0, '2020-07-03 00:16:00', '1', '2020-12-25 22:03:44', '1', 1, '2020-12-25 22:03:44', 1),
(5, 0, NULL, 'สินค้า มอก.', 'TIS product', NULL, NULL, '0', '0', 0, 1, 0, '2020-08-30 12:13:05', '1', '2020-12-29 00:01:05', '1', 0, NULL, NULL),
(6, 0, NULL, 'สินค้า อย.', 'FDA products', NULL, NULL, '0', '0', 0, 1, 0, '2020-08-30 12:14:28', '1', '2020-12-29 00:01:09', '1', 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `type_transportations_setting_list`
--

CREATE TABLE `type_transportations_setting_list` (
  `type_transportations_setting_list_id` int(11) NOT NULL,
  `type_transportations_setting_id` int(11) NOT NULL,
  `type_transportation_id` int(11) NOT NULL DEFAULT '0',
  `type_transportation_cust_id` int(11) NOT NULL DEFAULT '0',
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `title_en` varchar(250) DEFAULT NULL,
  `kg_min` varchar(100) DEFAULT NULL,
  `kg_max` varchar(100) DEFAULT NULL,
  `queue_min` varchar(100) DEFAULT '0',
  `queue_max` varchar(100) DEFAULT '0',
  `price_send` double(100,0) DEFAULT '0',
  `type` int(1) NOT NULL DEFAULT '0' COMMENT '0 = คิว, 1 = kg',
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `type_transportations_setting_list`
--

INSERT INTO `type_transportations_setting_list` (`type_transportations_setting_list_id`, `type_transportations_setting_id`, `type_transportation_id`, `type_transportation_cust_id`, `lang`, `title`, `title_en`, `kg_min`, `kg_max`, `queue_min`, `queue_max`, `price_send`, `type`, `active`, `order`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`) VALUES
(1, 1, 1, 1, NULL, NULL, NULL, '0', '0', '0', '0.99', 7000, 0, 1, 0, '2020-12-25 22:34:48', '1', '2021-03-29 13:44:33', '1', 0, '2020-12-25 23:30:29', 1),
(2, 1, 1, 1, NULL, NULL, NULL, '0', '0', '101', '200', 1500, 0, 0, 0, '2020-12-25 22:35:47', '1', '2021-03-23 22:26:24', '1', 1, '2021-03-23 22:26:24', 1),
(3, 5, 1, 1, NULL, NULL, NULL, '0', '0', '0', '100', 7000, 0, 0, 0, '2020-12-25 22:44:02', '1', '2021-03-23 22:26:24', '1', 1, '2021-03-23 22:26:24', 1),
(4, 5, 1, 1, NULL, NULL, NULL, '0', '0', '101', '200', 2000, 0, 0, 0, '2020-12-25 22:44:23', '1', '2021-03-23 22:26:24', '1', 1, '2021-03-23 22:26:24', 1),
(5, 1, 1, 1, NULL, NULL, NULL, '0', '100', '0', '0', 40, 1, 1, 0, '2021-03-22 14:40:23', '1', '2021-03-23 22:26:46', '1', 0, NULL, NULL),
(6, 1, 1, 1, NULL, NULL, NULL, '101', '200', '0', '0', 5000, 1, 0, 0, '2021-03-22 14:43:33', '1', '2021-03-23 22:26:24', '1', 1, '2021-03-23 22:26:24', 1),
(7, 5, 1, 1, NULL, NULL, NULL, '0', '100', '0', '0', 4000, 1, 0, 0, '2021-03-22 14:50:34', '1', '2021-03-23 22:26:24', '1', 1, '2021-03-23 22:26:24', 1),
(8, 5, 1, 1, NULL, NULL, NULL, '101', '200', '0', '0', 6000, 1, 0, 0, '2021-03-22 14:51:09', '1', '2021-03-23 22:26:24', '1', 1, '2021-03-23 22:26:24', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(5) UNSIGNED NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `fullname` varchar(100) NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `oauth_provider` enum('','facebook','google','twitter') DEFAULT NULL,
  `oauth_uid` varchar(50) DEFAULT NULL,
  `oauth_picture` varchar(500) DEFAULT NULL,
  `active` int(1) DEFAULT '0',
  `file` varchar(100) DEFAULT NULL,
  `couponCode` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `password`, `salt`, `fname`, `lname`, `fullname`, `avatar`, `email`, `phone`, `role_id`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`, `type`, `oauth_provider`, `oauth_uid`, `oauth_picture`, `active`, `file`, `couponCode`) VALUES
(1, 'super_super_admin', '38a142d6f6a7b889587a97379cdc0775334169ee', 'gmcZSu9Ce5DhZWl1w.bZ3e', 'ST', 'โปรแกรมเมอร์', 'ST โปรแกรมเมอร์', '', 'admin@admin.com', '', 1, '2019-02-03 16:25:30', 'ADMIN.S', '2019-03-31 17:45:05', '1', 0, NULL, NULL, 'developer', NULL, NULL, NULL, 1, NULL, ''),
(2, 'comsci2535@gmail.com', '38a142d6f6a7b889587a97379cdc0775334169ee', 'gmcZSu9Ce5DhZWl1w.bZ3e', 'Ruslee', 'Seebu', 'Ruslee Seebu', NULL, 'comsci2535@gmail.com', NULL, 16, '2019-12-26 12:12:46', '1', '2019-12-26 12:14:46', '1', 0, NULL, NULL, 'admin', NULL, NULL, NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `usertracking`
--

CREATE TABLE `usertracking` (
  `id` int(11) NOT NULL,
  `sessionId` varchar(100) NOT NULL,
  `identifier` varchar(255) NOT NULL,
  `requestUri` text NOT NULL,
  `timestamp` varchar(20) NOT NULL,
  `ip` varchar(50) NOT NULL,
  `agent` text NOT NULL,
  `referer` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Table structure for table `uses`
--

CREATE TABLE `uses` (
  `use_id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `lang` varchar(2) DEFAULT NULL,
  `title` varchar(250) DEFAULT NULL,
  `title_en` varchar(250) DEFAULT NULL,
  `excerpt` text,
  `excerpt_en` text,
  `detail` text,
  `detail_en` text,
  `active` tinyint(1) DEFAULT '0' COMMENT '0=Disabled,1=Enabled',
  `order` int(10) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(100) NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(100) NOT NULL,
  `recycle` int(11) DEFAULT '0',
  `recycle_at` datetime DEFAULT NULL,
  `recycle_by` int(11) DEFAULT NULL,
  `meta_title` text,
  `meta_description` text,
  `meta_keyword` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `uses`
--

INSERT INTO `uses` (`use_id`, `slug`, `lang`, `title`, `title_en`, `excerpt`, `excerpt_en`, `detail`, `detail_en`, `active`, `order`, `created_at`, `created_by`, `updated_at`, `updated_by`, `recycle`, `recycle_at`, `recycle_by`, `meta_title`, `meta_description`, `meta_keyword`) VALUES
(1, '', NULL, 'สมัครสมาชิกเว็บไซต์ www.ifcexpressshipping.com เพื่อใช้บริการรับพรีออเดอร์สินค้าจากจีน', 'Website membership www.ifcexpressshipping.com In order to receive pre-orders from China', '<p>ไม่ว่าจะเป็นเว็บ Aliexpress, Taobao, Tmall หรือเว็บไซต์ขายสินค้าราคาถูกจากประเทศจีนอื่นๆ ที่ทางเรารับสั่ง จากนั้นจึง Copy ลิ้งค์สินค้าที่ต้องการมาวางบริเวณแถบเมนูของทางเว็บ จากนั้น</p><p>จึงทำการเลือกแบบ สี จำนวน แล้วกดสั่งซื้อสินค้าตามปกติ</p>', '<p>Either it is the website Aliexpress, Taobao, Tmall or other websites selling cheap products from China. At the way we accept orders and then copy the product links that you want to place on the menu bar of the web and then</p><p>Therefore select the model, color, quantity and press to order as usual</p>', NULL, NULL, 1, 0, '2020-06-29 23:28:08', '1', '2020-08-29 18:22:50', '1', 0, NULL, NULL, NULL, NULL, NULL),
(2, '', NULL, 'กรอกข้อมูลในการสมัครสมาชิก และคลิก ยืนยันการสมัคร', 'Fill out the information in the membership application and click Confirm Registration.', '<p>ไม่ว่าจะเป็นเว็บ Aliexpress, Taobao, Tmall หรือเว็บไซต์ขายสินค้าราคาถูกจากประเทศจีนอื่นๆ ที่ทางเรารับสั่ง จากนั้นจึง Copy ลิ้งค์สินค้าที่ต้องการมาวางบริเวณแถบเมนูของทางเว็บ จากนั้น</p><p><span xss=\"removed\" xss=removed>จึงทำการเลือกแบบ สี จำนวน แล้วกดสั่งซื้อสินค้าตามปกติ</span></p>', '<p>Either it is the website Aliexpress, Taobao, Tmall or other websites selling cheap products from China. At the way we accept orders and then copy the product links that you want to place on the menu bar of the web and then</p><p>Therefore select the model, color, quantity and press to order as usual</p>', NULL, NULL, 1, 1, '2020-06-29 23:28:45', '1', '2020-08-29 18:19:19', '1', 0, NULL, NULL, NULL, NULL, NULL),
(3, '', NULL, 'กรอกเลขโทรศัพท์ที่ใช้งานได้จริง จากนั้นกด ', 'Enter a valid phone number and press', '<p>ไม่ว่าจะเป็นเว็บ Aliexpress, Taobao, Tmall หรือเว็บไซต์ขายสินค้าราคาถูกจากประเทศจีนอื่นๆ ที่ทางเรารับสั่ง จากนั้นจึง Copy ลิ้งค์สินค้าที่ต้องการมาวางบริเวณแถบเมนูของทางเว็บ จากนั้น</p><p>จึงทำการเลือกแบบ สี จำนวน แล้วกดสั่งซื้อสินค้าตามปกติ</p>', '<p>Either it is the website Aliexpress, Taobao, Tmall or other websites selling cheap products from China. At the way we accept orders and then copy the product links that you want to place on the menu bar of the web and then</p><p>Therefore select the model, color, quantity and press to order as usual</p>', NULL, NULL, 1, 2, '2020-06-29 23:29:07', '1', '2020-08-29 18:20:10', '1', 0, NULL, NULL, NULL, NULL, NULL),
(4, '', NULL, 'หลังจากสมัครสมาชิกแล้ว เมื่อต้องการสั่งสินค้าจากจีนก็สามารถเสิชหาสินค้าได้เลย', 'After subscribing When you want to order products from China, you can search for products right away.', '<p>ไม่ว่าจะเป็นเว็บ Aliexpress, Taobao, Tmall หรือเว็บไซต์ขายสินค้าราคาถูกจากประเทศจีนอื่นๆ ที่ทางเรารับสั่ง จากนั้นจึง Copy ลิ้งค์สินค้าที่ต้องการมาวางบริเวณแถบเมนูของทางเว็บ จากนั้น</p><p>จึงทำการเลือกแบบ สี จำนวน แล้วกดสั่งซื้อสินค้าตามปกติ</p>', '<p>Either it is the website Aliexpress, Taobao, Tmall or other websites selling cheap products from China. At the way we accept orders and then copy the product links that you want to place on the menu bar of the web and then</p><p>Therefore select the model, color, quantity and press to order as usual</p>', NULL, NULL, 1, 3, '2020-06-29 23:29:26', '1', '2020-08-29 18:23:38', '1', 0, NULL, NULL, NULL, NULL, NULL),
(5, '', NULL, 'รอทางเว็บตรวจสอบออเดอร์', 'Wait through the web to check the order.', '<p>ไม่ว่าจะเป็นเว็บ Aliexpress, Taobao, Tmall หรือเว็บไซต์ขายสินค้าราคาถูกจากประเทศจีนอื่นๆ ที่ทางเรารับสั่ง จากนั้นจึง Copy ลิ้งค์สินค้าที่ต้องการมาวางบริเวณแถบเมนูของทางเว็บ จากนั้น</p><p>จึงทำการเลือกแบบ สี จำนวน แล้วกดสั่งซื้อสินค้าตามปกติ</p>', '<p>Either it is the website Aliexpress, Taobao, Tmall or other websites selling cheap products from China. At the way we accept orders and then copy the product links that you want to place on the menu bar of the web and then</p><p>Therefore select the model, color, quantity and press to order as usual</p>', NULL, NULL, 1, 4, '2020-06-29 23:30:01', '1', '2020-08-29 18:22:08', '1', 0, NULL, NULL, NULL, NULL, NULL),
(6, '', NULL, 'เมื่อเราได้รับยอดชำระแล้วทางเว็บก็จะทำการสั่งซื้อสินค้าตามที่ท่านต้องการ', 'Once we have received the payment, the web will order the products you want.', '<p>ไม่ว่าจะเป็นเว็บ Aliexpress, Taobao, Tmall หรือเว็บไซต์ขายสินค้าราคาถูกจากประเทศจีนอื่นๆ ที่ทางเรารับสั่ง จากนั้นจึง Copy ลิ้งค์สินค้าที่ต้องการมาวางบริเวณแถบเมนูของทางเว็บ จากนั้น</p><p>จึงทำการเลือกแบบ สี จำนวน แล้วกดสั่งซื้อสินค้าตามปกติ</p>', '<p>Either it is the website Aliexpress, Taobao, Tmall or other websites selling cheap products from China. At the way we accept orders and then copy the product links that you want to place on the menu bar of the web and then</p><p>Therefore select the model, color, quantity and press to order as usual</p>', NULL, NULL, 1, 5, '2020-06-29 23:30:23', '1', '2020-08-29 18:24:05', '1', 0, NULL, NULL, NULL, NULL, NULL),
(7, '', NULL, 'คลังสินค้าจีนรับของ พร้อมส่งกลับมาไทย', 'Chinese warehouse pick up Ready to send back to Thailand', '<p>ไม่ว่าจะเป็นเว็บ Aliexpress, Taobao, Tmall หรือเว็บไซต์ขายสินค้าราคาถูกจากประเทศจีนอื่นๆ ที่ทางเรารับสั่ง จากนั้นจึง Copy ลิ้งค์สินค้าที่ต้องการมาวางบริเวณแถบเมนูของทางเว็บ จากนั้น</p><p>จึงทำการเลือกแบบ สี จำนวน แล้วกดสั่งซื้อสินค้าตามปกติ</p>', '<p>Either it is the website Aliexpress, Taobao, Tmall or other websites selling cheap products from China. At the way we accept orders and then copy the product links that you want to place on the menu bar of the web and then</p><p>Therefore select the model, color, quantity and press to order as usual</p>', NULL, NULL, 1, 6, '2020-06-29 23:30:42', '1', '2020-08-29 18:20:37', '1', 0, NULL, NULL, NULL, NULL, NULL),
(8, '', NULL, 'คลังสินค้าไทยรับของ แล้วเจ้าหน้าที่จะทำการประเมินค่าส่งสินค้าในประเทศ', 'Thai warehouse pick up goods Then the staff will assess the shipping cost in the country', '<p>ไม่ว่าจะเป็นเว็บ Aliexpress, Taobao, Tmall หรือเว็บไซต์ขายสินค้าราคาถูกจากประเทศจีนอื่นๆ ที่ทางเรารับสั่ง จากนั้นจึง Copy ลิ้งค์สินค้าที่ต้องการมาวางบริเวณแถบเมนูของทางเว็บ จากนั้น</p><p>จึงทำการเลือกแบบ สี จำนวน แล้วกดสั่งซื้อสินค้าตามปกติ</p>', '<p>Either it is the website Aliexpress, Taobao, Tmall or other websites selling cheap products from China. At the way we accept orders and then copy the product links that you want to place on the menu bar of the web and then</p><p>Therefore select the model, color, quantity and press to order as usual</p>', NULL, NULL, 1, 7, '2020-06-29 23:31:01', '1', '2020-08-29 18:21:14', '1', 0, NULL, NULL, NULL, NULL, NULL),
(9, '', NULL, 'จัดส่งสินค้าถึงมือท่านอย่างรวดเร็ว', 'Fast delivery to you.', '<p>ไม่ว่าจะเป็นเว็บ Aliexpress, Taobao, Tmall หรือเว็บไซต์ขายสินค้าราคาถูกจากประเทศจีนอื่นๆ ที่ทางเรารับสั่ง จากนั้นจึง Copy ลิ้งค์สินค้าที่ต้องการมาวางบริเวณแถบเมนูของทางเว็บ จากนั้น</p><p>จึงทำการเลือกแบบ สี จำนวน แล้วกดสั่งซื้อสินค้าตามปกติ</p>', '<p>Either it is the website Aliexpress, Taobao, Tmall or other websites selling cheap products from China. At the way we accept orders and then copy the product links that you want to place on the menu bar of the web and then</p><p>Therefore select the model, color, quantity and press to order as usual</p>', NULL, NULL, 1, 8, '2020-06-29 23:31:23', '1', '2020-08-29 18:21:35', '1', 0, NULL, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aboutus`
--
ALTER TABLE `aboutus`
  ADD PRIMARY KEY (`aboutus_id`) USING BTREE;

--
-- Indexes for table `attribute_sends`
--
ALTER TABLE `attribute_sends`
  ADD PRIMARY KEY (`attribute_send_id`) USING BTREE;

--
-- Indexes for table `auto_order_id`
--
ALTER TABLE `auto_order_id`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `auto_order_in_id`
--
ALTER TABLE `auto_order_in_id`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `banks`
--
ALTER TABLE `banks`
  ADD PRIMARY KEY (`bank_id`) USING BTREE;

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`banner_id`) USING BTREE;

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`categorie_id`) USING BTREE;

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`,`ip_address`) USING BTREE;

--
-- Indexes for table `conditions`
--
ALTER TABLE `conditions`
  ADD PRIMARY KEY (`condition_id`) USING BTREE;

--
-- Indexes for table `config`
--
ALTER TABLE `config`
  ADD PRIMARY KEY (`variable`,`lang`,`type`) USING BTREE;

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`contact_id`) USING BTREE;

--
-- Indexes for table `delivery_companys`
--
ALTER TABLE `delivery_companys`
  ADD PRIMARY KEY (`delivery_company_id`) USING BTREE;

--
-- Indexes for table `delivery_settings`
--
ALTER TABLE `delivery_settings`
  ADD PRIMARY KEY (`delivery_setting_id`) USING BTREE;

--
-- Indexes for table `info`
--
ALTER TABLE `info`
  ADD PRIMARY KEY (`info_id`) USING BTREE;

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `module`
--
ALTER TABLE `module`
  ADD PRIMARY KEY (`moduleId`) USING BTREE;

--
-- Indexes for table `news_articles`
--
ALTER TABLE `news_articles`
  ADD PRIMARY KEY (`news_article_id`) USING BTREE;

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`) USING BTREE;

--
-- Indexes for table `orders_detail`
--
ALTER TABLE `orders_detail`
  ADD PRIMARY KEY (`order_detail_id`) USING BTREE,
  ADD KEY `order_detail_index` (`order_id`,`product_id`,`product_price`,`quantity`,`created_at`) USING BTREE;

--
-- Indexes for table `orders_detail_import`
--
ALTER TABLE `orders_detail_import`
  ADD PRIMARY KEY (`orders_detail_import_id`) USING BTREE;

--
-- Indexes for table `orders_detail_url`
--
ALTER TABLE `orders_detail_url`
  ADD PRIMARY KEY (`order_detail_id`) USING BTREE,
  ADD KEY `order_detail_index` (`order_id`,`product_price`,`quantity`,`created_at`) USING BTREE;

--
-- Indexes for table `orders_log`
--
ALTER TABLE `orders_log`
  ADD PRIMARY KEY (`order_log_id`) USING BTREE;

--
-- Indexes for table `orders_payments`
--
ALTER TABLE `orders_payments`
  ADD PRIMARY KEY (`orders_payments_id`) USING BTREE;

--
-- Indexes for table `orders_status_action`
--
ALTER TABLE `orders_status_action`
  ADD PRIMARY KEY (`order_status_action_id`);

--
-- Indexes for table `orders_user_address_send`
--
ALTER TABLE `orders_user_address_send`
  ADD PRIMARY KEY (`user_id`) USING BTREE;

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`permission_id`) USING BTREE;

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`) USING BTREE;

--
-- Indexes for table `products_image`
--
ALTER TABLE `products_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_stars`
--
ALTER TABLE `product_stars`
  ADD PRIMARY KEY (`product_star_id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`question_id`) USING BTREE;

--
-- Indexes for table `repo`
--
ALTER TABLE `repo`
  ADD PRIMARY KEY (`repoId`) USING BTREE;

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_id`) USING BTREE;

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`service_id`) USING BTREE;

--
-- Indexes for table `status`
--
ALTER TABLE `status`
  ADD PRIMARY KEY (`status_id`) USING BTREE;

--
-- Indexes for table `stting_line`
--
ALTER TABLE `stting_line`
  ADD PRIMARY KEY (`line_id`) USING BTREE;

--
-- Indexes for table `type_transportations`
--
ALTER TABLE `type_transportations`
  ADD PRIMARY KEY (`type_transportation_id`) USING BTREE;

--
-- Indexes for table `type_transportations_cust`
--
ALTER TABLE `type_transportations_cust`
  ADD PRIMARY KEY (`type_transportation_cust_id`) USING BTREE;

--
-- Indexes for table `type_transportations_setting`
--
ALTER TABLE `type_transportations_setting`
  ADD PRIMARY KEY (`type_transportations_setting_id`) USING BTREE;

--
-- Indexes for table `type_transportations_setting_list`
--
ALTER TABLE `type_transportations_setting_list`
  ADD PRIMARY KEY (`type_transportations_setting_list_id`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`) USING BTREE;

--
-- Indexes for table `usertracking`
--
ALTER TABLE `usertracking`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `uses`
--
ALTER TABLE `uses`
  ADD PRIMARY KEY (`use_id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aboutus`
--
ALTER TABLE `aboutus`
  MODIFY `aboutus_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `attribute_sends`
--
ALTER TABLE `attribute_sends`
  MODIFY `attribute_send_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `banks`
--
ALTER TABLE `banks`
  MODIFY `bank_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `banner_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `categorie_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `conditions`
--
ALTER TABLE `conditions`
  MODIFY `condition_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `contact_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `delivery_companys`
--
ALTER TABLE `delivery_companys`
  MODIFY `delivery_company_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `delivery_settings`
--
ALTER TABLE `delivery_settings`
  MODIFY `delivery_setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `info`
--
ALTER TABLE `info`
  MODIFY `info_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `module`
--
ALTER TABLE `module`
  MODIFY `moduleId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=324;

--
-- AUTO_INCREMENT for table `news_articles`
--
ALTER TABLE `news_articles`
  MODIFY `news_article_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `orders_detail`
--
ALTER TABLE `orders_detail`
  MODIFY `order_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `orders_detail_import`
--
ALTER TABLE `orders_detail_import`
  MODIFY `orders_detail_import_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `orders_detail_url`
--
ALTER TABLE `orders_detail_url`
  MODIFY `order_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `orders_log`
--
ALTER TABLE `orders_log`
  MODIFY `order_log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `orders_payments`
--
ALTER TABLE `orders_payments`
  MODIFY `orders_payments_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `orders_status_action`
--
ALTER TABLE `orders_status_action`
  MODIFY `order_status_action_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `permission_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `product_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `products_image`
--
ALTER TABLE `products_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `product_stars`
--
ALTER TABLE `product_stars`
  MODIFY `product_star_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `question_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `repo`
--
ALTER TABLE `repo`
  MODIFY `repoId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `role_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
  MODIFY `status_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `stting_line`
--
ALTER TABLE `stting_line`
  MODIFY `line_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `type_transportations`
--
ALTER TABLE `type_transportations`
  MODIFY `type_transportation_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `type_transportations_cust`
--
ALTER TABLE `type_transportations_cust`
  MODIFY `type_transportation_cust_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `type_transportations_setting`
--
ALTER TABLE `type_transportations_setting`
  MODIFY `type_transportations_setting_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `type_transportations_setting_list`
--
ALTER TABLE `type_transportations_setting_list`
  MODIFY `type_transportations_setting_list_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `usertracking`
--
ALTER TABLE `usertracking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `uses`
--
ALTER TABLE `uses`
  MODIFY `use_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
