'use strict'

$(document).ready(function() {
    $('.frm-create').validate({
        rules: {
            title: true
        }
    })

    $.validator.addMethod(
        'htmlEditorEmpty',
        function(value, element) {
            if (value == '' || value == '<p><br></p>') {
                $('.note-editor').append('<span id="title-error" class="help-block">โปรดระบุ</span>')
                return false
            } else {
                return true
            }
        },
        ''
    )

    // form validate editor
    $('form').each(function() {
        if ($(this).data('validator')) $(this).data('validator').settings.ignore = '.note-editor *'
    })

    //fileinput;

    if (method == 'create') {
        $('#file').fileinput({
            maxFileCount: 1,
            validateInitialCount: true,
            overwriteInitial: false,
            showUpload: false,
            showRemove: true,
            required: true,
            initialPreviewAsData: true,
            maxFileSize: 2024,
            browseOnZoneClick: true,
            allowedFileExtensions: ['jpg', 'png', 'gif', 'svg'],
            browseLabel: 'Picture',
            browseClass: 'btn btn-primary btn-block',
            showCaption: false,
            showRemove: true,
            showUpload: false,
            removeClass: 'btn btn-danger',
            dropZoneTitle: 'Drag & drop picture here …'
        })
        $('#file_th').fileinput({
            maxFileCount: 1,
            validateInitialCount: true,
            overwriteInitial: false,
            showUpload: false,
            showRemove: true,
            required: true,
            initialPreviewAsData: true,
            maxFileSize: 2024,
            browseOnZoneClick: true,
            allowedFileExtensions: ['jpg', 'png', 'gif', 'svg'],
            browseLabel: 'Picture',
            browseClass: 'btn btn-primary btn-block',
            showCaption: false,
            showRemove: true,
            showUpload: false,
            removeClass: 'btn btn-danger',
            dropZoneTitle: 'Drag & drop picture here …'
        })
        $('#file_en').fileinput({
            maxFileCount: 1,
            validateInitialCount: true,
            overwriteInitial: false,
            showUpload: false,
            showRemove: true,
            required: true,
            initialPreviewAsData: true,
            maxFileSize: 2024,
            browseOnZoneClick: true,
            allowedFileExtensions: ['jpg', 'png', 'gif', 'svg'],
            browseLabel: 'Picture',
            browseClass: 'btn btn-primary btn-block',
            showCaption: false,
            showRemove: true,
            showUpload: false,
            removeClass: 'btn btn-danger',
            dropZoneTitle: 'Drag & drop picture here …'
        })
    }

    if (method == 'edit') {
        $('#file').fileinput({
            initialPreview: [file_image],
            initialPreviewAsData: false,
            initialPreviewConfig: [
                { downloadUrl: file_image, extra: { id: file_id, csrfToken: get_cookie('csrfCookie') } }
            ],
            deleteUrl: deleteUrl,
            maxFileCount: 1,
            validateInitialCount: true,
            overwriteInitial: false,
            showUpload: false,
            showRemove: true,
            required: required_icon,
            initialPreviewAsData: true,
            maxFileSize: 2024,
            browseOnZoneClick: true,
            allowedFileExtensions: ['jpg', 'png', 'gif', 'svg'],
            browseLabel: 'Select Image',
            browseClass: 'btn btn-primary btn-block',
            showCaption: false,
            showRemove: true,
            showUpload: false,
            removeClass: 'btn btn-danger',
            dropZoneTitle: 'Drag & drop icon here …'
        })

        $('#file_th').fileinput({
            initialPreview: [file_image_th],
            initialPreviewAsData: false,
            initialPreviewConfig: [
                { downloadUrl: file_image_th, extra: { id: file_id, csrfToken: get_cookie('csrfCookie') } }
            ],
            deleteUrl: deleteUrl,
            maxFileCount: 1,
            validateInitialCount: true,
            overwriteInitial: false,
            showUpload: false,
            showRemove: true,
            required: required_icon,
            initialPreviewAsData: true,
            maxFileSize: 2024,
            browseOnZoneClick: true,
            allowedFileExtensions: ['jpg', 'png', 'gif', 'svg'],
            browseLabel: 'Select Image',
            browseClass: 'btn btn-primary btn-block',
            showCaption: false,
            showRemove: true,
            showUpload: false,
            removeClass: 'btn btn-danger',
            dropZoneTitle: 'Drag & drop icon here …'
        })

        $('#file_en').fileinput({
            initialPreview: [file_image_en],
            initialPreviewAsData: false,
            initialPreviewConfig: [
                { downloadUrl: file_image_en, extra: { id: file_id, csrfToken: get_cookie('csrfCookie') } }
            ],
            deleteUrl: deleteUrl,
            maxFileCount: 1,
            validateInitialCount: true,
            overwriteInitial: false,
            showUpload: false,
            showRemove: true,
            required: required_icon,
            initialPreviewAsData: true,
            maxFileSize: 2024,
            browseOnZoneClick: true,
            allowedFileExtensions: ['jpg', 'png', 'gif', 'svg'],
            browseLabel: 'Select Image',
            browseClass: 'btn btn-primary btn-block',
            showCaption: false,
            showRemove: true,
            showUpload: false,
            removeClass: 'btn btn-danger',
            dropZoneTitle: 'Drag & drop icon here …'
        })
    }
})

$(window).on('load', function() {})

$(window).on('scroll', function() {})