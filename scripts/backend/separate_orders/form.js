'use strict'

$(document).ready(function() {
    $('.frm-create').validate({
        rules: {
            title: true
        }
    })

    // form validate editor
    $('form').each(function() {
        if ($(this).data('validator')) $(this).data('validator').settings.ignore = '.note-editor *'
    })
})

$('.m_datepicker_2').datetimepicker({ format: 'yyyy-mm-dd', autoclose: true, minView: 2 })
var date = '';
$(document).on('change', '.m_datepicker_2', function() {
    date = $(this).val();
    set_date(date);
})

$('.m_datepicker_2_1').datetimepicker({ format: 'yyyy-mm-dd', autoclose: true, minView: 2 })

function set_date(date) {
    console.log(date);
    $('.m_datepicker_2_1').datetimepicker('setStartDate', date);
}

$(document).on('change', '#type_transportations_setting_id', function() {
    var id = $(this).val();

    $('#type_transportation_cust_id').html('<option value="">เลือกประเภทการซื้อ</option>');
    $('#type_transportation_id').html('<option value="">เลือกประเภทขนส่ง</option>');
    $.ajax({
        type: "POST",
        url: controller + '/get_transportations_cust',
        data: {
            code: id,
            type: 'cust',
            csrfToken: get_cookie('csrfCookie')
        },
        success: function(result) {
            // // show modal 
            var html = '';
            if (result.status == 200) {
                $.each(result.info, function(key, val) {
                    html += ' <option value="' + val['type_transportation_cust_id'] + '">' + val['title'] + '</option>';
                });

                $('#type_transportation_cust_id').append(html);
            }
        },
        error: function(request, status, error) {
            console.log("ajax call went wrong:" + request.responseText);
        }
    });
});

$(document).on('change', '#type_transportation_cust_id', function() {
    $('#type_transportation_id').html('<option value="">เลือกประเภทขนส่ง</option>');
    var id = $(this).val();
    $.ajax({
        type: "POST",
        url: controller + '/get_transportations_cust',
        data: {
            code: id,
            type: 'trans',
            csrfToken: get_cookie('csrfCookie')
        },
        success: function(result) {
            // // show modal 
            var html = '';
            if (result.status == 200) {

                $.each(result.info, function(key, val) {
                    html += ' <option value="' + val['type_transportation_id'] + '">' + val['title'] + '</option>';
                });

                $('#type_transportation_id').append(html);

            }
        },
        error: function(request, status, error) {
            console.log("ajax call went wrong:" + request.responseText);
        }
    });
});

// $(document).on('change', '#type_transportation_id', function() {
//     get_data_calculate();
// });

$(document).on('click', '#btn-calculator', function() {

    var wide = $('#wide').val();
    var long = $('#longs').val();
    var high = $('#high').val();
    var weight = $('#weight').val();

    var type_transportations_setting_id = $('#type_transportations_setting_id').val();
    var type_transportation_cust_id = $('#type_transportation_cust_id').val();
    var type_transportation_id = $('#type_transportation_id').val();

    if (wide == '') {
        alert_box('กรุณาระบุความกว้าง');
        return false;
    }
    if (long == '') {
        alert_box('กรุณาระบุความยาว');
        return false;
    }
    if (high == '') {
        alert_box('กรุณาระบุความสูง');
        return false;
    }

    if (type_transportations_setting_id == '') {
        alert_box('กรุณาเลือกประเภทสินค้า');
        return false;
    }

    if (type_transportation_cust_id == '') {
        alert_box('กรุณาเลือกประเภทการซื้อ');
        return false;
    }

    if (type_transportation_id == '') {
        alert_box('กรุณาเลือกประเภทขนส่ง');
        return false;
    }

    $.ajax({
        type: "POST",
        url: controller + '/get_data',
        // dataType: 'json',
        data: {
            wide: wide,
            long: long,
            high: high,
            weight: weight,
            type_transportations_setting_id: type_transportations_setting_id,
            type_transportation_cust_id: type_transportation_cust_id,
            type_transportation_id: type_transportation_id,
            csrfToken: get_cookie('csrfCookie')
        },
        success: function(result) {
            // // show modal 
            $('#size').val(result.calculate);
            $('#import_cost').val(result.info.price_send);
        },
        error: function(request, status, error) {
            console.log("ajax call went wrong:" + request.responseText);
        }
    });

});

$(window).on('load', function() {})

$(window).on('scroll', function() {})