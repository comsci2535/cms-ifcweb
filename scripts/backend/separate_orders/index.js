'use strict'
$(document).ready(function () {
  dataList
    .DataTable({
      serverSide: true,
      searching: false,
      ajax: {
        url: controller + '/data_index',
        type: 'POST',
        data: { csrfToken: get_cookie('csrfCookie'), code: $('#code').val() }
      },
      order: [[1, 'asc']],
      pageLength: 10,
      columns: [
        { data: 'checkbox', width: '20px', className: 'text-center', orderable: false },
        { data: 'detail_send', width: '150px', className: '', orderable: false },
        { data: 'tracking_china', width: '150px', className: '', orderable: false },
        { data: 'detail', width: '150px', className: '', orderable: false },
        { data: 'service_charge', width: '150px', className: '', orderable: false },
        { data: 'import_cost', width: '150px', className: 'text-center', orderable: false },
        { data: 'action', width: '30px', className: 'text-center', orderable: false }
      ]
    })
    .on('draw', function () {
      $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt)
      $('.tb-check-single').iCheck(iCheckboxOpt)
    })
    .on('processing', function (e, settings, processing) {
      if (processing) {
        $('#overlay-box').removeClass('hidden')
      } else {
        $('#overlay-box').addClass('hidden')
      }
    })
})

$(window).on('load', function () {})

$(window).on('scroll', function () {})
