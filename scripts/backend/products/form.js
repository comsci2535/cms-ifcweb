'use strict'

$(document).ready(function () {
  $('.frm-create').validate({
    rules: {
      title: true,
      slug: true,
      excerpt: {
        required: true
      },
      file: {
        required: true
      }
    }
  })

  $('#product_color').select2({ placeholder: 'ระบุ', tags: !0 })
  $('#product_size').select2({ placeholder: 'ระบุ', tags: !0 })
  // form validate editor
  $('form').each(function () {
    if ($(this).data('validator')) $(this).data('validator').settings.ignore = '.note-editor *'
  })

  /* -- fileinput */
  if (method == 'create') {
    $('#file').fileinput({
      maxFileCount: 10,
      validateInitialCount: true,
      overwriteInitial: true,
      showUpload: true,
      showRemove: true,
      required: true,
      initialPreviewAsData: true,
      maxFileSize: 2024,
      browseOnZoneClick: true,
      allowedFileExtensions: ['jpg', 'png', 'gif', 'svg'],
      browseLabel: 'Picture',
      browseClass: 'btn btn-primary btn-block',
      showCaption: false,
      showRemove: true,
      showUpload: false,
      removeClass: 'btn btn-danger',
      dropZoneTitle: 'Drag & drop picture here …'
    })
  }

  if (method == 'edit') {
    $('#file').fileinput({
      initialPreview: initialPreview,
      initialPreviewAsData: false,
      initialPreviewConfig: initialPreviewConfig,
      maxFileCount: 10,
      validateInitialCount: true,
      overwriteInitial: false,
      required: required_icon,
      initialPreviewAsData: true,
      maxFileSize: 2024,
      browseOnZoneClick: true,
      allowedFileExtensions: ['jpg', 'png', 'gif', 'svg'],
      browseLabel: 'Select Image',
      browseClass: 'btn btn-primary btn-block',
      showCaption: false,
      showRemove: true,
      showUpload: false,
      removeClass: 'btn btn-danger',
      dropZoneTitle: 'Drag & drop icon here …'
    })
  }
})

$(document).on('click', '.kv-file-remove', function () {
  $('#text-file-remove').append(
    '<input type="hidden" name="file_key[]" value="' + $(this).attr('data-key') + '">'
  )
})

$(window).on('load', function () {})

$(window).on('scroll', function () {})
