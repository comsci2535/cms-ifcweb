'use strict'

$(document).ready(function () {
  $('.frm-create').validate({
    rules: {
      title: true,
      kg_min: true,
      kg_max: true,
      queue_max: true,
      queue_min: true,
      price_send: true
    }
  })
})

$(window).on('load', function () {})

$(window).on('scroll', function () {})
