"use strict";

$(document).ready(function () {

    if($(".perpage1").height() > 407){
        $('.perpage1').css('page-break-after','always');
        $('.perpage2').css('page-break-before','always');
    }   
    
    $('#btn-print').on('click',function(){
        var order_id_printcheck   = $('#order-id-printcheck').val();
        var order_id_printinvoice = $('#order-id-printinvoice').val();
        
        if(order_id_printcheck != null){
            $.ajax({
                url: controller+"/updateprintcheck",
                type: 'POST',
                data: {
                    order_id : order_id_printcheck,
                    csrfToken:get_cookie('csrfCookie')
                },
                success: function(result){}
            });
        }

        if(order_id_printinvoice != null){
            $.ajax({
                url: controller+"/updateprintinvoice",
                type: 'POST',
                data: {
                    order_id : order_id_printinvoice,
                    csrfToken:get_cookie('csrfCookie')
                },
                success: function(result){}
            });
        }

        printData();
    });
    $('#btn-print-copy').on('click',function(){
        printData_copy();
    });
});

function printData(){
    var divToPrint      = document.getElementById("printarea");
    var newWin= window.open("");
    newWin.document.write(divToPrint.outerHTML);
    newWin.print();
    newWin.close();
}

function printData_copy(){
    var divToPrint      = document.getElementById("printarea_copy");
    var newWin= window.open("");
    newWin.document.write(divToPrint.outerHTML);
    newWin.print();
    newWin.close();
}

