'use strict'
$(document).ready(function() {
    dataList
        .DataTable({
            //language: {url: "assets/bower_components/datatables.net/Thai.json"},
            serverSide: true,
            ajax: {
                url: controller + '/data_index',
                type: 'POST',
                data: {
                    csrfToken: get_cookie('csrfCookie'),
                    status: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
                }
            },
            order: [
                [1, 'DESC']
            ],
            pageLength: 25,
            columns: [
                { data: 'checkbox', width: '20px', className: 'text-center', orderable: false },
                { data: 'created_at', width: '100px', className: '', orderable: true },
                { data: 'order_code', className: '', orderable: true },
                { data: 'name', className: '', orderable: true },
                { data: 'qty', className: '', orderable: false },
                { data: 'discount', className: '', orderable: false },
                // { data: 'payment_type', width: '150px', className: '', orderable: false },
                // { data: 'status', width: '50px', className: 'text-center', orderable: false },
                { data: 'action', width: '30px', className: 'text-center', orderable: false }
            ]
        })
        .on('draw', function() {
            $('.bootstrapToggle').bootstrapToggle(bootstrapToggleOpt)
            $('.tb-check-single').iCheck(iCheckboxOpt)
        })
        .on('processing', function(e, settings, processing) {
            if (processing) {
                $('#overlay-box').removeClass('hidden')
            } else {
                $('#overlay-box').addClass('hidden')
            }
        })

    //ย้ายรายการ order ไปยังกำลังจัดส่งสินค้า
    $(document).on('click', '.multi-send-order', function() {
        //BOOTBOX
        var url = controller + '/send_order'
        var set = $('#data-list .tb-check-single')
        $('#overlay-box').removeClass('hidden')

        $(set).each(function() {
            if ($(this).is(':checked')) {
                arrayId.push($(this).closest('tr').attr('id'))
            }
        })
        if (arrayId.length > 0) {
            swal({
                title: 'กรุณายืนยันการทำรายการ',
                text: '',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'ตกลง',
                cancelButtonText: 'ยกเลิก'
            }).then(function(result) {
                if (result.value) {
                    $.ajax({
                            url: url,
                            type: 'POST',
                            dataType: 'json',
                            headers: {
                                'X-CSRF-TOKEN': csrfToken
                            },
                            data: {
                                id: arrayId,
                                csrfToken: csrfToken
                            }
                        })
                        .done(function(data) {
                            if (data.success === true) {
                                toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
                                $('#data-list .tb-check-all').iCheck('uncheck')
                                dataList.DataTable().draw()
                            } else if (data.success === false) {
                                $('#overlay-box').addClass('hidden')
                                toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
                            }
                            arrayId = []
                        })
                        .fail(function() {
                            toastr['error']('พบข้อผิดพลาดด้านการสื่อสาร', '')
                            arrayId = []
                        })
                }
            })
        } else {
            alert_box('กรุณาเลือกรายการสั่งซื้อที่ต้องการ')
        }
    })

    $(document).on('click', '.btn-edit-price', function() {
        var $this = $(this)
        set_value_null() // set value null
        $('#display-text-data-old').hide()
        var code = $this.attr('data-code')
        var order_code = $this.closest('tr').find('.text-order-code').text()
        var order_price = $this.closest('tr').find('.text-order-price').text()
        var discount = $this.closest('tr').find('.text-order-price').attr('data-price')
        $('#order-total').val(discount)
        $('#order-type').val('edit_price')
        $('#order-csrfToken').val(csrfToken)
        $('#order-code').val(code)
        $('.text-data-old').html(order_price)
        $('#modal-order-edit-title').html('Order ID (' + order_code + ')' + $this.text())

        $('#display-text-data-old').show()
        $('#modal-order-edit').modal('show')
    })

    $(document).on('click', '.btn-edit-qty', function() {
        var $this = $(this)
        set_value_null() // set value null
        $('#display-text-data-old').hide()
        var code = $this.attr('data-code')
        var order_code = $this.closest('tr').find('.text-order-code').text()
        var order_price = $this.closest('tr').find('.text-order-qty').text()
        var discount = $this.closest('tr').find('.text-order-qty').attr('data-qty')
        $('#order-total').val(discount)
        $('#order-type').val('edit_qty')
        $('#order-csrfToken').val(csrfToken)
        $('#order-code').val(code)
        $('.text-data-old').html(order_price)
        $('#modal-order-edit-title').html('Order ID (' + order_code + ')' + $this.text())

        $('#display-text-data-old').show()
        $('#modal-order-edit').modal('show')
    })

    $(document).on('click', '.btn-edit-comment', function() {
        var $this = $(this)
        var code = $this.attr('data-code')
        var order_code = $this.closest('tr').find('.text-order-code').text()
        var note = $this.closest('tr').find('.text-note').val()
        $('#note').val(note)
        $('#order-comment-type').val('edit_comment')
        $('#order-comment-csrfToken').val(csrfToken)
        $('#order-comment-code').val(code)
        $('#modal-order-comment-title').html('Order ID (' + order_code + ')' + $this.text())
        $('#modal-order-comment').modal('show')
    })

    $(document).on('click', '.btn-edit-status', function() {
        var $this = $(this)
        var code = $this.attr('data-code')
        var order_code = $this.closest('tr').find('.text-order-code').text()
        var status = $this.closest('tr').find('.text-order-status').attr('data-id')
        var status_text = $this.closest('tr').find('.text-order-status').text()
        $('#status_id').val(status).prop('disabled', false).trigger('change.select2')
        $('#modal-order-status-title').html('Order ID (' + order_code + ')' + $this.text())
        $('#text-status-name').html('สถานะปัจจุบัน : ' + status_text)

        $('#order-status-csrfToken').val(csrfToken)
        $('#order-status-code').val(code)
        $('#modal-order-status').modal('show')
    })

    function set_value_null() {
        $('#order_remark').val('')
        $('#order_edit').val('')
        $('#order-code').val('')
        $('#order-type').val('')
        $('#order-total').val('')
    }

    $('#form-order-edit').validate({
        rules: {
            order_edit: true
        }
    })

    $('#form-order-comment').validate({
        rules: {
            note: true
        }
    })

    $(document).on('submit', '#form-order-edit', function(e) {
        e.preventDefault()
        var url = controller + '/edit_order'
        swal({
            title: 'กรุณายืนยันการทำรายการ',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'ตกลง',
            cancelButtonText: 'ยกเลิก'
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': csrfToken
                        },
                        data: $('form#form-order-edit').serialize()
                    })
                    .done(function(data) {
                        if (data.success === true) {
                            toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
                            $('#modal-order-edit').modal('hide')
                            dataList.DataTable().draw()
                        } else if (data.success === false) {
                            toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
                        }
                    })
                    .fail(function() {
                        toastr['error']('พบข้อผิดพลาดด้านการสื่อสาร', '')
                    })
            }
        })
    })

    $(document).on('submit', '#form-order-comment', function(e) {
        e.preventDefault()
        var url = controller + '/comment_order'
        swal({
            title: 'กรุณายืนยันการทำรายการ',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'ตกลง',
            cancelButtonText: 'ยกเลิก'
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': csrfToken
                        },
                        data: $('form#form-order-comment').serialize()
                    })
                    .done(function(data) {
                        if (data.success === true) {
                            toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
                            $('#modal-order-comment').modal('hide')
                            dataList.DataTable().draw()
                        } else if (data.success === false) {
                            toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
                        }
                    })
                    .fail(function() {
                        toastr['error']('พบข้อผิดพลาดด้านการสื่อสาร', '')
                    })
            }
        })
    })

    $(document).on('submit', '#form-order-edit-status', function(e) {
        e.preventDefault()
        var url = controller + '/update_status'
        swal({
            title: 'กรุณายืนยันการทำรายการ',
            text: '',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'ตกลง',
            cancelButtonText: 'ยกเลิก'
        }).then(function(result) {
            if (result.value) {
                $.ajax({
                        url: url,
                        type: 'POST',
                        dataType: 'json',
                        headers: {
                            'X-CSRF-TOKEN': csrfToken
                        },
                        data: $('form#form-order-edit-status').serialize()
                    })
                    .done(function(data) {
                        if (data.success === true) {
                            toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
                            $('#modal-order-status').modal('hide')
                            dataList.DataTable().draw()
                        } else if (data.success === false) {
                            toastr[data.toastr.type](data.toastr.lineTwo, data.toastr.lineOne)
                        }
                    })
                    .fail(function() {
                        toastr['error']('พบข้อผิดพลาดด้านการสื่อสาร', '')
                    })
            }
        })
    })
})

$(window).on('load', function() {})

$(window).on('scroll', function() {})