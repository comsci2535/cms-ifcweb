'use strict'

$(document).ready(function () {
  $('.frm-create').validate({
    rules: {
      excerpt: {
        required: true
      }
    }
  })
})

$(window).on('load', function () {})

$(window).on('scroll', function () {})
