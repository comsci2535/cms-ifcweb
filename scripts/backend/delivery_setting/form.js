'use strict'

$(document).ready(function () {
  $('.frm-create').validate({
    rules: {
      title: true,
      weight_min: true,
      weight_max: true,
      price_in: true,
      price_out: true
    }
  })
})

$(window).on('load', function () {})

$(window).on('scroll', function () {})
