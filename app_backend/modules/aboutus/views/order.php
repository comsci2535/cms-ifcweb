<style type="text/css">
    .hidden {
    display: none!important;
}
</style>

<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       รายการ
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <?php echo $boxAction; ?>
            </div>
        </div> 
        <?php echo form_open($frmAction, array('class' => 'form-horizontal m-form m-form--fit m-form--label-align-right', 'method' => 'post')) ?>
            <div class="m-portlet__body"> 
                <div class="form-group m-form__group row ">
                    <div class="col-sm-1 text-right hidden">
                        <button title="ขยายทั้งหมด" id="expand-all" type="button" class="btn btn-xs btn-flat bg-purple"><i class="fa fa-plus"></i></button>
                        <button title="ยุบทั้งหมด" id="collapse-all" type="button" class="btn btn-xs btn-flat bg-purple"><i class="fa fa-minus"></i></button>
                    </div> 
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-1 col-form-label"></label>
                    <div class="col-sm-10">
                        <div class="dd" id="nestable" style="max-width: 100%"></div>
                    </div>
                </div>     
                <div class="form-group m-form__group row">
                    <label class="col-sm-1 col-form-label"></label>
                    <div class="col-sm-10">
                         <textarea class="form-control hidden" name="order" id="nestable-output"></textarea> 
                    </div>
                </div>  
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit"> 
                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-1">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success">บันทึก</button>
                    <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                        </div>
                    </div> 
                </div>
            </div>
        <div id="overlay-box" class="overlay hidden">
            <i class="fa fa-refresh fa-spin"></i>
        </div> 
        
        <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>"> 
        <?php echo form_close() ?>

        <!--end::Form-->
    </div>


    
</div>
<script>
    var category = <?php echo json_encode($treeData) ?>;
    var options = {'json': category}
    var categoryType = "";
</script>

