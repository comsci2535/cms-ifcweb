<style>
    page {
      background: white;
      display: block;
      margin: 0 auto;
      margin-bottom: 0.5cm;
      box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
  }
  page[size="A4"] {  
      width: 21cm;
      height: 23.7cm; 
      height: auto;
      padding-bottom: 70px;
  }

table{
    width: 100%;
}

.table-print{
    border: 1px solid;
}
.table-print tr td{
    border: 1px solid;
}

.td-border-no{
    border: none !important;
}

.table-print td{
    border: 1px solid;
}
.table-print th{
    border: 1px solid;
}
.table-print p, .table-sender p, .table-recipient p{
    margin: 0px 0px 0px 7px;
}

.table-sender tr td{
    width: 24%;
}

.table-recipient{
    width: 100%;
    position: relative;
    top: 460px;
}
.table-sender{
    width: 50%;
    position: relative;
    top: 491px;
    left: 0px;
}
.collect {
        border: 2px solid #32b312;
        border-radius: 6px;
        padding: 24px 20px;
        text-align: center;
        font-size: 20px;
        color: #2aa50b;
        width: 70%;
        margin-left: 55px;
    }
    .barcode_img{
        display: none;
    }
    .collect {
        border: 2px solid #32b312;
        border-radius: 6px;
        padding: 24px 20px;
        text-align: center;
        font-size: 20px;
        color: #2aa50b;
        width: 80%;
        margin-left: 55px;
    }
@media (min-width: 992px) { 
    .table-recipient{
        width: 100%;
        position: relative;
        top: 460px;
    }
    .table-sender{
        width: 100%;
        position: relative;
        top: 491px;
        left: 0px;
    }
}
@media (min-width: 1200px) { 
    .table-recipient{
        width: 100%;
        position: relative;
        top: 460px;
    }
    .table-sender{
        width: 100%;
        position: relative;
        top: 491px;
        left: 0px;
    }
}

</style>

<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                     แบบฟอร์ม
                 </h3>
             </div>
         </div>
         <div class="m-portlet__head-tools">
            <div class="btn-group mr-2" role="group" aria-label="1 group">
                <input id="order-id-printcheck" type="hidden" value="<?=$info->order_id?>">
                <button type='button' id="btn-print" class="btn btn-sm btn-success btn-flat box-add" title=""><i class="fa fa-print"></i> Print</button>
            </div>
        </div>
    </div>
    <div  class="m-portlet__body">
        <page id="printarea" size="A4">
            <style type="text/css"  media="print">
                @media print {
                    #printarea {  
                        margin: 0;
                        border: initial;
                        border-radius: initial;
                        width: initial;
                        min-height: initial;
                        box-shadow: initial;
                        background: initial;
                        page-break-after: always;
                    }
                    .table-print, .table-sender, .table-recipient{
                        border-collapse: collapse;
                    }
                    .table-print tr{
                        border: 1px solid;
                    } 
                    .table-print{
                        border: 1px solid;
                    }

                    .td-border-no{
                        border: 0px solid !important;
                    }  
                    .table-print td{
                        border: 1px solid;
                    }
                    .table-print th{
                        border: 1px solid;
                    }
                    .table-print p, .table-sender p, .table-recipient p{
                        margin: 0px 0px 0px 7px;
                    }
                    .table-sender{
                        width: 100%;
                        position: relative;
                        top: 400px;
                        left: 0px;
                    }
                    .table-recipient{
                        width: 100%;
                        position: relative;
                        top: 420px;
                    }
                    .table-recipient {
                                width: 100%;
                                position: relative;
                                top: 390px;
                            }
                            .barcode{
                                display: none;
                            }
                            .collect {
                                border: 2px solid #32b312;
                                border-radius: 6px;
                                padding: 24px 20px;
                                text-align: center;
                                font-size: 20px;
                                color: #2aa50b;
                                width: 70%;
                                margin-left: 55px;
                            }
                }
            </style>

            <div style="padding: 20px;">
                <div class="perpage1">
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 100px;">
                            <img alt="" src="<?=$this->config->item('root_url').'/images/logo/logo.png'?>" style="height: 75px;">
                        </td>
                        <td style="width: 450px;font-size: 12px;">
                            <strong>บริษัท ไอเอฟซี เอ็กซ์เพรส แอนด์ ชิปปิ้ง จำกัด</strong>
                            <br><?php echo !empty($company->title)? $company->title: '';?>
                            <br><?php echo !empty($company->excerpt)? $company->excerpt: '';?>
                            <br>เลขประจำตัวผู้เสียภาษีอากร : <?php echo !empty($company->tax_id)? $company->tax_id: '';?>
                          
                        </td>
                        <td style="width: 200px;font-size: 11px;text-align: right;">
                            สาขาที่ออกใบเสร็จรับเงิน : สำนักงานใหญ่
                            <br>Import worldwide products from China
                            <br>Tel : <?php echo !empty($company->tel)? $company->tel: '';?>
                            <br>ifcexpcessshipping@gmail.com
                            <br>Line : @ifcexpcessshipping
                        </td>
                    </tr>
                </table>
                <br>
                <table class="table-print" style="width: 100%;font-size: 12px;">
                    <tr style="background-color: #d2d2d2;">
                        <td colspan="4" style="text-align: center;"><strong>ใบคำกับภาษี/ใบเสร็จรับเงิน</strong></td>
                    </tr>
                    <tr>
                        <td class="td-border-no" style="width: 175px;">รหัสลูกค้า</td>
                        <td class="td-border-no" style="width: 175px;">IFC-168-GUY</td>
                        <td  class="td-border-no" style="text-align: right;width: 150px;">เลขที่บิล :</td>
                        <td  class="td-border-no" style="width: 250px;">68-210200002</td>
                    </tr>
                    <tr>
                        <td class="td-border-no">ผู้ชำระเงิน</td>
                        <td class="td-border-no">GUY</td>
                        <td class="td-border-no" style="text-align: right;">วันที่ออกบิล :</td>
                        <td class="td-border-no">05.01.2021</td>
                    </tr>
                    <tr>
                        <td class="td-border-no">เลขประจำตัวผู้เสียภาษีอากร</td>
                        <td class="td-border-no">909895494</td>
                        <td class="td-border-no" style="text-align: right;">สถานะ :</td>
                        <td class="td-border-no">รอชำระเงิน</td>
                    </tr>
                    <tr>
                        <td class="td-border-no">โทรศัพท์</td>
                        <td class="td-border-no">0807055XXX</td>
                        <td class="td-border-no"></td>
                        <td class="td-border-no"></td>
                    </tr>
                </table>
                <br>
                <table class="table-print" style="width: 100%;font-size: 12px;">
                    <tr style="background-color: #d2d2d2;">
                        <td colspan="9" style="text-align: center;"><strong>ค่าขนส่งระหว่างประเทศ</strong></td>
                    </tr>
                    <tr style="background-color: #d2d2d2;">
                        <td rowspan="2" style="text-align: center;"><strong>วิธีขนส่ง</strong></td>
                        <td colspan="4" style="text-align: center;"><strong>น้ำหนัก(KG)</strong></td>
                        <td colspan="4" style="text-align: center;"><strong>คิว(CBM)</strong></td>
                    </tr>
                    <tr style="background-color: #d2d2d2;">
                        <td style="text-align: center;"><strong>จำนวน(ชิ้น)</strong></td>
                        <td style="text-align: center;"><strong>น้ำหนัก(KG)</strong></td>
                        <td style="text-align: center;"><strong>ค่าขนส่ง(บาท/KG)</strong></td>
                        <td style="text-align: center;width: 75px;"><strong>รวม</strong></td>
                        <td style="text-align: center;"><strong>จำนวน(ชิ้น)</strong></td>
                        <td style="text-align: center;"><strong>คิว(CBM)</strong></td>
                        <td style="text-align: center;"><strong>ค่าขนส่ง(บาท/CBM)</strong></td>
                        <td style="text-align: center;width: 75px;"><strong>รวม</strong></td>
                    </tr>
                    <?php
                    for($i = 0; $i < 7; $i++):
                    ?>
                    <tr>
                        <td>ทางรถ(พิเศษ)</td>
                        <td style="text-align: right;">1.00</td>
                        <td style="text-align: right;">25.00</td>
                        <td style="text-align: right;">100.00</td>
                        <td style="text-align: right;">2,500.00</td>
                        <td style="text-align: right;">0.00</td>
                        <td style="text-align: right;">0.00</td>
                        <td style="text-align: right;">0.00</td>
                        <td style="text-align: right;">0.00</td>
                    </tr>
                    <?php
                    endfor;
                    ?>
                </table>
                <br>
                <table class="table-print" style="width: 100%;font-size: 12px;">
                    <tr style="background-color: #d2d2d2;">
                        <td colspan="2" style="text-align: center;"><strong>ข้อมูลการขนส่งในประเทศ</strong></td>
                    </tr>
                    <tr>
                        <td class="td-border-no" style="width: 150px;">ชื่อผู้รับ</td>
                        <td class="td-border-no">GUY</td>
                    </tr>
                    <tr>
                        <td class="td-border-no" >ที่อยู่</td>
                        <td class="td-border-no" ></td>
                    </tr>
                    <tr>
                        <td class="td-border-no" >ข้อมูลการขนส่งในประเทศ</td>
                        <td class="td-border-no" ></td>
                    </tr>
                </table>
                <br>
                <table style="width: 100%;font-size: 12px;">
                    <tr>
                        <td style="width: 150px;">อัตราแลกเปลี่ยน : </td>
                        <td>3.14</td>
                        <td style="text-align: right;">ค่าขนส่งระหว่างประเทศ(บาท) : &nbsp&nbsp&nbsp&nbsp</td>
                        <td style="text-align: right;border: 1px solid;">2,500.00</td>
                    </tr>
                    <tr>
                        <td style="width: 150px;">จำนวนแทร็ค : </td>
                        <td>ไม่ระบุ</td>
                        <td style="text-align: right;">ค่าขนส่งในประเทศจีน(บาท) : &nbsp&nbsp&nbsp&nbsp</td>
                        <td style="text-align: right;border: 1px solid;">0.00</td>
                    </tr>
                    <tr>
                        <td style="width: 150px;">หมายเหตุ(ลูกค้า) : </td>
                        <td>ไม่ระบุ</td>
                        <td style="text-align: right;">ค่าขนส่งในประเทศไทย(บาท) : &nbsp&nbsp&nbsp&nbsp</td>
                        <td style="text-align: right;border: 1px solid;">0.00</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td style="text-align: right;">ดำเนินการและค่าแพ็คสินค้า(บาท) : &nbsp&nbsp&nbsp&nbsp</td>
                        <td style="text-align: right;border: 1px solid;">0.00</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td style="text-align: right;">ยอดที่ต้องชำระ(บาท) : &nbsp&nbsp&nbsp&nbsp</td>
                        <td style="text-align: right;border: 1px solid;">2,500.00</td>
                    </tr>
                </table>
                <br>
                <table class="table-print" style="width: 100%;font-size: 12px;">
                    <tr style="background-color: #d2d2d2;">
                        <td style="text-align: center;"><strong>บัญชีธนาคารสำหรับชำระเงิน</strong></td>
                    </tr>
                    <tr>
                        <td style="text-align: center;">
                            ธนาคารกรุงศรีสาขารามอินทรากม.8
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<strong>ชื่อบัญชี</strong> : วรางครา สิริภูบาล
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<strong>เลขที่บัญชี</strong> : 276-1-56818-8
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center;">
                            ธนาคารกรุงศรีสาขารามอินทรากม.8
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<strong>ชื่อบัญชี</strong> : วรางครา สิริภูบาล
                            &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp<strong>เลขที่บัญชี</strong> : 276-1-56818-8
                        </td>
                    </tr>
                </table>
                <br>
                <table class="table-print" style="width: 100%;font-size: 12px;">
                    <tr style="background-color: #d2d2d2;text-align: center;">
                        <td style="width: 35px;"><strong>No.</strong></td>
                        <td style="width: 150px;"><strong>Products</strong></td>
                        <td style="width: 35px;"><strong>Pes</strong></td>
                        <td style="width: 35px;"><strong>Length</strong></td>
                        <td style="width: 35px;"><strong>Width</strong></td>
                        <td style="width: 35px;"><strong>Height</strong></td>
                        <td style="width: 35px;"><strong>CBM</strong></td>
                        <td style="width: 35px;"><strong>Weight(KG)</strong></td>
                        <td style="width: 35px;"><strong>วันที่ส่งออก</strong></td>
                    </tr>
                    <tr style="text-align: center;">
                        <td>1</td>
                        <td>เสื้อผ้า/กระเป๋าแบรนด์</td>
                        <td>1</td>
                        <td>61</td>
                        <td>41</td>
                        <td>52</td>
                        <td>
                            0.13
                            <br>
                            0.1
                        </td>
                        <td>
                            25
                            <br>25.0
                        </td>
                        <td>05.01.2021</td>
                    </tr>

                </table>
                <br>
                <p style="font-size: 12px;color:red">หมายเหตุ : ค่านำเข้าสินค้าหากวิธีการคำนวณใดได้ค่ามากกว่าจะคิดราคาตามนั้น คุณลูกค้าจำเป็นต้องชำระเงินให้เรียบร้อยก่อนการมารับสินค้าที่โกดังไทยและก่อนการจัดส่งสินค้าให้ทุกครั้ง</p>
                </div>
            </div>
        </page>                 
    </div>    
</div>
</div>




