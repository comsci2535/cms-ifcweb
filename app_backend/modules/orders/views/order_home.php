<div class="m-portlet m-portlet--full-height ">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    รายการสั่งซื้อล่าสุด
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a class="btn m-btn--pill btn-sm   btn-primary m-btn m-btn--custom" href="<?=base_url('orders')?>">
                    <i class="m-menu__link-icon fa fa-cart-plus"></i> แสดงทั้งหมด
                    </a>
                </li>  
            </ul>
        </div>
    </div>
    <div class="m-portlet__body text-overflow-body" style="height: 400px; overflow: auto;"> 
        <!--Begin::Timeline 3 -->
        <div class="m-timeline-3">
            <div class="m-timeline-3__items">
                <?php
                // arr($info);
                if(!empty($info)):
                    foreach($info as $order):
                ?>
                <div class="m-timeline-3__item m-timeline-3__item--info">
                    <span class="m-timeline-3__item-time"><?php echo !empty($order->created_at) ? date("H:i", strtotime($order->created_at)) : '';?></span>
                    <div class="m-timeline-3__item-desc">
                        <a href="<?=base_url('orders/edit/'.encode_id($order->order_id))?>">
                        <span class="m-timeline-3__item-text">
                            (เลขที่บิล : <?php echo !empty($order->order_code) ? $order->order_code : '';?>) , <?php echo !empty($order->status_name) ? $order->status_name : '';?>
                        </span><br>
                        <span class="m-timeline-3__item-user-name">
                            <span class=" m-link--metal m-timeline-3__item-link">
                                สั่งซื้อโดย. <?php echo !empty($order->name) ? $order->name : '';?> <?php echo !empty($order->lasname) ? $order->lasname : '';?>
                            </span>
                        </span>
                        </a>
                    </div>
                </div> 
                <?php
                    endforeach;
                endif;
                ?>
            </div> 
        </div>
        <!--End::Timeline 3 -->
    </div>
</div>
