
<div class="col-xl-12">
    <div class="m-portlet m-portlet--mobile ">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       รายการ
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                    <?php echo $boxAction; ?>
            </div>
        </div>
        <div class="m-portlet__body">

            <form  role="form">
                <div class="table-responsive">
                    <table id="data-list" class="table table-hover dataTable" width="100%">
                        <thead>
                            <tr>
                                <th><input name="tbCheckAll" type="checkbox" class="icheck tb-check-all"></th>
                                <th>วันที่สร้างรายการ</th>
                                <th>เลขที่ทำรายการ</th>
                                <th>ชื่อ-นามสกุล</th>
                                <th>จำนวนชิ้น</th>
                                <th>ยอดเงิน</th>
                                <!-- <th>ประเภทชำระเงิน</th>
                                <th>สถานะ</th> -->
                                <th>จัดการ</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </form> 
        </div>
    </div>
</div>

<!--begin::Modal order edit price-->
<div class="modal fade" id="modal-order-edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-order-edit-title">Order ID () แก้ไข</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-order-edit" method="POST" autocomplete="off">
                <div class="modal-body"> 
                    <div id="display-text-data-old" class="form-group" style="display: none;">
                        <h4 class="text-data-old"></h4> 
                    </div>
                    <div class="form-group">
                        <label for="order_edit" class="form-control-label">ต้องการแก้ไข:</label>
                        <input type="text" class="form-control" id="order_edit" name="order_edit" placeholder="ระบุต้องการแก้ไข..."  required>
                    </div>
                    <div class="form-group">
                        <label for="order_remark" class="form-control-label">หมายเหตุ:</label>
                        <textarea class="form-control" id="order_remark" name="order_remark" rows="5" cols="5" placeholder="ระบุหมายเหตุ..."></textarea>
                    </div> 
                    <input type="hidden" id="order-code" name="code">
                    <input type="hidden" id="order-type" name="type"> 
                    <input type="hidden" id="order-total" name="discount">
                    <input type="hidden" id="order-csrfToken" name="csrfToken"> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิดหน้าต่าง</button>
                    <button type="submit" class="btn btn-primary btn-submit-edit-order">บันทึกข้อมูล</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!--begin::Modal order edit ปรับสถานะ-->
<div class="modal fade" id="modal-order-status" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-order-status-title">Order ID () ปรับสถานะ</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-order-edit-status" method="POST" autocomplete="off">
                <div class="modal-body"> 
                    <div id="display-text-data-old" class="form-group" style="display: none;">
                        <h4 class="text-data-old"></h4> 
                    </div>
                    <div class="form-group"> 
                        <p id="text-status-name" class="m-badge m-badge--success m-badge--wide"></p>
                        <br>
                        <label for="status_id" class="form-control-label">เลือกสถานะ:</label> 
                        <select name="status_id" id="status_id" class="form-control select2" style="width: 100%;">
                            <?php
                            if(!empty($status)):
                                foreach($status as $item):
                            ?>
                                <option value="<?php echo !empty($item->status_id) ? $item->status_id : '';?>"><?php echo !empty($item->title) ? $item->title : '';?></option>
                            <?php
                                endforeach;
                            endif;
                            ?> 
                        </select> 
                    </div> 
                    <input type="hidden" id="order-status-code" name="code">   
                    <input type="hidden" id="order-status-csrfToken" name="csrfToken"> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิดหน้าต่าง</button>
                    <button type="submit" class="btn btn-primary btn-submit-edit-order">บันทึกข้อมูล</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!--begin::Modal order edit comment-->
<div class="modal fade" id="modal-order-comment" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modal-order-comment-title">Order ID () กรอกหมายเหตุ(ลูกค้า)</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="form-order-comment" method="POST" autocomplete="off">
                <div class="modal-body"> 
                    <div class="form-group">
                        <label for="note" class="form-control-label">หมายเหตุ(ลูกค้า):</label>
                        <textarea class="form-control" id="note" name="note" rows="5" cols="5" placeholder="ระบุหมายเหตุ(ลูกค้า)..." required></textarea>
                    </div> 
                    <input type="hidden" id="order-comment-type" name="type"> 
                    <input type="hidden" id="order-comment-code" name="code">
                    <input type="hidden" id="order-comment-csrfToken" name="csrfToken"> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิดหน้าต่าง</button>
                    <button type="submit" class="btn btn-primary btn-submit-edit-order">บันทึกข้อมูล</button>
                </div>
            </form>
        </div>
    </div>
</div>