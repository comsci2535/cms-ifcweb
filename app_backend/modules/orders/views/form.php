<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div> 
       
        <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post', 'autocomplete' => 'off')) ?>
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="order_code">เลขที่สั่งซื้อ</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->order_code) ? $info->order_code : NULL ?>" type="text" class="form-control m-input " name="order_code" id="order_code" placeholder="ระบุเลขที่สั่งซื้อ" disabled required>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="name">ชื่อ</label>
                    <div class="col-sm-3">
                        <input value="<?php echo isset($info->name) ? $info->name : NULL ?>" type="text" class="form-control m-input " name="name" id="name" placeholder="ระบุชื่อ" required disabled>
                    </div>
                    <label class="col-sm-1 col-form-label" for="lasname">นามสกุล</label>
                    <div class="col-sm-3">
                        <input value="<?php echo isset($info->lasname) ? $info->lasname : NULL ?>" type="text" class="form-control m-input " name="lasname" id="lasname" placeholder="ระบุนามสกุล" required disabled>
                    </div>
                </div> 
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="address">ที่อยู่</label>
                    <div class="col-sm-7">
                        <textarea name="address" rows="3" class="form-control" id="address" placeholder="ระบุ" required disabled><?php echo isset($info->address) ? $info->address : NULL ?></textarea>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <input type="hidden" id="datajson" name="datajson"
                    value="<?php echo $this->config->item('template') ?>assets/plugins/jquery.Thailand.js/database/db.json">
                    <label class="col-sm-2 col-form-label" for="districts">ตำบล <span class="text-danger"> *</span></label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control m-input" id="districts" placeholder="ตำบล"
                        name="districts"
                        value="<?php echo isset($info->districts) ? $info->districts : NULL ?>" disabled>
                    </div>
                    <label class="col-sm-1 col-form-label" for="amphures">อำเภอ <span class="text-danger"> *</span></label>
                    <div class="col-lg-3">
                        <input type="text" class="form-control m-input" id="amphures" placeholder="อำเภอ" name="amphures"
                        value="<?php echo isset($info->amphures) ? $info->amphures : NULL ?>" disabled>
                    </div>
                </div> 
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="provinces">จังหวัด <span class="text-danger"> *</span></label>
                    <div class="col-lg-3">
                        <input type="text" class="form-control m-input" id="provinces" placeholder="จังหวัด"
                        name="provinces"
                        value="<?php echo isset($info->provinces) ? $info->provinces : NULL ?>" disabled>
                    </div>
                    <label class="col-sm-1 col-form-label" for="zip_code">รหัสไปรษณีย์ <span class="text-danger">
                    *</span></label>
                    <div class="col-lg-3">
                        <input type="text" class="form-control m-input" id="zip_code" placeholder="รหัสไปรษณีย์"
                        name="zip_code" value="<?php echo isset($info->zip_code) ? $info->zip_code : NULL ?>" disabled>
                    </div>
                </div>  
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="tel">เบอร์โทรศัพท์</label>
                    <div class="col-sm-3">
                        <input value="<?php echo isset($info->tel) ? $info->tel : NULL ?>" type="text" class="form-control m-input " name="tel" id="tel" placeholder="ระบุเบอร์โทรศัพท์" required disabled>
                    </div>
                    <label class="col-sm-1 col-form-label" for="email">อีเมล</label>
                    <div class="col-sm-3">
                        <input value="<?php echo isset($info->email) ? $info->email : NULL ?>" type="text" class="form-control m-input " name="email" id="email" placeholder="ระบุอีเมล" disabled>
                    </div>
                </div> 
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="note">หมายเหตุ(ลูกค้า)</label>
                    <div class="col-sm-7">
                        <textarea name="note" rows="3" class="form-control" id="note" placeholder="ระบุ" disabled><?php echo isset($info->note) ? $info->note : NULL ?></textarea>
                    </div>
                </div>
                <!-- <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="order_code">ใบกำกับภาษี</label>
                    <div class="col-sm-7">
                        <div class="m-2 m-checkbox-inline">
                            <label class="m-checkbox">
                                <input type="checkbox" <?php if(isset($info->is_invoice) &&  $info->is_invoice=='1'){ echo "checked";}?>  class="text-attributes-line-checkbox" id="is_invoice" name="is_invoice" value="1"> ต้องการใบกำกับภาษี
                                <span></span>
                            </label>    
                            <label class="m-checkbox">
                                <input type="checkbox" <?php if(isset($info->invoice_type) &&  $info->invoice_type=='1'){ echo "checked";}?>  class="text-attributes-line-checkbox" id="invoice_type" name="invoice_type" value="1"> ใช้ที่อยู่จัดส่ง
                                <span></span>
                            </label>    
                        </div>
                    </div>
                </div> -->
                <?php
                $text = '';
                if(!isset($info->is_invoice) ? $info->is_invoice : 0  > 0 && !isset($info->invoice_type)? $info->invoice_type : 0 > 0):
                    $text = 'none';
                else:
                    $text = 'show';
                    
                endif;
                ?> 
                <div class="m-view display-vat vat-all" style="display: none;"> 
                    <div class="form-group m-form__group row">
                            <label class="col-sm-2"></label>
                            <div class="col-sm-7">
                                <h5>ข้อมูลใบกำกับภาษี</h5>
                            </div>
                        </div> 
                        <div class="form-group m-form__group row display-vat invoice_no">
                            <label class="col-sm-2 form-col-form-label">เลขที่ผู้เสียภาษี<span class="text-danger"> *</span></label>
                            <div class="col-sm-7">
                                <input type="text" name="vat_code" class="form-control m-input vat-code" required
                                placeholder="เลขที่ผู้เสียภาษี" value="<?php echo isset($info->invoice_no) ? $info->invoice_no : '' ?>"  >
                            </div>
                        </div>
                        <div class="m-view-vat">   
                            <div class="form-group m-form__group row">
                                <label class="col-sm-2 form-col-form-label">เบอร์โทรศัพท์<span class="text-danger"> *</span></label>
                                <div class="col-sm-7">
                                    <input type="text" name="invoice_tel" class="form-control m-input phone-with-add"
                                    placeholder="เบอร์โทรศัพท์" value="<?php echo isset($info->invoice_tel) ? $info->invoice_tel : '' ?>" >
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-sm-2 form-col-form-label">ที่อยู่<span class="text-danger"> *</span></label>
                                <div class="col-sm-7">
                                    <textarea name="customer_address_vat"  rows="3" class="form-control m-input"
                                    placeholder="ที่อยู่" ><?php echo isset($info->invoice_address) ? $info->invoice_address : '' ?></textarea>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <input type="hidden" id="datajson_vat" name="datajson_vat"
                                value="<?php echo $this->config->item('template') ?>assets/plugins/jquery.Thailand.js/database/db.json">
                                <div class="col-sm-3 offset-sm-2 m-form__group-sub">
                                    <label class="form-col-form-label">ตำบล<span class="text-danger"> *</span></label>
                                    <input type="text" class="form-control m-input" id="invoice_districts_id" placeholder="ตำบล"
                                    name="invoice_districts_id" value="<?php echo isset($info->invoice_districts_id) ? $info->invoice_districts_id : '' ?>">
                                </div>
                                <div class="col-sm-3 offset-sm-1 m-form__group-sub">
                                    <label class="form-col-form-label">อำเภอ<span class="text-danger"> *</span></label>
                                    <input type="text" class="form-control m-input" id="invoice_amphures_id" placeholder="อำเภอ"
                                    name="invoice_amphures_id" value="<?php echo isset($info->invoice_amphures_id) ? $info->invoice_amphures_id : '' ?>">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <div class="col-sm-3 offset-sm-2 m-form__group-sub">
                                    <label class="form-col-form-label">จังหวัด<span class="text-danger"> *</span></label>
                                    <input type="text" class="form-control m-input" id="invoice_provinces_id" placeholder="จังหวัด"
                                    name="invoice_provinces_id" value="<?php echo isset($info->invoice_provinces_id) ? $info->invoice_provinces_id : '' ?>">
                                </div>
                                <div class="col-sm-3 offset-sm-1 m-form__group-sub">
                                    <label class="form-col-form-label">รหัสไปรษณีย์<span class="text-danger"> *</span></label>
                                    <input type="text" class="form-control m-input" id="invoice_zip_code" placeholder="รหัสไปรษณีย์"
                                    name="invoice_zip_code" value="<?php echo isset($info->invoice_zip_code) ? $info->invoice_zip_code : '' ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                <?php
                if(!empty($info->status) && $info->status == 5):
                ?>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="tracking_code">เลขที่นำส่ง</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->tracking_code) ? $info->tracking_code : NULL ?>" type="text" class="form-control m-input " name="tracking_code" id="tracking_code" placeholder="ระบุเลขที่นำส่ง" required>
                    </div>
                </div>
                <?php
                endif;
                ?> 
                <?php
                 if(!empty($details)):
                ?>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="">รายการสินค้า</label>
                    <div class="col-sm-8">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th style="width:5%">ลำดับ</th>
                                        <th class="text-left" style="width:20%">ชื่อสินค้า</th>
                                        <th class="text-left" style="width:20%">ลักษณะสินค้า</th>
                                        <th style="width:7%">ราคา</th>
                                        <th style="width:7%">จำนวน</th>
                                        <th style="width:7%">รวม</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php  
                                    $sumTotal = 0;
                                    if(!empty($details)):
                                        $total = 0;
                                        foreach($details as $key => $item):
                                            $total      = $item->product_price * $item->quantity;
                                            $sumTotal  += $total;
                                    ?>
                                    <tr>
                                        <td class="text-center"><?=($key+1);?></td>
                                        <td>
                                            <?php echo !empty($item->title) ? $item->title : '';?>
                                            <?php
                                            if(!empty($item->image)):
                                            ?>
                                            <br>
                                            <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal-<?=$item->order_detail_id?>">
                                                <small><i class="fa fa-file-image" aria-hidden="true"></i> ดูรูปภาพ</small>
                                                <small>(<?php echo !empty($item->from_web) ? $item->from_web:'';?>)</small>
                                            </a>
                                            <br>
                                            <a href="<?php echo !empty($item->from_web_url) ? $item->from_web_url:'';?>" target="_blank">
                                                <div style="background-color: #f5f5f5;padding: 7px;width: 200px;overflow: auto;height: 80px;">
                                                    <small><?php echo !empty($item->from_web_url) ? $item->from_web_url:'';?></small>
                                                </div>
                                            </a>
                                            <!-- Modal -->
                                            <div class="modal fade" id="myModal-<?=$item->order_detail_id?>" role="dialog">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                       
                                                        <div class="modal-body">
                                                            <?php
                                                                foreach($item->image as $image):
                                                            ?>
                                                            <a href="<?php echo !empty($image) ? $image : '';?>" target="_blank">
                                                                <img src="<?php echo !empty($image) ? $image : '';?>" 
                                                                alt="<?php echo !empty($item->title) ? $item->title : '';?>"
                                                                style="width: 100%;">
                                                            </a>
                                                            <?php
                                                                endforeach;
                                                            ?>
                                                        
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            endif;
                                            ?>
                                        </td>
                                        <td>
                                            <div class="m-list-timeline m-list-timeline--skin-light">
                                            <?php
                                            if(!empty($item->attributes)):
                                                foreach($item->attributes as $key => $attribute):
                                            ?>
                                                <div class="m-list-timeline__items">
                                                    <div class="m-list-timeline__item">
                                                        <span class="m-list-timeline__badge m-list-timeline__badge--success"></span>
                                                        <span class="m-list-timeline__text">
                                                            <?php echo !empty($attribute->attribute_name) ? $attribute->attribute_name : '';?>
                                                            <?php echo !empty($attribute->attribute_value) ? '('.$attribute->attribute_value.')' : '';?>
                                                        </span>
                                                    </div>
                                                </div>
                                            <?php
                                                endforeach;
                                            else:
                                            ?>
                                            <div class="m-list-timeline__items">
                                                <div class="m-list-timeline__item">
                                                    <span class="m-list-timeline__badge m-list-timeline__badge--danger"></span>
                                                    <span class="m-list-timeline__text">
                                                        ไม่ระบุลักษณะสินค้า
                                                    </span>
                                                </div>
                                            </div>
                                            <?php
                                            endif;
                                            ?>
                                            </div>
                                        </td>
                                        <td class="text-right"><?php echo !empty($item->product_price) ? number_format($item->product_price) : 0;?></td>
                                        <td class="text-center"><?php echo !empty($item->quantity) ? $item->quantity : 0;?></td>
                                        <td class="text-right"><?=number_format($total);?></td>
                                    </tr>
                                    <?php
                                        endforeach;
                                    endif;
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="5" class="text-left">รวมเงิน</th>
                                        <th class="text-right"><?=number_format($sumTotal);?></th>
                                    </tr>
                                    <tr>
                                        <th colspan="6" class="text-left">ประเภทการชำระเงิน 
                                            <span for="" class="m-2 m-badge m-badge--primary  m-badge--wide">
                                            <i class="fa fa-check-circle"></i>     
                                            <?php echo !empty($banks_name) ? $banks_name : '';?></span>
                                            <span class="m-2 m-badge m-badge--success m-badge--wide">
                                            <i class="fa fa-check-circle"></i> 
                                            <?php echo !empty($payment) ? $payment : '';?>
                                            </span>
                                            <span class="m-2 m-badge m-badge--info m-badge--wide" data-toggle="modal" data-target="#modal-payment-img" style="cursor: pointer;">
                                            <i class="fa fa-file-image"></i> 
                                                หลักฐานชำระเงิน
                                            </span>

                                            <!--begin::Modal-->
                                            <div class="modal fade" id="modal-payment-img" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">หลักฐานชำระเงิน</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body"> 
                                                            <br>
                                                            <a href="<?php echo !empty($payment_file) ? $this->config->item('root_url').$payment_file : '';?>" target="_blank" rel="noopener noreferrer">
                                                                <img class="img-thumbnail" src="<?php echo !empty($payment_file) ? $this->config->item('root_url').$payment_file : '';?>" onerror="this.src='<?=$this->config->item('root_url').'images/no-image.png';?>'" style="width:100%">
                                                            </a> 
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิดหน้าต่าง</button> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </th> 
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div> 
                <?php
                endif;
                ?>
                
                <div class="form-group m-form__group row">
                    <div class="col-sm-12">
                        <hr>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="transports">บริษัทจัดส่ง</label>
                    <div class="col-sm-8">
                        <h5 class="m-2 m-badge m-badge--success m-badge--wide"><i class="fa fa-truck"></i>
                            <?php echo !empty($tracking_title) ? $tracking_title : 'ไม่ระบุ';?>
                        </h5> 
                    </div>
                </div>
                <?php
                if(!empty($import_order)):
                ?>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="transports">รายละเอียด Order</label>
                    <div class="col-sm-10">
                        <span class="m-2 m-badge m-badge--success m-badge--wide"><i class="fa fa-check-circle"></i>
                            ประเภท : <?php echo !empty($info->order_type) ? $info->order_type : 'ไม่ระบุ';?>
                        </span>
                        <span class="m-2 m-badge m-badge--success m-badge--wide"><i class="fa fa-check-circle"></i>
                            วันที่ขึ้นตู้ : <?php echo !empty($info->updated_at) ? $info->updated_at : 'ไม่ระบุ';?>
                        </span>
                        <span class="m-2 m-badge m-badge--success m-badge--wide"><i class="fa fa-check-circle"></i>
                            จำนวนชิ้น : <?php echo !empty($info->order_qty) ? $info->order_qty : 0;?>
                        </span>
                        <span class="m-2 m-badge m-badge--success m-badge--wide"><i class="fa fa-check-circle"></i>
                            <?php echo !empty($info->discount) ? $info->discount : '';?>
                        </span>    
                    </div>
                </div> 

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="">รายการสินค้า</label>
                    <div class="col-sm-9"> 
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr> 
                                        <th class="text-left" style="width:30%">ข้อมูลขนส่ง</th>
                                        <th class="text-left" style="width:15%">เลขพัสดุ</th>
                                        <th class="text-left" style="width:20%">รายละเอียดข้อมูล</th>
                                        <th class="text-left" style="width:20%">ค่าบริการ</th>
                                        <th class="text-left" style="width:20%">ค่านำส่ง</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php   
                                    $total_size             = 0;
                                    $total_weight           = 0;
                                    $total_service          = 0;
                                    $total_cost             = 0;
                                    $deliverys_price        = !empty($info->deliverys_price) ? $info->deliverys_price : 0;
                                    $deliverys_price_extra  = !empty($info->deliverys_price_extra) ? $info->deliverys_price_extra : 0;
                                    if(!empty($import_order)): 
                                        foreach($import_order as $item): 
                                            $total_size += !empty($item['size']) ? $item['size'] : 0;
                                            $total_weight += !empty($item['weight']) ? $item['weight'] : 0;
                                            $total_service += !empty($item['service']) ? $item['service'] : 0;
                                            $total_cost += !empty($item['cost']) ? $item['cost'] : 0;
                                            $total_repack_sack += !empty($item['repack_sack']) ? $item['repack_sack'] : 0; 
                                    ?>
                                    <tr> 
                                        <td><?php echo !empty($item['detail_send']) ?$item['detail_send'] : '';?></td>
                                        <td><?php echo !empty($item['tracking_china']) ?$item['tracking_china'] : '';?></td>
                                        <td><?php echo !empty($item['detail']) ?$item['detail'] : '';?></td>
                                        <td><?php echo !empty($item['service_charge']) ?$item['service_charge'] : '';?></td> 
                                        <td><?php echo !empty($item['import_cost']) ?$item['import_cost'] : '';?></td>
                                    </tr>
                                    <?php
                                        endforeach;

                                    endif;
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="4" style="text-align: right;">
                                            จำนวน :
                                            <br>น้ำหนัก :
                                            <br>ขนาด :
                                            <br>ค่าบริการ :
                                            <br>ค่านำเข้า/ขนส่ง :
                                            <br>ค่าขนส่งในประเทศ :
                                            <br>ค่าบริการรีแพ็คลัง/กระสอบ :
                                            <br>ค่าขนส่งในประเทศคำนวณเพิ่มอีก 20% :
                                            <br>รวม :
                                        </th> 
                                        <th style="text-align: right;">
                                            <?php echo !empty($info->order_qty) ? $info->order_qty : 0;?>
                                            <br><?php echo !empty($total_weight) ? $total_weight : 0;?>
                                            <br><?php echo !empty($total_size) ? $total_size : 0;?>
                                            <br><?php echo !empty($total_service) ? number_format($total_service) : 0;?> บาท
                                            <br><?php echo !empty($total_cost) ? number_format($total_cost) : 0;?> บาท
                                            <br><?php echo number_format($deliverys_price);?> บาท
                                            <br><?php echo number_format($total_repack_sack);?> บาท
                                            <br><?php echo number_format($deliverys_price_extra);?> บาท
                                            <br><?php echo number_format($total_service + $total_cost + $deliverys_price + $deliverys_price_extra + $total_repack_sack);?> บาท
                                        </th> 
                                    </tr>
                                    <?php
                                    // arr($payment_import);
                                    if(!empty($payment_import)):
                                    ?>
                                    <tr>
                                        <th colspan="5" class="text-left">ประเภทการชำระเงิน 
                                            <span for="" class="m-2 m-badge m-badge--primary  m-badge--wide">
                                            <i class="fa fa-check-circle"></i>     
                                            <?php echo !empty($payment_import_banks_name) ? $payment_import_banks_name : '';?></span>
                                            <span class="m-2 m-badge m-badge--success m-badge--wide">
                                            <i class="fa fa-check-circle"></i> 
                                            <?php echo !empty($payment_import) ? $payment_import : '';?>
                                            </span>
                                            <span class="m-2 m-badge m-badge--info m-badge--wide" data-toggle="modal" data-target="#modal-payment-import-img" style="cursor: pointer;">
                                            <i class="fa fa-file-image"></i> 
                                                หลักฐานชำระเงิน
                                            </span>

                                            <!--begin::Modal-->
                                            <div class="modal fade" id="modal-payment-import-img" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">หลักฐานชำระเงิน</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body"> 
                                                            <br>
                                                            <a href="<?php echo !empty($payment_import_file) ? $this->config->item('root_url').$payment_import_file : '';?>" target="_blank" rel="noopener noreferrer">
                                                                <img class="img-thumbnail" src="<?php echo !empty($payment_import_file) ? $this->config->item('root_url').$payment_import_file : '';?>" onerror="this.src='<?=$this->config->item('root_url').'images/no-image.png';?>'" style="width:100%">
                                                            </a> 
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิดหน้าต่าง</button> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </th> 
                                    </tr>
                                    <?php
                                    endif;
                                    ?>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div> 
                <?php
                endif;
                ?>

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="order_code">สถานะการสั่งซื้อ</label>
                    <div class="col-sm-7">
                        <p class="m-2 m-badge m-badge--primary m-badge--wide"><i class="fa fa-check-circle"></i> <?php echo !empty($status_title) ? $status_title : '';?></p> 
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">

                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <!-- <button type="submit" class="btn btn-success m-btn--wide">บันทึก</button> -->
                            <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                        </div>
                    </div>
                    
                </div>
            </div>
             <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
             <input type="hidden" class="form-control" name="db" id="db" value="orders">
             <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->order_id) ? encode_id($info->order_id) : 0 ?>">
        <?php echo form_close() ?> 
        <!--end::Form-->
    </div>
</div>





