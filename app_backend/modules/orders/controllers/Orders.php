<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

require APPPATH.'libraries/barcode/BarcodeGenerator.php'; 
require APPPATH.'libraries/barcode/BarcodeGeneratorHTML.php';
require APPPATH.'libraries/barcode/BarcodeGeneratorPNG.php'; 

class Orders extends MX_Controller 
{

    private $_title             = "จัดการข้อมูลสั่งซื้อสินค้า";
    private $_pageExcerpt       = "การจัดการข้อมูลเกี่ยวกับจัดการข้อมูลสั่งซื้อสินค้า";
    private $_grpContent        = "orders";
    private $_requiredExport    = true;
    private $_permission;

    public function __construct()
    {
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }

        $this->payment = array('ชำระผ่านการโอน', 'จ่ายผ่าน PayPal', 'เก็บเงินปลายทาง');
        $this->order_type = array('ฝากซื้อ', 'นำเข้าสินค้า');

        $this->load->library('ckeditor');
        $this->load->model("orders_m"); 
        $this->load->model("status/status_m"); 
        $this->load->model("info/info_m"); 
        $this->load->model("banks/banks_m"); 
        $this->load->model("delivery_companys/delivery_companys_m");
        $this->load->model("members/members_m");
        $this->load->model('separate_orders/separate_orders_m');
        $this->load->model('attribute_sends/attribute_sends_m');
        $this->load->model('type_transportations_setting/type_transportations_setting_m');
        
        
    }
    
    public function index()
    {
        $this->load->module('template');
        
        // toobar
        $export = array(
            'excel' => site_url("{$this->router->class}/excel"),
            'pdf'   => site_url("{$this->router->class}/pdf"),
        );

        // $action[0][]        = action_send_order(site_url("{$this->router->class}"));
        $action[1][]        = action_refresh(site_url("{$this->router->class}"));
        $action[2][]        = action_trash_multi("{$this->router->class}/destroy");

        $data['boxAction']  = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("{$this->router->class}"));
        
        $input['recycle']   = 0;
        $info               = $this->status_m->get_rows($input);
        $data['status']     = $info->result();

        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/index";
        
        $this->template->layout($data);
    }    

    public function data_index()
    {
        $input              = $this->input->post();
        $input['recycle']   = 0; 
        $info               = $this->orders_m->get_rows($input);
        $infoCount          = $this->orders_m->get_count($input);
        $column             = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }
        
        foreach ($info->result() as $key => $rs) {
            $id                         = encode_id($rs->order_id);
            $action                     = array(); 
            $action[1][]                = action_list($this->set_action($id));
            // $action[1][]                = table_edit(site_url("{$this->router->class}/edit/{$id}"));
            // $action[1][]                = table_print(site_url("{$this->router->class}/print/{$id}"), array(
            //                                                                                         'title' => 'ปริ้นใบสั่งสินค้า/ใบติดกล่อง',
            //                                                                                         'icon'  => 'print'
            //                                                                                         ));
            // if($rs->is_invoice > 0):                                                                                     
            //     $action[1][]  = table_print(site_url("{$this->router->class}/invoice/{$id}"), array(
            //                                                                                     'title' => 'ปริ้นใบกำกับภาษี',
            //                                                                                     'icon'  => 'file-text-o'
            //                                                                                     ));
            // endif; 
             
            $status_action  = $this->orders_m->get_orders_status_action_by_order_id($rs->order_id, 'desc')->row();
            
            $log_status     = table_status_action($status_action, 'one');
            $infoStatus     = $this->status_m->get_status_by_id($rs->status)->row(); 

            $text_order = '<p>';
            $text_order.= 'เลขที่บิล : <span class="text-order-code">'.$rs->order_code.'</span>';
            $text_order.= '<br>ขึ้นตู้ : '.(!empty($rs->updated_at) ? date_languagefull($rs->updated_at, false, ""): null);
            $text_order.= '<br>สถานะ : <span class="text-order-status" data-id="'.$rs->status.'">'.(!empty($infoStatus->title) ? $infoStatus->title : '').'</span>';
            $text_order.= '<br>ประเภท : '.$this->order_type[$rs->order_type];
            $text_order.= '</p>'; 

            $text_qty = '<p class="text-order-qty" data-qty="'.$rs->order_qty.'">';
            $text_qty.= 'จำนวนชิ้น  : '.$rs->order_qty; 
            $text_qty.= '</p>'; 
            
            $total          = 0;
            $import_order   = Modules::run('separate_orders/get_order_import', $rs->order_id);
            if(!empty($import_order)):
                foreach($import_order as $item):
                    $total += $item['service'] + $item['cost']; 
                endforeach;
            endif;
            $sumTotal           = 0;
            $input_['order_id'] = $rs->order_id;
            $orders_detail = $this->orders_m->get_orders_detail_by_param($input_)->result();
            if(!empty($orders_detail)):
                foreach($orders_detail as $detail):
                    $sumTotal  += $detail->product_price * $detail->quantity; 
                endforeach;
            endif;

            $input_['order_id'] = $rs->order_id;
            $orders_detail = $this->orders_m->get_orders_detail_url_by_param($input_)->result();
            if(!empty($orders_detail)):
                foreach($orders_detail as $detail):
                    $sumTotal  += $detail->product_price * $detail->quantity; 
                endforeach;
            endif;

            $deliverys_price        = !empty($rs->deliverys_price) ? $rs->deliverys_price : 0;
            $deliverys_price_extra  = !empty($rs->deliverys_price_extra) ? $rs->deliverys_price_extra : 0;
            $deliverys_repack_sack  = !empty($rs->deliverys_repack_sack) ? $rs->deliverys_repack_sack : 0;

            $total  = $total + $sumTotal + $deliverys_price + $deliverys_price_extra + $deliverys_repack_sack;
            
            $text_discount = '<p class="text-order-price" data-price="'.$total.'">';
            $text_discount.= 'ยอดรวมค่า'.$this->order_type[$rs->order_type].'  : '.number_format($total).' บาท'; 
            $text_discount.= '</p>';

            $column[$key]['DT_RowId']       = $id;
            $column[$key]['checkbox']       = "<input type='checkbox' class='icheck tb-check-single'><input type='hidden' class='text-note' value='".$rs->note."'>";
            $column[$key]['name']           = $rs->name.' '.$rs->lasname;
            $column[$key]['order_code']     = $text_order;
            $column[$key]['qty']            = $text_qty;
            $column[$key]['email']          = $rs->email;
            $column[$key]['tracking_code']  = $rs->tracking_code;
            $column[$key]['discount']       = $text_discount; 
            $column[$key]['created_at']     = datetime_table($rs->created_at).'<br>'.$log_status;
            $column[$key]['updated_at']     = datetime_table($rs->updated_at);
            $column[$key]['action']         = Modules::run('utils/build_button_group', $action); 
            
        }
        $data['data']               = $column;
        $data['recordsTotal']       = $info->num_rows();
        $data['recordsFiltered']    = $infoCount;
        $data['draw']               = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function get_orders_home()
    {
        $input['order'][0]['column']    = 1;
        $input['order'][0]['dir']       = 'DESC';
        $input['recycle']               = 0;
        $input['length']                = 10;
        $input['start']                 = 0;
        $info                           = $this->orders_m->get_rows($input)->result();  
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }  

        if(!empty($info)):
            foreach($info as $item):
                $infoStatus         = $this->status_m->get_status_by_id($item->status)->row(); 
                $item->status_name  = !empty($infoStatus->title) ? $infoStatus->title :'';
            endforeach;
        endif; 

        $data['info']               = $info;

        $this->load->view('order_home', $data);
    }

    private function set_action($code)
    {
        $action_arr = array(
            array(
                'title' => 'ดูรายละเอียด',
                'icon'  => 'fa fa-eye',
                'class' => '',
                'id'    => '',
                'code'  => '',
                'target'=> '',
                'url'   => site_url("{$this->router->class}/edit/{$code}")
            ),
            array(
                'title' => 'ปรับสถานะ',
                'icon'  => 'fa fa-edit',
                'class' => 'btn-edit-status',
                'id'    => '',
                'code'  => $code,
                'target'=> '',
                'url'   => ''
            ),
            // array(
            //     'title' => 'แก้ไขจำนวนเงิน',
            //     'icon'  => 'fa fa-edit',
            //     'class' => 'btn-edit-price',
            //     'id'    => '',
            //     'code'  => $code,
            //     'target'=> '',
            //     'url'   => ''
            // ),
            array(
                'title' => 'แก้ไขจำนวนชิ้น',
                'icon'  => 'fa fa-edit',
                'class' => 'btn-edit-qty',
                'id'    => '',
                'code'  => $code,
                'target'=> '',
                'url'   => ''
            ),
            array(
                'title' => ' แยกรายการ order',
                'icon'  => 'fa fa-retweet',
                'class' => '',
                'id'    => '',
                'code'  => '',
                'target'=> '',
                'url'   => 'separate_orders/index/'.$code
            ),
            array(
                'title' => 'กรอกหมายเหตุ(ลูกค้า)',
                'icon'  => 'fa fa-comments',
                'class' => 'btn-edit-comment',
                'id'    => '',
                'code'  => $code,
                'target'=> '',
                'url'   => ''
            )
            ,
            array(
                'title' => ' ปริ้นบิลการสั่งซื้อ',
                'icon'  => 'fa fa-print',
                'class' => '',
                'id'    => '',
                'code'  => '',
                'target'=> '_blank',
                'url'   => site_url("{$this->router->class}/pdf/{$code}")
            )
        );

        return $action_arr;
    }
    
    public function create()
    {
        $this->load->module('template');

        $data['grpContent']     = $this->_grpContent;
        $data['frmAction']      = site_url("{$this->router->class}/storage");
        
        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][]   = array('สร้าง', site_url("{$this->router->class}/create"));
        
        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/form";
        
        $this->template->layout($data);
    }
    
    public function storage()
    {
        $input  = $this->input->post(null, true);
        $value  = $this->_build_data($input);
        $result = $this->orders_m->insert($value);
        if ( $result ) {
           Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}"));
    }
    
    public function edit($id="")
    {
        $this->load->module('template');
        
        $id                    = decode_id($id);
        $input['order_id']     = $id;
        $info                  = $this->orders_m->get_rows($input);
        if ( $info->num_rows() == 0) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
            redirect_back();
        } 
        $info                   = $info->row(); 
        $sumTotal               = 0;
        $infoDetail             = $this->orders_m->get_orders_detail_by_order_id($info->order_id)->result(); 
        if(!empty($infoDetail)):
            foreach($infoDetail as $item):
                $sumTotal  += $item->product_price * $item->quantity; 
                $item->attributes = $this->orders_m->get_orders_detail_attribute_id($item->order_detail_uniqid)->result();
            endforeach;
        endif;
        
        $detail_url = $this->orders_m->get_orders_detail_url_by_order_id($info->order_id)->result();

        if(!empty($detail_url)):
            foreach($detail_url as $url):
                $sumTotal  += $url->product_price * $url->quantity; 
                $attribute = explode('<=>', $url->attribute);
                $obj_attr  =  new stdClass();
                if(!empty($attribute)):
                    foreach($attribute as $key=> $attr):
                        $attributes = explode(':', $attr);
                        @$obj_attr->$key->attribute_name = $attributes[0];
                        @$obj_attr->$key->attribute_value = $attributes[1];
                    endforeach;
                endif;
                $url->attributes = (array)$obj_attr;
                $url->image = explode(',', $url->image);
            endforeach;

        endif;
        
        $data['status_title']   = $this->status_m->get_status_by_id($info->status)->row()->title; 
        $data['details']        = array_merge($infoDetail, $detail_url);
        
        $input__['type']        = 0;
        $input__['order_id']    = $info->order_id;
        $orders_payment         = $this->orders_m->get_orders_payments_by_param($input__)->row();
        if(!empty($orders_payment)):
            $data['payment']            = $this->payment[$orders_payment->payment_type];
            $data['payment_file']       = $orders_payment->file;
            $input_['bank_id']          = $orders_payment->bank_id;
            $banks                      = $this->banks_m->get_rows($input_)->row();
            $data['banks_name']         = !empty($banks->title) ? $banks->title : null; 
        endif;


        $input___['type']        = 1;
        $input___['order_id']    = $info->order_id;
        $orders_payments         = $this->orders_m->get_orders_payments_by_param($input___)->row();
        
        if(!empty($orders_payments)):
            $data['payment_import']             = $this->payment[$orders_payments->payment_type];
            $data['payment_import_file']        = $orders_payments->file;
            $input_['bank_id']                  = $orders_payments->bank_id;
            $banks                              = $this->banks_m->get_rows($input_)->row();
            $data['payment_import_banks_name']  = !empty($banks->title) ? $banks->title : null; 
        endif;

        $info->order_type       = !empty($this->order_type[$info->order_type]) ? $this->order_type[$info->order_type] : '';
        $info->updated_at       = (!empty($info->updated_at) ? date_languagefull($info->updated_at, false, ""): null);  
        $data['import_order']   = Modules::run('separate_orders/get_order_import', $id); 
        $total                  = 0;
        if(!empty($data['import_order'])):
            foreach($data['import_order'] as $item):
                $total += $item['service'] + $item['cost']; 
            endforeach;
        endif;
        $info->discount         = 'ยอดรวมค่า'.$info->order_type.'  : '.number_format($total + $sumTotal).' บาท'; 

        if(!empty($info->transport_id)):
            $input____['delivery_company_id']   = $info->transport_id;
            $data['tracking_title'] = $this->delivery_companys_m->get_rows($input____)->row()->title;
        endif;

        // arr($info);
        // exit();
        $data['info']           = $info;
        $data['grpContent']     = $this->_grpContent;
        $data['frmAction']      = site_url("{$this->router->class}/update");
        
        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][]   = array('แก้ไข', site_url("{$this->router->class}/edit"));
        
        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/form";
        
        $this->template->layout($data);
    }
    
    public function update()
    {
        $input  = $this->input->post(null, true);
        $id     = decode_id($input['id']);
        $value  = $this->_build_data($input);
        $result = $this->orders_m->update($id, $value);
        if ( $result ) { 

            if(!empty($input['status'])): 
                $data['order_id']   = $id;
                $data['status']     = $input['status'];
                $data['is_process'] = 0;
                $data['title']      = null;
                $data['process']    = null;
                $this->set_orders_status_action($data);
            endif;

            Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}"));
    }
    
    private function _build_data($input)
    {
        
        $value['name']          = $input['name'];
        $value['lasname']       = $input['lasname'];
        $value['address']       = $input['address'];
        $value['provinces']     = $input['provinces'];
        $value['amphures']      = $input['amphures'];
        $value['districts']     = $input['districts'];
        $value['zip_code']      = $input['zip_code'];
        $value['tel']           = $input['tel'];
        $value['email']         = $input['email'];
        if(!empty($input['tracking_code'])):
            $value['tracking_code'] = $input['tracking_code'];
        endif;
        if(!empty($input['status'])): 
            $value['status']        = $input['status'];
        endif;

        $is_invoice                 = isset($input['is_invoice']) ? $input['is_invoice'] : 0;
        $invoice_type               = isset($input['invoice_type']) ? $input['invoice_type'] : 0;
        if($is_invoice > 0 && $invoice_type < 1):
            $value['invoice_no']            = $input['vat_code'];
            $value['invoice_address']       = $input['customer_address_vat'];
            $value['invoice_provinces_id']  = $input['invoice_provinces_id'];
            $value['invoice_amphures_id']   = $input['invoice_amphures_id'];
            $value['invoice_districts_id']  = $input['invoice_districts_id'];
            $value['invoice_tel']           = $input['invoice_tel'];
            $value['invoice_zip_code']      = $input['invoice_zip_code'];
        elseif($is_invoice > 0 && $invoice_type > 0): 
        $value['invoice_no']            = $input['vat_code'];  
        endif;

        $value['is_invoice']      = $is_invoice;
        $value['invoice_type']    = $invoice_type;
        
        if ( $input['mode'] == 'create' ) {
            $value['created_at'] = db_datetime_now();
            $value['created_by'] = $this->session->users['user_id'];
            $value['updated_at'] = db_datetime_now();
            $value['updated_by'] = $this->session->users['user_id'];
        } else {
            $value['updated_at'] = db_datetime_now();
            $value['updated_by'] = $this->session->users['user_id'];
        }
        return $value;
    }

    public function set_orders_status_action($data)
    {
        $value['order_id']      = $data['order_id'];
        $value['status']        = $data['status'];
        $value['is_process']    = $data['is_process'];
        $value['title']         = $data['title'];
        $value['process']       = $data['process'];
        $value['created_at']    = db_datetime_now();
        $value['created_by']    = $this->session->users['user_id'];

        $this->orders_m->insert_orders_status_action($value);
    }

    public function set_orders_log($data)
    {
        $value['order_id']      = $data['order_id'];
        $value['total_old']     = $data['total_old'];
        $value['total_new']     = $data['total_new'];
        $value['remark']        = $data['remark'];
        $value['type']          = $data['type'];
        $value['created_at']    = db_datetime_now();
        $value['created_by']    = $this->session->users['user_id']; 
        $this->orders_m->insert_orders_log($value);
    } 
    
    public function excel()
    {
        set_time_limit(0);
        ini_set('memory_limit', '128M');
        
        $input['recycle']       = 0;
        $input['grpContent']    = $this->_grpContent;
        if ( $this->session->condition[$this->_grpContent] ) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }
        $info           = $this->orders_m->get_rows($input);
        $fileName       = "orders";
        $sheetName      = "Sheet name";
        $sheetTitle     = "Sheet title";
        
        $spreadsheet    = new Spreadsheet();
        $sheet          = $spreadsheet->getActiveSheet();
        
        $styleH = excel_header();
        $styleB = excel_body();
        $colums = array(
            'A' => array('data'=>'รายการ', 'width'=>50),
            'B' => array('data'=>'เนื้อหาย่อ','width'=>50),
            'C' => array('data'=>'วันที่สร้าง', 'width'=>16),            
            'D' => array('data'=>'วันที่แก้ไข', 'width'=>16),
        );
        $fields = array(
            'A' => 'title',
            'B' => 'excerpt',
            'C' => 'created_at',            
            'D' => 'updated_at',
        );
        $sheet->setTitle($sheetName);
        
        //title
        $sheet->setCellValue('A1', $sheetTitle);
        $sheet->setCellValue('D1', 'วันที่สร้าง: '.date("d/m/Y, H:i"));
        $sheet->getStyle('D1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $sheet->getRowDimension(1)->setRowHeight(20);
        
        //header
        $rowCount = 2;
        foreach ( $colums as $colum => $data ) {
            $sheet->getStyle($colum . $rowCount)->applyFromArray($styleH);
            $sheet->SetCellValue($colum.$rowCount, $data['data']);
            $sheet->getColumnDimension($colum)->setWidth($data['width']);
        }
        
        // data
        $rowCount++;
        foreach ( $info->result() as $row ){
            foreach ( $fields as $key => $field ){
                $value = $row->{$field};
                if ( $field == 'created_at' || $field == 'updated_at' )
                    $value = datetime_table ($value);
                $sheet->getStyle($key . $rowCount)->applyFromArray($styleB);
                $sheet->SetCellValue($key . $rowCount, $value); 
            }
            $sheet->getRowDimension($rowCount)->setRowHeight(20);
            $rowCount++;
        }
        $sheet->getRowDimension($rowCount)->setRowHeight(20);  
        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
        $writer->save('php://output');
        exit; 
    }
    
    public function pdf($id="")
    {
        set_time_limit(0);
        ini_set('memory_limit', '128M');
        
        $id = decode_id($id);
        $input['order_id'] = $id;
        $info = $this->orders_m->get_rows($input);
        if ( $info->num_rows() == 0) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
            redirect_back();
        }
        $info                   = $info->row();  
        $info->status_name      = $this->status_m->get_status_by_id($info->status)->row()->title;
        $info->print_date       = date('d.m.Y');

        $input_['id']           = $info->user_id;
        $info->members          = $this->members_m->get_rows($input_)->row();

        if(!empty($info->transport_id)):
            $input__['delivery_company_id'] = $info->transport_id;
            $info->tracking_title           = $this->delivery_companys_m->get_rows($input__)->row()->title;
        endif;
        
        $data['info']           = $info;

        $input___['recycle']   = 0;
        $input___['order_id']  = $info->order_id;
        $order_import          = $this->separate_orders_m->get_rows($input___)->result();
        $arr_tracking          = [];
        if(!empty($order_import)):
            foreach($order_import as $import):
                if(!empty($import->tracking_code)):
                    array_push($arr_tracking, $import->tracking_code);
                endif;
                
                $attribute['recycle']            = 0;
                $attribute['attribute_send_in']  = explode(',', $import->attribute_send_id);
                $import->attribute               = $this->attribute_sends_m->get_rows($attribute)->result();

                // $input______['recycle']                            = 0;
                // $input______['type_transportations_setting_id']    = $import->type_transportations_setting_id;
                // $import->transportations_setting                   = $this->type_transportations_setting_m->get_rows($input______)->row();
                
            endforeach;
        endif;

        $data['trackings']          = $arr_tracking;
        $data['order_import']       = $order_import;
        
        $input____['type']          = 1;
        $input____['order_id']      = $info->order_id;
        $orders_payments            = $this->orders_m->get_orders_payments_by_param($input____)->row();
        if(!empty($orders_payments)):
            $input_____['bank_id']  = $orders_payments->bank_id;
            $banks                  = $this->banks_m->get_rows($input_____)->row();
            $data['banks']          = $banks; 
        endif;
        $data['orders_payments']    = $orders_payments;
        
        $code                = $info->order_code;
        $data['order_code']  = $code;

        // $generator           = new Picqer\Barcode\BarcodeGeneratorHTML();
        // $generator_img       = new Picqer\Barcode\BarcodeGeneratorPNG(); 
        // $border              = 2;
        // $height              = 50;
        // $data['img_barcode'] = '<img src="data:image/png;base64,' . base64_encode($generator_img->getBarcode($code, $generator_img::TYPE_CODE_128,$border,$height)) . '">';
        // $data['barcode']     = $generator->getBarcode($code , $generator::TYPE_CODE_128,$border,$height);
            
        //ดึงข้อมูลบริษัท
        $info_com           = $this->info_m->get_rows('');
        $info_com           = $info_com->row();
        
        $data['company']    =  $info_com;

        $money  = Modules::run('configs/get_setting', 'money_setting');
        $data['money'] = $money;
        
        $mpdfConfig = [
            'default_font_size' => 9,
            'default_font'      => 'garuda'
        ];

        require APPPATH.'../vendor/autoload.php';

        $mpdf = new \Mpdf\Mpdf($mpdfConfig);
        $content = $this->load->view("{$this->router->class}/pdf", $data, TRUE);
        $mpdf->WriteHTML($content);
        
        
        $download = FALSE;
        $saveFile = FALSE;
        $viewFile = TRUE;
        $fileName = "orders.pdf";
        $pathFile = "uploads/pdf/orders/";
        if ( $saveFile ) {
            create_dir($pathFile);
            $fileName = $pathFile.$fileName;
            $mpdf->Output($fileName, 'F');
        }
        if ( $download ) 
            $mpdf->Output($fileName, 'D');
        if ( $viewFile ) 
            $mpdf->Output();
    }
    
    public function trash()
    {
        $this->load->module('template');
        
        // toobar
        $action[1][]            = action_list_view(site_url("{$this->router->class}"));
        $action[2][]            = action_delete_multi(base_url("{$this->router->class}/delete"));
        $data['boxAction']      = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][]   = array("ถังขยะ", site_url("{$this->router->class}/trash"));
        
        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/trash";
        
        $this->template->layout($data);
    }

    public function data_trash()
    {
        $input              = $this->input->post();
        $input['recycle']   = 1;
        $info               = $this->orders_m->get_rows($input);
        $infoCount          = $this->orders_m->get_count($input);
        $column             = array();
        foreach ($info->result() as $key => $rs) {
            $id                         = encode_id($rs->order_id);
            $action                     = array();
            $action[1][]                = table_restore("{$this->router->class}/restore");         
            $active                     = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId']   = $id;
            $column[$key]['checkbox']   = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title']      = $rs->title;
            $column[$key]['excerpt']    = $rs->excerpt;
            $column[$key]['recycle_at'] = datetime_table($rs->recycle_at);
            $column[$key]['action']     = Modules::run('utils/build_toolbar', $action);
        }

        $data['data']               = $column;
        $data['recordsTotal']       = $info->num_rows();
        $data['recordsFiltered']    = $infoCount;
        $data['draw']               = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    } 

    public function destroy()
    {
        if ( !$this->_permission ) {
            $toastr['type']     = 'error';
            $toastr['lineOne']  = config_item('appName');
            $toastr['lineTwo']  = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']    = false;
            $data['toastr']     = $toastr;
        } else {
            $input  = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs                     = decode_id($rs);
                $dateTime               = db_datetime_now();
                $value['updated_at']    = $dateTime;
                $value['updated_by']    = $this->session->users['user_id'];
                
                $result                 = false;
                $value['active']        = 0;
                $value['recycle']       = 1;
                $value['recycle_at']    = $dateTime;
                $value['recycle_by']    = $this->session->users['user_id'];
                $result                 = $this->orders_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type']         = 'success';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type']         = 'error';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }

            $data['success']    = $result;
            $data['toastr']     = $toastr;
        }

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }

    public function restore()
    {
        if ( !$this->_permission ) {
            $toastr['type']     = 'error';
            $toastr['lineOne']  = config_item('appName');
            $toastr['lineTwo']  = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']    = false;
            $data['toastr']     = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs                     = decode_id($rs);
                $dateTime               = db_datetime_now();
                $value['updated_at']    = $dateTime;
                $value['updated_by']    = $this->session->users['user_id'];
                
                $result                 = false;
                $value['active']        = 0;
                $value['recycle']       = 0;
                $result                 = $this->orders_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type']         = 'success';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type']         = 'error';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }

            $data['success']    = $result;
            $data['toastr']     = $toastr;
        }

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    } 

    public function delete()
    {
        if ( !$this->_permission ) {
            $toastr['type']     = 'error';
            $toastr['lineOne']  = config_item('appName');
            $toastr['lineTwo']  = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']    = false;
            $data['toastr']     = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs                     = decode_id($rs);
                $dateTime               = db_datetime_now();
                $value['updated_at']    = $dateTime;
                $value['updated_by']    = $this->session->users['user_id'];
                
                $result                 = false;
                $value['active']        = 0;
                $value['recycle']       = 2;
                $result                 = $this->orders_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type']         = 'success';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type']         = 'error';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }

            $data['success']    = $result;
            $data['toastr']     = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }      
    
    public function action($type="")
    {
        if ( !$this->_permission ) {
            $toastr['type']         = 'error';
            $toastr['lineOne']      = config_item('appName');
            $toastr['lineTwo']      = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']        = false;
            $data['toastr']         = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs                     = decode_id($rs);
                $dateTime               = db_datetime_now();
                $value['updated_at']    = $dateTime;
                $value['updated_by']    = $this->session->users['user_id'];
                $result                 = false;

                if ( $type == "active" ) {
                    $value['active']    = $input['status'] == "true" ? 1 : 0;
                    $result             = $this->orders_m->update_in($input['id'], $value);
                }
                
                if ( $result ) {
                    $toastr['type']     = 'success';
                    $toastr['lineOne']  = config_item('appName');
                    $toastr['lineTwo']  = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
                } else {
                    $toastr['type']     = 'error';
                    $toastr['lineOne']  = config_item('appName');
                    $toastr['lineTwo']  = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
                }

                $data['success']    = $result;
                $data['toastr']     = $toastr;
            }
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }
    
    public function send_order()
    {
        if ( !$this->_permission ) {
            $toastr['type']         = 'error';
            $toastr['lineOne']      = config_item('appName');
            $toastr['lineTwo']      = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']        = false;
            $data['toastr']         = $toastr;
        } else {
            $input  = $this->input->post();
            $error  = 0;
            $arr =  array();
            foreach ( $input['id'] as $rs ):
                $rs                     = decode_id($rs);
                $dateTime               = db_datetime_now();
                $input['order_id']      = $rs; 
                $info                   = $this->orders_m->get_rows($input)->row();
                $value['status']        = 4; 

                $value['updated_at']    = $dateTime;
                $value['updated_by']    = $this->session->users['user_id'];
                $result = $this->orders_m->update($rs, $value);
                if($result):
                    $data['order_id']   = $rs;
                    $data['status']     = $value['status'];
                    $data['is_process'] = 0;
                    $data['title']      = null;
                    $data['process']    = null;
                    $this->set_orders_status_action($data);
                else:
                    $error++;
                endif;
                
            endforeach;

            if ( $error == 0) {
                $toastr['type']     = 'success';
                $toastr['lineOne']  = config_item('appName');
                $toastr['lineTwo']  = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type']     = 'error';
                $toastr['lineOne']  = config_item('appName');
                $toastr['lineTwo']  = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }

            $data['success']    = $result;
            $data['toastr']     = $toastr; 
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data)); 
        }       
    }

    public function update_status()
    {
        if ( !$this->_permission ):
            $toastr['type']         = 'error';
            $toastr['lineOne']      = config_item('appName');
            $toastr['lineTwo']      = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']        = false;
            $data['toastr']         = $toastr;
        else:
            $input                  = $this->input->post(); 
            $code                   = decode_id($input['code']);
            $status_id              = $input['status_id']; 
            $error                  = 0;
            $dateTime               = db_datetime_now();
            $value['status']        = $status_id; 
            $value['updated_at']    = $dateTime;
            $value['updated_by']    = $this->session->users['user_id'];
            $result = $this->orders_m->update($code, $value);
            if($result):
                $data['order_id']   = $code;
                $data['status']     = $value['status'];
                $data['is_process'] = 0;
                $data['title']      = null;
                $data['process']    = null;
                $this->set_orders_status_action($data);

                $order = $this->orders_m->orders_by_key($code);
                if(!empty($order->email)):
                    $data_arr['name']          = $order->name.' '.$order->lasname; 
                    $data_arr['email']         = $order->email; 
                    $data_arr['order_code']    = $order->order_code;
                    $data_arr['status_title']  = $this->status_m->get_status_by_id($order->status)->row()->title;
                    $this->sentMail($data_arr); //send email to user
                endif;
            else:
                $error = 1;
            endif;  

            if ( $error == 0):
                $toastr['type']     = 'success';
                $toastr['lineOne']  = config_item('appName');
                $toastr['lineTwo']  = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            else:
                $toastr['type']     = 'error';
                $toastr['lineOne']  = config_item('appName');
                $toastr['lineTwo']  = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            endif;

            $data['success']    = $result;
            $data['toastr']     = $toastr; 
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data)); 
        endif;      
    } 

    public function edit_order()
    {
        if ( !$this->_permission ) {
            $toastr['type']         = 'error';
            $toastr['lineOne']      = config_item('appName');
            $toastr['lineTwo']      = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']        = false;
            $data['toastr']         = $toastr;
        } else {
            $input                = $this->input->post();
            $order_edit           = $input['order_edit'];
            $code                 = decode_id($input['code']);
            $discount             = $input['discount'];
            $type                 = $input['type'];
            $order_remark         = !empty($input['order_remark']) ? $input['order_remark'] : '';
            $error                = 0;
            $dateTime             = db_datetime_now();
            $value['updated_at']  = $dateTime;
            if($input['type'] == 'edit_qty'):
                $value['order_qty']     = $order_edit;
            endif;
            if($input['type'] == 'edit_price'):
                $value['discount']      = $order_edit;
            endif;
            $value['updated_by']    = $this->session->users['user_id'];
            $result = $this->orders_m->update($code, $value);
            if($result):
                $data['order_id']      = $code;
                $data['total_old']     = $discount;
                $data['total_new']     = $order_edit;
                $data['remark']        = $order_remark;
                $data['type']          = $type;
                $data['created_at']    = $dateTime;
                $data['created_by']    = $this->session->users['user_id']; 
                $this->set_orders_log($data);
            else:
                $error = 1;
            endif; 

            if ( $error == 0):
                $toastr['type']     = 'success';
                $toastr['lineOne']  = config_item('appName');
                $toastr['lineTwo']  = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            else:
                $toastr['type']     = 'error';
                $toastr['lineOne']  = config_item('appName');
                $toastr['lineTwo']  = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            endif;
            $data['success']    = $result;
            $data['toastr']     = $toastr; 
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data)); 
        }       
    }

    public function comment_order()
    {
        if ( !$this->_permission ) {
            $toastr['type']         = 'error';
            $toastr['lineOne']      = config_item('appName');
            $toastr['lineTwo']      = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']        = false;
            $data['toastr']         = $toastr;
        } else {
            $input                  = $this->input->post();
            $code                   = decode_id($input['code']);
            $note                   = !empty($input['note']) ? $input['note'] : '';
            $error                  = 0;
            $dateTime               = db_datetime_now();
            $value['note']          = $note;
            $value['updated_at']    = $dateTime;
            $value['updated_by']    = $this->session->users['user_id'];
            $result = $this->orders_m->update($code, $value);

            if ( $error == 0):
                $toastr['type']     = 'success';
                $toastr['lineOne']  = config_item('appName');
                $toastr['lineTwo']  = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            else:
                $toastr['type']     = 'error';
                $toastr['lineOne']  = config_item('appName');
                $toastr['lineTwo']  = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            endif;
            $data['success']    = $result;
            $data['toastr']     = $toastr; 
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data)); 
        }       
    }

    public function sumernote_img_upload()
    {
		//sumernote/img-upload
		$path       = 'content';
        $upload     = $this->uploadfile_library->do_upload('file',TRUE,$path);
        
		if(isset($upload['index'])){
			$picture = $this->config->item('root_url').'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
		}

		echo $picture;
       
    }

    public function deletefile($ids)
    {

		$arrayName = array('file' => $ids);

		echo json_encode($arrayName);
    }
    
    public function print($id="")
    {
        $this->load->module('template');
    
        $id = decode_id($id);
        $input['order_id'] = $id;
        $info = $this->orders_m->get_rows($input);
        if ( $info->num_rows() == 0) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
            redirect_back();
        }
        $info = $info->row();  
        $data['info'] = $info;
        
        $infoDetail             = $this->orders_m->get_orders_detail_by_order_id($info->order_id)->result(); 
        if(!empty($infoDetail)):
            foreach($infoDetail as $item):
                $item->attributes = $this->orders_m->get_orders_detail_attribute_id($item->order_detail_uniqid)->result();
            endforeach;
        endif;
        $data['details']     = $infoDetail; 
 
        $code                = $info->order_code;
        $generator           = new Picqer\Barcode\BarcodeGeneratorHTML();
        $generator_img       = new Picqer\Barcode\BarcodeGeneratorPNG(); 
        $border              = 2;
        $height              = 50;
        $data['order_code']  = $code;
        $data['img_barcode'] = '<img src="data:image/png;base64,' . base64_encode($generator_img->getBarcode($code, $generator_img::TYPE_CODE_128,$border,$height)) . '">';
        $data['barcode']     = $generator->getBarcode($code , $generator::TYPE_CODE_128,$border,$height);
            
        //ดึงข้อมูลบริษัท
        $info_com           = $this->info_m->get_rows('');
        $info_com           = $info_com->row();
        $data['company']    =  $info_com;

        //add status action form order
        $input_action['process']        = 'print';
        $input_action['is_process']     = 1;
        $input_action['order_id']       = $id;
        $count_action                   = $this->orders_m->get_count_orders_status_action($input_action);
        if($count_action == 0):
            $status_action['order_id']   = $id;
            $status_action['status']     = 0;
            $status_action['is_process'] = 1;
            $status_action['title']      = 'ปริ้นใบสั่งสินค้า/ใบติดกล่อง';
            $status_action['process']    = 'print';
            $this->set_orders_status_action($status_action);
        endif; 
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/print_bill";
        $data['pageScript'] = "scripts/backend/orders/print.js";
    
        $this->template->layout($data);
    }

    public function invoice($id="")
    {
        $this->load->module('template');
    
        $id = decode_id($id);
        $input['order_id'] = $id;
        $info = $this->orders_m->get_rows($input);
        if ( $info->num_rows() == 0) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
            redirect_back();
        }
        $info = $info->row();

        if($info->invoice_type < 1):
            $info->address    = $info->invoice_address;
            $info->districts  = $info->invoice_districts_id;
            $info->amphures   = $info->invoice_amphures_id;
            $info->provinces  = $info->invoice_provinces_id;
            $info->zip_code   = $info->invoice_zip_code;
            $info->tel        = $info->invoice_tel;
        endif;

        $data['info'] = $info;
        
        $infoDetail             = $this->orders_m->get_orders_detail_by_order_id($info->order_id)->result(); 
        $data['details']        = $infoDetail;

        //add status action form order
        $input_action['process']        = 'print_invoice';
        $input_action['is_process']     = 1;
        $input_action['order_id']       = $id;
        $count_action                   = $this->orders_m->get_count_orders_status_action($input_action);
        if($count_action == 0):
            $status_action['order_id']   = $id;
            $status_action['status']     = 0;
            $status_action['is_process'] = 1;
            $status_action['title']      = 'ปริ้นใบกำกับภาษี';
            $status_action['process']    = 'print_invoice';
            $this->set_orders_status_action($status_action);
        endif;

    
            //ดึงข้อมูลบริษัท
        $info_com = $this->info_m->get_rows('');
        $info_com = $info_com->row();
        $data['company']  =  $info_com;
    
            // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][] = array('ปริ้น', site_url("{$this->router->class}/invoice"));
    
            // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/invoice";
        $data['pageScript'] = "scripts/backend/orders/invoice.js";
    
        $this->template->layout($data);
    }

    public function get_order_param($data)
    {
        $param['recycle']           = !empty($data['recycle'])? $data['recycle'] :'';
        $param['length']            = !empty($data['length'])? $data['length'] :'';
        $param['start']             = !empty($data['start'])? $data['start'] :'';
        $param['order_by']          = !empty($data['order_by'])? $data['order_by'] :'';
        $param['statuss']           = !empty($data['statuss'])? $data['statuss'] : '';
        $param['created_at']        = !empty($data['createDateRange'])? $data['createDateRange'] :'';
        $param['updated_at']        = !empty($data['updateDateRange'])? $data['updateDateRange'] : ''; 
        $orders             = $this->orders_m->get_rows($param)->result();
        $orders_count       = $this->orders_m->get_count($param);
        
        return array($orders, $orders_count);
    }

    public function updateprintcheck()
    {
        $input = $this->input->post();
        $value['print_check']     = 1;
    }

    private function sentMail($data)
    { 

        $temp  = Modules::run('configs/get_setting', 'mail');  

        if(!empty($temp)):
            // load mail config  
            $Host 		= !empty($temp['SMTPserver']) ? $temp['SMTPserver'] : '';
            $Username 	= !empty($temp['SMTPusername']) ? $temp['SMTPusername'] : '';
            $Password 	= !empty($temp['SMTPpassword']) ? $temp['SMTPpassword'] : '';
            $SMTPSecure = 'ssl';
            $Port 		= !empty($temp['SMTPport']) ? $temp['SMTPport'] : '';  
            $viewMail   = $this->load->view('emails/order_email', $data, TRUE);

            // load mail send config 
            
            require 'app_backend/third_party/phpmailer/PHPMailerAutoload.php';
            $mail = new PHPMailer;
            $mail->SMTPDebug = 0;                               	// Enable verbose debug output
            
            $mail->isSMTP();                                      	// Set mailer to use SMTP
            $mail->Host 		= $Host;              				// Specify main and backup SMTP servers
            $mail->SMTPAuth 	= true;                             // Enable SMTP authentication
            $mail->Username 	= $Username;                		// SMTP username
            $mail->Password 	= $Password;                        // SMTP password
            $mail->SMTPSecure 	= $SMTPSecure;                      // Enable TLS encryption, `ssl` also accepted
            $mail->Port 		= $Port;                            // TCP port to connect to
            $mail->CharSet 		= 'UTF-8';
            
            $mail->From 		= $Username;
            $mail->FromName 	= $Username;

            $email_to 			= $data['email'];

            $mail->addAddress($email_to);               			// Name is optional
            $mail->isHTML(false);                                  	// Set email format to HTML

            $mail->Subject = $email_to;
            $mail->Body    = $viewMail;
            $mail->AltBody = $viewMail; 
            $mail->Send();
            // $mail->ErrorInfo; 
        endif;
    }

    
}
