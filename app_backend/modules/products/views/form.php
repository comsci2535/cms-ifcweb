<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>

       
        <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post', 'autocomplete' => 'off')) ?>
            <div class="m-portlet__body">
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="recommend"></label>
                    <div class="col-sm-7">
                        <label class="m-checkbox m-checkbox--brand">
                            <input <?php if(!empty($info->recommend) &&  $info->recommend=='1'){ echo "checked";}?> type="checkbox" name="recommend" valus="1"> สินค้าแนะนำ
                            <span></span>
                        </label> 
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="categorie_id">ประเภทสินค้า</label>
                    <div class="col-sm-7">
                        <select id="categorie_id" name="categorie_id" class="form-control m-input select2" required>
                            <option value="">เลือกประเภทสินค้า</option>
                            <?php
                            if(!empty($categories)):
                                foreach($categories as $item):
                                    $select = '';
                                    if($item->categorie_id == $info->categorie_id):
                                        $select = 'selected';
                                    endif;
                            ?>
                                <option value="<?php echo !empty($item->categorie_id) ? $item->categorie_id : "";?>" <?=$select?>><?php echo !empty($item->title) ? $item->title : "";?></option>
                            <?php
                                endforeach;
                            endif;
                            ?>

                        </select>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title">หัวข้อ</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" class="form-control m-input " name="title" id="title" placeholder="ระบุ" required>
                        <label id="name-error-dup" class="error-dup" for="name" style="display: none;">มีชือนี้ในระบบแล้ว.</label>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title_en">หัวข้อ (อังกฤษ)</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->title_en) ? $info->title_en : NULL ?>" type="text" class="form-control m-input " name="title_en" id="title_en" placeholder="ระบุ" required> 
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="slug">Slug</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->slug) ? $info->slug : NULL ?>" type="text" class="form-control m-input " name="slug" id="slug" placeholder="ระบุ" required>
                        <label id="slug-error-dup" class="error-dup" for="slug" style="display: none;">มี Slug นี้ในระบบแล้ว.</label>
                    </div>
                </div> 

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="file">ไฟล์ (470x470 px)</label>
                    <div class="col-sm-7">
                        <input id="file" name="file[]" type="file" data-preview-file-type="text" multiple>
                    </div>
                </div> 
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="start_price">ราคา(บาท)</label>
                    <div class="col-sm-4">
                        <input value="<?php echo isset($info->start_price) ? $info->start_price : NULL ?>" type="text" class="form-control m-input " name="start_price" id="start_price" placeholder="ระบุ" required> 
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="price">ราคาขาย(บาท)</label>
                    <div class="col-sm-4">
                        <input value="<?php echo isset($info->price) ? $info->price : NULL ?>" type="text" class="form-control m-input " name="price" id="price" placeholder="ระบุ" required> 
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="excerpt">รายละเอียดย่อ</label>
                    <div class="col-sm-7">
                        <textarea name="excerpt" rows="3" class="form-control" id="excerpt" placeholder="ระบุ" required><?php echo isset($info->excerpt) ? $info->excerpt : NULL ?></textarea>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="excerpt_en">รายละเอียดย่อ (อังกฤษ)</label>
                    <div class="col-sm-7">
                        <textarea name="excerpt_en" rows="7" class="form-control" id="excerpt_en" placeholder="ระบุ"><?php echo isset($info->excerpt_en) ? $info->excerpt_en : NULL ?></textarea>
                    </div>
                </div> 
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="detail">รายละเอียด</label>
                    <div class="col-sm-7">
                        <textarea name="detail" rows="3" class="form-control summernote" id="detail" placeholder="ระบุ" required><?php echo isset($info->detail) ? $info->detail : NULL ?></textarea>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="detail_en">รายละเอียด (อังกฤษ)</label>
                    <div class="col-sm-7">
                        <textarea name="detail_en" rows="3" class="form-control summernote" id="detail_en" placeholder="ระบุ"><?php echo isset($info->detail_en) ? $info->detail_en : NULL ?></textarea>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <h5 class="col-sm-12">รายละเอียดเพิ่มเติม</h5> 
                </div>  
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="product_size">  ไซส์</label>
                    <div class="col-sm-7">
                        <select id="product_size" name="product_size[]" class="form-control m-input" multiple>
                            <option></option> 
                            <?php
                            if(!empty($products_size)):
                                foreach ($products_size as $key => $value):
                            ?>
                                <option value="<?php echo $value->title;?>" selected><?php echo $value->title;?></option> 
                            <?php 
                                endforeach;
                            endif;
                            ?>
                        </select> 
                    </div> 
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="product_color"> สี</label>
                    <div class="col-sm-7">
                        <select id="product_color" name="product_color[]" class="form-control m-input" multiple>
                            <option></option> 
                            <?php
                            if(!empty($products_color)):
                                foreach ($products_color as $key => $value):
                            ?>
                                <option value="<?php echo $value->title;?>" selected><?php echo $value->title;?></option> 
                            <?php 
                                endforeach;
                            endif;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" > <h5 class="block">ข้อมูล SEO</h5></label>
                    <div class="col-sm-10">
                       
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" >ชื่อเรื่อง (Title)</label>
                    <div class="col-sm-10">
                        <input value="<?php echo isset($info->meta_title) ? $info->meta_title : NULL ?>" type="text" id="" class="form-control" name="meta_title">
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">คำอธิบาย (Description)</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" rows="6"  class="form-control" name="meta_description"><?php echo isset($info->meta_description) ? $info->meta_description : NULL ?></textarea>
                    </div>
                </div>   
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">คำหลัก (Keyword)</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" rows="3"  class="form-control" name="meta_keyword"><?php echo isset($info->meta_keyword) ? $info->meta_keyword : NULL ?></textarea>
                    </div>
                </div> 
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">

                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success m-btn--wide">บันทึก</button>
                    <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                        </div>
                    </div>
                    
                </div>
            </div>
            <div id="text-file-remove"></div>
             <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
             <input type="hidden" class="form-control" name="db" id="db" value="products">
             <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->product_id) ? encode_id($info->product_id) : 0 ?>">
        <?php echo form_close() ?>

        <!--end::Form-->
    </div>


    
</div>

<script>
    //set par fileinput;
    var required_icon   = true;   
    var deleteUrl       = '<?=base_url();?><?=$this->router->class;?>/deletefile/<?=$info->product_id;?>';
    var initialPreviewConfig  = <?php
                                    if(!empty($products_image)){
                                    $arr_image = array();
                                    foreach($products_image as $item){
                                        array_push($arr_image, array(
                                            'downloadUrl' => $this->config->item('root_url').$item->full_path,
                                            'url'         => base_url().$this->router->class."/deletefile/".$item->id,
                                            'key'         => $item->id,
                                            'extra'       => array('csrfToken' => get_cookie('csrfCookie'))
                                        ));  
                                    }
                                    echo json_encode($arr_image);
                                 }else{
                                     echo "'';";
                                 }
                                ?>
                                
    var initialPreview  = [
                            <?php
                            if(!empty($image_name)){
                                foreach($image_name as $item){
                            ?>
                            "<?=$item?>",
                            <?php
                                }
                            }    
                            ?> 
                          ];
</script>




