<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Products extends MX_Controller 
{

    private $_title             = "จัดการข้อมูลสินค้า";
    private $_pageExcerpt       = "การจัดการข้อมูลเกี่ยวกับจัดการข้อมูลสินค้า";
    private $_grpContent        = "products";
    private $_requiredExport    = true;
    private $_permission;
    private $_orderData         = array();

    public function __construct()
    {
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('uploadfile_library');
        $this->load->library('ckeditor');
        $this->load->model("products_m");
        $this->load->model('categories/categories_m');
    }
    
    public function index()
    {
        $this->load->module('template');
        
        // toobar
        $export = array(
            'excel' => site_url("{$this->router->class}/excel"),
            'pdf'   => site_url("{$this->router->class}/pdf"),
        );
        $action[1][]        = action_refresh(site_url("{$this->router->class}"));
        //$action[1][]      = action_filter();
        $action[1][]        = action_order(base_url("{$this->router->class}/order"));
        $action[1][]        = action_add(site_url("{$this->router->class}/create"));
        //$action[2][]      = action_export_group($export);
        $action[3][]        = action_trash_multi("{$this->router->class}/destroy");
        $action[3][]        = action_trash_view(site_url("{$this->router->class}/trash"));
        $data['boxAction']  = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("{$this->router->class}"));
        
        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/index";
        
        $this->template->layout($data);
    }    

    public function data_index()
    {
        $input              = $this->input->post();
        $input['recycle']   = 0;
        $info               = $this->products_m->get_rows($input);
        $infoCount          = $this->products_m->get_count($input);
        $column             = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }
        
        foreach ($info->result() as $key => $rs) {
            $id                         = encode_id($rs->product_id);
            $action                     = array();
            $action[1][]                = table_edit(site_url("{$this->router->class}/edit/{$id}"));
            $active                     = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId']   = $id;
            $column[$key]['checkbox']   = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title']      = $rs->title;
            $column[$key]['excerpt']    = $rs->excerpt;
            $column[$key]['active']     = toggle_active($active, "{$this->router->class}/action/active");
            $column[$key]['created_at'] = datetime_table($rs->created_at);
            $column[$key]['updated_at'] = datetime_table($rs->updated_at);
            $column[$key]['action']     = Modules::run('utils/build_button_group', $action);
        }
        $data['data']               = $column;
        $data['recordsTotal']       = $info->num_rows();
        $data['recordsFiltered']    = $infoCount;
        $data['draw']               = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }
    
    public function create()
    {
        $this->load->module('template');

        $data['grpContent']     = $this->_grpContent;
        $data['frmAction']      = site_url("{$this->router->class}/storage");
        
        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][]   = array('สร้าง', site_url("{$this->router->class}/create"));

        $input['recycle']       = 0;
        $input['active']        = 1;
        $data['categories']     = $this->categories_m->get_rows($input)->result();
        
        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/form";
        
        $this->template->layout($data);
    }
    
    public function storage()
    {
        $input  = $this->input->post(null, false);
        $value  = $this->_build_data($input);
        $result = $this->products_m->insert($value);
        if ( $result ) {

            $value_ = $this->_build_data_image($input, $result);
            $this->products_m->insert_products_image_batch($value_);

            // set value product color
            $value_color = $this->_build_data_product_color($input, $result);  
            $this->products_m->insert_product_color($value_color, $result); 
            
            // set value product box
            $value_size = $this->_build_data_product_size($input, $result); 
            $this->products_m->insert_product_size($value_size, $result);

            Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}"));
    }
    
    public function edit($id="")
    {
        $this->load->module('template');
        
        $id                 = decode_id($id);
        $input['product_id']    = $id;
        $info               = $this->products_m->get_rows($input);
        if ( $info->num_rows() == 0) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
            redirect_back();
        }
        $info                   = $info->row();
        $data['info']           = $info;

        $image = $this->products_m->get_products_image($input)->result();
        $arr_image = array();
        $image_name = array(); 
        if(!empty($image)){ 
            foreach($image as $item){
                array_push($arr_image, array(
                    'downloadUrl' => $this->config->item('root_url').$item->full_path,
                    'url'         => base_url().$this->router->class."/deletefile/".$item->id,
                    'key'         => $item->id,
                    'csrfToken'   => get_cookie('csrfCookie')
                )); 

                $image_name[] = $this->config->item('root_url').$item->full_path;
            }
            
        }
        $data['image_name']     = $image_name; 
        $data['products_image'] = $image; 

        // get query  select_map_products_size
        $products_size               = $this->products_m->select_map_products_size($input)->result(); 
        $data['products_size']       = $products_size;

        // get query  products_color
        $products_color             = $this->products_m->select_map_products_color($input)->result(); 
        $data['products_color']     = $products_color;  

        $input_['recycle']       = 0;
        $input_['active']        = 1;
        $data['categories']     = $this->categories_m->get_rows($input_)->result();

        $data['grpContent']     = $this->_grpContent;
        $data['frmAction']      = site_url("{$this->router->class}/update");
        
        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][]   = array('แก้ไข', site_url("{$this->router->class}/edit"));
        
        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/form";
        
        $this->template->layout($data);
    }
    
    public function update()
    {
        $input  = $this->input->post(null, false); 
        $id     = decode_id($input['id']);
        $value  = $this->_build_data($input);
        $result = $this->products_m->update($id, $value);
        if ( $result ) {
            $value_ = $this->_build_data_image($input, $id); 
            if(!empty($value_)){
                $this->products_m->insert_products_image_batch($value_);
            }
            // set value product color
            $value_color = $this->_build_data_product_color($input, $id);  
            $this->products_m->insert_product_color($value_color, $id); 

            // set value product box
            $value_size = $this->_build_data_product_size($input, $id); 
            $this->products_m->insert_product_size($value_size, $id);

           Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}"));
    }
    
    private function _build_data($input)
    { 
        $value['title']         = $input['title'];
        $value['excerpt']       = $input['excerpt'];
        $value['categorie_id']  = $input['categorie_id'];
        $value['detail']        = html_escape($input['detail']);
        $value['slug']          = $input['slug'];
        $value['start_price']   = $input['start_price'];
        $value['price']         = $input['price']; 
        $value['title_en']      = $input['title_en'];
        $value['excerpt_en']    = $input['excerpt_en'];
        $value['detail_en']     = html_escape($input['detail_en']);
        if($input['meta_title']!=""){
            $value['meta_title'] = $input['meta_title'];
        }else{
             $value['meta_title'] = $input['title'];
        }
        if(!empty($input['meta_description'])){
            $value['meta_description'] = $input['meta_description'];
        }else{
            $value['meta_description'] =  $input['excerpt'];
        }
        if($input['meta_keyword']!=""){
            $value['meta_keyword'] = $input['meta_keyword'];
        }else{
             $value['meta_keyword'] = $input['title'];
        }
        $value['recommend']  = !empty($input['recommend']) ? 1 : 0;
        if ( $input['mode'] == 'create' ) {
            $value['created_at'] = db_datetime_now();
            $value['created_by'] = $this->session->users['user_id'];
            $value['updated_at'] = db_datetime_now();
            $value['updated_by'] = $this->session->users['user_id'];
        } else {
            $value['updated_at'] = db_datetime_now();
            $value['updated_by'] = $this->session->users['user_id'];
        }

       
        return $value;
    }

    private function _build_data_image($input, $result)
    {
        $path = 'products';
        $upload = $this->uploadfile_library->do_uploadMultiple('file[]',TRUE,$path, 0, 0, 0); 
        $value = array();
        if(!empty($upload['filenames'])){
            foreach($upload['filenames'] as $item){
                if(!empty($item['index'])){  
                    array_push($value, array(
                        'product_id'    => $result,
                        'file_type'     => $item['index']['file_type'],
                        'full_path'     => $item['upload_path']."/".$item['index']['file_name'],
                        'file_ext'      => $item['index']['file_ext'],
                        'file_size'     => !empty($item['index']['file_size']) ? $item['index']['file_size'] : 0,
                        'image_width'   => !empty($item['index']['image_width']) ? $item['index']['image_width'] : 0,
                        'image_height'  => !empty($item['index']['image_height']) ? $item['index']['image_height'] : 0
                    )); 
                }
            }
        }

        // remove file multiple
        $this->remove_multiple_file($input);
        
        return $value;
    }

    private function remove_multiple_file($input)
    {
        if(!empty($input['file_key'])){
            foreach($input['file_key'] as $remove){
                $input_['id'] = $remove;
                $products_image = $this->products_m->get_products_image($input_)->row();
                if(!empty($products_image->full_path)){
                    @unlink($products_image->full_path);
                    $this->db->where('id', $remove);
                    $this->db->delete('products_image');
                } 
            }
        } 
    }

    private function _build_data_product_size($input, $product_id)
    {
        $value_arr = array();
        if(!empty($input['product_size'])):
            foreach($input['product_size'] as $value): 
                $value_arr[] = array(
                    'product_id'    => $product_id,
                    'title'         => $value
                ); 
            endforeach;
        endif;
       
        return $value_arr; 
    }

    private function _build_data_product_color($input, $product_id)
    {
        $value_arr = array();
        if(!empty($input['product_color'])):
            foreach($input['product_color'] as $value): 
                $value_arr[] = array(
                    'product_id'    => $product_id,
                    'title'         => $value
                ); 
            endforeach;
        endif;
       
        return $value_arr; 
    }
    
    public function excel()
    {
        set_time_limit(0);
        ini_set('memory_limit', '128M');
        
        $input['recycle']       = 0;
        $input['grpContent']    = $this->_grpContent;
        if ( $this->session->condition[$this->_grpContent] ) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }
        $info           = $this->products_m->get_rows($input);
        $fileName       = "products";
        $sheetName      = "Sheet name";
        $sheetTitle     = "Sheet title";
        
        $spreadsheet    = new Spreadsheet();
        $sheet          = $spreadsheet->getActiveSheet();
        
        $styleH = excel_header();
        $styleB = excel_body();
        $colums = array(
            'A' => array('data'=>'รายการ', 'width'=>50),
            'B' => array('data'=>'เนื้อหาย่อ','width'=>50),
            'C' => array('data'=>'วันที่สร้าง', 'width'=>16),            
            'D' => array('data'=>'วันที่แก้ไข', 'width'=>16),
        );
        $fields = array(
            'A' => 'title',
            'B' => 'excerpt',
            'C' => 'created_at',            
            'D' => 'updated_at',
        );
        $sheet->setTitle($sheetName);
        
        //title
        $sheet->setCellValue('A1', $sheetTitle);
        $sheet->setCellValue('D1', 'วันที่สร้าง: '.date("d/m/Y, H:i"));
        $sheet->getStyle('D1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $sheet->getRowDimension(1)->setRowHeight(20);
        
        //header
        $rowCount = 2;
        foreach ( $colums as $colum => $data ) {
            $sheet->getStyle($colum . $rowCount)->applyFromArray($styleH);
            $sheet->SetCellValue($colum.$rowCount, $data['data']);
            $sheet->getColumnDimension($colum)->setWidth($data['width']);
        }
        
        // data
        $rowCount++;
        foreach ( $info->result() as $row ){
            foreach ( $fields as $key => $field ){
                $value = $row->{$field};
                if ( $field == 'created_at' || $field == 'updated_at' )
                    $value = datetime_table ($value);
                $sheet->getStyle($key . $rowCount)->applyFromArray($styleB);
                $sheet->SetCellValue($key . $rowCount, $value); 
            }
            $sheet->getRowDimension($rowCount)->setRowHeight(20);
            $rowCount++;
        }
        $sheet->getRowDimension($rowCount)->setRowHeight(20);  
        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
        $writer->save('php://output');
        exit; 
    }
    
    public function pdf()
    {
        set_time_limit(0);
        ini_set('memory_limit', '128M');
        
        $input['recycle']       = 0;
        $input['grpContent']    = $this->_grpContent;
        if ( $this->session->condition[$this->_grpContent] ) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }
        $info           = $this->products_m->get_rows($input);
        $data['info']   = $info;
        
        $mpdfConfig = [
            'default_font_size' => 9,
            'default_font'      => 'garuda'
        ];
        $mpdf = new \Mpdf\Mpdf($mpdfConfig);
        $content = $this->load->view("{$this->router->class}/pdf", $data, TRUE);
        $mpdf->WriteHTML($content);
        
        
        $download = FALSE;
        $saveFile = FALSE;
        $viewFile = TRUE;
        $fileName = "products.pdf";
        $pathFile = "uploads/pdf/products/";
        if ( $saveFile ) {
            create_dir($pathFile);
            $fileName = $pathFile.$fileName;
            $mpdf->Output($fileName, 'F');
        }
        if ( $download ) 
            $mpdf->Output($fileName, 'D');
        if ( $viewFile ) 
            $mpdf->Output();
    }
    
    public function trash()
    {
        $this->load->module('template');
        
        // toobar
        $action[1][]            = action_list_view(site_url("{$this->router->class}"));
        $action[2][]            = action_delete_multi(base_url("{$this->router->class}/delete"));
        $data['boxAction']      = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][]   = array("ถังขยะ", site_url("{$this->router->class}/trash"));
        
        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/trash";
        
        $this->template->layout($data);
    }

    public function data_trash()
    {
        $input              = $this->input->post();
        $input['recycle']   = 1;
        $info               = $this->products_m->get_rows($input);
        $infoCount          = $this->products_m->get_count($input);
        $column             = array();
        foreach ($info->result() as $key => $rs) {
            $id                         = encode_id($rs->product_id);
            $action                     = array();
            $action[1][]                = table_restore("{$this->router->class}/restore");         
            $active                     = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId']   = $id;
            $column[$key]['checkbox']   = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title']      = $rs->title;
            $column[$key]['excerpt']    = $rs->excerpt;
            $column[$key]['recycle_at'] = datetime_table($rs->recycle_at);
            $column[$key]['action']     = Modules::run('utils/build_toolbar', $action);
        }

        $data['data']               = $column;
        $data['recordsTotal']       = $info->num_rows();
        $data['recordsFiltered']    = $infoCount;
        $data['draw']               = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    } 

    public function destroy()
    {
        if ( !$this->_permission ) {
            $toastr['type']     = 'error';
            $toastr['lineOne']  = config_item('appName');
            $toastr['lineTwo']  = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']    = false;
            $data['toastr']     = $toastr;
        } else {
            $input  = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs                     = decode_id($rs);
                $dateTime               = db_datetime_now();
                $value['updated_at']    = $dateTime;
                $value['updated_by']    = $this->session->users['user_id'];
                
                $result                 = false;
                $value['active']        = 0;
                $value['recycle']       = 1;
                $value['recycle_at']    = $dateTime;
                $value['recycle_by']    = $this->session->users['user_id'];
                $result                 = $this->products_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type']         = 'success';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type']         = 'error';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }

            $data['success']    = $result;
            $data['toastr']     = $toastr;
        }

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }
    
    public function restore()
    {
        if ( !$this->_permission ) {
            $toastr['type']     = 'error';
            $toastr['lineOne']  = config_item('appName');
            $toastr['lineTwo']  = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']    = false;
            $data['toastr']     = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs                     = decode_id($rs);
                $dateTime               = db_datetime_now();
                $value['updated_at']    = $dateTime;
                $value['updated_by']    = $this->session->users['user_id'];
                
                $result                 = false;
                $value['active']        = 0;
                $value['recycle']       = 0;
                $result                 = $this->products_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type']         = 'success';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type']         = 'error';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }

            $data['success']    = $result;
            $data['toastr']     = $toastr;
        }

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    } 

    public function delete()
    {
        if ( !$this->_permission ) {
            $toastr['type']     = 'error';
            $toastr['lineOne']  = config_item('appName');
            $toastr['lineTwo']  = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']    = false;
            $data['toastr']     = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs                     = decode_id($rs);
                $dateTime               = db_datetime_now();
                $value['updated_at']    = $dateTime;
                $value['updated_by']    = $this->session->users['user_id'];
                
                $result                 = false;
                $value['active']        = 0;
                $value['recycle']       = 2;
                $result                 = $this->products_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type']         = 'success';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type']         = 'error';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }

            $data['success']    = $result;
            $data['toastr']     = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }      
    
    public function action($type="")
    {
        if ( !$this->_permission ) {
            $toastr['type']         = 'error';
            $toastr['lineOne']      = config_item('appName');
            $toastr['lineTwo']      = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']        = false;
            $data['toastr']         = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs                     = decode_id($rs);
                $dateTime               = db_datetime_now();
                $value['updated_at']    = $dateTime;
                $value['updated_by']    = $this->session->users['user_id'];
                $result                 = false;

                if ( $type == "active" ) {
                    $value['active']    = $input['status'] == "true" ? 1 : 0;
                    $result             = $this->products_m->update_in($input['id'], $value);
                }
                
                if ( $result ) {
                    $toastr['type']     = 'success';
                    $toastr['lineOne']  = config_item('appName');
                    $toastr['lineTwo']  = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
                } else {
                    $toastr['type']     = 'error';
                    $toastr['lineOne']  = config_item('appName');
                    $toastr['lineTwo']  = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
                }

                $data['success']    = $result;
                $data['toastr']     = $toastr;
            }
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }  

    public function sumernote_img_upload()
    {
		//sumernote/img-upload
		$path       = 'products';
        $upload     = $this->uploadfile_library->do_upload('file',TRUE,$path);
        
		if(isset($upload['index'])){
			$picture = $this->config->item('root_url').'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
		}

		echo $picture;
       
    }

    public function deletefile($ids)
    {

		$arrayName = array('file' => $ids);

		echo json_encode($arrayName);
    }
    
    public function order() {
        $this->load->module('template'); 
        $input['active']                = 1;
        $input['recommend']             = 1; 
        $input['recycle']               = 0;
        $input['order'][0]['column']    = 5;
        $input['order'][0]['dir']       = 'asc';
        $info       = $this->products_m->get_rows($input);  
        $treeData   = array();
        foreach ($info->result() as $key => $rs) {  
                array_push($treeData, array(
                    'id'        => $rs->product_id,
                    'content'   => $rs->title
                )); 
        } 
        $data['treeData']   = $treeData;
        $data['frmAction']  = site_url("{$this->router->class}/update_order"); 
        // toobar
        $action[1][]        = action_refresh(base_url("{$this->router->class}/order"));
        $action[1][]        = action_list_view(base_url("{$this->router->class}"));
        $data['boxAction']  = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb

        $data['breadcrumb'][] = array($this->_title, base_url("{$this->router->class}"));
        $data['breadcrumb'][] = array('จัดลำดับสินค้าแนะนำ', base_url("{$this->router->class}/order"));
        
        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/order";

        $this->template->layout($data);
    }

    public function update_order()
    {
        $input  = $this->input->post();
        $order  = json_decode($input['order']);
        $value  = $this->_build_data_order($order);
        $result = $this->db->update_batch('products', $value, 'product_id');
        if ( $result ) {
            Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}/order"));
    }

    private function _build_data_order($info, $parentId=0)
    {
        $order = 0;
        foreach ($info as $key => $rs) {
            $this->_orderData[$order]['product_id'] = $rs->id; 
            $this->_orderData[$order]['order']      = $order; 
            $order++;
        }

        return $this->_orderData;
    }
    
}
