<?php 

/**
 * 
 */
class Users_model extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function find_users_by_user($username){

		$query = $this->db->where('username',$username)
		         ->get('users');
		return $query->row();         
	}

	public function find_users_by_user_and_salt($username,$salt){

		$query = $this->db->where('username',$username)
						  ->where('salt',$salt)
		                  ->get('users');
		return $query->row();         
	}

	
}