<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>

       
        <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post', 'autocomplete' => 'off')) ?>
            <div class="m-portlet__body"> 
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="delivery_title">จัดการบริษัทขนส่ง</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($delivery_title) ? $delivery_title : NULL ?>" type="text" class="form-control m-input " name="delivery_title" id="delivery_title" placeholder="ระบุ" disabled> 
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title">หัวข้อ</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" class="form-control m-input " name="title" id="titles" placeholder="ระบุ" required> 
                    </div>
                </div> 
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="weight_min">น้ำหนักเริ่มจาก</label>
                    <div class="col-sm-2">
                        <input value="<?php echo isset($info->weight_min) ? $info->weight_min : NULL ?>" type="text" class="form-control m-input " name="weight_min" id="weight_min" placeholder="ระบุ" required> 
                    </div>
                    <label class="col-sm-2 col-form-label" for="weight_max">ถึงน้ำหนัก</label>
                    <div class="col-sm-3">
                        <input value="<?php echo isset($info->weight_max) ? $info->weight_max : NULL ?>" type="text" class="form-control m-input " name="weight_max" id="weight_max" placeholder="ระบุ" required> 
                    </div>
                </div> 
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="price_in">ราคาส่งกรุงเทพมหานคร และปริมณฑล</label>
                    <div class="col-sm-3">
                        <input value="<?php echo isset($info->price_in) ? $info->price_in : NULL ?>" type="text" class="form-control m-input " name="price_in" id="price_in" placeholder="ระบุ" required> 
                    </div>
                </div> 
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="price_out">ราคาส่งต่างจังหวัด</label>
                    <div class="col-sm-3">
                        <input value="<?php echo isset($info->price_out) ? $info->price_out : NULL ?>" type="text" class="form-control m-input " name="price_out" id="price_out" placeholder="ระบุ" required> 
                    </div>
                </div> 
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">

                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success m-btn--wide">บันทึก</button>
                    <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                        </div>
                    </div>
                    
                </div>
            </div>
             <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
             <input type="hidden" class="form-control" name="db" id="db" value="delivery_settings">
             <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->delivery_setting_id) ? encode_id($info->delivery_setting_id) : 0 ?>">
             <input type="hidden" name="delivery_company_id" id="delivery_company_id" value="<?php echo isset($delivery_company_id) ? $delivery_company_id : 0 ?>">
        <?php echo form_close() ?>

        <!--end::Form-->
    </div> 
    
</div>  