<div class="m-portlet  m-portlet--full-height ">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    รายการสมัครล่าสุด
                </h3>  
            </div> 
        </div> 
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a class="btn m-btn--pill btn-sm   btn-primary m-btn m-btn--custom" href="<?=base_url('members')?>">
                    <i class="m-menu__link-icon fa fa-users"></i> แสดงทั้งหมด
                    </a>
                </li>  
            </ul>
        </div>
    </div>
    <div class="m-portlet__body text-overflow-body" style="height: 400px; overflow: auto;">
        <div >
            <div class="m-list-timeline m-list-timeline--skin-light"> 
            <div class="m-list-timeline__items">
                <?php
                if(!empty($info)): 
                    foreach($info as $item):
                        $text_badge = 'danger';
                        if($item->active > 0):
                            $text_badge = 'success';
                        endif; 
                ?> 
                    <div class="m-list-timeline__item">
                        <span class="m-list-timeline__badge m-list-timeline__badge--<?=$text_badge?>"></span>
                        <span class="m-list-timeline__text">
                            <?php echo !empty($item->full_name) ? $item->full_name : '';?><br>
                            <?php
                            if($item->active > 0):
                            ?>
                            <span class="m-badge m-badge--success m-badge--wide">ใช้งาน</span>
                            <?php
                            else:
                            ?>
                            <span class="m-badge m-badge--danger m-badge--wide">ระงับใช้งาน</span>
                            <?php
                            endif;
                            ?>
                        </span>
                        <span class="m-list-timeline__time" style="width: 100%;"><?php echo !empty($item->created_at) ? date_languagefull($item->created_at, true, 'th') : '';?> น.</span> 
                    </div> 
                
                <?php
                    endforeach;
                endif;
                ?>
                </div>
            </div>
        </div>
    </div>
</div> 