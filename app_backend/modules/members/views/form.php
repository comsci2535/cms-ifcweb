<div class="col-xl-3 col-lg-4">
    <div class="m-portlet m-portlet--full-height  ">
        <div class="m-portlet__body">
            <div class="m-card-profile">
                <div class="m-card-profile__title m--hide">
                    Your Profile
                </div>
                <div class="m-card-profile__pic">
                    <div class="m-card-profile__pic-wrapper">
                        <?php
                        if(!empty($info->picture)):
                        ?>
                        <img src="<?php echo !empty($info->picture) ? $this->config->item('root_url').$info->picture :''; ?>" alt=""
                        onerror="this.src='<?=$this->config->item('root_url').'images/users/8.png';?>'"
                        />
                        <?php
                        endif;
                        ?>
                        <?php
                        if(!empty($info->oauth_picture)):
                        ?>
                        <img src="<?php echo !empty($info->oauth_picture) ? $info->oauth_picture :''; ?>" alt=""
                        onerror="this.src='<?=$this->config->item('root_url').'images/users/8.png';?>'"
                        />
                        <?php
                        endif;
                        ?>
                    </div>
                </div>
                <div class="m-card-profile__details">
                    <span class="m-card-profile__name"><?php echo !empty($info->full_name) ? $info->full_name :''; ?></span> 
                </div>
            </div> 
        </div>
    </div>
</div>
<div class="col-xl-9 col-lg-8">
    <div class="m-portlet m-portlet--full-height m-portlet--tabs "> 
        <div class="m-portlet__head">
            <div class="m-portlet__head-tools">
                <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
                    <li class="nav-item m-tabs__item">
                        <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
                            <i class="flaticon-share m--hide"></i>
                            ข้อมูลทั่วไป
                        </a>
                    </li>  
                </ul>
            </div>
        </div> 
        <div class="tab-content">
            <div class="tab-pane active" id="m_user_profile_tab_1">
                <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-edit', 'method' => 'post', 'autocomplete'=> 'off')) ?>
                 
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" >รหัสมาชิก</label>
                    <div class="col-sm-7">
                        <input value="<?php echo !empty($info->member_id) ? $info->member_id :''; ?>" type="text" id="member_id" class="form-control" name="member_id" required>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" >เลขประจำตัวผู้เสียภาษีอากร</label>
                    <div class="col-sm-7">
                        <input value="<?php echo !empty($info->invoice_no) ? $info->invoice_no :''; ?>" type="text" id="invoice_no" class="form-control" name="invoice_no">
                    </div>
                </div>   

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" >ชื่อ-นามสกุล</label>
                    <div class="col-sm-7">
                        <input value="<?php echo !empty($info->full_name) ? $info->full_name :''; ?>" type="text" id="full_name" class="form-control" name="full_name" disabled>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" >บ้านเลขที่</label>
                    <div class="col-sm-7">
                        <textarea id="house_number" class="form-control" name="house_number" cols="30" rows="4" disabled><?php echo !empty($info->house_number) ? $info->house_number :''; ?></textarea>
      
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" >อาคาร / หมุ่บ้าน</label>
                    <div class="col-sm-3">
                        <input value="<?php echo !empty($info->building) ? $info->building :''; ?>" type="text" id="building" class="form-control" name="building" disabled>
                    </div>
                    <label class="col-sm-1 col-form-label" >ถนน</label>
                    <div class="col-sm-3">
                        <input value="<?php echo !empty($info->road) ? $info->road :''; ?>" type="text" id="road" class="form-control" name="road" disabled>
                    </div>
                </div> 
                
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" > แขวง / ตำบล</label>
                    <div class="col-sm-3">
                        <input value="<?php echo !empty($info->district) ? $info->district :''; ?>" type="text" id="district" class="form-control" name="district" disabled>
                    </div>
                    <label class="col-sm-1 col-form-label" >เขต / อำเภอ</label>
                    <div class="col-sm-3">
                        <input value="<?php echo !empty($info->amphoe) ? $info->amphoe :''; ?>" type="text" id="amphoe" class="form-control" name="amphoe" disabled>
                    </div>
                </div> 

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" >จังหวัด</label>
                    <div class="col-sm-3">
                        <input value="<?php echo !empty($info->province) ? $info->province :''; ?>" type="text" id="province" class="form-control" name="province" disabled>
                    </div>
                    <label class="col-sm-1 col-form-label" >รหัสไปรษณีย์</label>
                    <div class="col-sm-3">
                        <input value="<?php echo !empty($info->zipcode) ? $info->zipcode :''; ?>" type="text" id="zipcode" class="form-control" name="zipcode" disabled>
                    </div>
                </div> 
               
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" >อีเมล์</label>
                    <div class="col-sm-7">
                        <input value="<?php echo !empty($info->email) ? $info->email :''; ?>" type="email" id="email" class="form-control" name="email" disabled>
                    </div>
                </div> 
                
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" >เบอร์โทร</label>
                    <div class="col-sm-7">
                        <input value="<?php echo !empty($info->phone) ? $info->phone :''; ?>" type="text" id="phone" class="form-control" name="phone" disabled>
                    </div>
                </div>  
                
               <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" >ชื่อผู้ใช้งาน</label>
                    <div class="col-sm-7">
                        <input value="<?php echo $info->username ?>" type="text" id="input-username" class="form-control" name="username" disabled>
                    </div>
                </div>   

                <div class="m-portlet__foot m-portlet__foot--fit">

                    <div class="m-form__actions">
                        <div class="row">
                            <div class="col-2">
                            </div>
                            <div class="col-10">
                            <button type="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">บันทึก</button>
                        <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                            </div>
                        </div>
                        
                    </div>
                </div>
                 <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
                 <input type="hidden" class="form-control" name="db" id="db" value="member">
                 <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->id) ? encode_id($info->id) : 0 ?>">
                <?php echo form_close() ?>
            </div>
             
        </div>
    </div>  
</div> 




