<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>

       
        <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post', 'autocomplete' => 'off')) ?>
            <div class="m-portlet__body">
                    
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title">ชื่อธนาคาร</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" class="form-control m-input " name="title" id="titles" placeholder="ระบุ" required> 
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title_en">ชื่อธนาคาร (อังกฤษ)</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->title_en) ? $info->title_en : NULL ?>" type="text" class="form-control m-input " name="title_en" id="title_en" placeholder="ระบุ" required> 
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="account_number">เลขที่บัญชี</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->account_number) ? $info->account_number : NULL ?>" type="text" class="form-control m-input " name="account_number" id="account_number" placeholder="ระบุ" required> 
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="account_name">ชื่อบัญชี</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->account_name) ? $info->account_name : NULL ?>" type="text" class="form-control m-input " name="account_name" id="account_name" placeholder="ระบุ" required> 
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="account_name_en">ชื่อบัญชี (อังกฤษ)</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->account_name_en) ? $info->account_name_en : NULL ?>" type="text" class="form-control m-input " name="account_name_en" id="account_name_en" placeholder="ระบุ" required> 
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="account_branch">สาขา</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->account_branch) ? $info->account_branch : NULL ?>" type="text" class="form-control m-input " name="account_branch" id="account_branch" placeholder="ระบุ" required> 
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="account_branch_en">สาขา (อังกฤษ)</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->account_branch_en) ? $info->account_branch_en : NULL ?>" type="text" class="form-control m-input " name="account_branch_en" id="account_branch_en" placeholder="ระบุ" required> 
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="file_logo">File Logo</label>
                    <div class="col-sm-7">
                        <input id="file_logo" name="file_logo" type="file" data-preview-file-type="text">
                         <input type="hidden" name="outfilelogo" value="<?=(isset($info->file_logo)) ? $info->file_logo : ''; ?>">
                    </div>
                </div> 
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="file">File QR Code</label>
                    <div class="col-sm-7">
                        <input id="file" name="file" type="file" data-preview-file-type="text">
                         <input type="hidden" name="outfile" value="<?=(isset($info->file)) ? $info->file : ''; ?>">
                    </div>
                </div>  
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">

                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success m-btn--wide">บันทึก</button>
                    <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                        </div>
                    </div>
                    
                </div>
            </div>
             <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
             <input type="hidden" class="form-control" name="db" id="db" value="banks">
             <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->bank_id) ? encode_id($info->bank_id) : 0 ?>">
        <?php echo form_close() ?>

        <!--end::Form-->
    </div>


    
</div>

<script>
    //set par fileinput;
    var required_icon   = false; 
    var file_image      = '<?=(isset($info->file)) ? $this->config->item('root_url').$info->file : ''; ?>';
    var file_id         = '<?=$info->bank_id;?>';
    var deleteUrl       = '<?=base_url();?><?=$this->router->class;?>/deletefile/<?=$info->bank_id;?>';

    var required_icon_logo   = true; 
    var file_image_logo      = '<?=(isset($info->file_logo)) ? $this->config->item('root_url').$info->file_logo : ''; ?>';
    var file_id_logo         = '<?=$info->bank_id;?>';
    var deleteUrl_logo       = '<?=base_url();?><?=$this->router->class;?>/deletefile/<?=$info->bank_id;?>';

</script>




