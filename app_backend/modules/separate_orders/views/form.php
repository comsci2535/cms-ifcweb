<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div> 
       
        <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post', 'autocomplete' => 'off')) ?>
            <div class="m-portlet__body">  
                <?php 
                if(!empty($order_type)):
                ?>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="tracking_china">เลขพัสดุจีน</label>
                    <div class="col-sm-7">
                        <textarea class="form-control m-input " name="tracking_china" id="tracking_china" placeholder="ระบุ" required readonly cols="30" rows="5"><?php echo isset($info->tracking_china) ? $info->tracking_china : NULL ?></textarea>
                    </div>
                </div>
                <?php
                endif;
                ?>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="tracking_receive">เลขพัสดุที่ได้รับ</label>
                    <div class="col-sm-5">
                    <input value="<?php echo isset($info->tracking_receive) ? $info->tracking_receive : NULL ?>" type="text" class="form-control m-input " name="tracking_receive" id="tracking_receive" placeholder="ระบุ"> 
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="tracking_in_code">Tracking</label>
                    <div class="col-sm-5">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><?php echo !empty($info->tracking_key) ? $info->tracking_key : '';?></span>
                            </div>
                            <input value="<?php echo isset($info->tracking_in_code) ? $info->tracking_in_code : NULL ?>" type="text" class="form-control m-input " name="tracking_in_code" id="tracking_in_code" placeholder="ระบุ" <?php //echo !empty($order_type) ? "readonly" :"";?>> 
                            <input value="<?php echo isset($info->tracking_key) ? $info->tracking_key : NULL ?>" type="hidden" class="form-control m-input " name="tracking_key"> 
                            
                        </div>
                        
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="tracking_code">เลขพัสดุไทย</label>
                    <div class="col-sm-5">
                        <input value="<?php echo isset($info->tracking_code) ? $info->tracking_code : NULL ?>" type="text" class="form-control m-input " name="tracking_code" id="tracking_code" placeholder="ระบุ"> 
                    </div>
                </div>
                
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="tracking_code">วันที่ขึ้นตู้</label>
                    <div class="col-sm-3">
                        <div class="input-group date">
                            <input type="text" class="form-control m-input m_datepicker_2" name="date_up" value="<?php echo isset($info->date_up) ? $info->date_up : NULL ?>" readonly placeholder="เลือกวันที่" data-date-format="yyyy-mm-dd"/>
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="la la-calendar-check-o"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <label class="col-sm-1 col-form-label" for="tracking_code">วันที่ถึง</label>
                    <div class="col-sm-3">
                        <div class="input-group date">
                            <input type="text" class="form-control m-input m_datepicker_2_1" name="date_down" value="<?php echo isset($info->date_down) ? $info->date_down : NULL ?>" readonly placeholder="เลือกวันที่"  data-date-format="yyyy-mm-dd"/>
                            <div class="input-group-append">
                                <span class="input-group-text">
                                    <i class="la la-calendar-check-o"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="type_note">ประเภท</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->type_note) ? $info->type_note : NULL ?>" type="text" class="form-control m-input " name="type_note" id="type_note" placeholder="ระบุ"> 
                    </div>
                </div> 
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="qty_china">จำนวน</label>
                    <div class="col-sm-4">
                        <input value="<?php echo isset($info->qty_china) ? $info->qty_china : NULL ?>" type="text" class="form-control m-input " name="qty_china" id="qty_china" placeholder="ระบุ"> 
                    </div>
                </div> 

                <div class="form-group m-form__group row">
                    <div class="col-sm-12">
                        <hr>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="wide">กว้าง</label>
                    <div class="col-sm-2">
                        <input value="<?php echo isset($info->wide) ? $info->wide : NULL ?>" type="text" class="form-control m-input " name="wide" id="wide" placeholder="ระบุ"> 
                        <input value="<?php echo isset($info->wide) ? $info->wide : NULL ?>" type="hidden" class="form-control m-input " name="wide_old"> 
                    </div>
                    <label class="col-sm-1 col-form-label" for="longs">ยาว</label>
                    <div class="col-sm-2">
                        <input value="<?php echo isset($info->longs) ? $info->longs : NULL ?>" type="text" class="form-control m-input " name="longs" id="longs" placeholder="ระบุ"> 
                        <input value="<?php echo isset($info->longs) ? $info->longs : NULL ?>" type="hidden" class="form-control m-input " name="longs_old"> 
                    </div>
                    <label class="col-sm-1 col-form-label" for="high">สูง</label>
                    <div class="col-sm-2">
                        <input value="<?php echo isset($info->high) ? $info->high : NULL ?>" type="text" class="form-control m-input " name="high" id="high" placeholder="ระบุ"> 
                        <input value="<?php echo isset($info->high) ? $info->high : NULL ?>" type="hidden" class="form-control m-input " name="high_old"> 
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="weight">น้ำหนัก</label>
                    <div class="col-sm-2">
                        <input value="<?php echo isset($info->weight) ? $info->weight : NULL ?>" type="text" class="form-control m-input " name="weight" id="weight" placeholder="ระบุ"> 
                        <input value="<?php echo isset($info->weight) ? $info->weight : NULL ?>" type="hidden" class="form-control m-input " name="weight_old"> 
                    </div>
                </div>
               
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="type_transportations_setting_id">ประเภทสินค้า</label>
                    <div class="col-sm-5">
                        <select class="form-control select2" id="type_transportations_setting_id" name="type_transportations_setting_id">
                            <option value="">เลือกประเภทสินค้า</option>
                            <?php
                            if(!empty($type_transportations_setting)):
                                foreach($type_transportations_setting as $custs):
                            ?>
                                <option value="<?php echo !empty($custs->type_transportations_setting_id) ? $custs->type_transportations_setting_id : null;?>"><?php echo !empty($custs->title) ? $custs->title : null;?></option>
                            <?php
                                endforeach;
                            endif;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="type_transportation_cust_id">ประเภทการซื้อ</label>
                    <div class="col-sm-5">
                        <select class="form-control select2" id="type_transportation_cust_id" name="type_transportation_cust_id">
                            <option value="">เลือกประเภทการซื้อ</option>
                        </select>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="type_transportation_id">ประเภทขนส่ง</label>
                    <div class="col-sm-5">
                        <select class="form-control select2" id="type_transportation_id" name="type_transportation_id">
                            <option value="">เลือกประเภทขนส่ง</option>
                        </select>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for=""></label>
                    <div class="col-sm-5">
                        <button type="button" id="btn-calculator" class="btn btn-primary m-btn--wide"><i class="fas fa-calculator"></i> คำนวณขนาด/ค่านำเข้า</button>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <div class="col-sm-12">
                        <hr>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                   
                    <label class="col-sm-2 col-form-label" for="size">ขนาด</label>
                    <div class="col-sm-2">
                        <input value="<?php echo isset($info->size) ? $info->size : NULL ?>" type="text" class="form-control m-input " name="size" id="size" placeholder="" readonly> 
                        <input value="<?php echo isset($info->size) ? $info->size : NULL ?>" type="hidden" class="form-control m-input " name="size_old"> 
                    </div>
                    <label class="col-sm-3 col-form-label" style="text-align: left;color: #f16c6c;"><small>(กว้าง x ยาว x สูง) / 1,000,000</small></label>
                </div>  
               
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="import_cost">ค่านำเข้า</label>
                    <div class="col-sm-2">
                        <input value="<?php echo !empty($info->import_cost) ? $info->import_cost : '' ?>" type="text" class="form-control m-input " name="import_cost" id="import_cost" placeholder="" readonly> 
                        <input value="<?php echo isset($info->import_cost) ? $info->import_cost : NULL ?>" type="hidden" class="form-control m-input " name="import_cost_old"> 
                    </div>
                    <label class="col-sm-2 col-form-label" style="text-align: left;color: #f16c6c;"><small>ขนาด x เรทราคาที่ตั้งค่า</small></label>
                    <label class="col-sm-1 col-form-label" for="service_charge">ค่าบริการ</label>
                    <div class="col-sm-2">
                        <input value="<?php echo !empty($info->service_charge) ? $info->service_charge : '' ?>" type="text" class="form-control m-input " name="service_charge" id="service_charge" placeholder="ระบุ"> 
                        <input value="<?php echo isset($info->service_charge) ? $info->service_charge : NULL ?>" type="hidden" class="form-control m-input " name="service_charge_old"> 
                    </div>
                </div>  
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="repack_sack">ค่าบริการรีแพ็คลัง/กระสอบ</label>
                    <div class="col-sm-2">
                        <input value="<?php echo !empty($info->repack_sack) ? $info->repack_sack : 50; ?>" type="text" class="form-control m-input " name="repack_sack" id="repack_sack" placeholder="ระบุ"> 
                        <input value="<?php echo isset($info->repack_sack) ? $info->repack_sack : 0; ?>" type="hidden" class="form-control m-input " name="repack_sack_old"> 
                    </div>
                </div>  
                
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="detail">ข้อมูล</label>
                    <div class="col-sm-7">
                        <textarea name="detail" rows="3" class="form-control" id="detail" placeholder="ระบุ"><?php echo isset($info->detail) ? $info->detail : NULL ?></textarea>
                    </div>
                </div>  
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">

                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success m-btn--wide">บันทึก</button>
                    <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                        </div>
                    </div>
                    
                </div>
            </div>
             <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
             <input type="hidden" class="form-control" name="db" id="db" value="orders_detail_import"> 
             <input type="hidden" name="order_id" id="input-order-id" value="<?php echo isset($order_id) ? encode_id($order_id) : '' ?>">
             <input type="hidden" name="user_id" id="input-user-id" value="<?php echo isset($user_id) ? $user_id : '' ?>">
             <input type="hidden" name="attribute_send_id" value="<?php echo isset($info->attribute_send_id) ? $info->attribute_send_id : 0 ?>"> 
             <input type="hidden" name="type_transportations_setting_id" value="<?php echo isset($info->type_transportations_setting_id) ? $info->type_transportations_setting_id : 0 ?>">
             <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->orders_detail_import_id) ? encode_id($info->orders_detail_import_id) : 0 ?>">
        <?php echo form_close() ?>

        <!--end::Form-->
    </div> 
</div> 




