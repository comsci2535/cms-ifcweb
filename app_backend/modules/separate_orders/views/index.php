
<div class="col-xl-12">
    <div class="m-portlet m-portlet--mobile ">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       รายการ
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                    <?php echo $boxAction; ?>
            </div>
        </div>
        <div class="m-portlet__body">

            <form  role="form">
                <div class="alert m-alert--default">
                    <span>
                        <strong>เลขที่บิล</strong> : <?php echo !empty($order_code)? $order_code :''; ?> </br>
                        <strong>รหัสสมาชิก</strong> : <?php echo !empty($member_id)? $member_id :''; ?> </br>
                        <strong>ชื่อสมาชิก</strong> : <?php echo !empty($full_name)? $full_name :''; ?> </br>
                        <strong>วันที่นำเข้าระบบ</strong> : <?php echo !empty($created_at)? $created_at :''; ?>
                    </span>
                </div>
                <div class="table-responsive">
                    <table id="data-list" class="table table-hover dataTable" width="100%">
                        <input type="hidden" id="code" value="<?php echo !empty($code) ? $code : '';?>">
                        <thead>
                            <tr>
                                <th><input name="tbCheckAll" type="checkbox" class="icheck tb-check-all"></th>
                                <th>ข้อมูลขนส่ง</th>
                                <th>เลขพัสดุ</th>
                                <th>รายละเอียดข้อมูล</th>
                                <th>ค่าบริการ</th>
                                <th>ค่านำส่ง</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </form>

        </div>
    </div>
</div>