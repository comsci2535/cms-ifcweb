<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Separate_orders extends MX_Controller 
{

    private $_title             = "จัดการแยกรายการสินค้า";
    private $_pageExcerpt       = "การจัดการข้อมูลเกี่ยวกับจัดการแยกรายการสินค้า";
    private $_grpContent        = "separate_orders";
    private $_requiredExport    = true;
    private $_permission;
    private $_tracking_key      = '68-';

    public function __construct()
    {
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('ckeditor');
        $this->load->model("separate_orders_m");
        $this->load->model("orders/orders_m");
        $this->load->model("members/members_m");
        $this->load->model("type_transportations_setting_list/type_transportations_setting_list_m");
        $this->load->model("type_transportations/type_transportations_m");
        $this->load->model("type_transportations_cust/type_transportations_cust_m");
        $this->load->model("type_transportations_setting/type_transportations_setting_m");
        $this->load->model('attribute_sends/attribute_sends_m');
        $this->load->model('type_transportations_setting/type_transportations_setting_m');
    }
    
    public function index($code)
    {
        $this->load->module('template');
        
        // toobar
        $export = array(
            'excel' => site_url("{$this->router->class}/excel"),
            'pdf'   => site_url("{$this->router->class}/pdf"),
        );
        $action[1][]        = action_refresh(site_url("{$this->router->class}/index/{$code}"));
        //$action[1][]      = action_filter();
        $action[1][]        = action_add(site_url("{$this->router->class}/create/{$code}"));
        //$action[2][]      = action_export_group($export);
        $action[3][]        = action_trash_multi("{$this->router->class}/destroy"); 
        $data['boxAction']  = Modules::run('utils/build_toolbar', $action);

        $input['recycle']               = 0;
        $input['order_id']              = decode_id($code);
        $info                           = $this->orders_m->get_rows($input)->row();  
        $title                          = !empty($info->order_code) ? $info->order_code : ""; 
        $data['code']                   = $code; 
        $data['order_code']             = $title;
        $data['created_at']             = $info->created_at;

        $input['recycle']               = 0;
        $input['id']                    = $info->user_id;
        $members                        = $this->members_m->get_rows($input)->row();  
        $data['full_name']              = $members->full_name;
        $data['member_id']              = $members->member_id;
        
        // breadcrumb
        $data['breadcrumb'][] = array($title, "orders");
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}/index/{$code}"));  
        
        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/index";
        
        $this->template->layout($data);
    }    

    public function data_index()
    {
        $input              = $this->input->post();
        $input['recycle']   = 0;
        $input['order_id']  = decode_id($input['code']);
        $info               = $this->separate_orders_m->get_rows($input);
        $infoCount          = $this->separate_orders_m->get_count($input);
        $column             = array();
        // ถ้าต้องการ export ตารางให้เก็บ parameter request ลง session
        if ( $this->_requiredExport ) {
            $condition[$this->_grpContent] = $input; 
            $this->session->set_userdata("condition", $condition);
        }
        
        foreach ($info->result() as $key => $rs) {
            $id                                 = encode_id($rs->orders_detail_import_id);
            $action                             = array();
            $action[1][]                        = table_edit(site_url("{$this->router->class}/edit/{$input['code']}/{$id}")); 

            $attribute_['recycle']            = 0;
            $attribute_['attribute_send_in']  = explode(',', $rs->attribute_send_id);
            $attributes                   = $this->attribute_sends_m->get_rows($attribute_)->result();
            $attribute_text = '';
            if(!empty($attributes)):
                foreach($attributes as $keys => $attribute):
                    if($keys > 0):
                        $attribute_text.= ', '.$attribute->title;
                    else:
                        $attribute_text.= $attribute->title;
                    endif;
                endforeach;
            endif;

            $input_['recycle']                                  = 0;
            $input_['type_transportations_setting_id']          = $rs->type_transportations_setting_id;
            $transportations_setting                            = $this->type_transportations_setting_m->get_rows($input_)->row();
            $attribute_text.= !empty($transportations_setting->title) ? ', '.$transportations_setting->title : '';

            $detail_send = '<p>';
            $detail_send.= '<span> Tracking : '.(!empty($rs->tracking_in_code) ? $rs->tracking_in_code : 'ไม่ระบุ').'</span>';
            $detail_send.= '<br><span> เลขพัสดุไทย : '.(!empty($rs->tracking_code) ? $rs->tracking_code : 'ไม่ระบุ').'</span>';
            $detail_send.= '<br><span> เลขพัสดุที่ได้รับ : '.(!empty($rs->tracking_receive) ? $rs->tracking_receive : 'ไม่ระบุ').'</span>';
            $detail_send.= '<br><span> ขึ้นตู้ : '.$rs->date_up.'</span>';
            $detail_send.= '<br><span> ถึง : '.$rs->date_down.'</span>'; 

            $detail = '<p>';
            $detail.= '<span> ข้อมูล : '.$rs->detail.'</span>';
            $detail.= '<br><span> ประเภท : '.$rs->type_note.'</span>';
            $detail.= '<br><span> น้ำหนัก : '.$rs->weight.'</span>'; 
            $detail.= '<br><span> ขนาด : '.$rs->size.'</span>'; 

            $tracking_china         = !empty($rs->tracking_china) ? $rs->tracking_china : 'ไม่ระบุ';
            $tracking_china_text    = str_replace(","," ,", $tracking_china);
            
            $tracking_china = '<p>เลขพัสดุจีน : <br>'.$tracking_china_text.'<br><small>'.$attribute_text.'</small></p>'; 
            $service_charge = '<p>ค่าบริการ : <br>'.$rs->service_charge.' บาท</p>'; 
            $import_cost    = '<p>ค่านำเข้า : <br>'.$rs->import_cost.' บาท</p>'; 
            $import_cost   .= '<p>ค่าบริการรีแพ็คลัง / กระสอบ : <br>'.$rs->repack_sack.' บาท</p>'; 

            $column[$key]['DT_RowId']           = $id;
            $column[$key]['checkbox']           = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['detail_send']        = $detail_send;
            $column[$key]['tracking_china']     = $tracking_china;
            $column[$key]['service_charge']     = $service_charge;
            $column[$key]['detail']             = $detail;
            $column[$key]['import_cost']        = $import_cost;
            $column[$key]['action']             = Modules::run('utils/build_button_group', $action);
        }
        $data['data']               = $column;
        $data['recordsTotal']       = $info->num_rows();
        $data['recordsFiltered']    = $infoCount;
        $data['draw']               = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function get_order_import($id)
    {
        $input['recycle']   = 0;
        $input['order_id']  = $id;
        $info               = $this->separate_orders_m->get_rows($input); 
        $column             = array(); 
        
        foreach ($info->result() as $key => $rs):

            $attribute_['recycle']            = 0;
            $attribute_['attribute_send_in']  = explode(',', $rs->attribute_send_id);
            $attributes                   = $this->attribute_sends_m->get_rows($attribute_)->result();
            $attribute_text = '';
            if(!empty($attributes)):
                foreach($attributes as $keys => $attribute):
                    if($keys > 0):
                        $attribute_text.= ', '.$attribute->title;
                    else:
                        $attribute_text.= $attribute->title;
                    endif;
                endforeach;
            endif;

            $tracking_china         = !empty($rs->tracking_china) ? $rs->tracking_china : 'ไม่ระบุ';
            $tracking_china_text    = str_replace(","," ,", $tracking_china);

            $detail_send = '<p>';
            $detail_send.= '<span> Tracking : '.(!empty($rs->tracking_in_code) ? $rs->tracking_in_code : 'ไม่ระบุ').'</span>';
            $detail_send.= '<br><span> เลขพัสดุไทย : '.(!empty($rs->tracking_code) ? $rs->tracking_code : 'ไม่ระบุ').'</span>';
            $detail_send.= '<br><span> เลขพัสดุที่ได้รับ : '.(!empty($rs->tracking_receive) ? $rs->tracking_receive : 'ไม่ระบุ').'</span>';
            $detail_send.= '<br><span> ขึ้นตู้ : '.$rs->date_up.'</span>';
            $detail_send.= '<br><span> ถึง : '.$rs->date_down.'</span>'; 

            $detail = '<p>';
            $detail.= '<span> ข้อมูล : '.$rs->detail.'</span>';
            $detail.= '<br><span> ประเภท : '.$rs->type_note.'</span>';
            $detail.= '<br><span> น้ำหนัก : '.$rs->weight.'</span>'; 
            $detail.= '<br><span> ขนาด : '.$rs->size.'</span>'; 
            
            $tracking_china = '<p>เลขพัสดุจีน : <br>'.$tracking_china_text.'<br><small>'.$attribute_text.'</small></p>'; 
            $service_charge = '<p>ค่าบริการ : <br>'.$rs->service_charge.' บาท</p>'; 
            $import_cost    = '<p>ค่านำเข้า : <br>'.$rs->import_cost.' บาท</p>'; 
            $import_cost   .= '<p>ค่าบริการรีแพ็คลัง/กระสอบ : <br>'.$rs->repack_sack.' บาท</p>'; 

            $column[$key]['weight']             = $rs->weight;
            $column[$key]['size']               = $rs->size;
            $column[$key]['service']            = $rs->service_charge; 
            $column[$key]['cost']               = $rs->import_cost;
            $column[$key]['repack_sack']        = $rs->repack_sack;
            $column[$key]['detail_send']        = $detail_send;
            $column[$key]['tracking_china']     = $tracking_china;
            $column[$key]['service_charge']     = $service_charge;
            $column[$key]['detail']             = $detail;
            $column[$key]['import_cost']        = $import_cost;

        endforeach; 
        
        return $column;
    }
    
    public function create($id)
    {
        $this->load->module('template');

        $data['grpContent']     = $this->_grpContent;
        $data['frmAction']      = site_url("{$this->router->class}/storage");
        
        $order_id               = decode_id($id); 
        $input['order_id']      = $order_id; 
        $info                   = $this->separate_orders_m->get_rows($input)->row();
        
        $info->tracking_code    = '';
        $info->type_note        = '';
        $info->weight           = '';
        $info->size             = '';
        $info->service_charge   = '';
        $info->import_cost      = '';
        $info->detail           = '';
        $info->wide             = '';  
        $info->longs            = '';  
        $info->high             = '';  
        $info->repack_sack      = '';
        $info->tracking_receive = '';

        $info->tracking_key      = $this->_tracking_key;
        if(!empty($info->tracking_in_code)):
            $tracking_in_code       = explode('-', $info->tracking_in_code);
            $info->tracking_in_code =  $tracking_in_code[1];
            $info->tracking_key      = $tracking_in_code[0].'-';
        endif;
        $orders                 = $this->orders_m->get_rows($input)->row();  
        $data['order_type']     = !empty($orders->order_type) ? $orders->order_type : 0;
        $data['user_id']        = !empty($orders->user_id) ? $orders->user_id : 0;
        $data['order_id']       = !empty($orders->order_id) ? $orders->order_id : 0;
        

        $data['info']           = $info;

        $input___['recycle']                    = 0;
        $input___['active']                     = 1;
        $data['type_transportations_setting']   = $this->type_transportations_setting_m->get_rows($input___)->result();

        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("{$this->router->class}/index/{$id}"));
        $data['breadcrumb'][]   = array('สร้าง', site_url("{$this->router->class}/create"));
        
        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/form";
        
        $this->template->layout($data);
    }
    
    public function storage()
    {
        $input  = $this->input->post(null, true);
        $value  = $this->_build_data($input);
        $result = $this->separate_orders_m->insert($value);
        if ( $result ) {
           Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
           Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}/index/{$input['order_id']}")); 
    }
    
    public function edit($id="", $code="")
    {
        $this->load->module('template');
 
        
        $order_id               = decode_id($id);
        $code                   = decode_id($code);
        $input['order_id']      = $order_id;
        $input['orders_detail_import_id']  = $code;
        $info               = $this->separate_orders_m->get_rows($input);
        
        if ( $info->num_rows() == 0) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยไม่พบหน้าที่ต้องการ');
            redirect_back();
        } 
 
        $orders                  = $this->orders_m->get_rows($input)->row();  
        $data['order_type']      = !empty($orders->order_type) ? $orders->order_type : 0;

        $info                    = $info->row();
        $info->tracking_key      = $this->_tracking_key;
        if(!empty($info->tracking_in_code)):
            $tracking_in_code       = explode('-', $info->tracking_in_code);
            $info->tracking_in_code =  $tracking_in_code[1];
            $info->tracking_key      = $tracking_in_code[0].'-';
        endif;
        
        $info->service_charge   = ($info->service_charge == 0.00) ? '' : $info->service_charge;
        $info->import_cost      = ($info->import_cost == 0.00) ? '' : $info->import_cost;
        $info->wide             = ($info->wide == 0.00) ? '' : $info->wide;
        $info->longs            = ($info->longs == 0.00) ? '' : $info->longs;
        $info->high             = ($info->high == 0.00) ? '' : $info->high;
        $info->repack_sack      = ($info->repack_sack == 0.00) ? '' : $info->repack_sack;

        $data['info']           = $info;

        $input___['recycle']                    = 0;
        $input___['active']                     = 1;
        $data['type_transportations_setting']   = $this->type_transportations_setting_m->get_rows($input___)->result();

        $data['grpContent']     = $this->_grpContent;
        $data['frmAction']      = site_url("{$this->router->class}/update");
        
        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("{$this->router->class}/index/{$id}"));
        $data['breadcrumb'][]   = array('แก้ไข', site_url("{$this->router->class}{$id}/edit")); 
        $data['order_id']       = $id;
        $data['user_id']        = $info->user_id;
        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/form";
        
        $this->template->layout($data);
    }
    
    public function update()
    {
        $input  = $this->input->post(null, true);
        $id     = decode_id($input['id']);
        $value  = $this->_build_data($input);
        $result = $this->separate_orders_m->update($id, $value);
        if ( $result ) {

            // แก้ไขน้ำหนัก
            $input_weight['total_old']    = $input['weight_old']; 
            $input_weight['total_new']    = $input['weight']; 
            $input_weight['remark']       = 'แก้ไขน้ำหนัก'; 
            $input_weight['type']         = 'edit_weight';  
            $value_weight = $this->_build_data_edit($input_weight, $id);
            if(!empty($value_weight)):
                $this->set_orders_log($value_weight);
            endif;

            // แก้ไขขนาด
            $input_size['total_old']    = $input['size_old']; 
            $input_size['total_new']    = $input['size']; 
            $input_size['remark']       = 'แก้ไขขนาด'; 
            $input_size['type']         = 'edit_size';  
            $value_size = $this->_build_data_edit($input_size, $id);
            if(!empty($value_size)):
                $this->set_orders_log($value_size);
            endif;

            // แก้ไขค่าบริการ
            $input_service_charge['total_old']    = $input['service_charge_old']; 
            $input_service_charge['total_new']    = $input['service_charge']; 
            $input_service_charge['remark']       = 'แก้ไขค่าบริการ'; 
            $input_service_charge['type']         = 'edit_service_charge';  
            $value_service_charge = $this->_build_data_edit($input_service_charge, $id);
            if(!empty($value_service_charge)):
                $this->set_orders_log($value_service_charge);
            endif;
            // แก้ไขค่านำเข้า
            $input_import_cost['total_old']    = $input['import_cost_old']; 
            $input_import_cost['total_new']    = $input['import_cost']; 
            $input_import_cost['remark']       = 'แก้ไขค่านำเข้า'; 
            $input_import_cost['type']         = 'edit_import_cost';  
            $value_import_cost = $this->_build_data_edit($input_import_cost, $id);
            if(!empty($value_import_cost)):
                $this->set_orders_log($value_import_cost);
            endif;

            // แก้ไขความกว้าง
            $input_wide['total_old']    = $input['wide_old']; 
            $input_wide['total_new']    = $input['wide']; 
            $input_wide['remark']       = 'แก้ไขความกว้าง'; 
            $input_wide['type']         = 'edit_wide';  
            $value_wide                 = $this->_build_data_edit($input_wide, $id);
            if(!empty($value_wide)):
                $this->set_orders_log($value_wide);
            endif;

            // แก้ไขความยาว
            $input_longs['total_old']    = $input['longs_old']; 
            $input_longs['total_new']    = $input['longs']; 
            $input_longs['remark']       = 'แก้ไขความยาว'; 
            $input_longs['type']         = 'edit_longs';  
            $value_longs = $this->_build_data_edit($input_longs, $id);
            if(!empty($value_longs)):
                $this->set_orders_log($value_longs);
            endif;

            // แก้ไขความสูง
            $input_high['total_old']    = $input['high_old']; 
            $input_high['total_new']    = $input['high']; 
            $input_high['remark']       = 'แก้ไขความสูง'; 
            $input_high['type']         = 'edit_import_cost';  
            $value_high = $this->_build_data_edit($input_high, $id);
            if(!empty($value_high)):
                $this->set_orders_log($value_high);
            endif;

            // แก้ไขค่าบริการรีแพ็คลัง/กระสอบ
            $input_repack_sack['total_old']    = $input['repack_sack_old']; 
            $input_repack_sack['total_new']    = $input['repack_sack']; 
            $input_repack_sack['remark']       = 'แก้ไขค่าบริการรีแพ็คลัง/กระสอบ'; 
            $input_repack_sack['type']         = 'edit_repack_sack';  
            $value_repack_sack = $this->_build_data_edit($input_repack_sack, $id);
            if(!empty($value_repack_sack)):
                $this->set_orders_log($value_repack_sack);
            endif;
            Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}/index/{$input['order_id']}"));
    }

    private function set_orders_log($data)
    {
        $value['order_id']      = $data['order_id'];
        $value['total_old']     = $data['total_old'];
        $value['total_new']     = $data['total_new'];
        $value['remark']        = $data['remark'];
        $value['type']          = $data['type'];
        $value['created_at']    = db_datetime_now();
        $value['created_by']    = $this->session->users['user_id']; 
        $this->orders_m->insert_orders_log($value);
    } 
    
    private function _build_data($input)
    { 
        $value['tracking_china']    = $input['tracking_china'];
        $value['tracking_code']     = $input['tracking_code'];
        $value['tracking_receive']  = $input['tracking_receive'];
        $value['attribute_send_id'] = $input['attribute_send_id'];
        $value['type_transportations_setting_id'] = !empty($input['type_transportations_setting_id']) ? $input['type_transportations_setting_id'] : 0;
        if(!empty($input['tracking_in_code'])):
            $value['tracking_in_code']  = $input['tracking_key'].$input['tracking_in_code'];
        endif;
        $value['qty_china']         = !empty($input['qty_china']) ? $input['qty_china'] : 0;
        $value['detail']            = $input['detail'];
        $value['date_up']           = $input['date_up'];
        $value['date_down']         = $input['date_down'];
        $value['weight']            = $input['weight'];
        $value['size']              = $input['size'];
        $value['wide']              = $input['wide'];
        $value['longs']             = $input['longs'];
        $value['high']              = $input['high'];
        $value['repack_sack']       = $input['repack_sack'];
        $value['service_charge']    = $input['service_charge'];
        $value['import_cost']       = $input['import_cost'];
        $value['type_note']         = $input['type_note'];
        $value['active']            = 1;
        $value['order_id']          = decode_id($input['order_id']);  
        $value['user_id']           = !empty($input['user_id']) ? $input['user_id'] : '';
        
        if ( $input['mode'] == 'create' ) {
            $value['created_at'] = db_datetime_now();
            $value['created_by'] = $this->session->users['user_id'];
            $value['updated_at'] = db_datetime_now();
            $value['updated_by'] = $this->session->users['user_id'];
        } else {
            $value['updated_at'] = db_datetime_now();
            $value['updated_by'] = $this->session->users['user_id'];
        }
        return $value;
    } 

    private function _build_data_edit($input, $order_id)
    {   
        $value  = array();
        if(intval($input['total_old']) != intval($input['total_new'])):
            $value['total_old']    = $input['total_old']; 
            $value['total_new']    = $input['total_new']; 
            $value['remark']       = $input['remark']; 
            $value['type']         = $input['type']; 
            $value['order_id']     = $order_id;  
        endif;  
        return $value;
    }
    
    public function excel()
    {
        set_time_limit(0);
        ini_set('memory_limit', '128M');
        
        $input['recycle']       = 0;
        $input['grpContent']    = $this->_grpContent;
        if ( $this->session->condition[$this->_grpContent] ) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }
        $info           = $this->separate_orders_m->get_rows($input);
        $fileName       = "separate_orders";
        $sheetName      = "Sheet name";
        $sheetTitle     = "Sheet title";
        
        $spreadsheet    = new Spreadsheet();
        $sheet          = $spreadsheet->getActiveSheet();
        
        $styleH = excel_header();
        $styleB = excel_body();
        $colums = array(
            'A' => array('data'=>'รายการ', 'width'=>50),
            'B' => array('data'=>'เนื้อหาย่อ','width'=>50),
            'C' => array('data'=>'วันที่สร้าง', 'width'=>16),            
            'D' => array('data'=>'วันที่แก้ไข', 'width'=>16),
        );
        $fields = array(
            'A' => 'title',
            'B' => 'excerpt',
            'C' => 'created_at',            
            'D' => 'updated_at',
        );
        $sheet->setTitle($sheetName);
        
        //title
        $sheet->setCellValue('A1', $sheetTitle);
        $sheet->setCellValue('D1', 'วันที่สร้าง: '.date("d/m/Y, H:i"));
        $sheet->getStyle('D1')->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_RIGHT);
        $sheet->getRowDimension(1)->setRowHeight(20);
        
        //header
        $rowCount = 2;
        foreach ( $colums as $colum => $data ) {
            $sheet->getStyle($colum . $rowCount)->applyFromArray($styleH);
            $sheet->SetCellValue($colum.$rowCount, $data['data']);
            $sheet->getColumnDimension($colum)->setWidth($data['width']);
        }
        
        // data
        $rowCount++;
        foreach ( $info->result() as $row ){
            foreach ( $fields as $key => $field ){
                $value = $row->{$field};
                if ( $field == 'created_at' || $field == 'updated_at' )
                    $value = datetime_table ($value);
                $sheet->getStyle($key . $rowCount)->applyFromArray($styleB);
                $sheet->SetCellValue($key . $rowCount, $value); 
            }
            $sheet->getRowDimension($rowCount)->setRowHeight(20);
            $rowCount++;
        }
        $sheet->getRowDimension($rowCount)->setRowHeight(20);  
        $writer = new Xlsx($spreadsheet);
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
        $writer->save('php://output');
        exit; 
    }
    
    public function pdf()
    {
        set_time_limit(0);
        ini_set('memory_limit', '128M');
        
        $input['recycle']       = 0;
        $input['grpContent']    = $this->_grpContent;
        if ( $this->session->condition[$this->_grpContent] ) {
            $input = $this->session->condition[$this->_grpContent];
            unset($input['length']);
        }
        $info           = $this->separate_orders_m->get_rows($input);
        $data['info']   = $info;
        
        $mpdfConfig = [
            'default_font_size' => 9,
            'default_font'      => 'garuda'
        ];
        $mpdf = new \Mpdf\Mpdf($mpdfConfig);
        $content = $this->load->view("{$this->router->class}/pdf", $data, TRUE);
        $mpdf->WriteHTML($content);
        
        
        $download = FALSE;
        $saveFile = FALSE;
        $viewFile = TRUE;
        $fileName = "separate_orders.pdf";
        $pathFile = "uploads/pdf/separate_orders/";
        if ( $saveFile ) {
            create_dir($pathFile);
            $fileName = $pathFile.$fileName;
            $mpdf->Output($fileName, 'F');
        }
        if ( $download ) 
            $mpdf->Output($fileName, 'D');
        if ( $viewFile ) 
            $mpdf->Output();
    }
    
    public function trash()
    {
        $this->load->module('template');
        
        // toobar
        $action[1][]            = action_list_view(site_url("{$this->router->class}"));
        $action[2][]            = action_delete_multi(base_url("{$this->router->class}/delete"));
        $data['boxAction']      = Modules::run('utils/build_toolbar', $action);
        
        // breadcrumb
        $data['breadcrumb'][]   = array($this->_title, site_url("{$this->router->class}"));
        $data['breadcrumb'][]   = array("ถังขยะ", site_url("{$this->router->class}/trash"));
        
        // page detail
        $data['pageHeader']     = $this->_title;
        $data['pageExcerpt']    = $this->_pageExcerpt;
        $data['contentView']    = "{$this->router->class}/trash";
        
        $this->template->layout($data);
    }

    public function data_trash()
    {
        $input              = $this->input->post();
        $input['recycle']   = 1;
        $info               = $this->separate_orders_m->get_rows($input);
        $infoCount          = $this->separate_orders_m->get_count($input);
        $column             = array();
        foreach ($info->result() as $key => $rs) {
            $id                         = encode_id($rs->repoId);
            $action                     = array();
            $action[1][]                = table_restore("{$this->router->class}/restore");         
            $active                     = $rs->active ? "checked" : null;
            $column[$key]['DT_RowId']   = $id;
            $column[$key]['checkbox']   = "<input type='checkbox' class='icheck tb-check-single'>";
            $column[$key]['title']      = $rs->title;
            $column[$key]['excerpt']    = $rs->excerpt;
            $column[$key]['recycle_at'] = datetime_table($rs->recycle_at);
            $column[$key]['action']     = Modules::run('utils/build_toolbar', $action);
        }

        $data['data']               = $column;
        $data['recordsTotal']       = $info->num_rows();
        $data['recordsFiltered']    = $infoCount;
        $data['draw']               = $input['draw'];
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    } 

    public function destroy()
    {
        if ( !$this->_permission ) {
            $toastr['type']     = 'error';
            $toastr['lineOne']  = config_item('appName');
            $toastr['lineTwo']  = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']    = false;
            $data['toastr']     = $toastr;
        } else {
            $input  = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs                     = decode_id($rs);
                $dateTime               = db_datetime_now();
                $value['updated_at']    = $dateTime;
                $value['updated_by']    = $this->session->users['user_id'];
                
                $result                 = false;
                $value['active']        = 0;
                $value['recycle']       = 1;
                $value['recycle_at']    = $dateTime;
                $value['recycle_by']    = $this->session->users['user_id'];
                $result                 = $this->separate_orders_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type']         = 'success';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type']         = 'error';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }

            $data['success']    = $result;
            $data['toastr']     = $toastr;
        }

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }

    public function restore()
    {
        if ( !$this->_permission ) {
            $toastr['type']     = 'error';
            $toastr['lineOne']  = config_item('appName');
            $toastr['lineTwo']  = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']    = false;
            $data['toastr']     = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs                     = decode_id($rs);
                $dateTime               = db_datetime_now();
                $value['updated_at']    = $dateTime;
                $value['updated_by']    = $this->session->users['user_id'];
                
                $result                 = false;
                $value['active']        = 0;
                $value['recycle']       = 0;
                $result                 = $this->separate_orders_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type']         = 'success';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type']         = 'error';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }

            $data['success']    = $result;
            $data['toastr']     = $toastr;
        }

        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    } 

    public function delete()
    {
        if ( !$this->_permission ) {
            $toastr['type']     = 'error';
            $toastr['lineOne']  = config_item('appName');
            $toastr['lineTwo']  = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']    = false;
            $data['toastr']     = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs                     = decode_id($rs);
                $dateTime               = db_datetime_now();
                $value['updated_at']    = $dateTime;
                $value['updated_by']    = $this->session->users['user_id'];
                
                $result                 = false;
                $value['active']        = 0;
                $value['recycle']       = 2;
                $result                 = $this->separate_orders_m->update_in($input['id'], $value);
             
            if ( $result ) {
                $toastr['type']         = 'success';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
            } else {
                $toastr['type']         = 'error';
                $toastr['lineOne']      = config_item('appName');
                $toastr['lineTwo']      = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
            }

            $data['success']    = $result;
            $data['toastr']     = $toastr;
        }
        $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }      
    
    public function action($type="")
    {
        if ( !$this->_permission ) {
            $toastr['type']         = 'error';
            $toastr['lineOne']      = config_item('appName');
            $toastr['lineTwo']      = 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้';
            $data['success']        = false;
            $data['toastr']         = $toastr;
        } else {
            $input = $this->input->post();
            foreach ( $input['id'] as &$rs ) 
                $rs                     = decode_id($rs);
                $dateTime               = db_datetime_now();
                $value['updated_at']    = $dateTime;
                $value['updated_by']    = $this->session->users['user_id'];
                $result                 = false;

                if ( $type == "active" ) {
                    $value['active']    = $input['status'] == "true" ? 1 : 0;
                    $result             = $this->separate_orders_m->update_in($input['id'], $value);
                }
                
                if ( $result ) {
                    $toastr['type']     = 'success';
                    $toastr['lineOne']  = config_item('appName');
                    $toastr['lineTwo']  = 'บันทึการเปลี่ยนแปลงเรียบร้อย';
                } else {
                    $toastr['type']     = 'error';
                    $toastr['lineOne']  = config_item('appName');
                    $toastr['lineTwo']  = 'พบข้อผิดพลาดกรุณาติดต่อผู้ดูแลระบบ';
                }

                $data['success']    = $result;
                $data['toastr']     = $toastr;
            }
            $this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));        
    }  

    public function sumernote_img_upload()
    {
		//sumernote/img-upload
		$path       = 'content';
        $upload     = $this->uploadfile_library->do_upload('file',TRUE,$path);
        
		if(isset($upload['index'])){
			$picture = $this->config->item('root_url').'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
		}

		echo $picture;
       
    }

    public function deletefile($ids)
    {

		$arrayName = array('file' => $ids);

		echo json_encode($arrayName);
    }
    
    public function get_transportations_cust()
	{
		$input = $this->input->post(null, true);
		if($input['type'] == "cust"):
			$input['type_transportations_setting_id'] = $input['code'];
		endif;

		if($input['type'] == "trans"):
			$input['type_transportation_cust_id'] = $input['code'];
		endif;
		
		$input['active']    			= 1;
		$input['recycle']   			= 0;
		$data_arr = array();
		$info = $this->type_transportations_setting_list_m->get_rows($input)->result();
		if(!empty($info)):
			foreach($info as $item):
				if($input['type'] == "cust"):
					$input_['type_transportation_cust_id'] = $item->type_transportation_cust_id;
					$input_['active']    			= 1;
					$input_['recycle']   			= 0;
					$type_transportations_cust      = $this->type_transportations_cust_m->get_by_key($input_)->row();
					$title                          = !empty($type_transportations_cust->title) ? $type_transportations_cust->title : null; 
					$data_arr[$item->type_transportation_cust_id] = array(
						'title' => $title,
						'type_transportation_cust_id' => $item->type_transportation_cust_id
					);
				endif;

				if($input['type'] == "trans"):
					$input_['type_transportation_id'] = $item->type_transportation_id;
					$input_['active']    		= 1;
					$input_['recycle']   		= 0;
					$type_transportations       = $this->type_transportations_m->get_rows($input_)->row();
					$title                      = !empty($type_transportations->title) ? $type_transportations->title : $type_transportations->title; 
					$data_arr[$item->type_transportation_id] = array(
						'title' => $title,
						'type_transportation_id' => $item->type_transportation_id
					);
				endif;
			endforeach;
			$data['info'] 	= $data_arr;
			$data['status'] = 200;
		else:
			$data['status'] = 500;
		endif;
		
		$this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
    }

    public function get_data()
	{
		header('Content-Type: application/json');

		$input = $this->input->post(null, true);

		$input['recycle'] = 0;
		$input['active'] = 1;

		$wide   	= $input['wide'];
		$long   	= $input['long'];
		$high   	= $input['high'];
		$weight   	= $input['weight'];

		$input_else_arr = array();
		$total     = 0;

		$info = $this->type_transportations_setting_list_m->get_param($input)->result();

        $total = ($wide * $long * $high) / 1000000;
        $total = number_format($total, 3);
		$info_arr = array();
		if(!empty($info)):
			foreach($info as $item):
				//set data on type
				$info_arr[$item->type][] = $item;
			endforeach;

			if(!empty($info_arr)):
				foreach($info_arr as $key => $loops):
					if(!empty($loops)):
						//check type
						if($key == 0):
							foreach($loops as $loop):
								$queue_min = $loop->queue_min;
								$queue_max = $loop->queue_max;
								if($queue_min <= $total && $total <= $queue_max):
									$total_sum = $total * $loop->price_send;
									$input_else_arr[$key] = array(
										'queue_min' 	=> $loop->queue_min,
										'queue_max' 	=> $loop->queue_max,
										'price' 		=> $loop->price_send,
                                        'price_send' 	=> $total_sum,
                                        'price_check' 	=> $total_sum,
										'type'			=> $key
									);
								endif;
							endforeach;
						endif;
						//check type
						if($key == 1):
							if(!empty($weight)):
								foreach($loops as $loop_1):
									$queue_min = $loop_1->kg_min;
									$queue_max = $loop_1->kg_max;
									if($queue_min <= $weight && $weight <= $queue_max):
										$total_sum = $weight * $loop_1->price_send;
										$input_else_arr[$key] = array(
											'queue_min' 	=> $loop_1->kg_min,
											'queue_max' 	=> $loop_1->kg_max,
											'price' 		=> $loop_1->price_send,
                                            'price_send' 	=> $total_sum,
                                            'price_check' 	=> $total_sum,
											'type'			=> $key
										);
									endif;
								endforeach;
							endif;
						endif;
					endif;
				endforeach;
			endif;

			if(!empty($input_else_arr)):
				$price_0 = !empty($input_else_arr[0]['price_check']) ? $input_else_arr[0]['price_check'] : 0;
				$price_1 = !empty($input_else_arr[1]['price_check']) ? $input_else_arr[1]['price_check'] : 0;
				//check price max
				if($price_1 > $price_0):
					$input_else_arr = $input_else_arr[1];
				else:
					$input_else_arr = $input_else_arr[0];
				endif;
			endif;

		endif;

		if(empty($input_else_arr) ):
			$input_else_arr = array(
				'queue_min' 	=> 0,
				'queue_max' 	=> 0,
				'price_send' 	=> number_format(0,1)
			);
		endif;

		$data['calculate'] = $total;
		$data['info'] = $input_else_arr;
		// $data['data'] = $info;
		// $data['info_arr'] = $info_arr;

		$this->output
                ->set_content_type('application/json')
                ->set_output(json_encode($data));
	}
    
}
