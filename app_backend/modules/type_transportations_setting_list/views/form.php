<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>

       
        <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post', 'autocomplete' => 'off')) ?>
            <div class="m-portlet__body"> 
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="type_transportations_setting_id">ประเภทส่งสินค้าขนส่ง</label>
                    <div class="col-sm-7">
                        <select id="type_transportations_setting_id" name="type_transportations_setting_id" class="form-control m-input select2" required>
                            <option value="">เลือกประเภทส่งสินค้าขนส่ง</option>
                            <?php
                            if(!empty($type_transportations_setting)):
                                foreach($type_transportations_setting as $item):
                                    $select = '';
                                    if($item->type_transportations_setting_id == $info->type_transportations_setting_id):
                                        $select = 'selected';
                                    endif;
                            ?>
                                <option value="<?php echo !empty($item->type_transportations_setting_id) ? $item->type_transportations_setting_id : "";?>" <?=$select?>><?php echo !empty($item->title) ? $item->title : "";?></option>
                            <?php
                                endforeach;
                            endif;
                            ?>

                        </select>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="type_transportation_cust_id">ประเภทลูกค้าขนส่ง</label>
                    <div class="col-sm-7">
                        <select id="type_transportation_cust_id" name="type_transportation_cust_id" class="form-control m-input select2" required>
                            <option value="">เลือกประเภทลูกค้าขนส่ง</option>
                            <?php
                            if(!empty($type_transportations_cust)):
                                foreach($type_transportations_cust as $item_cust):
                                    $select_item_cust = '';
                                    if($item_cust->type_transportation_cust_id == $info->type_transportation_cust_id):
                                        $select_item_cust = 'selected';
                                    endif;
                            ?>
                                <option value="<?php echo !empty($item_cust->type_transportation_cust_id) ? $item_cust->type_transportation_cust_id : "";?>" <?=$select_item_cust?>><?php echo !empty($item_cust->title) ? $item_cust->title : "";?></option>
                            <?php
                                endforeach;
                            endif;
                            ?>

                        </select>
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="type_transportation_id">ประเภทช่องทางขนส่ง</label>
                    <div class="col-sm-7">
                        <select id="type_transportation_id" name="type_transportation_id" class="form-control m-input select2" required>
                            <option value="">เลือกประเภทช่องทางขนส่ง</option>
                            <?php
                            if(!empty($type_transportations)):
                                foreach($type_transportations as $item_transportation):
                                    $select_item_transportation = '';
                                    if($item_transportation->type_transportation_id == $info->type_transportation_id):
                                        $select_item_transportation = 'selected';
                                    endif;
                            ?>
                                <option value="<?php echo !empty($item_transportation->type_transportation_id) ? $item_transportation->type_transportation_id : "";?>" <?=$select_item_transportation?>><?php echo !empty($item_transportation->title) ? $item_transportation->title : "";?></option>
                            <?php
                                endforeach;
                            endif;
                            ?>

                        </select>
                    </div>
                </div>
                <?php
                if(in_array($this->router->method, array('create'))):
                ?>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">ประเภท</label>
                    <div class="col-sm-7 icheck-inline">
                        <label><input <?php echo $info->type==0 ? "checked" : NULL ?> type="radio" name="type" class="icheck" value="0"> คิวบิตเมตร</label>
                        <label><input <?php echo $info->type==1 ? "checked" : NULL ?> type="radio" name="type" class="icheck" value="1"> กิโลกรัม</label>
                    </div>
                </div>
                <?php
                endif;
                ?>
                <?php
                if(in_array($this->router->method, array('edit'))):
                ?>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label">ประเภท</label>
                    <div class="col-sm-7 icheck-inline">
                        <label><input <?php echo $info->type==0 ? "checked" : NULL ?> type="radio" name="type" class="icheck" value="0" disabled> คิวบิต</label>
                        <label><input <?php echo $info->type==1 ? "checked" : NULL ?> type="radio" name="type" class="icheck" value="1" disabled> กิโลกรัม</label>
                        <input type="hidden" name="type" value="<?php echo isset($info->type) ? $info->type : 0 ?>">
                    </div>
                </div>
                <?php
                endif;
                ?>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="rate_min">ค่าเริ่มต้น</label>
                    <div class="col-sm-2">
                        <input value="<?php echo isset($info->rate_min) ? $info->rate_min : NULL ?>" type="text" class="form-control m-input " name="rate_min" id="rate_min" placeholder="ระบุ" required> 
                    </div>
                    <label class="col-sm-2 col-form-label" for="rate_max">ถึง</label>
                    <div class="col-sm-3">
                        <input value="<?php echo isset($info->rate_max) ? $info->rate_max : NULL ?>" type="text" class="form-control m-input " name="rate_max" id="rate_max" placeholder="ระบุ" required> 
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="price_send">ราคาขนส่ง</label>
                    <div class="col-sm-3">
                        <input value="<?php echo isset($info->price_send) ? $info->price_send : NULL ?>" type="text" class="form-control m-input " name="price_send" id="price_send" placeholder="ระบุ" required> 
                    </div>
                </div>  
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">

                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success m-btn--wide">บันทึก</button>
                    <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                        </div>
                    </div>
                    
                </div>
            </div>
             <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
             <input type="hidden" class="form-control" name="db" id="db" value="type_transportations_setting_list">
             <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->type_transportations_setting_list_id) ? encode_id($info->type_transportations_setting_list_id) : 0 ?>">
            
             
        <?php echo form_close() ?>

        <!--end::Form-->
    </div> 
    
</div>  