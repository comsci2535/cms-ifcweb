<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Type_transportations_setting_list_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    public function get_rows($param) 
    {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $query = $this->db
                        ->select(
                            'a.*, 
                            b.title as transportations_title,
                            c.title as transportations_cust_title,
                            d.title as transportations_setting_title')
                        ->from('type_transportations_setting_list a')
                        ->join('type_transportations b', 'a.type_transportation_id = b.type_transportation_id', 'left')
                        ->join('type_transportations_cust c', 'a.type_transportation_cust_id = c.type_transportation_cust_id', 'left')
                        ->join('type_transportations_setting d', 'a.type_transportations_setting_id = d.type_transportations_setting_id', 'left')
                        ->get();
        return $query;
    }

    public function get_count($param) 
    {
        $this->_condition($param);
        $query = $this->db
                        ->select(
                            'a.*, 
                            b.title as transportations_title,
                            c.title as transportations_cust_title,
                            d.title as transportations_setting_title')
                        ->from('type_transportations_setting_list a')
                        ->join('type_transportations b', 'a.type_transportation_id = b.type_transportation_id', 'left')
                        ->join('type_transportations_cust c', 'a.type_transportation_cust_id = c.type_transportation_cust_id', 'left')
                        ->join('type_transportations_setting d', 'a.type_transportations_setting_id = d.type_transportations_setting_id', 'left')
                        ->get();
        return $query->num_rows();
    }

    private function _condition($param) 
    {   
        // START form filter 
        if ( isset($param['keyword']) && $param['keyword'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('a.title', $param['keyword'])
                    ->or_like('a.weight_min', $param['keyword'])
                    ->or_like('a.weight_max', $param['keyword'])
                    ->group_end();
        }
        if ( isset($param['createDateRange']) && $param['createDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.created_at,'%Y-%m-%d') BETWEEN '{$param['createStartDate']}' AND '{$param['createEndDate']}'");
        }
        if ( isset($param['updateDateRange']) && $param['updateDateRange'] != "" ) {
            $this->db->where("DATE_FORMAT(a.updated_at,'%Y-%m-%d') BETWEEN '{$param['updateStartDate']}' AND '{$param['updateEndDate']}'");
        }     
        if ( isset($param['active']) && $param['active'] != "" ) {
            $this->db->where('a.active', $param['active']);
        }         
        // END form filter
        
        if ( isset($param['search']['value']) && $param['search']['value'] != "" ) {
            $this->db
                    ->group_start()
                    ->like('b.title', $param['search']['value'])
                    ->or_like('c.title', $param['search']['value'])
                    ->or_like('d.title', $param['search']['value'])
                    ->or_like('a.queue_min', $param['search']['value'])
                    ->or_like('a.queue_max', $param['search']['value'])
                    ->or_like('a.price_send', $param['search']['value'])
                    ->group_end();
        }

        if ( isset($param['order']) ){
            if ($param['order'][0]['column'] == 1) $columnOrder = "a.type_transportations_setting_id";
            if ($param['order'][0]['column'] == 2) $columnOrder = "a.type_transportation_cust_id";
            if ($param['order'][0]['column'] == 3) $columnOrder = "a.type_transportation_id";
            if ($param['order'][0]['column'] == 4) $columnOrder = "a.queue_min";
            if ($param['order'][0]['column'] == 5) $columnOrder = "a.price_send";            
            if ( $this->router->method =="data_index" ) {
                if ($param['order'][0]['column'] == 6) $columnOrder = "a.created_at";
                if ($param['order'][0]['column'] == 7) $columnOrder = "a.updated_at";
            } else if ( $this->router->method =="data_trash" ) {
                if ($param['order'][0]['column'] == 3) $columnOrder = "a.recycle_at";
            }
            $this->db
                    ->order_by($columnOrder, $param['order'][0]['dir']);
        } 
        
        if ( isset($param['type_transportations_setting_list_id']) ) 
            $this->db->where('a.type_transportations_setting_list_id', $param['type_transportations_setting_list_id']);
            
        if ( isset($param['type_transportations_setting_id']) ) 
            $this->db->where('a.type_transportations_setting_id', $param['type_transportations_setting_id']);;

        if ( isset($param['type_transportation_cust_id']) ) 
            $this->db->where('a.type_transportation_cust_id', $param['type_transportation_cust_id']);
        
        if ( isset($param['type_transportation_id']) ) 
            $this->db->where('a.type_transportation_id', $param['type_transportation_id']);

        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);

    } 
    
    public function insert($value) 
    {
        $this->db->insert('type_transportations_setting_list', $value);
        return $this->db->insert_id();
    }
    
    public function cont_type_transportations_setting_list($id)
    {
        $this->db->where('active', 1);
        $this->db->where('type_transportations_setting_id', $id);
        $this->db->from('type_transportations_setting_list');
        return $this->db->count_all_results();
    }
    
    public function update($id, $value)
    {
        $query = $this->db
                        ->where('type_transportations_setting_list_id', $id)
                        ->update('type_transportations_setting_list', $value);
        return $query;
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('type_transportations_setting_list_id', $id)
                        ->update('type_transportations_setting_list', $value);
        return $query;
    }  
    
    public function get_param($param)
    {

        if ( isset($param['type_transportations_setting_list_id']) ) 
            $this->db->where('a.type_transportations_setting_list_id', $param['type_transportations_setting_list_id']);
            
        if ( isset($param['type_transportations_setting_id']) ) 
            $this->db->where('a.type_transportations_setting_id', $param['type_transportations_setting_id']);
        
        if ( isset($param['type_transportation_cust_id']) ) 
            $this->db->where('a.type_transportation_cust_id', $param['type_transportation_cust_id']);
            
        if ( isset($param['type_transportation_id']) ) 
            $this->db->where('a.type_transportation_id', $param['type_transportation_id']);
        
        $this->db->where('a.recycle', $param['recycle']); 

        $this->db->where('a.active', $param['active']); 

        $query = $this->db
            ->select('a.*')
            ->from('type_transportations_setting_list a')
            ->order_by('type', 'asc')
            ->get();
        return $query;
    }


}
