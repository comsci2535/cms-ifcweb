<!--Begin::Section--> 
    <style>
        /* Webkit Scrollbar */
        .text-overflow-body::-webkit-scrollbar { 
        width: 0px;
    }
    .text-overflow-body::-webkit-scrollbar-track {
        background: #ebedf2;
        -webkit-box-shadow: inset 1px 1px 2px #E0E0E0;
        border: 0px solid #D8D8D8;
    }
    .text-overflow-body::-webkit-scrollbar-thumb {
        background: #646464;
        -webkit-box-shadow: inset 1px 1px 2px rgba(155, 155, 155, 0.4);
    }
    .text-overflow-body::-webkit-scrollbar-thumb:hover {
    background: #AAA;
    }
    .text-overflow-body::-webkit-scrollbar-thumb:active {
        background: #888;
        -webkit-box-shadow: inset 1px 1px 2px rgba(0,0,0,0.3);
    }
</style>
<div class="col-xl-6 col-lg-12"> 
    <!--Begin::Portlet-->
    <?php echo Modules::run('members/get_members_home');?> 
    <!--End::Portlet-->
</div>
<div class="col-xl-6 col-lg-12">

    <!--Begin::Portlet-->
    <?php echo Modules::run('orders/get_orders_home');?>  
    <!--End::Portlet-->
</div> 
<!--End::Section-->