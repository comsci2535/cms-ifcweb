<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Config_contactus extends MX_Controller {

    private $_title = 'ตั้งค่า';
    private $_pageExcerpt = 'การจัดการข้อมูลเกี่ยวกับติดต่อเรา';
    private $_grpContent = 'config';
    private $_permission;

    public function __construct() {
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if (!$this->_permission && !$this->input->is_ajax_request()) {
            Modules::run('utils/toastr', 'error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('uploadfile_library');
        $this->load->library('ckeditor');
        $this->load->model('config_m');
    }

    public function index() {
        $this->load->module('template');

        $type = 'contactus';
        $info = $this->config_m->get_config($type);
        $temp = array();
        foreach ($info->result_array() as $rs) {
            $temp[$rs['variable']] = $rs['value'];
        }
        $data['info'] = $temp;
        $data['frmAction'] = site_url("{$this->router->class}/update/{$type}");

        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/{$this->router->method}";

        $data['breadcrumb'][] = array("ตั้งค่า", "javascript:void(0)");
        $data['breadcrumb'][] = array("ติดต่อเรา", site_url("{$this->router->class}/{$this->router->method}"));

        $this->template->layout($data);
    }

    public function update($type) {

        $input = $this->input->post();
        $input['type'] = $type;
        $value = $this->_build_data($input);
        $result = $this->config_m->update($value, $type); 
        
        if ($result) {
            Modules::run('utils/toastr', 'success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('utils/toastr', 'error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}"));
    }

    private function _build_data($input) {
        $value = array();
        $i = 0;
        foreach ($input as $key => $rs) {
            $i++;
            if ($key != "type") {
                $value[$i]['value'] = html_escape($rs);
                $value[$i]['variable'] = html_escape($key);
                $value[$i]['type'] = html_escape($input['type']);
                $value[$i]['lang'] = config_item('language_abbr');
                $value[$i]['updateDate'] = db_datetime_now();
            }
        }
        return $value;
    }

    private function _build_data_upload($result) {
        $i = 0;
        $value = array();
        foreach ($result as $key => $rs) {
            if (isset($rs['upload_data'])) {
                $i++;
                $value[$i]['value'] = html_escape($rs['upload_data']['file_name']);
                $value[$i]['variable'] = html_escape($key);
            }
        }
        return $value;
    }

    private function _upload_fb_config() {
        $config['upload_path'] = config_item('uploadPath') . 'config/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['overwrite'] = TRUE;
        $config['max_width'] = 0;
        $config['max_height'] = 0;
        return $config;
    }

    public function sumernote_img_upload()
    {
		//sumernote/img-upload
		$path       = 'config_contactus';
        $upload     = $this->uploadfile_library->do_upload('file',TRUE,$path);
        
		if(isset($upload['index'])){
			$picture = $this->config->item('root_url').'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
		}

		echo $picture;
       
    }
    
}
