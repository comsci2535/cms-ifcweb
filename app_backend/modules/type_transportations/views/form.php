<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>

       
        <?php echo form_open_multipart($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post', 'autocomplete' => 'off')) ?>
            <div class="m-portlet__body">
                    
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title">หัวข้อ</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->title) ? $info->title : NULL ?>" type="text" class="form-control m-input " name="title" id="titles" placeholder="ระบุ" required>
                        <label id="name-error-dup" class="error-dup" for="name" style="display: none;">มีชือนี้ในระบบแล้ว.</label>
                    </div>
                </div>  
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="title_en">หัวข้อ (อังกฤษ)</label>
                    <div class="col-sm-7">
                        <input value="<?php echo isset($info->title_en) ? $info->title_en : NULL ?>" type="text" class="form-control m-input " name="title_en" id="title_en" placeholder="ระบุ" required> 
                    </div>
                </div>
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="excerpt">รายละเอียด</label>
                    <div class="col-sm-7">
                        <textarea name="excerpt" rows="7" class="form-control" id="excerpt" placeholder="ระบุ" required><?php echo isset($info->excerpt) ? $info->excerpt : NULL ?></textarea>
                    </div>
                </div> 
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="excerpt_en">รายละเอียดย่อ (อังกฤษ)</label>
                    <div class="col-sm-7">
                        <textarea name="excerpt_en" rows="7" class="form-control" id="excerpt_en" placeholder="ระบุ"><?php echo isset($info->excerpt_en) ? $info->excerpt_en : NULL ?></textarea>
                    </div>
                </div>  
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">

                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success m-btn--wide">บันทึก</button>
                    <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                        </div>
                    </div>
                    
                </div>
            </div>
             <input type="hidden" name="mode" id="input-mode" value="<?php echo $this->router->method ?>">
             <input type="hidden" class="form-control" name="db" id="db" value="type_transportations">
             <input type="hidden" name="id" id="input-id" value="<?php echo isset($info->type_transportation_id) ? encode_id($info->type_transportation_id) : 0 ?>">
        <?php echo form_close() ?>

        <!--end::Form-->
    </div>  
</div> 




