
<div class="col-xl-12">
    <div class="m-portlet m-portlet--mobile ">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       รายการ
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                    <?php echo $boxAction; ?>
            </div>
        </div>
        <div class="m-portlet__body">
            <form  role="form">
                <div class="table-responsive">
                    <table id="data-list" class="table table-hover dataTable" width="100%">
                        <thead>
                            <tr>
                                <th><input name="tbCheckAll" type="checkbox" class="icheck tb-check-all"></th>
                                <th>รายการ</th> 
                                <th>สร้าง</th>
                                <th>แก้ไข</th>
                                <th>สถานะ</th>
                                <th></th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="col-xl-12">
    <div class="m-portlet m-portlet--mobile ">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        แบบฟอร์มหัวข้อตกลงการใช้บริการ
                    </h3>
                </div>
            </div>
        </div>
        <div class="m-portlet__body">
        <?php echo form_open($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal frm-main frm-create', 'method' => 'post', 'autocomplete' => 'off')) ?>
            <div class="m-portlet__body">
                    
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="siteTitle">หัวข้อ</label>
                    <div class="col-sm-7">
                        <textarea name="siteTitle" rows="5" class="form-control" id="siteTitle" placeholder="ระบุ" required><?php echo isset($info['siteTitle']) ? $info['siteTitle'] : NULL ?></textarea>
                    </div>
                </div> 
                <div class="form-group m-form__group row">
                    <label class="col-sm-2 col-form-label" for="siteTitle_en">หัวข้อ (อังกฤษ)</label>
                    <div class="col-sm-7">
                        <textarea name="siteTitle_en" rows="5" class="form-control" id="siteTitle_en" placeholder="ระบุ" required><?php echo isset($info['siteTitle_en']) ? $info['siteTitle_en'] : NULL ?></textarea>
                    </div>
                </div>
            </div>
            <div class="m-portlet__foot m-portlet__foot--fit">

                <div class="m-form__actions">
                    <div class="row">
                        <div class="col-2">
                        </div>
                        <div class="col-10">
                            <button type="submit" class="btn btn-success m-btn--wide">บันทึก</button>
                    <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                        </div>
                    </div>
                    
                </div>
            </div>
            <?php echo form_close() ?> 
        </div>
    </div>
</div>